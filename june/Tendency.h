//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TENDENCY_H
#define TENDENCY_H
#include <iostream>
//TODO Tendency finish this class
class Tendency
{
  public:
    //constructor
    Tendency();
    //copy constructor
    Tendency(const Tendency &rhs);
    //move constructor
    Tendency(Tendency &&rhs);
    //destructor
    ~Tendency();
    //copy assignment operator
    Tendency& operator=(const Tendency &rhs);
    //move assignment operator
    Tendency& operator=(Tendency &&rhs);
    //swap
    void swap(Tendency &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Tendency &rhs);
    friend std::istream& operator>>(std::istream &input, Tendency &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TENDENCY_H */