//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CURVE_H
#define CURVE_H
#include <iostream>
//TODO Curve finish this class
class Curve
{
  public:
    //constructor
    Curve();
    //copy constructor
    Curve(const Curve &rhs);
    //move constructor
    Curve(Curve &&rhs);
    //destructor
    ~Curve();
    //copy assignment operator
    Curve& operator=(const Curve &rhs);
    //move assignment operator
    Curve& operator=(Curve &&rhs);
    //swap
    void swap(Curve &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Curve &rhs);
    friend std::istream& operator>>(std::istream &input, Curve &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CURVE_H */