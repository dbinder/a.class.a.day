//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SNEEZE_H
#define SNEEZE_H
#include <iostream>
//TODO Sneeze finish this class
class Sneeze
{
  public:
    //constructor
    Sneeze();
    //copy constructor
    Sneeze(const Sneeze &rhs);
    //move constructor
    Sneeze(Sneeze &&rhs);
    //destructor
    ~Sneeze();
    //copy assignment operator
    Sneeze& operator=(const Sneeze &rhs);
    //move assignment operator
    Sneeze& operator=(Sneeze &&rhs);
    //swap
    void swap(Sneeze &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Sneeze &rhs);
    friend std::istream& operator>>(std::istream &input, Sneeze &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SNEEZE_H */