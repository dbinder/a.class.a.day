//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Space.h>
//constructor
Space::Space():
  m_placeholder(0)
{}

Space::Space(const Space &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Space::Space(Space &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Space::~Space(){}
Space& Space::operator=(const Space &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Space& Space::operator=(Space &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Space &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Space &lhs)
{
  return input;
}

