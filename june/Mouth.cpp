//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Mouth.h>
//constructor
Mouth::Mouth():
  m_placeholder(0)
{}

Mouth::Mouth(const Mouth &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Mouth::Mouth(Mouth &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Mouth::~Mouth(){}
Mouth& Mouth::operator=(const Mouth &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Mouth& Mouth::operator=(Mouth &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Mouth &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Mouth &lhs)
{
  return input;
}

