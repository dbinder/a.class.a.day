//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SEAT_H
#define SEAT_H
#include <iostream>
//TODO Seat finish this class
class Seat
{
  public:
    //constructor
    Seat();
    //copy constructor
    Seat(const Seat &rhs);
    //move constructor
    Seat(Seat &&rhs);
    //destructor
    ~Seat();
    //copy assignment operator
    Seat& operator=(const Seat &rhs);
    //move assignment operator
    Seat& operator=(Seat &&rhs);
    //swap
    void swap(Seat &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Seat &rhs);
    friend std::istream& operator>>(std::istream &input, Seat &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SEAT_H */