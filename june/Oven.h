//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef OVEN_H
#define OVEN_H
#include <iostream>
//TODO Oven finish this class
class Oven
{
  public:
    //constructor
    Oven();
    //copy constructor
    Oven(const Oven &rhs);
    //move constructor
    Oven(Oven &&rhs);
    //destructor
    ~Oven();
    //copy assignment operator
    Oven& operator=(const Oven &rhs);
    //move assignment operator
    Oven& operator=(Oven &&rhs);
    //swap
    void swap(Oven &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Oven &rhs);
    friend std::istream& operator>>(std::istream &input, Oven &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* OVEN_H */