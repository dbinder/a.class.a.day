//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Creator.h>
//constructor
Creator::Creator():
  m_placeholder(0)
{}

Creator::Creator(const Creator &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Creator::Creator(Creator &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Creator::~Creator(){}
Creator& Creator::operator=(const Creator &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Creator& Creator::operator=(Creator &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Creator &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Creator &lhs)
{
  return input;
}

