//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef COAL_H
#define COAL_H
#include <iostream>
//TODO Coal finish this class
class Coal
{
  public:
    //constructor
    Coal();
    //copy constructor
    Coal(const Coal &rhs);
    //move constructor
    Coal(Coal &&rhs);
    //destructor
    ~Coal();
    //copy assignment operator
    Coal& operator=(const Coal &rhs);
    //move assignment operator
    Coal& operator=(Coal &&rhs);
    //swap
    void swap(Coal &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Coal &rhs);
    friend std::istream& operator>>(std::istream &input, Coal &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* COAL_H */