//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef YARN_H
#define YARN_H
#include <iostream>
//TODO Yarn finish this class
class Yarn
{
  public:
    //constructor
    Yarn();
    //copy constructor
    Yarn(const Yarn &rhs);
    //move constructor
    Yarn(Yarn &&rhs);
    //destructor
    ~Yarn();
    //copy assignment operator
    Yarn& operator=(const Yarn &rhs);
    //move assignment operator
    Yarn& operator=(Yarn &&rhs);
    //swap
    void swap(Yarn &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Yarn &rhs);
    friend std::istream& operator>>(std::istream &input, Yarn &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* YARN_H */