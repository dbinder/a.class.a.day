//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MOON_H
#define MOON_H
#include <iostream>
//TODO Moon finish this class
class Moon
{
  public:
    //constructor
    Moon();
    //copy constructor
    Moon(const Moon &rhs);
    //move constructor
    Moon(Moon &&rhs);
    //destructor
    ~Moon();
    //copy assignment operator
    Moon& operator=(const Moon &rhs);
    //move assignment operator
    Moon& operator=(Moon &&rhs);
    //swap
    void swap(Moon &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Moon &rhs);
    friend std::istream& operator>>(std::istream &input, Moon &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MOON_H */