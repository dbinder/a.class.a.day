//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Haircut.h>
//constructor
Haircut::Haircut():
  m_placeholder(0)
{}

Haircut::Haircut(const Haircut &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Haircut::Haircut(Haircut &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Haircut::~Haircut(){}
Haircut& Haircut::operator=(const Haircut &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Haircut& Haircut::operator=(Haircut &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Haircut &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Haircut &lhs)
{
  return input;
}

