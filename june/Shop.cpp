//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Shop.h>
//constructor
Shop::Shop():
  m_placeholder(0)
{}

Shop::Shop(const Shop &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Shop::Shop(Shop &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Shop::~Shop(){}
Shop& Shop::operator=(const Shop &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Shop& Shop::operator=(Shop &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Shop &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Shop &lhs)
{
  return input;
}

