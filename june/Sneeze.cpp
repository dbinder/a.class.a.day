//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Sneeze.h>
//constructor
Sneeze::Sneeze():
  m_placeholder(0)
{}

Sneeze::Sneeze(const Sneeze &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Sneeze::Sneeze(Sneeze &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Sneeze::~Sneeze(){}
Sneeze& Sneeze::operator=(const Sneeze &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Sneeze& Sneeze::operator=(Sneeze &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Sneeze &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Sneeze &lhs)
{
  return input;
}

