//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Coal.h>
//constructor
Coal::Coal():
  m_placeholder(0)
{}

Coal::Coal(const Coal &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Coal::Coal(Coal &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Coal::~Coal(){}
Coal& Coal::operator=(const Coal &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Coal& Coal::operator=(Coal &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Coal &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Coal &lhs)
{
  return input;
}

