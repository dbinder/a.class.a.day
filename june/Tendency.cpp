//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Tendency.h>
//constructor
Tendency::Tendency():
  m_placeholder(0)
{}

Tendency::Tendency(const Tendency &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Tendency::Tendency(Tendency &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Tendency::~Tendency(){}
Tendency& Tendency::operator=(const Tendency &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Tendency& Tendency::operator=(Tendency &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Tendency &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Tendency &lhs)
{
  return input;
}

