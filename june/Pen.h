//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PEN_H
#define PEN_H
#include <iostream>
//TODO Pen finish this class
class Pen
{
  public:
    //constructor
    Pen();
    //copy constructor
    Pen(const Pen &rhs);
    //move constructor
    Pen(Pen &&rhs);
    //destructor
    ~Pen();
    //copy assignment operator
    Pen& operator=(const Pen &rhs);
    //move assignment operator
    Pen& operator=(Pen &&rhs);
    //swap
    void swap(Pen &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pen &rhs);
    friend std::istream& operator>>(std::istream &input, Pen &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PEN_H */