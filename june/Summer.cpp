//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Summer.h>
//constructor
Summer::Summer():
  m_placeholder(0)
{}

Summer::Summer(const Summer &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Summer::Summer(Summer &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Summer::~Summer(){}
Summer& Summer::operator=(const Summer &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Summer& Summer::operator=(Summer &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Summer &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Summer &lhs)
{
  return input;
}

