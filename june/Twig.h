//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TWIG_H
#define TWIG_H
#include <iostream>
//TODO Twig finish this class
class Twig
{
  public:
    //constructor
    Twig();
    //copy constructor
    Twig(const Twig &rhs);
    //move constructor
    Twig(Twig &&rhs);
    //destructor
    ~Twig();
    //copy assignment operator
    Twig& operator=(const Twig &rhs);
    //move assignment operator
    Twig& operator=(Twig &&rhs);
    //swap
    void swap(Twig &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Twig &rhs);
    friend std::istream& operator>>(std::istream &input, Twig &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TWIG_H */