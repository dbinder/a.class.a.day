//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ALARM_H
#define ALARM_H
#include <iostream>
//TODO Alarm finish this class
class Alarm
{
  public:
    //constructor
    Alarm();
    //copy constructor
    Alarm(const Alarm &rhs);
    //move constructor
    Alarm(Alarm &&rhs);
    //destructor
    ~Alarm();
    //copy assignment operator
    Alarm& operator=(const Alarm &rhs);
    //move assignment operator
    Alarm& operator=(Alarm &&rhs);
    //swap
    void swap(Alarm &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Alarm &rhs);
    friend std::istream& operator>>(std::istream &input, Alarm &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ALARM_H */