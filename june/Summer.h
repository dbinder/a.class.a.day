//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SUMMER_H
#define SUMMER_H
#include <iostream>
//TODO Summer finish this class
class Summer
{
  public:
    //constructor
    Summer();
    //copy constructor
    Summer(const Summer &rhs);
    //move constructor
    Summer(Summer &&rhs);
    //destructor
    ~Summer();
    //copy assignment operator
    Summer& operator=(const Summer &rhs);
    //move assignment operator
    Summer& operator=(Summer &&rhs);
    //swap
    void swap(Summer &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Summer &rhs);
    friend std::istream& operator>>(std::istream &input, Summer &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SUMMER_H */