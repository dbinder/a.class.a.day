//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Appliance.h>
//constructor
Appliance::Appliance():
  m_placeholder(0)
{}

Appliance::Appliance(const Appliance &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Appliance::Appliance(Appliance &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Appliance::~Appliance(){}
Appliance& Appliance::operator=(const Appliance &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Appliance& Appliance::operator=(Appliance &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Appliance &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Appliance &lhs)
{
  return input;
}

