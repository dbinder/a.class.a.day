//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PANCAKE_H
#define PANCAKE_H
#include <iostream>
//TODO Pancake finish this class
class Pancake
{
  public:
    //constructor
    Pancake();
    //copy constructor
    Pancake(const Pancake &rhs);
    //move constructor
    Pancake(Pancake &&rhs);
    //destructor
    ~Pancake();
    //copy assignment operator
    Pancake& operator=(const Pancake &rhs);
    //move assignment operator
    Pancake& operator=(Pancake &&rhs);
    //swap
    void swap(Pancake &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pancake &rhs);
    friend std::istream& operator>>(std::istream &input, Pancake &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PANCAKE_H */