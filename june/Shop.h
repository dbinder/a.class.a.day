//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SHOP_H
#define SHOP_H
#include <iostream>
//TODO Shop finish this class
class Shop
{
  public:
    //constructor
    Shop();
    //copy constructor
    Shop(const Shop &rhs);
    //move constructor
    Shop(Shop &&rhs);
    //destructor
    ~Shop();
    //copy assignment operator
    Shop& operator=(const Shop &rhs);
    //move assignment operator
    Shop& operator=(Shop &&rhs);
    //swap
    void swap(Shop &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Shop &rhs);
    friend std::istream& operator>>(std::istream &input, Shop &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SHOP_H */