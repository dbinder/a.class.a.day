//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Frog.h>
//constructor
Frog::Frog():
  m_placeholder(0)
{}

Frog::Frog(const Frog &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Frog::Frog(Frog &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Frog::~Frog(){}
Frog& Frog::operator=(const Frog &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Frog& Frog::operator=(Frog &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Frog &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Frog &lhs)
{
  return input;
}

