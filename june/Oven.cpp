//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Oven.h>
//constructor
Oven::Oven():
  m_placeholder(0)
{}

Oven::Oven(const Oven &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Oven::Oven(Oven &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Oven::~Oven(){}
Oven& Oven::operator=(const Oven &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Oven& Oven::operator=(Oven &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Oven &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Oven &lhs)
{
  return input;
}

