//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PLATE_H
#define PLATE_H
#include <iostream>
//TODO Plate finish this class
class Plate
{
  public:
    //constructor
    Plate();
    //copy constructor
    Plate(const Plate &rhs);
    //move constructor
    Plate(Plate &&rhs);
    //destructor
    ~Plate();
    //copy assignment operator
    Plate& operator=(const Plate &rhs);
    //move assignment operator
    Plate& operator=(Plate &&rhs);
    //swap
    void swap(Plate &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Plate &rhs);
    friend std::istream& operator>>(std::istream &input, Plate &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PLATE_H */