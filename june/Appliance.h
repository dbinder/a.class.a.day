//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef APPLIANCE_H
#define APPLIANCE_H
#include <iostream>
//TODO Appliance finish this class
class Appliance
{
  public:
    //constructor
    Appliance();
    //copy constructor
    Appliance(const Appliance &rhs);
    //move constructor
    Appliance(Appliance &&rhs);
    //destructor
    ~Appliance();
    //copy assignment operator
    Appliance& operator=(const Appliance &rhs);
    //move assignment operator
    Appliance& operator=(Appliance &&rhs);
    //swap
    void swap(Appliance &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Appliance &rhs);
    friend std::istream& operator>>(std::istream &input, Appliance &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* APPLIANCE_H */