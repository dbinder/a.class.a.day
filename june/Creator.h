//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CREATOR_H
#define CREATOR_H
#include <iostream>
//TODO Creator finish this class
class Creator
{
  public:
    //constructor
    Creator();
    //copy constructor
    Creator(const Creator &rhs);
    //move constructor
    Creator(Creator &&rhs);
    //destructor
    ~Creator();
    //copy assignment operator
    Creator& operator=(const Creator &rhs);
    //move assignment operator
    Creator& operator=(Creator &&rhs);
    //swap
    void swap(Creator &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Creator &rhs);
    friend std::istream& operator>>(std::istream &input, Creator &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CREATOR_H */