//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BUILDING_H
#define BUILDING_H
#include <iostream>
//TODO Building finish this class
class Building
{
  public:
    //constructor
    Building();
    //copy constructor
    Building(const Building &rhs);
    //move constructor
    Building(Building &&rhs);
    //destructor
    ~Building();
    //copy assignment operator
    Building& operator=(const Building &rhs);
    //move assignment operator
    Building& operator=(Building &&rhs);
    //swap
    void swap(Building &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Building &rhs);
    friend std::istream& operator>>(std::istream &input, Building &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BUILDING_H */