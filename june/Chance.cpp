//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Chance.h>
//constructor
Chance::Chance():
  m_placeholder(0)
{}

Chance::Chance(const Chance &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Chance::Chance(Chance &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Chance::~Chance(){}
Chance& Chance::operator=(const Chance &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Chance& Chance::operator=(Chance &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Chance &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Chance &lhs)
{
  return input;
}

