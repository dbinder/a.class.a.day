//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TROUSERS_H
#define TROUSERS_H
#include <iostream>
//TODO Trousers finish this class
class Trousers
{
  public:
    //constructor
    Trousers();
    //copy constructor
    Trousers(const Trousers &rhs);
    //move constructor
    Trousers(Trousers &&rhs);
    //destructor
    ~Trousers();
    //copy assignment operator
    Trousers& operator=(const Trousers &rhs);
    //move assignment operator
    Trousers& operator=(Trousers &&rhs);
    //swap
    void swap(Trousers &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Trousers &rhs);
    friend std::istream& operator>>(std::istream &input, Trousers &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TROUSERS_H */