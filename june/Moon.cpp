//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Moon.h>
//constructor
Moon::Moon():
  m_placeholder(0)
{}

Moon::Moon(const Moon &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Moon::Moon(Moon &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Moon::~Moon(){}
Moon& Moon::operator=(const Moon &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Moon& Moon::operator=(Moon &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Moon &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Moon &lhs)
{
  return input;
}

