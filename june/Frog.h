//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef FROG_H
#define FROG_H
#include <iostream>
//TODO Frog finish this class
class Frog
{
  public:
    //constructor
    Frog();
    //copy constructor
    Frog(const Frog &rhs);
    //move constructor
    Frog(Frog &&rhs);
    //destructor
    ~Frog();
    //copy assignment operator
    Frog& operator=(const Frog &rhs);
    //move assignment operator
    Frog& operator=(Frog &&rhs);
    //swap
    void swap(Frog &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Frog &rhs);
    friend std::istream& operator>>(std::istream &input, Frog &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* FROG_H */