//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Salt.h>
//constructor
Salt::Salt():
  m_placeholder(0)
{}

Salt::Salt(const Salt &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Salt::Salt(Salt &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Salt::~Salt(){}
Salt& Salt::operator=(const Salt &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Salt& Salt::operator=(Salt &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Salt &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Salt &lhs)
{
  return input;
}

