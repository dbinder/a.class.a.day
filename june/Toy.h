//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TOY_H
#define TOY_H
#include <iostream>
//TODO Toy finish this class
class Toy
{
  public:
    //constructor
    Toy();
    //copy constructor
    Toy(const Toy &rhs);
    //move constructor
    Toy(Toy &&rhs);
    //destructor
    ~Toy();
    //copy assignment operator
    Toy& operator=(const Toy &rhs);
    //move assignment operator
    Toy& operator=(Toy &&rhs);
    //swap
    void swap(Toy &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Toy &rhs);
    friend std::istream& operator>>(std::istream &input, Toy &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TOY_H */