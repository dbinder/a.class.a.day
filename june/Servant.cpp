//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Servant.h>
//constructor
Servant::Servant():
  m_placeholder(0)
{}

Servant::Servant(const Servant &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Servant::Servant(Servant &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Servant::~Servant(){}
Servant& Servant::operator=(const Servant &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Servant& Servant::operator=(Servant &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Servant &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Servant &lhs)
{
  return input;
}

