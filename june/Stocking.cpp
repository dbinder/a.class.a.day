//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Stocking.h>
//constructor
Stocking::Stocking():
  m_placeholder(0)
{}

Stocking::Stocking(const Stocking &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Stocking::Stocking(Stocking &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Stocking::~Stocking(){}
Stocking& Stocking::operator=(const Stocking &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Stocking& Stocking::operator=(Stocking &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Stocking &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Stocking &lhs)
{
  return input;
}

