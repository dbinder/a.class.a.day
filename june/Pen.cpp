//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pen.h>
//constructor
Pen::Pen():
  m_placeholder(0)
{}

Pen::Pen(const Pen &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pen::Pen(Pen &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pen::~Pen(){}
Pen& Pen::operator=(const Pen &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pen& Pen::operator=(Pen &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pen &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pen &lhs)
{
  return input;
}

