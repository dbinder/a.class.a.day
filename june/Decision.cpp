//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Decision.h>
//constructor
Decision::Decision():
  m_placeholder(0)
{}

Decision::Decision(const Decision &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Decision::Decision(Decision &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Decision::~Decision(){}
Decision& Decision::operator=(const Decision &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Decision& Decision::operator=(Decision &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Decision &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Decision &lhs)
{
  return input;
}

