//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DECISION_H
#define DECISION_H
#include <iostream>
//TODO Decision finish this class
class Decision
{
  public:
    //constructor
    Decision();
    //copy constructor
    Decision(const Decision &rhs);
    //move constructor
    Decision(Decision &&rhs);
    //destructor
    ~Decision();
    //copy assignment operator
    Decision& operator=(const Decision &rhs);
    //move assignment operator
    Decision& operator=(Decision &&rhs);
    //swap
    void swap(Decision &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Decision &rhs);
    friend std::istream& operator>>(std::istream &input, Decision &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DECISION_H */