//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SPACE_H
#define SPACE_H
#include <iostream>
//TODO Space finish this class
class Space
{
  public:
    //constructor
    Space();
    //copy constructor
    Space(const Space &rhs);
    //move constructor
    Space(Space &&rhs);
    //destructor
    ~Space();
    //copy assignment operator
    Space& operator=(const Space &rhs);
    //move assignment operator
    Space& operator=(Space &&rhs);
    //swap
    void swap(Space &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Space &rhs);
    friend std::istream& operator>>(std::istream &input, Space &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SPACE_H */