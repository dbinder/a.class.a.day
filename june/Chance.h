//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CHANCE_H
#define CHANCE_H
#include <iostream>
//TODO Chance finish this class
class Chance
{
  public:
    //constructor
    Chance();
    //copy constructor
    Chance(const Chance &rhs);
    //move constructor
    Chance(Chance &&rhs);
    //destructor
    ~Chance();
    //copy assignment operator
    Chance& operator=(const Chance &rhs);
    //move assignment operator
    Chance& operator=(Chance &&rhs);
    //swap
    void swap(Chance &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Chance &rhs);
    friend std::istream& operator>>(std::istream &input, Chance &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CHANCE_H */