//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Yarn.h>
//constructor
Yarn::Yarn():
  m_placeholder(0)
{}

Yarn::Yarn(const Yarn &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Yarn::Yarn(Yarn &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Yarn::~Yarn(){}
Yarn& Yarn::operator=(const Yarn &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Yarn& Yarn::operator=(Yarn &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Yarn &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Yarn &lhs)
{
  return input;
}

