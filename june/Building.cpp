//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Building.h>
//constructor
Building::Building():
  m_placeholder(0)
{}

Building::Building(const Building &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Building::Building(Building &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Building::~Building(){}
Building& Building::operator=(const Building &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Building& Building::operator=(Building &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Building &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Building &lhs)
{
  return input;
}

