//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Letter.h>
//constructor
Letter::Letter():
  m_placeholder(0)
{}

Letter::Letter(const Letter &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Letter::Letter(Letter &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Letter::~Letter(){}
Letter& Letter::operator=(const Letter &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Letter& Letter::operator=(Letter &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Letter &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Letter &lhs)
{
  return input;
}

