//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef LETTER_H
#define LETTER_H
#include <iostream>
//TODO Letter finish this class
class Letter
{
  public:
    //constructor
    Letter();
    //copy constructor
    Letter(const Letter &rhs);
    //move constructor
    Letter(Letter &&rhs);
    //destructor
    ~Letter();
    //copy assignment operator
    Letter& operator=(const Letter &rhs);
    //move assignment operator
    Letter& operator=(Letter &&rhs);
    //swap
    void swap(Letter &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Letter &rhs);
    friend std::istream& operator>>(std::istream &input, Letter &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* LETTER_H */