//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Seat.h>
//constructor
Seat::Seat():
  m_placeholder(0)
{}

Seat::Seat(const Seat &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Seat::Seat(Seat &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Seat::~Seat(){}
Seat& Seat::operator=(const Seat &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Seat& Seat::operator=(Seat &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Seat &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Seat &lhs)
{
  return input;
}

