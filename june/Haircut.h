//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HAIRCUT_H
#define HAIRCUT_H
#include <iostream>
//TODO Haircut finish this class
class Haircut
{
  public:
    //constructor
    Haircut();
    //copy constructor
    Haircut(const Haircut &rhs);
    //move constructor
    Haircut(Haircut &&rhs);
    //destructor
    ~Haircut();
    //copy assignment operator
    Haircut& operator=(const Haircut &rhs);
    //move assignment operator
    Haircut& operator=(Haircut &&rhs);
    //swap
    void swap(Haircut &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Haircut &rhs);
    friend std::istream& operator>>(std::istream &input, Haircut &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HAIRCUT_H */