//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Plate.h>
//constructor
Plate::Plate():
  m_placeholder(0)
{}

Plate::Plate(const Plate &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Plate::Plate(Plate &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Plate::~Plate(){}
Plate& Plate::operator=(const Plate &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Plate& Plate::operator=(Plate &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Plate &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Plate &lhs)
{
  return input;
}

