//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Twig.h>
//constructor
Twig::Twig():
  m_placeholder(0)
{}

Twig::Twig(const Twig &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Twig::Twig(Twig &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Twig::~Twig(){}
Twig& Twig::operator=(const Twig &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Twig& Twig::operator=(Twig &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Twig &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Twig &lhs)
{
  return input;
}

