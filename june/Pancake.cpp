//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pancake.h>
//constructor
Pancake::Pancake():
  m_placeholder(0)
{}

Pancake::Pancake(const Pancake &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pancake::Pancake(Pancake &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pancake::~Pancake(){}
Pancake& Pancake::operator=(const Pancake &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pancake& Pancake::operator=(Pancake &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pancake &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pancake &lhs)
{
  return input;
}

