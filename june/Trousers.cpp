//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Trousers.h>
//constructor
Trousers::Trousers():
  m_placeholder(0)
{}

Trousers::Trousers(const Trousers &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Trousers::Trousers(Trousers &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Trousers::~Trousers(){}
Trousers& Trousers::operator=(const Trousers &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Trousers& Trousers::operator=(Trousers &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Trousers &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Trousers &lhs)
{
  return input;
}

