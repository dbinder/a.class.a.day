//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Alarm.h>
//constructor
Alarm::Alarm():
  m_placeholder(0)
{}

Alarm::Alarm(const Alarm &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Alarm::Alarm(Alarm &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Alarm::~Alarm(){}
Alarm& Alarm::operator=(const Alarm &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Alarm& Alarm::operator=(Alarm &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Alarm &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Alarm &lhs)
{
  return input;
}

