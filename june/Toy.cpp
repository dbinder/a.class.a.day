//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Toy.h>
//constructor
Toy::Toy():
  m_placeholder(0)
{}

Toy::Toy(const Toy &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Toy::Toy(Toy &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Toy::~Toy(){}
Toy& Toy::operator=(const Toy &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Toy& Toy::operator=(Toy &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Toy &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Toy &lhs)
{
  return input;
}

