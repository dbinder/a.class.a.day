//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MOUTH_H
#define MOUTH_H
#include <iostream>
//TODO Mouth finish this class
class Mouth
{
  public:
    //constructor
    Mouth();
    //copy constructor
    Mouth(const Mouth &rhs);
    //move constructor
    Mouth(Mouth &&rhs);
    //destructor
    ~Mouth();
    //copy assignment operator
    Mouth& operator=(const Mouth &rhs);
    //move assignment operator
    Mouth& operator=(Mouth &&rhs);
    //swap
    void swap(Mouth &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Mouth &rhs);
    friend std::istream& operator>>(std::istream &input, Mouth &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MOUTH_H */