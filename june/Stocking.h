//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STOCKING_H
#define STOCKING_H
#include <iostream>
//TODO Stocking finish this class
class Stocking
{
  public:
    //constructor
    Stocking();
    //copy constructor
    Stocking(const Stocking &rhs);
    //move constructor
    Stocking(Stocking &&rhs);
    //destructor
    ~Stocking();
    //copy assignment operator
    Stocking& operator=(const Stocking &rhs);
    //move assignment operator
    Stocking& operator=(Stocking &&rhs);
    //swap
    void swap(Stocking &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Stocking &rhs);
    friend std::istream& operator>>(std::istream &input, Stocking &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STOCKING_H */