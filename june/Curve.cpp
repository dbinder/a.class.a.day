//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Curve.h>
//constructor
Curve::Curve():
  m_placeholder(0)
{}

Curve::Curve(const Curve &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Curve::Curve(Curve &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Curve::~Curve(){}
Curve& Curve::operator=(const Curve &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Curve& Curve::operator=(Curve &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Curve &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Curve &lhs)
{
  return input;
}

