//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SERVANT_H
#define SERVANT_H
#include <iostream>
//TODO Servant finish this class
class Servant
{
  public:
    //constructor
    Servant();
    //copy constructor
    Servant(const Servant &rhs);
    //move constructor
    Servant(Servant &&rhs);
    //destructor
    ~Servant();
    //copy assignment operator
    Servant& operator=(const Servant &rhs);
    //move assignment operator
    Servant& operator=(Servant &&rhs);
    //swap
    void swap(Servant &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Servant &rhs);
    friend std::istream& operator>>(std::istream &input, Servant &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SERVANT_H */