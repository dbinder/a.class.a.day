//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SALT_H
#define SALT_H
#include <iostream>
//TODO Salt finish this class
class Salt
{
  public:
    //constructor
    Salt();
    //copy constructor
    Salt(const Salt &rhs);
    //move constructor
    Salt(Salt &&rhs);
    //destructor
    ~Salt();
    //copy assignment operator
    Salt& operator=(const Salt &rhs);
    //move assignment operator
    Salt& operator=(Salt &&rhs);
    //swap
    void swap(Salt &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Salt &rhs);
    friend std::istream& operator>>(std::istream &input, Salt &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SALT_H */