//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Step.h>
//constructor
Step::Step():
  m_placeholder(0)
{}

Step::Step(const Step &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Step::Step(Step &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Step::~Step(){}
Step& Step::operator=(const Step &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Step& Step::operator=(Step &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Step &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Step &lhs)
{
  return input;
}

