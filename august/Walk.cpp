//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Walk.h>
//constructor
Walk::Walk():
  m_placeholder(0)
{}

Walk::Walk(const Walk &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Walk::Walk(Walk &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Walk::~Walk(){}
Walk& Walk::operator=(const Walk &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Walk& Walk::operator=(Walk &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Walk &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Walk &lhs)
{
  return input;
}

