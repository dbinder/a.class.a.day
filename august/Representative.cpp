//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Representative.h>
//constructor
Representative::Representative():
  m_placeholder(0)
{}

Representative::Representative(const Representative &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Representative::Representative(Representative &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Representative::~Representative(){}
Representative& Representative::operator=(const Representative &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Representative& Representative::operator=(Representative &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Representative &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Representative &lhs)
{
  return input;
}

