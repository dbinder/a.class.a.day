//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Liquid.h>
//constructor
Liquid::Liquid():
  m_placeholder(0)
{}

Liquid::Liquid(const Liquid &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Liquid::Liquid(Liquid &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Liquid::~Liquid(){}
Liquid& Liquid::operator=(const Liquid &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Liquid& Liquid::operator=(Liquid &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Liquid &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Liquid &lhs)
{
  return input;
}

