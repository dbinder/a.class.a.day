//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef POT_H
#define POT_H
#include <iostream>
//TODO Pot finish this class
class Pot
{
  public:
    //constructor
    Pot();
    //copy constructor
    Pot(const Pot &rhs);
    //move constructor
    Pot(Pot &&rhs);
    //destructor
    ~Pot();
    //copy assignment operator
    Pot& operator=(const Pot &rhs);
    //move assignment operator
    Pot& operator=(Pot &&rhs);
    //swap
    void swap(Pot &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pot &rhs);
    friend std::istream& operator>>(std::istream &input, Pot &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* POT_H */