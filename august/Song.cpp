//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Song.h>
//constructor
Song::Song():
  m_placeholder(0)
{}

Song::Song(const Song &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Song::Song(Song &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Song::~Song(){}
Song& Song::operator=(const Song &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Song& Song::operator=(Song &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Song &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Song &lhs)
{
  return input;
}

