//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef NOSE_H
#define NOSE_H
#include <iostream>
//TODO Nose finish this class
class Nose
{
  public:
    //constructor
    Nose();
    //copy constructor
    Nose(const Nose &rhs);
    //move constructor
    Nose(Nose &&rhs);
    //destructor
    ~Nose();
    //copy assignment operator
    Nose& operator=(const Nose &rhs);
    //move assignment operator
    Nose& operator=(Nose &&rhs);
    //swap
    void swap(Nose &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Nose &rhs);
    friend std::istream& operator>>(std::istream &input, Nose &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* NOSE_H */