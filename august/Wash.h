//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WASH_H
#define WASH_H
#include <iostream>
//TODO Wash finish this class
class Wash
{
  public:
    //constructor
    Wash();
    //copy constructor
    Wash(const Wash &rhs);
    //move constructor
    Wash(Wash &&rhs);
    //destructor
    ~Wash();
    //copy assignment operator
    Wash& operator=(const Wash &rhs);
    //move assignment operator
    Wash& operator=(Wash &&rhs);
    //swap
    void swap(Wash &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wash &rhs);
    friend std::istream& operator>>(std::istream &input, Wash &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WASH_H */