//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HOUSES_H
#define HOUSES_H
#include <iostream>
//TODO Houses finish this class
class Houses
{
  public:
    //constructor
    Houses();
    //copy constructor
    Houses(const Houses &rhs);
    //move constructor
    Houses(Houses &&rhs);
    //destructor
    ~Houses();
    //copy assignment operator
    Houses& operator=(const Houses &rhs);
    //move assignment operator
    Houses& operator=(Houses &&rhs);
    //swap
    void swap(Houses &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Houses &rhs);
    friend std::istream& operator>>(std::istream &input, Houses &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HOUSES_H */