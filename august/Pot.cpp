//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pot.h>
//constructor
Pot::Pot():
  m_placeholder(0)
{}

Pot::Pot(const Pot &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pot::Pot(Pot &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pot::~Pot(){}
Pot& Pot::operator=(const Pot &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pot& Pot::operator=(Pot &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pot &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pot &lhs)
{
  return input;
}

