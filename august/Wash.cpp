//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wash.h>
//constructor
Wash::Wash():
  m_placeholder(0)
{}

Wash::Wash(const Wash &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wash::Wash(Wash &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wash::~Wash(){}
Wash& Wash::operator=(const Wash &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wash& Wash::operator=(Wash &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wash &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wash &lhs)
{
  return input;
}

