//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Brake.h>
//constructor
Brake::Brake():
  m_placeholder(0)
{}

Brake::Brake(const Brake &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Brake::Brake(Brake &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Brake::~Brake(){}
Brake& Brake::operator=(const Brake &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Brake& Brake::operator=(Brake &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Brake &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Brake &lhs)
{
  return input;
}

