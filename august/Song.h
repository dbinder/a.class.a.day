//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SONG_H
#define SONG_H
#include <iostream>
//TODO Song finish this class
class Song
{
  public:
    //constructor
    Song();
    //copy constructor
    Song(const Song &rhs);
    //move constructor
    Song(Song &&rhs);
    //destructor
    ~Song();
    //copy assignment operator
    Song& operator=(const Song &rhs);
    //move assignment operator
    Song& operator=(Song &&rhs);
    //swap
    void swap(Song &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Song &rhs);
    friend std::istream& operator>>(std::istream &input, Song &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SONG_H */