//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Weight.h>
//constructor
Weight::Weight():
  m_placeholder(0)
{}

Weight::Weight(const Weight &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Weight::Weight(Weight &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Weight::~Weight(){}
Weight& Weight::operator=(const Weight &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Weight& Weight::operator=(Weight &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Weight &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Weight &lhs)
{
  return input;
}

