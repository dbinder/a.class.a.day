//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BATH_H
#define BATH_H
#include <iostream>
//TODO Bath finish this class
class Bath
{
  public:
    //constructor
    Bath();
    //copy constructor
    Bath(const Bath &rhs);
    //move constructor
    Bath(Bath &&rhs);
    //destructor
    ~Bath();
    //copy assignment operator
    Bath& operator=(const Bath &rhs);
    //move assignment operator
    Bath& operator=(Bath &&rhs);
    //swap
    void swap(Bath &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Bath &rhs);
    friend std::istream& operator>>(std::istream &input, Bath &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BATH_H */