//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef REACTION_H
#define REACTION_H
#include <iostream>
//TODO Reaction finish this class
class Reaction
{
  public:
    //constructor
    Reaction();
    //copy constructor
    Reaction(const Reaction &rhs);
    //move constructor
    Reaction(Reaction &&rhs);
    //destructor
    ~Reaction();
    //copy assignment operator
    Reaction& operator=(const Reaction &rhs);
    //move assignment operator
    Reaction& operator=(Reaction &&rhs);
    //swap
    void swap(Reaction &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Reaction &rhs);
    friend std::istream& operator>>(std::istream &input, Reaction &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* REACTION_H */