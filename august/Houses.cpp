//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Houses.h>
//constructor
Houses::Houses():
  m_placeholder(0)
{}

Houses::Houses(const Houses &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Houses::Houses(Houses &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Houses::~Houses(){}
Houses& Houses::operator=(const Houses &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Houses& Houses::operator=(Houses &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Houses &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Houses &lhs)
{
  return input;
}

