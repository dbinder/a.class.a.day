//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Account.h>
//constructor
Account::Account():
  m_placeholder(0)
{}

Account::Account(const Account &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Account::Account(Account &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Account::~Account(){}
Account& Account::operator=(const Account &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Account& Account::operator=(Account &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Account &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Account &lhs)
{
  return input;
}

