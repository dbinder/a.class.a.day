//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Drop.h>
//constructor
Drop::Drop():
  m_placeholder(0)
{}

Drop::Drop(const Drop &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Drop::Drop(Drop &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Drop::~Drop(){}
Drop& Drop::operator=(const Drop &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Drop& Drop::operator=(Drop &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Drop &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Drop &lhs)
{
  return input;
}

