//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ACCOUNT_H
#define ACCOUNT_H
#include <iostream>
//TODO Account finish this class
class Account
{
  public:
    //constructor
    Account();
    //copy constructor
    Account(const Account &rhs);
    //move constructor
    Account(Account &&rhs);
    //destructor
    ~Account();
    //copy assignment operator
    Account& operator=(const Account &rhs);
    //move assignment operator
    Account& operator=(Account &&rhs);
    //swap
    void swap(Account &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Account &rhs);
    friend std::istream& operator>>(std::istream &input, Account &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ACCOUNT_H */