//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WINTER_H
#define WINTER_H
#include <iostream>
//TODO Winter finish this class
class Winter
{
  public:
    //constructor
    Winter();
    //copy constructor
    Winter(const Winter &rhs);
    //move constructor
    Winter(Winter &&rhs);
    //destructor
    ~Winter();
    //copy assignment operator
    Winter& operator=(const Winter &rhs);
    //move assignment operator
    Winter& operator=(Winter &&rhs);
    //swap
    void swap(Winter &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Winter &rhs);
    friend std::istream& operator>>(std::istream &input, Winter &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WINTER_H */