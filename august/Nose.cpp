//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Nose.h>
//constructor
Nose::Nose():
  m_placeholder(0)
{}

Nose::Nose(const Nose &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Nose::Nose(Nose &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Nose::~Nose(){}
Nose& Nose::operator=(const Nose &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Nose& Nose::operator=(Nose &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Nose &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Nose &lhs)
{
  return input;
}

