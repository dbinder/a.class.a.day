//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Winter.h>
//constructor
Winter::Winter():
  m_placeholder(0)
{}

Winter::Winter(const Winter &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Winter::Winter(Winter &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Winter::~Winter(){}
Winter& Winter::operator=(const Winter &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Winter& Winter::operator=(Winter &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Winter &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Winter &lhs)
{
  return input;
}

