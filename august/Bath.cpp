//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Bath.h>
//constructor
Bath::Bath():
  m_placeholder(0)
{}

Bath::Bath(const Bath &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Bath::Bath(Bath &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Bath::~Bath(){}
Bath& Bath::operator=(const Bath &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Bath& Bath::operator=(Bath &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Bath &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Bath &lhs)
{
  return input;
}

