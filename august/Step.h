//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STEP_H
#define STEP_H
#include <iostream>
//TODO Step finish this class
class Step
{
  public:
    //constructor
    Step();
    //copy constructor
    Step(const Step &rhs);
    //move constructor
    Step(Step &&rhs);
    //destructor
    ~Step();
    //copy assignment operator
    Step& operator=(const Step &rhs);
    //move assignment operator
    Step& operator=(Step &&rhs);
    //swap
    void swap(Step &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Step &rhs);
    friend std::istream& operator>>(std::istream &input, Step &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STEP_H */