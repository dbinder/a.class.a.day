//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wilderness.h>
//constructor
Wilderness::Wilderness():
  m_placeholder(0)
{}

Wilderness::Wilderness(const Wilderness &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wilderness::Wilderness(Wilderness &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wilderness::~Wilderness(){}
Wilderness& Wilderness::operator=(const Wilderness &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wilderness& Wilderness::operator=(Wilderness &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wilderness &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wilderness &lhs)
{
  return input;
}

