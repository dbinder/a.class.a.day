//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DROP_H
#define DROP_H
#include <iostream>
//TODO Drop finish this class
class Drop
{
  public:
    //constructor
    Drop();
    //copy constructor
    Drop(const Drop &rhs);
    //move constructor
    Drop(Drop &&rhs);
    //destructor
    ~Drop();
    //copy assignment operator
    Drop& operator=(const Drop &rhs);
    //move assignment operator
    Drop& operator=(Drop &&rhs);
    //swap
    void swap(Drop &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Drop &rhs);
    friend std::istream& operator>>(std::istream &input, Drop &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DROP_H */