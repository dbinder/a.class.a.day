//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Discovery.h>
//constructor
Discovery::Discovery():
  m_placeholder(0)
{}

Discovery::Discovery(const Discovery &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Discovery::Discovery(Discovery &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Discovery::~Discovery(){}
Discovery& Discovery::operator=(const Discovery &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Discovery& Discovery::operator=(Discovery &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Discovery &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Discovery &lhs)
{
  return input;
}

