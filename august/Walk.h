//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WALK_H
#define WALK_H
#include <iostream>
//TODO Walk finish this class
class Walk
{
  public:
    //constructor
    Walk();
    //copy constructor
    Walk(const Walk &rhs);
    //move constructor
    Walk(Walk &&rhs);
    //destructor
    ~Walk();
    //copy assignment operator
    Walk& operator=(const Walk &rhs);
    //move assignment operator
    Walk& operator=(Walk &&rhs);
    //swap
    void swap(Walk &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Walk &rhs);
    friend std::istream& operator>>(std::istream &input, Walk &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WALK_H */