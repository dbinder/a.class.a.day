//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cart.h>
//constructor
Cart::Cart():
  m_placeholder(0)
{}

Cart::Cart(const Cart &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cart::Cart(Cart &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cart::~Cart(){}
Cart& Cart::operator=(const Cart &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cart& Cart::operator=(Cart &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cart &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cart &lhs)
{
  return input;
}

