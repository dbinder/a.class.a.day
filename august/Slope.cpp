//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Slope.h>
//constructor
Slope::Slope():
  m_placeholder(0)
{}

Slope::Slope(const Slope &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Slope::Slope(Slope &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Slope::~Slope(){}
Slope& Slope::operator=(const Slope &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Slope& Slope::operator=(Slope &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Slope &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Slope &lhs)
{
  return input;
}

