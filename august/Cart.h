//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CART_H
#define CART_H
#include <iostream>
//TODO Cart finish this class
class Cart
{
  public:
    //constructor
    Cart();
    //copy constructor
    Cart(const Cart &rhs);
    //move constructor
    Cart(Cart &&rhs);
    //destructor
    ~Cart();
    //copy assignment operator
    Cart& operator=(const Cart &rhs);
    //move assignment operator
    Cart& operator=(Cart &&rhs);
    //swap
    void swap(Cart &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cart &rhs);
    friend std::istream& operator>>(std::istream &input, Cart &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CART_H */