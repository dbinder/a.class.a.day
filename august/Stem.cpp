//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Stem.h>
//constructor
Stem::Stem():
  m_placeholder(0)
{}

Stem::Stem(const Stem &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Stem::Stem(Stem &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Stem::~Stem(){}
Stem& Stem::operator=(const Stem &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Stem& Stem::operator=(Stem &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Stem &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Stem &lhs)
{
  return input;
}

