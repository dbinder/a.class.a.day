//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DISCOVERY_H
#define DISCOVERY_H
#include <iostream>
//TODO Discovery finish this class
class Discovery
{
  public:
    //constructor
    Discovery();
    //copy constructor
    Discovery(const Discovery &rhs);
    //move constructor
    Discovery(Discovery &&rhs);
    //destructor
    ~Discovery();
    //copy assignment operator
    Discovery& operator=(const Discovery &rhs);
    //move assignment operator
    Discovery& operator=(Discovery &&rhs);
    //swap
    void swap(Discovery &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Discovery &rhs);
    friend std::istream& operator>>(std::istream &input, Discovery &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DISCOVERY_H */