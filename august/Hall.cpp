//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Hall.h>
//constructor
Hall::Hall():
  m_placeholder(0)
{}

Hall::Hall(const Hall &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Hall::Hall(Hall &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Hall::~Hall(){}
Hall& Hall::operator=(const Hall &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Hall& Hall::operator=(Hall &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Hall &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Hall &lhs)
{
  return input;
}

