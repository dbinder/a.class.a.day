//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SNAKES_H
#define SNAKES_H
#include <iostream>
//TODO Snakes finish this class
class Snakes
{
  public:
    //constructor
    Snakes();
    //copy constructor
    Snakes(const Snakes &rhs);
    //move constructor
    Snakes(Snakes &&rhs);
    //destructor
    ~Snakes();
    //copy assignment operator
    Snakes& operator=(const Snakes &rhs);
    //move assignment operator
    Snakes& operator=(Snakes &&rhs);
    //swap
    void swap(Snakes &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Snakes &rhs);
    friend std::istream& operator>>(std::istream &input, Snakes &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SNAKES_H */