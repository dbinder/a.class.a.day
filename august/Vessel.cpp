//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Vessel.h>
//constructor
Vessel::Vessel():
  m_placeholder(0)
{}

Vessel::Vessel(const Vessel &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Vessel::Vessel(Vessel &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Vessel::~Vessel(){}
Vessel& Vessel::operator=(const Vessel &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Vessel& Vessel::operator=(Vessel &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Vessel &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Vessel &lhs)
{
  return input;
}

