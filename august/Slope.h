//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SLOPE_H
#define SLOPE_H
#include <iostream>
//TODO Slope finish this class
class Slope
{
  public:
    //constructor
    Slope();
    //copy constructor
    Slope(const Slope &rhs);
    //move constructor
    Slope(Slope &&rhs);
    //destructor
    ~Slope();
    //copy assignment operator
    Slope& operator=(const Slope &rhs);
    //move assignment operator
    Slope& operator=(Slope &&rhs);
    //swap
    void swap(Slope &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Slope &rhs);
    friend std::istream& operator>>(std::istream &input, Slope &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SLOPE_H */