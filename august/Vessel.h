//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef VESSEL_H
#define VESSEL_H
#include <iostream>
//TODO Vessel finish this class
class Vessel
{
  public:
    //constructor
    Vessel();
    //copy constructor
    Vessel(const Vessel &rhs);
    //move constructor
    Vessel(Vessel &&rhs);
    //destructor
    ~Vessel();
    //copy assignment operator
    Vessel& operator=(const Vessel &rhs);
    //move assignment operator
    Vessel& operator=(Vessel &&rhs);
    //swap
    void swap(Vessel &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Vessel &rhs);
    friend std::istream& operator>>(std::istream &input, Vessel &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* VESSEL_H */