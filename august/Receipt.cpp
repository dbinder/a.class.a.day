//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Receipt.h>
//constructor
Receipt::Receipt():
  m_placeholder(0)
{}

Receipt::Receipt(const Receipt &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Receipt::Receipt(Receipt &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Receipt::~Receipt(){}
Receipt& Receipt::operator=(const Receipt &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Receipt& Receipt::operator=(Receipt &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Receipt &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Receipt &lhs)
{
  return input;
}

