//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WEIGHT_H
#define WEIGHT_H
#include <iostream>
//TODO Weight finish this class
class Weight
{
  public:
    //constructor
    Weight();
    //copy constructor
    Weight(const Weight &rhs);
    //move constructor
    Weight(Weight &&rhs);
    //destructor
    ~Weight();
    //copy assignment operator
    Weight& operator=(const Weight &rhs);
    //move assignment operator
    Weight& operator=(Weight &&rhs);
    //swap
    void swap(Weight &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Weight &rhs);
    friend std::istream& operator>>(std::istream &input, Weight &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WEIGHT_H */