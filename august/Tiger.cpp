//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Tiger.h>
//constructor
Tiger::Tiger():
  m_placeholder(0)
{}

Tiger::Tiger(const Tiger &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Tiger::Tiger(Tiger &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Tiger::~Tiger(){}
Tiger& Tiger::operator=(const Tiger &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Tiger& Tiger::operator=(Tiger &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Tiger &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Tiger &lhs)
{
  return input;
}

