//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Kitty.h>
//constructor
Kitty::Kitty():
  m_placeholder(0)
{}

Kitty::Kitty(const Kitty &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Kitty::Kitty(Kitty &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Kitty::~Kitty(){}
Kitty& Kitty::operator=(const Kitty &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Kitty& Kitty::operator=(Kitty &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Kitty &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Kitty &lhs)
{
  return input;
}

