//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef REPRESENTATIVE_H
#define REPRESENTATIVE_H
#include <iostream>
//TODO Representative finish this class
class Representative
{
  public:
    //constructor
    Representative();
    //copy constructor
    Representative(const Representative &rhs);
    //move constructor
    Representative(Representative &&rhs);
    //destructor
    ~Representative();
    //copy assignment operator
    Representative& operator=(const Representative &rhs);
    //move assignment operator
    Representative& operator=(Representative &&rhs);
    //swap
    void swap(Representative &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Representative &rhs);
    friend std::istream& operator>>(std::istream &input, Representative &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* REPRESENTATIVE_H */