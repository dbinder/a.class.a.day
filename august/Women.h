//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WOMEN_H
#define WOMEN_H
#include <iostream>
//TODO Women finish this class
class Women
{
  public:
    //constructor
    Women();
    //copy constructor
    Women(const Women &rhs);
    //move constructor
    Women(Women &&rhs);
    //destructor
    ~Women();
    //copy assignment operator
    Women& operator=(const Women &rhs);
    //move assignment operator
    Women& operator=(Women &&rhs);
    //swap
    void swap(Women &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Women &rhs);
    friend std::istream& operator>>(std::istream &input, Women &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WOMEN_H */