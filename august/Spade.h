//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SPADE_H
#define SPADE_H
#include <iostream>
//TODO Spade finish this class
class Spade
{
  public:
    //constructor
    Spade();
    //copy constructor
    Spade(const Spade &rhs);
    //move constructor
    Spade(Spade &&rhs);
    //destructor
    ~Spade();
    //copy assignment operator
    Spade& operator=(const Spade &rhs);
    //move assignment operator
    Spade& operator=(Spade &&rhs);
    //swap
    void swap(Spade &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Spade &rhs);
    friend std::istream& operator>>(std::istream &input, Spade &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SPADE_H */