//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef KITTY_H
#define KITTY_H
#include <iostream>
//TODO Kitty finish this class
class Kitty
{
  public:
    //constructor
    Kitty();
    //copy constructor
    Kitty(const Kitty &rhs);
    //move constructor
    Kitty(Kitty &&rhs);
    //destructor
    ~Kitty();
    //copy assignment operator
    Kitty& operator=(const Kitty &rhs);
    //move assignment operator
    Kitty& operator=(Kitty &&rhs);
    //swap
    void swap(Kitty &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Kitty &rhs);
    friend std::istream& operator>>(std::istream &input, Kitty &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* KITTY_H */