//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BOOK_H
#define BOOK_H
#include <iostream>
//TODO Book finish this class
class Book
{
  public:
    //constructor
    Book();
    //copy constructor
    Book(const Book &rhs);
    //move constructor
    Book(Book &&rhs);
    //destructor
    ~Book();
    //copy assignment operator
    Book& operator=(const Book &rhs);
    //move assignment operator
    Book& operator=(Book &&rhs);
    //swap
    void swap(Book &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Book &rhs);
    friend std::istream& operator>>(std::istream &input, Book &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BOOK_H */