//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TIGER_H
#define TIGER_H
#include <iostream>
//TODO Tiger finish this class
class Tiger
{
  public:
    //constructor
    Tiger();
    //copy constructor
    Tiger(const Tiger &rhs);
    //move constructor
    Tiger(Tiger &&rhs);
    //destructor
    ~Tiger();
    //copy assignment operator
    Tiger& operator=(const Tiger &rhs);
    //move assignment operator
    Tiger& operator=(Tiger &&rhs);
    //swap
    void swap(Tiger &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Tiger &rhs);
    friend std::istream& operator>>(std::istream &input, Tiger &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TIGER_H */