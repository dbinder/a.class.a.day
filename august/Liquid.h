//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef LIQUID_H
#define LIQUID_H
#include <iostream>
//TODO Liquid finish this class
class Liquid
{
  public:
    //constructor
    Liquid();
    //copy constructor
    Liquid(const Liquid &rhs);
    //move constructor
    Liquid(Liquid &&rhs);
    //destructor
    ~Liquid();
    //copy assignment operator
    Liquid& operator=(const Liquid &rhs);
    //move assignment operator
    Liquid& operator=(Liquid &&rhs);
    //swap
    void swap(Liquid &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Liquid &rhs);
    friend std::istream& operator>>(std::istream &input, Liquid &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* LIQUID_H */