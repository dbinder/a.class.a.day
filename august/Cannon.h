//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CANNON_H
#define CANNON_H
#include <iostream>
//TODO Cannon finish this class
class Cannon
{
  public:
    //constructor
    Cannon();
    //copy constructor
    Cannon(const Cannon &rhs);
    //move constructor
    Cannon(Cannon &&rhs);
    //destructor
    ~Cannon();
    //copy assignment operator
    Cannon& operator=(const Cannon &rhs);
    //move assignment operator
    Cannon& operator=(Cannon &&rhs);
    //swap
    void swap(Cannon &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cannon &rhs);
    friend std::istream& operator>>(std::istream &input, Cannon &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CANNON_H */