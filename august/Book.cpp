//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Book.h>
//constructor
Book::Book():
  m_placeholder(0)
{}

Book::Book(const Book &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Book::Book(Book &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Book::~Book(){}
Book& Book::operator=(const Book &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Book& Book::operator=(Book &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Book &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Book &lhs)
{
  return input;
}

