//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HALL_H
#define HALL_H
#include <iostream>
//TODO Hall finish this class
class Hall
{
  public:
    //constructor
    Hall();
    //copy constructor
    Hall(const Hall &rhs);
    //move constructor
    Hall(Hall &&rhs);
    //destructor
    ~Hall();
    //copy assignment operator
    Hall& operator=(const Hall &rhs);
    //move assignment operator
    Hall& operator=(Hall &&rhs);
    //swap
    void swap(Hall &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Hall &rhs);
    friend std::istream& operator>>(std::istream &input, Hall &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HALL_H */