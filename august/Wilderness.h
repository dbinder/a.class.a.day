//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WILDERNESS_H
#define WILDERNESS_H
#include <iostream>
//TODO Wilderness finish this class
class Wilderness
{
  public:
    //constructor
    Wilderness();
    //copy constructor
    Wilderness(const Wilderness &rhs);
    //move constructor
    Wilderness(Wilderness &&rhs);
    //destructor
    ~Wilderness();
    //copy assignment operator
    Wilderness& operator=(const Wilderness &rhs);
    //move assignment operator
    Wilderness& operator=(Wilderness &&rhs);
    //swap
    void swap(Wilderness &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wilderness &rhs);
    friend std::istream& operator>>(std::istream &input, Wilderness &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WILDERNESS_H */