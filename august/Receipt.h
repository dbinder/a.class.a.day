//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RECEIPT_H
#define RECEIPT_H
#include <iostream>
//TODO Receipt finish this class
class Receipt
{
  public:
    //constructor
    Receipt();
    //copy constructor
    Receipt(const Receipt &rhs);
    //move constructor
    Receipt(Receipt &&rhs);
    //destructor
    ~Receipt();
    //copy assignment operator
    Receipt& operator=(const Receipt &rhs);
    //move assignment operator
    Receipt& operator=(Receipt &&rhs);
    //swap
    void swap(Receipt &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Receipt &rhs);
    friend std::istream& operator>>(std::istream &input, Receipt &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RECEIPT_H */