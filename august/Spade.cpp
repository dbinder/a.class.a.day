//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Spade.h>
//constructor
Spade::Spade():
  m_placeholder(0)
{}

Spade::Spade(const Spade &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Spade::Spade(Spade &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Spade::~Spade(){}
Spade& Spade::operator=(const Spade &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Spade& Spade::operator=(Spade &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Spade &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Spade &lhs)
{
  return input;
}

