//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STEM_H
#define STEM_H
#include <iostream>
//TODO Stem finish this class
class Stem
{
  public:
    //constructor
    Stem();
    //copy constructor
    Stem(const Stem &rhs);
    //move constructor
    Stem(Stem &&rhs);
    //destructor
    ~Stem();
    //copy assignment operator
    Stem& operator=(const Stem &rhs);
    //move assignment operator
    Stem& operator=(Stem &&rhs);
    //swap
    void swap(Stem &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Stem &rhs);
    friend std::istream& operator>>(std::istream &input, Stem &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STEM_H */