//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Snakes.h>
//constructor
Snakes::Snakes():
  m_placeholder(0)
{}

Snakes::Snakes(const Snakes &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Snakes::Snakes(Snakes &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Snakes::~Snakes(){}
Snakes& Snakes::operator=(const Snakes &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Snakes& Snakes::operator=(Snakes &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Snakes &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Snakes &lhs)
{
  return input;
}

