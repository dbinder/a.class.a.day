//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BRAKE_H
#define BRAKE_H
#include <iostream>
//TODO Brake finish this class
class Brake
{
  public:
    //constructor
    Brake();
    //copy constructor
    Brake(const Brake &rhs);
    //move constructor
    Brake(Brake &&rhs);
    //destructor
    ~Brake();
    //copy assignment operator
    Brake& operator=(const Brake &rhs);
    //move assignment operator
    Brake& operator=(Brake &&rhs);
    //swap
    void swap(Brake &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Brake &rhs);
    friend std::istream& operator>>(std::istream &input, Brake &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BRAKE_H */