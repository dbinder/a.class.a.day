//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cannon.h>
//constructor
Cannon::Cannon():
  m_placeholder(0)
{}

Cannon::Cannon(const Cannon &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cannon::Cannon(Cannon &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cannon::~Cannon(){}
Cannon& Cannon::operator=(const Cannon &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cannon& Cannon::operator=(Cannon &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cannon &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cannon &lhs)
{
  return input;
}

