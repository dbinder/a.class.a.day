//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Women.h>
//constructor
Women::Women():
  m_placeholder(0)
{}

Women::Women(const Women &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Women::Women(Women &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Women::~Women(){}
Women& Women::operator=(const Women &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Women& Women::operator=(Women &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Women &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Women &lhs)
{
  return input;
}

