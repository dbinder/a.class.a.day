//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Reaction.h>
//constructor
Reaction::Reaction():
  m_placeholder(0)
{}

Reaction::Reaction(const Reaction &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Reaction::Reaction(Reaction &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Reaction::~Reaction(){}
Reaction& Reaction::operator=(const Reaction &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Reaction& Reaction::operator=(Reaction &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Reaction &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Reaction &lhs)
{
  return input;
}

