//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef LOSS_H
#define LOSS_H
#include <iostream>
//TODO Loss finish this class
class Loss
{
  public:
    //constructor
    Loss();
    //copy constructor
    Loss(const Loss &rhs);
    //move constructor
    Loss(Loss &&rhs);
    //destructor
    ~Loss();
    //copy assignment operator
    Loss& operator=(const Loss &rhs);
    //move assignment operator
    Loss& operator=(Loss &&rhs);
    //swap
    void swap(Loss &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Loss &rhs);
    friend std::istream& operator>>(std::istream &input, Loss &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* LOSS_H */