//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef QUARTZ_H
#define QUARTZ_H
#include <iostream>
//TODO Quartz finish this class
class Quartz
{
  public:
    //constructor
    Quartz();
    //copy constructor
    Quartz(const Quartz &rhs);
    //move constructor
    Quartz(Quartz &&rhs);
    //destructor
    ~Quartz();
    //copy assignment operator
    Quartz& operator=(const Quartz &rhs);
    //move assignment operator
    Quartz& operator=(Quartz &&rhs);
    //swap
    void swap(Quartz &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Quartz &rhs);
    friend std::istream& operator>>(std::istream &input, Quartz &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* QUARTZ_H */