//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SISTER_H
#define SISTER_H
#include <iostream>
//TODO Sister finish this class
class Sister
{
  public:
    //constructor
    Sister();
    //copy constructor
    Sister(const Sister &rhs);
    //move constructor
    Sister(Sister &&rhs);
    //destructor
    ~Sister();
    //copy assignment operator
    Sister& operator=(const Sister &rhs);
    //move assignment operator
    Sister& operator=(Sister &&rhs);
    //swap
    void swap(Sister &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Sister &rhs);
    friend std::istream& operator>>(std::istream &input, Sister &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SISTER_H */