//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Bit.h>
//constructor
Bit::Bit():
  m_placeholder(0)
{}

Bit::Bit(const Bit &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Bit::Bit(Bit &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Bit::~Bit(){}
Bit& Bit::operator=(const Bit &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Bit& Bit::operator=(Bit &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Bit &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Bit &lhs)
{
  return input;
}

