//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Army.h>
//constructor
Army::Army():
  m_placeholder(0)
{}

Army::Army(const Army &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Army::Army(Army &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Army::~Army(){}
Army& Army::operator=(const Army &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Army& Army::operator=(Army &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Army &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Army &lhs)
{
  return input;
}

