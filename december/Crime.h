//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CRIME_H
#define CRIME_H
#include <iostream>
//TODO Crime finish this class
class Crime
{
  public:
    //constructor
    Crime();
    //copy constructor
    Crime(const Crime &rhs);
    //move constructor
    Crime(Crime &&rhs);
    //destructor
    ~Crime();
    //copy assignment operator
    Crime& operator=(const Crime &rhs);
    //move assignment operator
    Crime& operator=(Crime &&rhs);
    //swap
    void swap(Crime &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Crime &rhs);
    friend std::istream& operator>>(std::istream &input, Crime &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CRIME_H */