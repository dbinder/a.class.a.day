//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BED_H
#define BED_H
#include <iostream>
//TODO Bed finish this class
class Bed
{
  public:
    //constructor
    Bed();
    //copy constructor
    Bed(const Bed &rhs);
    //move constructor
    Bed(Bed &&rhs);
    //destructor
    ~Bed();
    //copy assignment operator
    Bed& operator=(const Bed &rhs);
    //move assignment operator
    Bed& operator=(Bed &&rhs);
    //swap
    void swap(Bed &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Bed &rhs);
    friend std::istream& operator>>(std::istream &input, Bed &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BED_H */