//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Credit.h>
//constructor
Credit::Credit():
  m_placeholder(0)
{}

Credit::Credit(const Credit &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Credit::Credit(Credit &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Credit::~Credit(){}
Credit& Credit::operator=(const Credit &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Credit& Credit::operator=(Credit &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Credit &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Credit &lhs)
{
  return input;
}

