//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Animal.h>
//constructor
Animal::Animal():
  m_placeholder(0)
{}

Animal::Animal(const Animal &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Animal::Animal(Animal &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Animal::~Animal(){}
Animal& Animal::operator=(const Animal &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Animal& Animal::operator=(Animal &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Animal &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Animal &lhs)
{
  return input;
}

