//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DIGESTION_H
#define DIGESTION_H
#include <iostream>
//TODO Digestion finish this class
class Digestion
{
  public:
    //constructor
    Digestion();
    //copy constructor
    Digestion(const Digestion &rhs);
    //move constructor
    Digestion(Digestion &&rhs);
    //destructor
    ~Digestion();
    //copy assignment operator
    Digestion& operator=(const Digestion &rhs);
    //move assignment operator
    Digestion& operator=(Digestion &&rhs);
    //swap
    void swap(Digestion &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Digestion &rhs);
    friend std::istream& operator>>(std::istream &input, Digestion &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DIGESTION_H */