//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CATTLE_H
#define CATTLE_H
#include <iostream>
//TODO Cattle finish this class
class Cattle
{
  public:
    //constructor
    Cattle();
    //copy constructor
    Cattle(const Cattle &rhs);
    //move constructor
    Cattle(Cattle &&rhs);
    //destructor
    ~Cattle();
    //copy assignment operator
    Cattle& operator=(const Cattle &rhs);
    //move assignment operator
    Cattle& operator=(Cattle &&rhs);
    //swap
    void swap(Cattle &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cattle &rhs);
    friend std::istream& operator>>(std::istream &input, Cattle &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CATTLE_H */