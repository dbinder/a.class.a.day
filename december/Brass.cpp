//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Brass.h>
//constructor
Brass::Brass():
  m_placeholder(0)
{}

Brass::Brass(const Brass &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Brass::Brass(Brass &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Brass::~Brass(){}
Brass& Brass::operator=(const Brass &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Brass& Brass::operator=(Brass &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Brass &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Brass &lhs)
{
  return input;
}

