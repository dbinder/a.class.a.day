//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WISH_H
#define WISH_H
#include <iostream>
//TODO Wish finish this class
class Wish
{
  public:
    //constructor
    Wish();
    //copy constructor
    Wish(const Wish &rhs);
    //move constructor
    Wish(Wish &&rhs);
    //destructor
    ~Wish();
    //copy assignment operator
    Wish& operator=(const Wish &rhs);
    //move assignment operator
    Wish& operator=(Wish &&rhs);
    //swap
    void swap(Wish &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wish &rhs);
    friend std::istream& operator>>(std::istream &input, Wish &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WISH_H */