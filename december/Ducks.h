//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DUCKS_H
#define DUCKS_H
#include <iostream>
//TODO Ducks finish this class
class Ducks
{
  public:
    //constructor
    Ducks();
    //copy constructor
    Ducks(const Ducks &rhs);
    //move constructor
    Ducks(Ducks &&rhs);
    //destructor
    ~Ducks();
    //copy assignment operator
    Ducks& operator=(const Ducks &rhs);
    //move assignment operator
    Ducks& operator=(Ducks &&rhs);
    //swap
    void swap(Ducks &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Ducks &rhs);
    friend std::istream& operator>>(std::istream &input, Ducks &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DUCKS_H */