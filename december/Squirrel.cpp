//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Squirrel.h>
//constructor
Squirrel::Squirrel():
  m_placeholder(0)
{}

Squirrel::Squirrel(const Squirrel &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Squirrel::Squirrel(Squirrel &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Squirrel::~Squirrel(){}
Squirrel& Squirrel::operator=(const Squirrel &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Squirrel& Squirrel::operator=(Squirrel &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Squirrel &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Squirrel &lhs)
{
  return input;
}

