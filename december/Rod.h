//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ROD_H
#define ROD_H
#include <iostream>
//TODO Rod finish this class
class Rod
{
  public:
    //constructor
    Rod();
    //copy constructor
    Rod(const Rod &rhs);
    //move constructor
    Rod(Rod &&rhs);
    //destructor
    ~Rod();
    //copy assignment operator
    Rod& operator=(const Rod &rhs);
    //move assignment operator
    Rod& operator=(Rod &&rhs);
    //swap
    void swap(Rod &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Rod &rhs);
    friend std::istream& operator>>(std::istream &input, Rod &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ROD_H */