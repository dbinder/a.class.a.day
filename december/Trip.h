//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TRIP_H
#define TRIP_H
#include <iostream>
//TODO Trip finish this class
class Trip
{
  public:
    //constructor
    Trip();
    //copy constructor
    Trip(const Trip &rhs);
    //move constructor
    Trip(Trip &&rhs);
    //destructor
    ~Trip();
    //copy assignment operator
    Trip& operator=(const Trip &rhs);
    //move assignment operator
    Trip& operator=(Trip &&rhs);
    //swap
    void swap(Trip &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Trip &rhs);
    friend std::istream& operator>>(std::istream &input, Trip &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TRIP_H */