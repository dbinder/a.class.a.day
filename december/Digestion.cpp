//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Digestion.h>
//constructor
Digestion::Digestion():
  m_placeholder(0)
{}

Digestion::Digestion(const Digestion &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Digestion::Digestion(Digestion &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Digestion::~Digestion(){}
Digestion& Digestion::operator=(const Digestion &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Digestion& Digestion::operator=(Digestion &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Digestion &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Digestion &lhs)
{
  return input;
}

