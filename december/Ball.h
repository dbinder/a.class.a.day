//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BALL_H
#define BALL_H
#include <iostream>
//TODO Ball finish this class
class Ball
{
  public:
    //constructor
    Ball();
    //copy constructor
    Ball(const Ball &rhs);
    //move constructor
    Ball(Ball &&rhs);
    //destructor
    ~Ball();
    //copy assignment operator
    Ball& operator=(const Ball &rhs);
    //move assignment operator
    Ball& operator=(Ball &&rhs);
    //swap
    void swap(Ball &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Ball &rhs);
    friend std::istream& operator>>(std::istream &input, Ball &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BALL_H */