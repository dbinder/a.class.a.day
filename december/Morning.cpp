//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Morning.h>
//constructor
Morning::Morning():
  m_placeholder(0)
{}

Morning::Morning(const Morning &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Morning::Morning(Morning &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Morning::~Morning(){}
Morning& Morning::operator=(const Morning &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Morning& Morning::operator=(Morning &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Morning &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Morning &lhs)
{
  return input;
}

