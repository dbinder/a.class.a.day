//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef FISH_H
#define FISH_H
#include <iostream>
//TODO Fish finish this class
class Fish
{
  public:
    //constructor
    Fish();
    //copy constructor
    Fish(const Fish &rhs);
    //move constructor
    Fish(Fish &&rhs);
    //destructor
    ~Fish();
    //copy assignment operator
    Fish& operator=(const Fish &rhs);
    //move assignment operator
    Fish& operator=(Fish &&rhs);
    //swap
    void swap(Fish &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Fish &rhs);
    friend std::istream& operator>>(std::istream &input, Fish &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* FISH_H */