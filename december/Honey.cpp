//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Honey.h>
//constructor
Honey::Honey():
  m_placeholder(0)
{}

Honey::Honey(const Honey &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Honey::Honey(Honey &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Honey::~Honey(){}
Honey& Honey::operator=(const Honey &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Honey& Honey::operator=(Honey &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Honey &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Honey &lhs)
{
  return input;
}

