//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Loss.h>
//constructor
Loss::Loss():
  m_placeholder(0)
{}

Loss::Loss(const Loss &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Loss::Loss(Loss &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Loss::~Loss(){}
Loss& Loss::operator=(const Loss &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Loss& Loss::operator=(Loss &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Loss &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Loss &lhs)
{
  return input;
}

