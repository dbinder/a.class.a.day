//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef POCKET_H
#define POCKET_H
#include <iostream>
//TODO Pocket finish this class
class Pocket
{
  public:
    //constructor
    Pocket();
    //copy constructor
    Pocket(const Pocket &rhs);
    //move constructor
    Pocket(Pocket &&rhs);
    //destructor
    ~Pocket();
    //copy assignment operator
    Pocket& operator=(const Pocket &rhs);
    //move assignment operator
    Pocket& operator=(Pocket &&rhs);
    //swap
    void swap(Pocket &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pocket &rhs);
    friend std::istream& operator>>(std::istream &input, Pocket &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* POCKET_H */