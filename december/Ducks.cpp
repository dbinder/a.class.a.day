//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Ducks.h>
//constructor
Ducks::Ducks():
  m_placeholder(0)
{}

Ducks::Ducks(const Ducks &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Ducks::Ducks(Ducks &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Ducks::~Ducks(){}
Ducks& Ducks::operator=(const Ducks &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Ducks& Ducks::operator=(Ducks &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Ducks &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Ducks &lhs)
{
  return input;
}

