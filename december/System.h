//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SYSTEM_H
#define SYSTEM_H
#include <iostream>
//TODO System finish this class
class System
{
  public:
    //constructor
    System();
    //copy constructor
    System(const System &rhs);
    //move constructor
    System(System &&rhs);
    //destructor
    ~System();
    //copy assignment operator
    System& operator=(const System &rhs);
    //move assignment operator
    System& operator=(System &&rhs);
    //swap
    void swap(System &rhs);
    friend std::ostream& operator<<(std::ostream &output, const System &rhs);
    friend std::istream& operator>>(std::istream &input, System &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SYSTEM_H */