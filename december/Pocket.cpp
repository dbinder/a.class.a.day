//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pocket.h>
//constructor
Pocket::Pocket():
  m_placeholder(0)
{}

Pocket::Pocket(const Pocket &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pocket::Pocket(Pocket &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pocket::~Pocket(){}
Pocket& Pocket::operator=(const Pocket &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pocket& Pocket::operator=(Pocket &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pocket &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pocket &lhs)
{
  return input;
}

