//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wall.h>
//constructor
Wall::Wall():
  m_placeholder(0)
{}

Wall::Wall(const Wall &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wall::Wall(Wall &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wall::~Wall(){}
Wall& Wall::operator=(const Wall &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wall& Wall::operator=(Wall &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wall &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wall &lhs)
{
  return input;
}

