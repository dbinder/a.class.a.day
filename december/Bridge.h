//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BRIDGE_H
#define BRIDGE_H
#include <iostream>
//TODO Bridge finish this class
class Bridge
{
  public:
    //constructor
    Bridge();
    //copy constructor
    Bridge(const Bridge &rhs);
    //move constructor
    Bridge(Bridge &&rhs);
    //destructor
    ~Bridge();
    //copy assignment operator
    Bridge& operator=(const Bridge &rhs);
    //move assignment operator
    Bridge& operator=(Bridge &&rhs);
    //swap
    void swap(Bridge &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Bridge &rhs);
    friend std::istream& operator>>(std::istream &input, Bridge &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BRIDGE_H */