//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pail.h>
//constructor
Pail::Pail():
  m_placeholder(0)
{}

Pail::Pail(const Pail &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pail::Pail(Pail &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pail::~Pail(){}
Pail& Pail::operator=(const Pail &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pail& Pail::operator=(Pail &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pail &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pail &lhs)
{
  return input;
}

