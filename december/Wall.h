//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WALL_H
#define WALL_H
#include <iostream>
//TODO Wall finish this class
class Wall
{
  public:
    //constructor
    Wall();
    //copy constructor
    Wall(const Wall &rhs);
    //move constructor
    Wall(Wall &&rhs);
    //destructor
    ~Wall();
    //copy assignment operator
    Wall& operator=(const Wall &rhs);
    //move assignment operator
    Wall& operator=(Wall &&rhs);
    //swap
    void swap(Wall &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wall &rhs);
    friend std::istream& operator>>(std::istream &input, Wall &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WALL_H */