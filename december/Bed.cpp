//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Bed.h>
//constructor
Bed::Bed():
  m_placeholder(0)
{}

Bed::Bed(const Bed &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Bed::Bed(Bed &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Bed::~Bed(){}
Bed& Bed::operator=(const Bed &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Bed& Bed::operator=(Bed &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Bed &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Bed &lhs)
{
  return input;
}

