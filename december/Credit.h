//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CREDIT_H
#define CREDIT_H
#include <iostream>
//TODO Credit finish this class
class Credit
{
  public:
    //constructor
    Credit();
    //copy constructor
    Credit(const Credit &rhs);
    //move constructor
    Credit(Credit &&rhs);
    //destructor
    ~Credit();
    //copy assignment operator
    Credit& operator=(const Credit &rhs);
    //move assignment operator
    Credit& operator=(Credit &&rhs);
    //swap
    void swap(Credit &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Credit &rhs);
    friend std::istream& operator>>(std::istream &input, Credit &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CREDIT_H */