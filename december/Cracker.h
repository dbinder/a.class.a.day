//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CRACKER_H
#define CRACKER_H
#include <iostream>
//TODO Cracker finish this class
class Cracker
{
  public:
    //constructor
    Cracker();
    //copy constructor
    Cracker(const Cracker &rhs);
    //move constructor
    Cracker(Cracker &&rhs);
    //destructor
    ~Cracker();
    //copy assignment operator
    Cracker& operator=(const Cracker &rhs);
    //move assignment operator
    Cracker& operator=(Cracker &&rhs);
    //swap
    void swap(Cracker &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cracker &rhs);
    friend std::istream& operator>>(std::istream &input, Cracker &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CRACKER_H */