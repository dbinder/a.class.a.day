//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cracker.h>
//constructor
Cracker::Cracker():
  m_placeholder(0)
{}

Cracker::Cracker(const Cracker &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cracker::Cracker(Cracker &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cracker::~Cracker(){}
Cracker& Cracker::operator=(const Cracker &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cracker& Cracker::operator=(Cracker &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cracker &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cracker &lhs)
{
  return input;
}

