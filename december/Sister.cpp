//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Sister.h>
//constructor
Sister::Sister():
  m_placeholder(0)
{}

Sister::Sister(const Sister &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Sister::Sister(Sister &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Sister::~Sister(){}
Sister& Sister::operator=(const Sister &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Sister& Sister::operator=(Sister &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Sister &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Sister &lhs)
{
  return input;
}

