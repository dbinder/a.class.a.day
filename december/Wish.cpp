//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wish.h>
//constructor
Wish::Wish():
  m_placeholder(0)
{}

Wish::Wish(const Wish &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wish::Wish(Wish &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wish::~Wish(){}
Wish& Wish::operator=(const Wish &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wish& Wish::operator=(Wish &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wish &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wish &lhs)
{
  return input;
}

