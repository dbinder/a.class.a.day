//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HONEY_H
#define HONEY_H
#include <iostream>
//TODO Honey finish this class
class Honey
{
  public:
    //constructor
    Honey();
    //copy constructor
    Honey(const Honey &rhs);
    //move constructor
    Honey(Honey &&rhs);
    //destructor
    ~Honey();
    //copy assignment operator
    Honey& operator=(const Honey &rhs);
    //move assignment operator
    Honey& operator=(Honey &&rhs);
    //swap
    void swap(Honey &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Honey &rhs);
    friend std::istream& operator>>(std::istream &input, Honey &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HONEY_H */