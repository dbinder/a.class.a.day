//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Crime.h>
//constructor
Crime::Crime():
  m_placeholder(0)
{}

Crime::Crime(const Crime &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Crime::Crime(Crime &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Crime::~Crime(){}
Crime& Crime::operator=(const Crime &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Crime& Crime::operator=(Crime &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Crime &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Crime &lhs)
{
  return input;
}

