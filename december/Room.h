//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ROOM_H
#define ROOM_H
#include <iostream>
//TODO Room finish this class
class Room
{
  public:
    //constructor
    Room();
    //copy constructor
    Room(const Room &rhs);
    //move constructor
    Room(Room &&rhs);
    //destructor
    ~Room();
    //copy assignment operator
    Room& operator=(const Room &rhs);
    //move assignment operator
    Room& operator=(Room &&rhs);
    //swap
    void swap(Room &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Room &rhs);
    friend std::istream& operator>>(std::istream &input, Room &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ROOM_H */