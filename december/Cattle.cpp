//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cattle.h>
//constructor
Cattle::Cattle():
  m_placeholder(0)
{}

Cattle::Cattle(const Cattle &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cattle::Cattle(Cattle &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cattle::~Cattle(){}
Cattle& Cattle::operator=(const Cattle &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cattle& Cattle::operator=(Cattle &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cattle &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cattle &lhs)
{
  return input;
}

