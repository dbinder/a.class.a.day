//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SQUIRREL_H
#define SQUIRREL_H
#include <iostream>
//TODO Squirrel finish this class
class Squirrel
{
  public:
    //constructor
    Squirrel();
    //copy constructor
    Squirrel(const Squirrel &rhs);
    //move constructor
    Squirrel(Squirrel &&rhs);
    //destructor
    ~Squirrel();
    //copy assignment operator
    Squirrel& operator=(const Squirrel &rhs);
    //move assignment operator
    Squirrel& operator=(Squirrel &&rhs);
    //swap
    void swap(Squirrel &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Squirrel &rhs);
    friend std::istream& operator>>(std::istream &input, Squirrel &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SQUIRREL_H */