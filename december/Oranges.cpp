//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Oranges.h>
//constructor
Oranges::Oranges():
  m_placeholder(0)
{}

Oranges::Oranges(const Oranges &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Oranges::Oranges(Oranges &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Oranges::~Oranges(){}
Oranges& Oranges::operator=(const Oranges &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Oranges& Oranges::operator=(Oranges &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Oranges &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Oranges &lhs)
{
  return input;
}

