//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Clover.h>
//constructor
Clover::Clover():
  m_placeholder(0)
{}

Clover::Clover(const Clover &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Clover::Clover(Clover &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Clover::~Clover(){}
Clover& Clover::operator=(const Clover &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Clover& Clover::operator=(Clover &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Clover &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Clover &lhs)
{
  return input;
}

