//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MORNING_H
#define MORNING_H
#include <iostream>
//TODO Morning finish this class
class Morning
{
  public:
    //constructor
    Morning();
    //copy constructor
    Morning(const Morning &rhs);
    //move constructor
    Morning(Morning &&rhs);
    //destructor
    ~Morning();
    //copy assignment operator
    Morning& operator=(const Morning &rhs);
    //move assignment operator
    Morning& operator=(Morning &&rhs);
    //swap
    void swap(Morning &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Morning &rhs);
    friend std::istream& operator>>(std::istream &input, Morning &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MORNING_H */