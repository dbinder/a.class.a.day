//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Trip.h>
//constructor
Trip::Trip():
  m_placeholder(0)
{}

Trip::Trip(const Trip &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Trip::Trip(Trip &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Trip::~Trip(){}
Trip& Trip::operator=(const Trip &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Trip& Trip::operator=(Trip &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Trip &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Trip &lhs)
{
  return input;
}

