//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Rod.h>
//constructor
Rod::Rod():
  m_placeholder(0)
{}

Rod::Rod(const Rod &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Rod::Rod(Rod &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Rod::~Rod(){}
Rod& Rod::operator=(const Rod &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Rod& Rod::operator=(Rod &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Rod &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Rod &lhs)
{
  return input;
}

