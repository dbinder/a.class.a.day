//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BIT_H
#define BIT_H
#include <iostream>
//TODO Bit finish this class
class Bit
{
  public:
    //constructor
    Bit();
    //copy constructor
    Bit(const Bit &rhs);
    //move constructor
    Bit(Bit &&rhs);
    //destructor
    ~Bit();
    //copy assignment operator
    Bit& operator=(const Bit &rhs);
    //move assignment operator
    Bit& operator=(Bit &&rhs);
    //swap
    void swap(Bit &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Bit &rhs);
    friend std::istream& operator>>(std::istream &input, Bit &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BIT_H */