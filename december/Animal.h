//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ANIMAL_H
#define ANIMAL_H
#include <iostream>
//TODO Animal finish this class
class Animal
{
  public:
    //constructor
    Animal();
    //copy constructor
    Animal(const Animal &rhs);
    //move constructor
    Animal(Animal &&rhs);
    //destructor
    ~Animal();
    //copy assignment operator
    Animal& operator=(const Animal &rhs);
    //move assignment operator
    Animal& operator=(Animal &&rhs);
    //swap
    void swap(Animal &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Animal &rhs);
    friend std::istream& operator>>(std::istream &input, Animal &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ANIMAL_H */