//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef VEIL_H
#define VEIL_H
#include <iostream>
//TODO Veil finish this class
class Veil
{
  public:
    //constructor
    Veil();
    //copy constructor
    Veil(const Veil &rhs);
    //move constructor
    Veil(Veil &&rhs);
    //destructor
    ~Veil();
    //copy assignment operator
    Veil& operator=(const Veil &rhs);
    //move assignment operator
    Veil& operator=(Veil &&rhs);
    //swap
    void swap(Veil &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Veil &rhs);
    friend std::istream& operator>>(std::istream &input, Veil &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* VEIL_H */