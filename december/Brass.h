//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BRASS_H
#define BRASS_H
#include <iostream>
//TODO Brass finish this class
class Brass
{
  public:
    //constructor
    Brass();
    //copy constructor
    Brass(const Brass &rhs);
    //move constructor
    Brass(Brass &&rhs);
    //destructor
    ~Brass();
    //copy assignment operator
    Brass& operator=(const Brass &rhs);
    //move assignment operator
    Brass& operator=(Brass &&rhs);
    //swap
    void swap(Brass &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Brass &rhs);
    friend std::istream& operator>>(std::istream &input, Brass &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BRASS_H */