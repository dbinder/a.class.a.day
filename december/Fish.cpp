//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Fish.h>
//constructor
Fish::Fish():
  m_placeholder(0)
{}

Fish::Fish(const Fish &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Fish::Fish(Fish &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Fish::~Fish(){}
Fish& Fish::operator=(const Fish &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Fish& Fish::operator=(Fish &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Fish &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Fish &lhs)
{
  return input;
}

