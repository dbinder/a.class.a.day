//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Bridge.h>
//constructor
Bridge::Bridge():
  m_placeholder(0)
{}

Bridge::Bridge(const Bridge &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Bridge::Bridge(Bridge &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Bridge::~Bridge(){}
Bridge& Bridge::operator=(const Bridge &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Bridge& Bridge::operator=(Bridge &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Bridge &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Bridge &lhs)
{
  return input;
}

