//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PAIL_H
#define PAIL_H
#include <iostream>
//TODO Pail finish this class
class Pail
{
  public:
    //constructor
    Pail();
    //copy constructor
    Pail(const Pail &rhs);
    //move constructor
    Pail(Pail &&rhs);
    //destructor
    ~Pail();
    //copy assignment operator
    Pail& operator=(const Pail &rhs);
    //move assignment operator
    Pail& operator=(Pail &&rhs);
    //swap
    void swap(Pail &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pail &rhs);
    friend std::istream& operator>>(std::istream &input, Pail &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PAIL_H */