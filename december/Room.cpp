//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Room.h>
//constructor
Room::Room():
  m_placeholder(0)
{}

Room::Room(const Room &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Room::Room(Room &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Room::~Room(){}
Room& Room::operator=(const Room &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Room& Room::operator=(Room &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Room &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Room &lhs)
{
  return input;
}

