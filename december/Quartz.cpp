//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Quartz.h>
//constructor
Quartz::Quartz():
  m_placeholder(0)
{}

Quartz::Quartz(const Quartz &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Quartz::Quartz(Quartz &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Quartz::~Quartz(){}
Quartz& Quartz::operator=(const Quartz &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Quartz& Quartz::operator=(Quartz &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Quartz &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Quartz &lhs)
{
  return input;
}

