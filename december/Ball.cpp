//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Ball.h>
//constructor
Ball::Ball():
  m_placeholder(0)
{}

Ball::Ball(const Ball &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Ball::Ball(Ball &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Ball::~Ball(){}
Ball& Ball::operator=(const Ball &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Ball& Ball::operator=(Ball &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Ball &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Ball &lhs)
{
  return input;
}

