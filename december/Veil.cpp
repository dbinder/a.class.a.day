//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Veil.h>
//constructor
Veil::Veil():
  m_placeholder(0)
{}

Veil::Veil(const Veil &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Veil::Veil(Veil &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Veil::~Veil(){}
Veil& Veil::operator=(const Veil &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Veil& Veil::operator=(Veil &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Veil &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Veil &lhs)
{
  return input;
}

