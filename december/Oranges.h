//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ORANGES_H
#define ORANGES_H
#include <iostream>
//TODO Oranges finish this class
class Oranges
{
  public:
    //constructor
    Oranges();
    //copy constructor
    Oranges(const Oranges &rhs);
    //move constructor
    Oranges(Oranges &&rhs);
    //destructor
    ~Oranges();
    //copy assignment operator
    Oranges& operator=(const Oranges &rhs);
    //move assignment operator
    Oranges& operator=(Oranges &&rhs);
    //swap
    void swap(Oranges &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Oranges &rhs);
    friend std::istream& operator>>(std::istream &input, Oranges &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ORANGES_H */