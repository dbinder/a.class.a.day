//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ARMY_H
#define ARMY_H
#include <iostream>
//TODO Army finish this class
class Army
{
  public:
    //constructor
    Army();
    //copy constructor
    Army(const Army &rhs);
    //move constructor
    Army(Army &&rhs);
    //destructor
    ~Army();
    //copy assignment operator
    Army& operator=(const Army &rhs);
    //move assignment operator
    Army& operator=(Army &&rhs);
    //swap
    void swap(Army &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Army &rhs);
    friend std::istream& operator>>(std::istream &input, Army &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ARMY_H */