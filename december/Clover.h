//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CLOVER_H
#define CLOVER_H
#include <iostream>
//TODO Clover finish this class
class Clover
{
  public:
    //constructor
    Clover();
    //copy constructor
    Clover(const Clover &rhs);
    //move constructor
    Clover(Clover &&rhs);
    //destructor
    ~Clover();
    //copy assignment operator
    Clover& operator=(const Clover &rhs);
    //move assignment operator
    Clover& operator=(Clover &&rhs);
    //swap
    void swap(Clover &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Clover &rhs);
    friend std::istream& operator>>(std::istream &input, Clover &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CLOVER_H */