root_dir = os.getcwd()
build_dir = path.join(root_dir, 'build')
bin_dir = path.join(build_dir, 'bin')
lib_dir = path.join(build_dir, 'lib')
obj_dir = path.join(build_dir, 'obj')
project_dir = path.join(root_dir, 'projects')

solution 'A Class A Day'
  configurations {'Debug', 'Release'}
  language 'C++'
  location (project_dir)
  targetdir (bin_dir)
  startproject 'Test'
  objdir (obj_dir)
  --vpaths
  --{
    --['Header Files'] = {'**.h'},
    --['Source Files'] = {'**.cpp', '**.c'},
    --['Shaders']      = {'**.vert', '**.frag'},
    --['OpenGL'] = { 'source/demos/GL/*'}
--    
  --}
  defines{}
  flags{ 'Unicode'}
  platforms {'x86', 'x86_64'}
  filter 'platforms:x86'
    architecture 'x86'
  filter 'platforms:x86_64'
    architecture 'x86_64'


group 'Contrib libs'
  dofile('../../github/unittest-cpp/unittest.lua')

group ''
project 'A Class A Day'
  kind 'ConsoleApp'
  location 'Projects'
  language 'C++'
  location (path.join(project_dir, 'demos'))
  files 
  {
    'source/**',
    'include/**',
  }
  includedirs
  {
    'include',
    'january'
  }
  defines{}
  links
  {
    'January'    
  }
  configuration 'Debug'
    flags{'Symbols'}
  configuration 'Release'
    flags{'Optimize'}

group 'Libs'
  project 'January'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'january/**',
    }
    includedirs{ 'january' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'February'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'february/**',
    }
    includedirs{ 'february' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'March'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'march/**',
    }
    includedirs{ 'march' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}

  project 'April'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'april/**',
    }
    includedirs{ 'april' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}

  project 'May'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'may/**',
    }
    includedirs{ 'may' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
        project 'May'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'may/**',
    }
    includedirs{ 'may' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'June'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'june/**',
    }
    includedirs{ 'june' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
        
  project 'July'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'july/**',
    }
    includedirs{ 'july' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'August'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'august/**',
    }
    includedirs{ 'august' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'September'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'september/**',
    }
    includedirs{ 'september' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'October'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'october/**',
    }
    includedirs{ 'october' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
  
  project 'November'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'november/**',
    }
    includedirs{ 'november' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}

  project 'December'
    kind 'StaticLib'
    location 'projects/libs'
    language 'C++'
    files 
    {
      'december/**',
    }
    includedirs{ 'december' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}

group 'Tests'
  project 'Test'
    kind 'ConsoleApp'
    location(path.join(project_dir, 'tests'))
    language 'C++'
    vpaths
    {
      ['Header Files/*'] = '**.h',
      ['Source Files/*'] = {'test/**.c', 'test/**.cpp'}
    }
    targetdir (build_dir)
    files 
    {
      'include/*.h',
      'test/**',
    }
    includedirs
    {
      'include',
      'January',
      'February',
      '../../github/unittest-cpp/Unittest++'
    }
    links
    {
      'January',
      'February',
      'Unittest',
    }
    postbuildcommands { '$(OutDir)$(TargetFileName)' }
    configuration 'Debug'
      flags{'Symbols'}
    configuration 'Release'
      flags{'Optimize'}
