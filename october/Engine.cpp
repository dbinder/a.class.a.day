//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Engine.h>
//constructor
Engine::Engine():
  m_placeholder(0)
{}

Engine::Engine(const Engine &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Engine::Engine(Engine &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Engine::~Engine(){}
Engine& Engine::operator=(const Engine &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Engine& Engine::operator=(Engine &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Engine &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Engine &lhs)
{
  return input;
}

