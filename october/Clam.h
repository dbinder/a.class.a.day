//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CLAM_H
#define CLAM_H
#include <iostream>
//TODO Clam finish this class
class Clam
{
  public:
    //constructor
    Clam();
    //copy constructor
    Clam(const Clam &rhs);
    //move constructor
    Clam(Clam &&rhs);
    //destructor
    ~Clam();
    //copy assignment operator
    Clam& operator=(const Clam &rhs);
    //move assignment operator
    Clam& operator=(Clam &&rhs);
    //swap
    void swap(Clam &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Clam &rhs);
    friend std::istream& operator>>(std::istream &input, Clam &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CLAM_H */