//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wrench.h>
//constructor
Wrench::Wrench():
  m_placeholder(0)
{}

Wrench::Wrench(const Wrench &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wrench::Wrench(Wrench &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wrench::~Wrench(){}
Wrench& Wrench::operator=(const Wrench &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wrench& Wrench::operator=(Wrench &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wrench &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wrench &lhs)
{
  return input;
}

