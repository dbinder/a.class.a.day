//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Rhythm.h>
//constructor
Rhythm::Rhythm():
  m_placeholder(0)
{}

Rhythm::Rhythm(const Rhythm &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Rhythm::Rhythm(Rhythm &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Rhythm::~Rhythm(){}
Rhythm& Rhythm::operator=(const Rhythm &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Rhythm& Rhythm::operator=(Rhythm &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Rhythm &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Rhythm &lhs)
{
  return input;
}

