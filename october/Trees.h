//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TREES_H
#define TREES_H
#include <iostream>
//TODO Trees finish this class
class Trees
{
  public:
    //constructor
    Trees();
    //copy constructor
    Trees(const Trees &rhs);
    //move constructor
    Trees(Trees &&rhs);
    //destructor
    ~Trees();
    //copy assignment operator
    Trees& operator=(const Trees &rhs);
    //move assignment operator
    Trees& operator=(Trees &&rhs);
    //swap
    void swap(Trees &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Trees &rhs);
    friend std::istream& operator>>(std::istream &input, Trees &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TREES_H */