//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef GHOST_H
#define GHOST_H
#include <iostream>
//TODO Ghost finish this class
class Ghost
{
  public:
    //constructor
    Ghost();
    //copy constructor
    Ghost(const Ghost &rhs);
    //move constructor
    Ghost(Ghost &&rhs);
    //destructor
    ~Ghost();
    //copy assignment operator
    Ghost& operator=(const Ghost &rhs);
    //move assignment operator
    Ghost& operator=(Ghost &&rhs);
    //swap
    void swap(Ghost &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Ghost &rhs);
    friend std::istream& operator>>(std::istream &input, Ghost &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* GHOST_H */