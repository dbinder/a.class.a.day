//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Position.h>
//constructor
Position::Position():
  m_placeholder(0)
{}

Position::Position(const Position &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Position::Position(Position &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Position::~Position(){}
Position& Position::operator=(const Position &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Position& Position::operator=(Position &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Position &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Position &lhs)
{
  return input;
}

