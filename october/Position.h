//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef POSITION_H
#define POSITION_H
#include <iostream>
//TODO Position finish this class
class Position
{
  public:
    //constructor
    Position();
    //copy constructor
    Position(const Position &rhs);
    //move constructor
    Position(Position &&rhs);
    //destructor
    ~Position();
    //copy assignment operator
    Position& operator=(const Position &rhs);
    //move assignment operator
    Position& operator=(Position &&rhs);
    //swap
    void swap(Position &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Position &rhs);
    friend std::istream& operator>>(std::istream &input, Position &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* POSITION_H */