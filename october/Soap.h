//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SOAP_H
#define SOAP_H
#include <iostream>
//TODO Soap finish this class
class Soap
{
  public:
    //constructor
    Soap();
    //copy constructor
    Soap(const Soap &rhs);
    //move constructor
    Soap(Soap &&rhs);
    //destructor
    ~Soap();
    //copy assignment operator
    Soap& operator=(const Soap &rhs);
    //move assignment operator
    Soap& operator=(Soap &&rhs);
    //swap
    void swap(Soap &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Soap &rhs);
    friend std::istream& operator>>(std::istream &input, Soap &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SOAP_H */