//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef COAT_H
#define COAT_H
#include <iostream>
//TODO Coat finish this class
class Coat
{
  public:
    //constructor
    Coat();
    //copy constructor
    Coat(const Coat &rhs);
    //move constructor
    Coat(Coat &&rhs);
    //destructor
    ~Coat();
    //copy assignment operator
    Coat& operator=(const Coat &rhs);
    //move assignment operator
    Coat& operator=(Coat &&rhs);
    //swap
    void swap(Coat &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Coat &rhs);
    friend std::istream& operator>>(std::istream &input, Coat &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* COAT_H */