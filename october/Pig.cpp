//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pig.h>
//constructor
Pig::Pig():
  m_placeholder(0)
{}

Pig::Pig(const Pig &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pig::Pig(Pig &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pig::~Pig(){}
Pig& Pig::operator=(const Pig &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pig& Pig::operator=(Pig &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pig &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pig &lhs)
{
  return input;
}

