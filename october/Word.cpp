//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Word.h>
//constructor
Word::Word():
  m_placeholder(0)
{}

Word::Word(const Word &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Word::Word(Word &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Word::~Word(){}
Word& Word::operator=(const Word &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Word& Word::operator=(Word &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Word &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Word &lhs)
{
  return input;
}

