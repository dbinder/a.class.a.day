//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SCARECROW_H
#define SCARECROW_H
#include <iostream>
//TODO Scarecrow finish this class
class Scarecrow
{
  public:
    //constructor
    Scarecrow();
    //copy constructor
    Scarecrow(const Scarecrow &rhs);
    //move constructor
    Scarecrow(Scarecrow &&rhs);
    //destructor
    ~Scarecrow();
    //copy assignment operator
    Scarecrow& operator=(const Scarecrow &rhs);
    //move assignment operator
    Scarecrow& operator=(Scarecrow &&rhs);
    //swap
    void swap(Scarecrow &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Scarecrow &rhs);
    friend std::istream& operator>>(std::istream &input, Scarecrow &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SCARECROW_H */