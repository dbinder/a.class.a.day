//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RABBIT_H
#define RABBIT_H
#include <iostream>
//TODO Rabbit finish this class
class Rabbit
{
  public:
    //constructor
    Rabbit();
    //copy constructor
    Rabbit(const Rabbit &rhs);
    //move constructor
    Rabbit(Rabbit &&rhs);
    //destructor
    ~Rabbit();
    //copy assignment operator
    Rabbit& operator=(const Rabbit &rhs);
    //move assignment operator
    Rabbit& operator=(Rabbit &&rhs);
    //swap
    void swap(Rabbit &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Rabbit &rhs);
    friend std::istream& operator>>(std::istream &input, Rabbit &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RABBIT_H */