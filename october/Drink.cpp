//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Drink.h>
//constructor
Drink::Drink():
  m_placeholder(0)
{}

Drink::Drink(const Drink &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Drink::Drink(Drink &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Drink::~Drink(){}
Drink& Drink::operator=(const Drink &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Drink& Drink::operator=(Drink &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Drink &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Drink &lhs)
{
  return input;
}

