//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Flavor.h>
//constructor
Flavor::Flavor():
  m_placeholder(0)
{}

Flavor::Flavor(const Flavor &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Flavor::Flavor(Flavor &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Flavor::~Flavor(){}
Flavor& Flavor::operator=(const Flavor &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Flavor& Flavor::operator=(Flavor &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Flavor &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Flavor &lhs)
{
  return input;
}

