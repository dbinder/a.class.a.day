//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Tub.h>
//constructor
Tub::Tub():
  m_placeholder(0)
{}

Tub::Tub(const Tub &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Tub::Tub(Tub &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Tub::~Tub(){}
Tub& Tub::operator=(const Tub &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Tub& Tub::operator=(Tub &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Tub &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Tub &lhs)
{
  return input;
}

