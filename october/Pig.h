//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PIG_H
#define PIG_H
#include <iostream>
//TODO Pig finish this class
class Pig
{
  public:
    //constructor
    Pig();
    //copy constructor
    Pig(const Pig &rhs);
    //move constructor
    Pig(Pig &&rhs);
    //destructor
    ~Pig();
    //copy assignment operator
    Pig& operator=(const Pig &rhs);
    //move assignment operator
    Pig& operator=(Pig &&rhs);
    //swap
    void swap(Pig &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pig &rhs);
    friend std::istream& operator>>(std::istream &input, Pig &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PIG_H */