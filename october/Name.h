//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef NAME_H
#define NAME_H
#include <iostream>
//TODO Name finish this class
class Name
{
  public:
    //constructor
    Name();
    //copy constructor
    Name(const Name &rhs);
    //move constructor
    Name(Name &&rhs);
    //destructor
    ~Name();
    //copy assignment operator
    Name& operator=(const Name &rhs);
    //move assignment operator
    Name& operator=(Name &&rhs);
    //swap
    void swap(Name &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Name &rhs);
    friend std::istream& operator>>(std::istream &input, Name &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* NAME_H */