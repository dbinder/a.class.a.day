//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Ghost.h>
//constructor
Ghost::Ghost():
  m_placeholder(0)
{}

Ghost::Ghost(const Ghost &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Ghost::Ghost(Ghost &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Ghost::~Ghost(){}
Ghost& Ghost::operator=(const Ghost &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Ghost& Ghost::operator=(Ghost &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Ghost &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Ghost &lhs)
{
  return input;
}

