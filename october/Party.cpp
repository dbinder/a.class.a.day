//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Party.h>
//constructor
Party::Party():
  m_placeholder(0)
{}

Party::Party(const Party &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Party::Party(Party &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Party::~Party(){}
Party& Party::operator=(const Party &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Party& Party::operator=(Party &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Party &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Party &lhs)
{
  return input;
}

