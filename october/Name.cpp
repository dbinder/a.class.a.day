//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Name.h>
//constructor
Name::Name():
  m_placeholder(0)
{}

Name::Name(const Name &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Name::Name(Name &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Name::~Name(){}
Name& Name::operator=(const Name &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Name& Name::operator=(Name &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Name &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Name &lhs)
{
  return input;
}

