//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef FLAVOR_H
#define FLAVOR_H
#include <iostream>
//TODO Flavor finish this class
class Flavor
{
  public:
    //constructor
    Flavor();
    //copy constructor
    Flavor(const Flavor &rhs);
    //move constructor
    Flavor(Flavor &&rhs);
    //destructor
    ~Flavor();
    //copy assignment operator
    Flavor& operator=(const Flavor &rhs);
    //move assignment operator
    Flavor& operator=(Flavor &&rhs);
    //swap
    void swap(Flavor &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Flavor &rhs);
    friend std::istream& operator>>(std::istream &input, Flavor &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* FLAVOR_H */