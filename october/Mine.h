//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MINE_H
#define MINE_H
#include <iostream>
//TODO Mine finish this class
class Mine
{
  public:
    //constructor
    Mine();
    //copy constructor
    Mine(const Mine &rhs);
    //move constructor
    Mine(Mine &&rhs);
    //destructor
    ~Mine();
    //copy assignment operator
    Mine& operator=(const Mine &rhs);
    //move assignment operator
    Mine& operator=(Mine &&rhs);
    //swap
    void swap(Mine &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Mine &rhs);
    friend std::istream& operator>>(std::istream &input, Mine &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MINE_H */