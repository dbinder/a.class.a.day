//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RHYTHM_H
#define RHYTHM_H
#include <iostream>
//TODO Rhythm finish this class
class Rhythm
{
  public:
    //constructor
    Rhythm();
    //copy constructor
    Rhythm(const Rhythm &rhs);
    //move constructor
    Rhythm(Rhythm &&rhs);
    //destructor
    ~Rhythm();
    //copy assignment operator
    Rhythm& operator=(const Rhythm &rhs);
    //move assignment operator
    Rhythm& operator=(Rhythm &&rhs);
    //swap
    void swap(Rhythm &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Rhythm &rhs);
    friend std::istream& operator>>(std::istream &input, Rhythm &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RHYTHM_H */