//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PARTY_H
#define PARTY_H
#include <iostream>
//TODO Party finish this class
class Party
{
  public:
    //constructor
    Party();
    //copy constructor
    Party(const Party &rhs);
    //move constructor
    Party(Party &&rhs);
    //destructor
    ~Party();
    //copy assignment operator
    Party& operator=(const Party &rhs);
    //move assignment operator
    Party& operator=(Party &&rhs);
    //swap
    void swap(Party &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Party &rhs);
    friend std::istream& operator>>(std::istream &input, Party &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PARTY_H */