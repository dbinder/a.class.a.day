//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SCISSORS_H
#define SCISSORS_H
#include <iostream>
//TODO Scissors finish this class
class Scissors
{
  public:
    //constructor
    Scissors();
    //copy constructor
    Scissors(const Scissors &rhs);
    //move constructor
    Scissors(Scissors &&rhs);
    //destructor
    ~Scissors();
    //copy assignment operator
    Scissors& operator=(const Scissors &rhs);
    //move assignment operator
    Scissors& operator=(Scissors &&rhs);
    //swap
    void swap(Scissors &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Scissors &rhs);
    friend std::istream& operator>>(std::istream &input, Scissors &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SCISSORS_H */