//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Mine.h>
//constructor
Mine::Mine():
  m_placeholder(0)
{}

Mine::Mine(const Mine &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Mine::Mine(Mine &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Mine::~Mine(){}
Mine& Mine::operator=(const Mine &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Mine& Mine::operator=(Mine &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Mine &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Mine &lhs)
{
  return input;
}

