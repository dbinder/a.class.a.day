//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WORD_H
#define WORD_H
#include <iostream>
//TODO Word finish this class
class Word
{
  public:
    //constructor
    Word();
    //copy constructor
    Word(const Word &rhs);
    //move constructor
    Word(Word &&rhs);
    //destructor
    ~Word();
    //copy assignment operator
    Word& operator=(const Word &rhs);
    //move assignment operator
    Word& operator=(Word &&rhs);
    //swap
    void swap(Word &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Word &rhs);
    friend std::istream& operator>>(std::istream &input, Word &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WORD_H */