//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CHURCH_H
#define CHURCH_H
#include <iostream>
//TODO Church finish this class
class Church
{
  public:
    //constructor
    Church();
    //copy constructor
    Church(const Church &rhs);
    //move constructor
    Church(Church &&rhs);
    //destructor
    ~Church();
    //copy assignment operator
    Church& operator=(const Church &rhs);
    //move assignment operator
    Church& operator=(Church &&rhs);
    //swap
    void swap(Church &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Church &rhs);
    friend std::istream& operator>>(std::istream &input, Church &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CHURCH_H */