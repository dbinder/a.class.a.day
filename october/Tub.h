//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TUB_H
#define TUB_H
#include <iostream>
//TODO Tub finish this class
class Tub
{
  public:
    //constructor
    Tub();
    //copy constructor
    Tub(const Tub &rhs);
    //move constructor
    Tub(Tub &&rhs);
    //destructor
    ~Tub();
    //copy assignment operator
    Tub& operator=(const Tub &rhs);
    //move assignment operator
    Tub& operator=(Tub &&rhs);
    //swap
    void swap(Tub &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Tub &rhs);
    friend std::istream& operator>>(std::istream &input, Tub &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TUB_H */