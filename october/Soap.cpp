//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Soap.h>
//constructor
Soap::Soap():
  m_placeholder(0)
{}

Soap::Soap(const Soap &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Soap::Soap(Soap &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Soap::~Soap(){}
Soap& Soap::operator=(const Soap &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Soap& Soap::operator=(Soap &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Soap &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Soap &lhs)
{
  return input;
}

