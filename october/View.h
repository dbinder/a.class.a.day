//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef VIEW_H
#define VIEW_H
#include <iostream>
//TODO View finish this class
class View
{
  public:
    //constructor
    View();
    //copy constructor
    View(const View &rhs);
    //move constructor
    View(View &&rhs);
    //destructor
    ~View();
    //copy assignment operator
    View& operator=(const View &rhs);
    //move assignment operator
    View& operator=(View &&rhs);
    //swap
    void swap(View &rhs);
    friend std::ostream& operator<<(std::ostream &output, const View &rhs);
    friend std::istream& operator>>(std::istream &input, View &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* VIEW_H */