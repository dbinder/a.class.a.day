//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DEVELOPMENT_H
#define DEVELOPMENT_H
#include <iostream>
//TODO Development finish this class
class Development
{
  public:
    //constructor
    Development();
    //copy constructor
    Development(const Development &rhs);
    //move constructor
    Development(Development &&rhs);
    //destructor
    ~Development();
    //copy assignment operator
    Development& operator=(const Development &rhs);
    //move assignment operator
    Development& operator=(Development &&rhs);
    //swap
    void swap(Development &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Development &rhs);
    friend std::istream& operator>>(std::istream &input, Development &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DEVELOPMENT_H */