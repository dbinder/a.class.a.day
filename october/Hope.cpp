//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Hope.h>
//constructor
Hope::Hope():
  m_placeholder(0)
{}

Hope::Hope(const Hope &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Hope::Hope(Hope &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Hope::~Hope(){}
Hope& Hope::operator=(const Hope &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Hope& Hope::operator=(Hope &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Hope &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Hope &lhs)
{
  return input;
}

