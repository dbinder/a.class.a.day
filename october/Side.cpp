//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Side.h>
//constructor
Side::Side():
  m_placeholder(0)
{}

Side::Side(const Side &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Side::Side(Side &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Side::~Side(){}
Side& Side::operator=(const Side &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Side& Side::operator=(Side &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Side &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Side &lhs)
{
  return input;
}

