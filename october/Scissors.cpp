//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Scissors.h>
//constructor
Scissors::Scissors():
  m_placeholder(0)
{}

Scissors::Scissors(const Scissors &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Scissors::Scissors(Scissors &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Scissors::~Scissors(){}
Scissors& Scissors::operator=(const Scissors &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Scissors& Scissors::operator=(Scissors &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Scissors &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Scissors &lhs)
{
  return input;
}

