//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Church.h>
//constructor
Church::Church():
  m_placeholder(0)
{}

Church::Church(const Church &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Church::Church(Church &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Church::~Church(){}
Church& Church::operator=(const Church &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Church& Church::operator=(Church &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Church &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Church &lhs)
{
  return input;
}

