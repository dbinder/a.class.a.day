//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SIDE_H
#define SIDE_H
#include <iostream>
//TODO Side finish this class
class Side
{
  public:
    //constructor
    Side();
    //copy constructor
    Side(const Side &rhs);
    //move constructor
    Side(Side &&rhs);
    //destructor
    ~Side();
    //copy assignment operator
    Side& operator=(const Side &rhs);
    //move assignment operator
    Side& operator=(Side &&rhs);
    //swap
    void swap(Side &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Side &rhs);
    friend std::istream& operator>>(std::istream &input, Side &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SIDE_H */