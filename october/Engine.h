//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ENGINE_H
#define ENGINE_H
#include <iostream>
//TODO Engine finish this class
class Engine
{
  public:
    //constructor
    Engine();
    //copy constructor
    Engine(const Engine &rhs);
    //move constructor
    Engine(Engine &&rhs);
    //destructor
    ~Engine();
    //copy assignment operator
    Engine& operator=(const Engine &rhs);
    //move assignment operator
    Engine& operator=(Engine &&rhs);
    //swap
    void swap(Engine &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Engine &rhs);
    friend std::istream& operator>>(std::istream &input, Engine &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ENGINE_H */