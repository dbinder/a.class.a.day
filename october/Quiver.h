//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef QUIVER_H
#define QUIVER_H
#include <iostream>
//TODO Quiver finish this class
class Quiver
{
  public:
    //constructor
    Quiver();
    //copy constructor
    Quiver(const Quiver &rhs);
    //move constructor
    Quiver(Quiver &&rhs);
    //destructor
    ~Quiver();
    //copy assignment operator
    Quiver& operator=(const Quiver &rhs);
    //move assignment operator
    Quiver& operator=(Quiver &&rhs);
    //swap
    void swap(Quiver &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Quiver &rhs);
    friend std::istream& operator>>(std::istream &input, Quiver &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* QUIVER_H */