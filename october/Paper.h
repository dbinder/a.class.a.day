//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PAPER_H
#define PAPER_H
#include <iostream>
//TODO Paper finish this class
class Paper
{
  public:
    //constructor
    Paper();
    //copy constructor
    Paper(const Paper &rhs);
    //move constructor
    Paper(Paper &&rhs);
    //destructor
    ~Paper();
    //copy assignment operator
    Paper& operator=(const Paper &rhs);
    //move assignment operator
    Paper& operator=(Paper &&rhs);
    //swap
    void swap(Paper &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Paper &rhs);
    friend std::istream& operator>>(std::istream &input, Paper &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PAPER_H */