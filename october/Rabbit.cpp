//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Rabbit.h>
//constructor
Rabbit::Rabbit():
  m_placeholder(0)
{}

Rabbit::Rabbit(const Rabbit &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Rabbit::Rabbit(Rabbit &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Rabbit::~Rabbit(){}
Rabbit& Rabbit::operator=(const Rabbit &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Rabbit& Rabbit::operator=(Rabbit &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Rabbit &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Rabbit &lhs)
{
  return input;
}

