//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DRINK_H
#define DRINK_H
#include <iostream>
//TODO Drink finish this class
class Drink
{
  public:
    //constructor
    Drink();
    //copy constructor
    Drink(const Drink &rhs);
    //move constructor
    Drink(Drink &&rhs);
    //destructor
    ~Drink();
    //copy assignment operator
    Drink& operator=(const Drink &rhs);
    //move assignment operator
    Drink& operator=(Drink &&rhs);
    //swap
    void swap(Drink &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Drink &rhs);
    friend std::istream& operator>>(std::istream &input, Drink &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DRINK_H */