//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Coat.h>
//constructor
Coat::Coat():
  m_placeholder(0)
{}

Coat::Coat(const Coat &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Coat::Coat(Coat &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Coat::~Coat(){}
Coat& Coat::operator=(const Coat &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Coat& Coat::operator=(Coat &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Coat &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Coat &lhs)
{
  return input;
}

