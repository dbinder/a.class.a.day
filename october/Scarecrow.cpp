//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Scarecrow.h>
//constructor
Scarecrow::Scarecrow():
  m_placeholder(0)
{}

Scarecrow::Scarecrow(const Scarecrow &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Scarecrow::Scarecrow(Scarecrow &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Scarecrow::~Scarecrow(){}
Scarecrow& Scarecrow::operator=(const Scarecrow &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Scarecrow& Scarecrow::operator=(Scarecrow &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Scarecrow &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Scarecrow &lhs)
{
  return input;
}

