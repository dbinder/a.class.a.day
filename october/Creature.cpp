//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Creature.h>
//constructor
Creature::Creature():
  m_placeholder(0)
{}

Creature::Creature(const Creature &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Creature::Creature(Creature &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Creature::~Creature(){}
Creature& Creature::operator=(const Creature &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Creature& Creature::operator=(Creature &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Creature &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Creature &lhs)
{
  return input;
}

