//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Development.h>
//constructor
Development::Development():
  m_placeholder(0)
{}

Development::Development(const Development &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Development::Development(Development &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Development::~Development(){}
Development& Development::operator=(const Development &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Development& Development::operator=(Development &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Development &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Development &lhs)
{
  return input;
}

