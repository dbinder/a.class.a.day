//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Clam.h>
//constructor
Clam::Clam():
  m_placeholder(0)
{}

Clam::Clam(const Clam &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Clam::Clam(Clam &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Clam::~Clam(){}
Clam& Clam::operator=(const Clam &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Clam& Clam::operator=(Clam &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Clam &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Clam &lhs)
{
  return input;
}

