//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Quiver.h>
//constructor
Quiver::Quiver():
  m_placeholder(0)
{}

Quiver::Quiver(const Quiver &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Quiver::Quiver(Quiver &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Quiver::~Quiver(){}
Quiver& Quiver::operator=(const Quiver &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Quiver& Quiver::operator=(Quiver &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Quiver &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Quiver &lhs)
{
  return input;
}

