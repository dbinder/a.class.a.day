//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RIVER_H
#define RIVER_H
#include <iostream>
//TODO River finish this class
class River
{
  public:
    //constructor
    River();
    //copy constructor
    River(const River &rhs);
    //move constructor
    River(River &&rhs);
    //destructor
    ~River();
    //copy assignment operator
    River& operator=(const River &rhs);
    //move assignment operator
    River& operator=(River &&rhs);
    //swap
    void swap(River &rhs);
    friend std::ostream& operator<<(std::ostream &output, const River &rhs);
    friend std::istream& operator>>(std::istream &input, River &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RIVER_H */