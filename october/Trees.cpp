//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Trees.h>
//constructor
Trees::Trees():
  m_placeholder(0)
{}

Trees::Trees(const Trees &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Trees::Trees(Trees &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Trees::~Trees(){}
Trees& Trees::operator=(const Trees &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Trees& Trees::operator=(Trees &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Trees &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Trees &lhs)
{
  return input;
}

