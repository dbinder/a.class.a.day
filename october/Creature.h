//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CREATURE_H
#define CREATURE_H
#include <iostream>
//TODO Creature finish this class
class Creature
{
  public:
    //constructor
    Creature();
    //copy constructor
    Creature(const Creature &rhs);
    //move constructor
    Creature(Creature &&rhs);
    //destructor
    ~Creature();
    //copy assignment operator
    Creature& operator=(const Creature &rhs);
    //move assignment operator
    Creature& operator=(Creature &&rhs);
    //swap
    void swap(Creature &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Creature &rhs);
    friend std::istream& operator>>(std::istream &input, Creature &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CREATURE_H */