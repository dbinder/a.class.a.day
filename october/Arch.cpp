//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Arch.h>
//constructor
Arch::Arch():
  m_placeholder(0)
{}

Arch::Arch(const Arch &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Arch::Arch(Arch &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Arch::~Arch(){}
Arch& Arch::operator=(const Arch &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Arch& Arch::operator=(Arch &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Arch &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Arch &lhs)
{
  return input;
}

