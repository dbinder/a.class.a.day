//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ARCH_H
#define ARCH_H
#include <iostream>
//TODO Arch finish this class
class Arch
{
  public:
    //constructor
    Arch();
    //copy constructor
    Arch(const Arch &rhs);
    //move constructor
    Arch(Arch &&rhs);
    //destructor
    ~Arch();
    //copy assignment operator
    Arch& operator=(const Arch &rhs);
    //move assignment operator
    Arch& operator=(Arch &&rhs);
    //swap
    void swap(Arch &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Arch &rhs);
    friend std::istream& operator>>(std::istream &input, Arch &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ARCH_H */