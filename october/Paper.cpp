//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Paper.h>
//constructor
Paper::Paper():
  m_placeholder(0)
{}

Paper::Paper(const Paper &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Paper::Paper(Paper &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Paper::~Paper(){}
Paper& Paper::operator=(const Paper &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Paper& Paper::operator=(Paper &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Paper &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Paper &lhs)
{
  return input;
}

