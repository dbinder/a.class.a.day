//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WRENCH_H
#define WRENCH_H
#include <iostream>
//TODO Wrench finish this class
class Wrench
{
  public:
    //constructor
    Wrench();
    //copy constructor
    Wrench(const Wrench &rhs);
    //move constructor
    Wrench(Wrench &&rhs);
    //destructor
    ~Wrench();
    //copy assignment operator
    Wrench& operator=(const Wrench &rhs);
    //move assignment operator
    Wrench& operator=(Wrench &&rhs);
    //swap
    void swap(Wrench &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wrench &rhs);
    friend std::istream& operator>>(std::istream &input, Wrench &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WRENCH_H */