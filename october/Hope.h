//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HOPE_H
#define HOPE_H
#include <iostream>
//TODO Hope finish this class
class Hope
{
  public:
    //constructor
    Hope();
    //copy constructor
    Hope(const Hope &rhs);
    //move constructor
    Hope(Hope &&rhs);
    //destructor
    ~Hope();
    //copy assignment operator
    Hope& operator=(const Hope &rhs);
    //move assignment operator
    Hope& operator=(Hope &&rhs);
    //swap
    void swap(Hope &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Hope &rhs);
    friend std::istream& operator>>(std::istream &input, Hope &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HOPE_H */