//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pigs.h>
//constructor
Pigs::Pigs():
  m_placeholder(0)
{}

Pigs::Pigs(const Pigs &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pigs::Pigs(Pigs &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pigs::~Pigs(){}
Pigs& Pigs::operator=(const Pigs &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pigs& Pigs::operator=(Pigs &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pigs &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pigs &lhs)
{
  return input;
}

