//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Skate.h>
//constructor
Skate::Skate():
  m_placeholder(0)
{}

Skate::Skate(const Skate &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Skate::Skate(Skate &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Skate::~Skate(){}
Skate& Skate::operator=(const Skate &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Skate& Skate::operator=(Skate &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Skate &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Skate &lhs)
{
  return input;
}

