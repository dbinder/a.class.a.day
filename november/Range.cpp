//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Range.h>
//constructor
Range::Range():
  m_placeholder(0)
{}

Range::Range(const Range &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Range::Range(Range &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Range::~Range(){}
Range& Range::operator=(const Range &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Range& Range::operator=(Range &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Range &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Range &lhs)
{
  return input;
}

