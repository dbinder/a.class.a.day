//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Partner.h>
//constructor
Partner::Partner():
  m_placeholder(0)
{}

Partner::Partner(const Partner &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Partner::Partner(Partner &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Partner::~Partner(){}
Partner& Partner::operator=(const Partner &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Partner& Partner::operator=(Partner &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Partner &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Partner &lhs)
{
  return input;
}

