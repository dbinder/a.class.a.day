//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PAYMENT_H
#define PAYMENT_H
#include <iostream>
//TODO Payment finish this class
class Payment
{
  public:
    //constructor
    Payment();
    //copy constructor
    Payment(const Payment &rhs);
    //move constructor
    Payment(Payment &&rhs);
    //destructor
    ~Payment();
    //copy assignment operator
    Payment& operator=(const Payment &rhs);
    //move assignment operator
    Payment& operator=(Payment &&rhs);
    //swap
    void swap(Payment &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Payment &rhs);
    friend std::istream& operator>>(std::istream &input, Payment &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PAYMENT_H */