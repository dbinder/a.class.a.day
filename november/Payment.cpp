//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Payment.h>
//constructor
Payment::Payment():
  m_placeholder(0)
{}

Payment::Payment(const Payment &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Payment::Payment(Payment &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Payment::~Payment(){}
Payment& Payment::operator=(const Payment &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Payment& Payment::operator=(Payment &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Payment &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Payment &lhs)
{
  return input;
}

