//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Quill.h>
//constructor
Quill::Quill():
  m_placeholder(0)
{}

Quill::Quill(const Quill &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Quill::Quill(Quill &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Quill::~Quill(){}
Quill& Quill::operator=(const Quill &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Quill& Quill::operator=(Quill &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Quill &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Quill &lhs)
{
  return input;
}

