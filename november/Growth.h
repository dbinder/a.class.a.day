//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef GROWTH_H
#define GROWTH_H
#include <iostream>
//TODO Growth finish this class
class Growth
{
  public:
    //constructor
    Growth();
    //copy constructor
    Growth(const Growth &rhs);
    //move constructor
    Growth(Growth &&rhs);
    //destructor
    ~Growth();
    //copy assignment operator
    Growth& operator=(const Growth &rhs);
    //move assignment operator
    Growth& operator=(Growth &&rhs);
    //swap
    void swap(Growth &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Growth &rhs);
    friend std::istream& operator>>(std::istream &input, Growth &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* GROWTH_H */