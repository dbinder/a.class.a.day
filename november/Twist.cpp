//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Twist.h>
//constructor
Twist::Twist():
  m_placeholder(0)
{}

Twist::Twist(const Twist &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Twist::Twist(Twist &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Twist::~Twist(){}
Twist& Twist::operator=(const Twist &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Twist& Twist::operator=(Twist &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Twist &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Twist &lhs)
{
  return input;
}

