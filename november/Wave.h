//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WAVE_H
#define WAVE_H
#include <iostream>
//TODO Wave finish this class
class Wave
{
  public:
    //constructor
    Wave();
    //copy constructor
    Wave(const Wave &rhs);
    //move constructor
    Wave(Wave &&rhs);
    //destructor
    ~Wave();
    //copy assignment operator
    Wave& operator=(const Wave &rhs);
    //move assignment operator
    Wave& operator=(Wave &&rhs);
    //swap
    void swap(Wave &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wave &rhs);
    friend std::istream& operator>>(std::istream &input, Wave &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WAVE_H */