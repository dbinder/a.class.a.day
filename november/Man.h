//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MAN_H
#define MAN_H
#include <iostream>
//TODO Man finish this class
class Man
{
  public:
    //constructor
    Man();
    //copy constructor
    Man(const Man &rhs);
    //move constructor
    Man(Man &&rhs);
    //destructor
    ~Man();
    //copy assignment operator
    Man& operator=(const Man &rhs);
    //move assignment operator
    Man& operator=(Man &&rhs);
    //swap
    void swap(Man &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Man &rhs);
    friend std::istream& operator>>(std::istream &input, Man &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MAN_H */