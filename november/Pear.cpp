//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pear.h>
//constructor
Pear::Pear():
  m_placeholder(0)
{}

Pear::Pear(const Pear &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pear::Pear(Pear &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pear::~Pear(){}
Pear& Pear::operator=(const Pear &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pear& Pear::operator=(Pear &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pear &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pear &lhs)
{
  return input;
}

