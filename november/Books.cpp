//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Books.h>
//constructor
Books::Books():
  m_placeholder(0)
{}

Books::Books(const Books &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Books::Books(Books &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Books::~Books(){}
Books& Books::operator=(const Books &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Books& Books::operator=(Books &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Books &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Books &lhs)
{
  return input;
}

