//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WRIST_H
#define WRIST_H
#include <iostream>
//TODO Wrist finish this class
class Wrist
{
  public:
    //constructor
    Wrist();
    //copy constructor
    Wrist(const Wrist &rhs);
    //move constructor
    Wrist(Wrist &&rhs);
    //destructor
    ~Wrist();
    //copy assignment operator
    Wrist& operator=(const Wrist &rhs);
    //move assignment operator
    Wrist& operator=(Wrist &&rhs);
    //swap
    void swap(Wrist &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wrist &rhs);
    friend std::istream& operator>>(std::istream &input, Wrist &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WRIST_H */