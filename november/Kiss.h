//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef KISS_H
#define KISS_H
#include <iostream>
//TODO Kiss finish this class
class Kiss
{
  public:
    //constructor
    Kiss();
    //copy constructor
    Kiss(const Kiss &rhs);
    //move constructor
    Kiss(Kiss &&rhs);
    //destructor
    ~Kiss();
    //copy assignment operator
    Kiss& operator=(const Kiss &rhs);
    //move assignment operator
    Kiss& operator=(Kiss &&rhs);
    //swap
    void swap(Kiss &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Kiss &rhs);
    friend std::istream& operator>>(std::istream &input, Kiss &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* KISS_H */