//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RAKE_H
#define RAKE_H
#include <iostream>
//TODO Rake finish this class
class Rake
{
  public:
    //constructor
    Rake();
    //copy constructor
    Rake(const Rake &rhs);
    //move constructor
    Rake(Rake &&rhs);
    //destructor
    ~Rake();
    //copy assignment operator
    Rake& operator=(const Rake &rhs);
    //move assignment operator
    Rake& operator=(Rake &&rhs);
    //swap
    void swap(Rake &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Rake &rhs);
    friend std::istream& operator>>(std::istream &input, Rake &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RAKE_H */