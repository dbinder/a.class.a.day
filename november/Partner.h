//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PARTNER_H
#define PARTNER_H
#include <iostream>
//TODO Partner finish this class
class Partner
{
  public:
    //constructor
    Partner();
    //copy constructor
    Partner(const Partner &rhs);
    //move constructor
    Partner(Partner &&rhs);
    //destructor
    ~Partner();
    //copy assignment operator
    Partner& operator=(const Partner &rhs);
    //move assignment operator
    Partner& operator=(Partner &&rhs);
    //swap
    void swap(Partner &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Partner &rhs);
    friend std::istream& operator>>(std::istream &input, Partner &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PARTNER_H */