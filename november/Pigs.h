//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PIGS_H
#define PIGS_H
#include <iostream>
//TODO Pigs finish this class
class Pigs
{
  public:
    //constructor
    Pigs();
    //copy constructor
    Pigs(const Pigs &rhs);
    //move constructor
    Pigs(Pigs &&rhs);
    //destructor
    ~Pigs();
    //copy assignment operator
    Pigs& operator=(const Pigs &rhs);
    //move assignment operator
    Pigs& operator=(Pigs &&rhs);
    //swap
    void swap(Pigs &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pigs &rhs);
    friend std::istream& operator>>(std::istream &input, Pigs &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PIGS_H */