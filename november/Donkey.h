//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DONKEY_H
#define DONKEY_H
#include <iostream>
//TODO Donkey finish this class
class Donkey
{
  public:
    //constructor
    Donkey();
    //copy constructor
    Donkey(const Donkey &rhs);
    //move constructor
    Donkey(Donkey &&rhs);
    //destructor
    ~Donkey();
    //copy assignment operator
    Donkey& operator=(const Donkey &rhs);
    //move assignment operator
    Donkey& operator=(Donkey &&rhs);
    //swap
    void swap(Donkey &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Donkey &rhs);
    friend std::istream& operator>>(std::istream &input, Donkey &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DONKEY_H */