//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pickle.h>
//constructor
Pickle::Pickle():
  m_placeholder(0)
{}

Pickle::Pickle(const Pickle &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pickle::Pickle(Pickle &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pickle::~Pickle(){}
Pickle& Pickle::operator=(const Pickle &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pickle& Pickle::operator=(Pickle &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pickle &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pickle &lhs)
{
  return input;
}

