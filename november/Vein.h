//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef VEIN_H
#define VEIN_H
#include <iostream>
//TODO Vein finish this class
class Vein
{
  public:
    //constructor
    Vein();
    //copy constructor
    Vein(const Vein &rhs);
    //move constructor
    Vein(Vein &&rhs);
    //destructor
    ~Vein();
    //copy assignment operator
    Vein& operator=(const Vein &rhs);
    //move assignment operator
    Vein& operator=(Vein &&rhs);
    //swap
    void swap(Vein &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Vein &rhs);
    friend std::istream& operator>>(std::istream &input, Vein &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* VEIN_H */