//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Eggnog.h>
//constructor
Eggnog::Eggnog():
  m_placeholder(0)
{}

Eggnog::Eggnog(const Eggnog &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Eggnog::Eggnog(Eggnog &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Eggnog::~Eggnog(){}
Eggnog& Eggnog::operator=(const Eggnog &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Eggnog& Eggnog::operator=(Eggnog &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Eggnog &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Eggnog &lhs)
{
  return input;
}

