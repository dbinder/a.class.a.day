//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Growth.h>
//constructor
Growth::Growth():
  m_placeholder(0)
{}

Growth::Growth(const Growth &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Growth::Growth(Growth &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Growth::~Growth(){}
Growth& Growth::operator=(const Growth &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Growth& Growth::operator=(Growth &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Growth &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Growth &lhs)
{
  return input;
}

