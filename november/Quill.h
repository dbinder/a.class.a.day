//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef QUILL_H
#define QUILL_H
#include <iostream>
//TODO Quill finish this class
class Quill
{
  public:
    //constructor
    Quill();
    //copy constructor
    Quill(const Quill &rhs);
    //move constructor
    Quill(Quill &&rhs);
    //destructor
    ~Quill();
    //copy assignment operator
    Quill& operator=(const Quill &rhs);
    //move assignment operator
    Quill& operator=(Quill &&rhs);
    //swap
    void swap(Quill &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Quill &rhs);
    friend std::istream& operator>>(std::istream &input, Quill &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* QUILL_H */