//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Join.h>
//constructor
Join::Join():
  m_placeholder(0)
{}

Join::Join(const Join &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Join::Join(Join &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Join::~Join(){}
Join& Join::operator=(const Join &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Join& Join::operator=(Join &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Join &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Join &lhs)
{
  return input;
}

