//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PEAR_H
#define PEAR_H
#include <iostream>
//TODO Pear finish this class
class Pear
{
  public:
    //constructor
    Pear();
    //copy constructor
    Pear(const Pear &rhs);
    //move constructor
    Pear(Pear &&rhs);
    //destructor
    ~Pear();
    //copy assignment operator
    Pear& operator=(const Pear &rhs);
    //move assignment operator
    Pear& operator=(Pear &&rhs);
    //swap
    void swap(Pear &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pear &rhs);
    friend std::istream& operator>>(std::istream &input, Pear &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PEAR_H */