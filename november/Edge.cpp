//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Edge.h>
//constructor
Edge::Edge():
  m_placeholder(0)
{}

Edge::Edge(const Edge &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Edge::Edge(Edge &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Edge::~Edge(){}
Edge& Edge::operator=(const Edge &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Edge& Edge::operator=(Edge &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Edge &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Edge &lhs)
{
  return input;
}

