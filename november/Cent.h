//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CENT_H
#define CENT_H
#include <iostream>
//TODO Cent finish this class
class Cent
{
  public:
    //constructor
    Cent();
    //copy constructor
    Cent(const Cent &rhs);
    //move constructor
    Cent(Cent &&rhs);
    //destructor
    ~Cent();
    //copy assignment operator
    Cent& operator=(const Cent &rhs);
    //move assignment operator
    Cent& operator=(Cent &&rhs);
    //swap
    void swap(Cent &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cent &rhs);
    friend std::istream& operator>>(std::istream &input, Cent &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CENT_H */