//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PICKLE_H
#define PICKLE_H
#include <iostream>
//TODO Pickle finish this class
class Pickle
{
  public:
    //constructor
    Pickle();
    //copy constructor
    Pickle(const Pickle &rhs);
    //move constructor
    Pickle(Pickle &&rhs);
    //destructor
    ~Pickle();
    //copy assignment operator
    Pickle& operator=(const Pickle &rhs);
    //move assignment operator
    Pickle& operator=(Pickle &&rhs);
    //swap
    void swap(Pickle &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pickle &rhs);
    friend std::istream& operator>>(std::istream &input, Pickle &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PICKLE_H */