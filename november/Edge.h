//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef EDGE_H
#define EDGE_H
#include <iostream>
//TODO Edge finish this class
class Edge
{
  public:
    //constructor
    Edge();
    //copy constructor
    Edge(const Edge &rhs);
    //move constructor
    Edge(Edge &&rhs);
    //destructor
    ~Edge();
    //copy assignment operator
    Edge& operator=(const Edge &rhs);
    //move assignment operator
    Edge& operator=(Edge &&rhs);
    //swap
    void swap(Edge &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Edge &rhs);
    friend std::istream& operator>>(std::istream &input, Edge &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* EDGE_H */