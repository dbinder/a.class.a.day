//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BOOKS_H
#define BOOKS_H
#include <iostream>
//TODO Books finish this class
class Books
{
  public:
    //constructor
    Books();
    //copy constructor
    Books(const Books &rhs);
    //move constructor
    Books(Books &&rhs);
    //destructor
    ~Books();
    //copy assignment operator
    Books& operator=(const Books &rhs);
    //move assignment operator
    Books& operator=(Books &&rhs);
    //swap
    void swap(Books &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Books &rhs);
    friend std::istream& operator>>(std::istream &input, Books &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BOOKS_H */