//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cabbage.h>
//constructor
Cabbage::Cabbage():
  m_placeholder(0)
{}

Cabbage::Cabbage(const Cabbage &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cabbage::Cabbage(Cabbage &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cabbage::~Cabbage(){}
Cabbage& Cabbage::operator=(const Cabbage &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cabbage& Cabbage::operator=(Cabbage &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cabbage &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cabbage &lhs)
{
  return input;
}

