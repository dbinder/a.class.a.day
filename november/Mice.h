//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MICE_H
#define MICE_H
#include <iostream>
//TODO Mice finish this class
class Mice
{
  public:
    //constructor
    Mice();
    //copy constructor
    Mice(const Mice &rhs);
    //move constructor
    Mice(Mice &&rhs);
    //destructor
    ~Mice();
    //copy assignment operator
    Mice& operator=(const Mice &rhs);
    //move assignment operator
    Mice& operator=(Mice &&rhs);
    //swap
    void swap(Mice &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Mice &rhs);
    friend std::istream& operator>>(std::istream &input, Mice &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MICE_H */