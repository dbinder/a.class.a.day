//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cent.h>
//constructor
Cent::Cent():
  m_placeholder(0)
{}

Cent::Cent(const Cent &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cent::Cent(Cent &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cent::~Cent(){}
Cent& Cent::operator=(const Cent &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cent& Cent::operator=(Cent &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cent &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cent &lhs)
{
  return input;
}

