//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SWING_H
#define SWING_H
#include <iostream>
//TODO Swing finish this class
class Swing
{
  public:
    //constructor
    Swing();
    //copy constructor
    Swing(const Swing &rhs);
    //move constructor
    Swing(Swing &&rhs);
    //destructor
    ~Swing();
    //copy assignment operator
    Swing& operator=(const Swing &rhs);
    //move assignment operator
    Swing& operator=(Swing &&rhs);
    //swap
    void swap(Swing &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Swing &rhs);
    friend std::istream& operator>>(std::istream &input, Swing &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SWING_H */