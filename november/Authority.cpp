//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Authority.h>
//constructor
Authority::Authority():
  m_placeholder(0)
{}

Authority::Authority(const Authority &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Authority::Authority(Authority &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Authority::~Authority(){}
Authority& Authority::operator=(const Authority &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Authority& Authority::operator=(Authority &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Authority &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Authority &lhs)
{
  return input;
}

