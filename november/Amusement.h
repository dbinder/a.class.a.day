//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef AMUSEMENT_H
#define AMUSEMENT_H
#include <iostream>
//TODO Amusement finish this class
class Amusement
{
  public:
    //constructor
    Amusement();
    //copy constructor
    Amusement(const Amusement &rhs);
    //move constructor
    Amusement(Amusement &&rhs);
    //destructor
    ~Amusement();
    //copy assignment operator
    Amusement& operator=(const Amusement &rhs);
    //move assignment operator
    Amusement& operator=(Amusement &&rhs);
    //swap
    void swap(Amusement &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Amusement &rhs);
    friend std::istream& operator>>(std::istream &input, Amusement &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* AMUSEMENT_H */