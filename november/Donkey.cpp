//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Donkey.h>
//constructor
Donkey::Donkey():
  m_placeholder(0)
{}

Donkey::Donkey(const Donkey &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Donkey::Donkey(Donkey &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Donkey::~Donkey(){}
Donkey& Donkey::operator=(const Donkey &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Donkey& Donkey::operator=(Donkey &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Donkey &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Donkey &lhs)
{
  return input;
}

