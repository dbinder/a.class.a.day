//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Vein.h>
//constructor
Vein::Vein():
  m_placeholder(0)
{}

Vein::Vein(const Vein &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Vein::Vein(Vein &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Vein::~Vein(){}
Vein& Vein::operator=(const Vein &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Vein& Vein::operator=(Vein &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Vein &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Vein &lhs)
{
  return input;
}

