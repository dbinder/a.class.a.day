//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Amusement.h>
//constructor
Amusement::Amusement():
  m_placeholder(0)
{}

Amusement::Amusement(const Amusement &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Amusement::Amusement(Amusement &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Amusement::~Amusement(){}
Amusement& Amusement::operator=(const Amusement &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Amusement& Amusement::operator=(Amusement &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Amusement &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Amusement &lhs)
{
  return input;
}

