//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wrist.h>
//constructor
Wrist::Wrist():
  m_placeholder(0)
{}

Wrist::Wrist(const Wrist &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wrist::Wrist(Wrist &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wrist::~Wrist(){}
Wrist& Wrist::operator=(const Wrist &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wrist& Wrist::operator=(Wrist &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wrist &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wrist &lhs)
{
  return input;
}

