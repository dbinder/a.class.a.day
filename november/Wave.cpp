//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wave.h>
//constructor
Wave::Wave():
  m_placeholder(0)
{}

Wave::Wave(const Wave &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wave::Wave(Wave &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wave::~Wave(){}
Wave& Wave::operator=(const Wave &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wave& Wave::operator=(Wave &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wave &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wave &lhs)
{
  return input;
}

