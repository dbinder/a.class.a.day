//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Suit.h>
//constructor
Suit::Suit():
  m_placeholder(0)
{}

Suit::Suit(const Suit &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Suit::Suit(Suit &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Suit::~Suit(){}
Suit& Suit::operator=(const Suit &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Suit& Suit::operator=(Suit &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Suit &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Suit &lhs)
{
  return input;
}

