//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Kiss.h>
//constructor
Kiss::Kiss():
  m_placeholder(0)
{}

Kiss::Kiss(const Kiss &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Kiss::Kiss(Kiss &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Kiss::~Kiss(){}
Kiss& Kiss::operator=(const Kiss &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Kiss& Kiss::operator=(Kiss &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Kiss &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Kiss &lhs)
{
  return input;
}

