//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Mice.h>
//constructor
Mice::Mice():
  m_placeholder(0)
{}

Mice::Mice(const Mice &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Mice::Mice(Mice &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Mice::~Mice(){}
Mice& Mice::operator=(const Mice &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Mice& Mice::operator=(Mice &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Mice &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Mice &lhs)
{
  return input;
}

