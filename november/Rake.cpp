//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Rake.h>
//constructor
Rake::Rake():
  m_placeholder(0)
{}

Rake::Rake(const Rake &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Rake::Rake(Rake &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Rake::~Rake(){}
Rake& Rake::operator=(const Rake &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Rake& Rake::operator=(Rake &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Rake &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Rake &lhs)
{
  return input;
}

