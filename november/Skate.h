//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SKATE_H
#define SKATE_H
#include <iostream>
//TODO Skate finish this class
class Skate
{
  public:
    //constructor
    Skate();
    //copy constructor
    Skate(const Skate &rhs);
    //move constructor
    Skate(Skate &&rhs);
    //destructor
    ~Skate();
    //copy assignment operator
    Skate& operator=(const Skate &rhs);
    //move assignment operator
    Skate& operator=(Skate &&rhs);
    //swap
    void swap(Skate &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Skate &rhs);
    friend std::istream& operator>>(std::istream &input, Skate &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SKATE_H */