//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Man.h>
//constructor
Man::Man():
  m_placeholder(0)
{}

Man::Man(const Man &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Man::Man(Man &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Man::~Man(){}
Man& Man::operator=(const Man &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Man& Man::operator=(Man &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Man &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Man &lhs)
{
  return input;
}

