//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CABBAGE_H
#define CABBAGE_H
#include <iostream>
//TODO Cabbage finish this class
class Cabbage
{
  public:
    //constructor
    Cabbage();
    //copy constructor
    Cabbage(const Cabbage &rhs);
    //move constructor
    Cabbage(Cabbage &&rhs);
    //destructor
    ~Cabbage();
    //copy assignment operator
    Cabbage& operator=(const Cabbage &rhs);
    //move assignment operator
    Cabbage& operator=(Cabbage &&rhs);
    //swap
    void swap(Cabbage &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cabbage &rhs);
    friend std::istream& operator>>(std::istream &input, Cabbage &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CABBAGE_H */