//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef EGGNOG_H
#define EGGNOG_H
#include <iostream>
//TODO Eggnog finish this class
class Eggnog
{
  public:
    //constructor
    Eggnog();
    //copy constructor
    Eggnog(const Eggnog &rhs);
    //move constructor
    Eggnog(Eggnog &&rhs);
    //destructor
    ~Eggnog();
    //copy assignment operator
    Eggnog& operator=(const Eggnog &rhs);
    //move assignment operator
    Eggnog& operator=(Eggnog &&rhs);
    //swap
    void swap(Eggnog &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Eggnog &rhs);
    friend std::istream& operator>>(std::istream &input, Eggnog &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* EGGNOG_H */