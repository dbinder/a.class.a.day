//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Swing.h>
//constructor
Swing::Swing():
  m_placeholder(0)
{}

Swing::Swing(const Swing &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Swing::Swing(Swing &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Swing::~Swing(){}
Swing& Swing::operator=(const Swing &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Swing& Swing::operator=(Swing &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Swing &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Swing &lhs)
{
  return input;
}

