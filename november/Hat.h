//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HAT_H
#define HAT_H
#include <iostream>
//TODO Hat finish this class
class Hat
{
  public:
    //constructor
    Hat();
    //copy constructor
    Hat(const Hat &rhs);
    //move constructor
    Hat(Hat &&rhs);
    //destructor
    ~Hat();
    //copy assignment operator
    Hat& operator=(const Hat &rhs);
    //move assignment operator
    Hat& operator=(Hat &&rhs);
    //swap
    void swap(Hat &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Hat &rhs);
    friend std::istream& operator>>(std::istream &input, Hat &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HAT_H */