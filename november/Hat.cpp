//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Hat.h>
//constructor
Hat::Hat():
  m_placeholder(0)
{}

Hat::Hat(const Hat &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Hat::Hat(Hat &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Hat::~Hat(){}
Hat& Hat::operator=(const Hat &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Hat& Hat::operator=(Hat &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Hat &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Hat &lhs)
{
  return input;
}

