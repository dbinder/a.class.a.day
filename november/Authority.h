//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef AUTHORITY_H
#define AUTHORITY_H
#include <iostream>
//TODO Authority finish this class
class Authority
{
  public:
    //constructor
    Authority();
    //copy constructor
    Authority(const Authority &rhs);
    //move constructor
    Authority(Authority &&rhs);
    //destructor
    ~Authority();
    //copy assignment operator
    Authority& operator=(const Authority &rhs);
    //move assignment operator
    Authority& operator=(Authority &&rhs);
    //swap
    void swap(Authority &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Authority &rhs);
    friend std::istream& operator>>(std::istream &input, Authority &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* AUTHORITY_H */