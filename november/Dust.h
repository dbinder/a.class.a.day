//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DUST_H
#define DUST_H
#include <iostream>
//TODO Dust finish this class
class Dust
{
  public:
    //constructor
    Dust();
    //copy constructor
    Dust(const Dust &rhs);
    //move constructor
    Dust(Dust &&rhs);
    //destructor
    ~Dust();
    //copy assignment operator
    Dust& operator=(const Dust &rhs);
    //move assignment operator
    Dust& operator=(Dust &&rhs);
    //swap
    void swap(Dust &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Dust &rhs);
    friend std::istream& operator>>(std::istream &input, Dust &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DUST_H */