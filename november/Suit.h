//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SUIT_H
#define SUIT_H
#include <iostream>
//TODO Suit finish this class
class Suit
{
  public:
    //constructor
    Suit();
    //copy constructor
    Suit(const Suit &rhs);
    //move constructor
    Suit(Suit &&rhs);
    //destructor
    ~Suit();
    //copy assignment operator
    Suit& operator=(const Suit &rhs);
    //move assignment operator
    Suit& operator=(Suit &&rhs);
    //swap
    void swap(Suit &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Suit &rhs);
    friend std::istream& operator>>(std::istream &input, Suit &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SUIT_H */