//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TWIST_H
#define TWIST_H
#include <iostream>
//TODO Twist finish this class
class Twist
{
  public:
    //constructor
    Twist();
    //copy constructor
    Twist(const Twist &rhs);
    //move constructor
    Twist(Twist &&rhs);
    //destructor
    ~Twist();
    //copy assignment operator
    Twist& operator=(const Twist &rhs);
    //move assignment operator
    Twist& operator=(Twist &&rhs);
    //swap
    void swap(Twist &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Twist &rhs);
    friend std::istream& operator>>(std::istream &input, Twist &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TWIST_H */