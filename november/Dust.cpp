//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Dust.h>
//constructor
Dust::Dust():
  m_placeholder(0)
{}

Dust::Dust(const Dust &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Dust::Dust(Dust &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Dust::~Dust(){}
Dust& Dust::operator=(const Dust &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Dust& Dust::operator=(Dust &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Dust &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Dust &lhs)
{
  return input;
}

