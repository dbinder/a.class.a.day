//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RANGE_H
#define RANGE_H
#include <iostream>
//TODO Range finish this class
class Range
{
  public:
    //constructor
    Range();
    //copy constructor
    Range(const Range &rhs);
    //move constructor
    Range(Range &&rhs);
    //destructor
    ~Range();
    //copy assignment operator
    Range& operator=(const Range &rhs);
    //move assignment operator
    Range& operator=(Range &&rhs);
    //swap
    void swap(Range &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Range &rhs);
    friend std::istream& operator>>(std::istream &input, Range &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RANGE_H */