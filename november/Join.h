//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef JOIN_H
#define JOIN_H
#include <iostream>
//TODO Join finish this class
class Join
{
  public:
    //constructor
    Join();
    //copy constructor
    Join(const Join &rhs);
    //move constructor
    Join(Join &&rhs);
    //destructor
    ~Join();
    //copy assignment operator
    Join& operator=(const Join &rhs);
    //move assignment operator
    Join& operator=(Join &&rhs);
    //swap
    void swap(Join &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Join &rhs);
    friend std::istream& operator>>(std::istream &input, Join &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* JOIN_H */