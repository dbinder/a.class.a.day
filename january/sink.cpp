//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <sink.h>
//constructor
sink::sink():
  m_length(0),
  m_width(0), 
  m_waterFlow(0)
{}

sink::sink(const sink &rhs)
{
  m_length = rhs.m_length;
  m_waterFlow = rhs.m_waterFlow;
  m_width = rhs.m_width;
}

sink& sink::operator=(const sink &rhs)
{
  if (this == &rhs)
    return *this;
  m_length = rhs.m_length;
  m_waterFlow = rhs.m_waterFlow;
  m_width = rhs.m_width;

  return *this;
}
//destructor
sink::~sink(){}
std::ostream &operator<<(std::ostream &output, const sink &rhs)
{
  output << rhs.m_length
    << rhs.m_waterFlow
    << rhs.m_width;
  return output;
}
std::istream &operator>>(std::istream &input, sink &lhs)
{
  input >> lhs.m_length
    >> lhs.m_waterFlow
    >> lhs.m_width;
  return input;
}


unsigned int sink::getWaterFlow() const{ return m_waterFlow; }
double sink::getLength() const{ return m_length; }
double sink::getWidth() const{ return m_width; }
//Set
void sink::setWaterFlow(unsigned int i)
{
  m_waterFlow = i;
}
void sink::setLength(double n)
{
  m_length = n;
}
void sink::setWidth(double n)
{
  m_width = n;
}