//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <stomach.h>
//constructor
stomach::stomach():
  m_state(EMPTY)
{}
//copy constructor
stomach::stomach(const stomach &rhs):
  m_state(rhs.m_state)
{}
//move constructor
stomach::stomach(stomach &&rhs):
m_state(EMPTY)
{
  m_state = rhs.m_state;
  rhs.m_state = EMPTY;
}
//destructor
stomach::~stomach(){}
//copy assignment operator
stomach& stomach::operator=(const stomach &rhs)
{
  if (this != &rhs)
  {
    m_state = rhs.m_state;
  }
  return *this;
}
//move assignment operator
stomach& stomach::operator=(stomach &&rhs)
{
  if (this != &rhs)
  {
    m_state = rhs.m_state;
    rhs.m_state = EMPTY;
  }
  return *this;
}
stomach::state stomach::getState() const
{
  return this->m_state;
}