//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <support.h>
//constructor
//TODO a better implementation of this class. 
support::support():
  m_duration(0)
{
  m_ticket = new char[4];
  strcpy(m_ticket, "tet");
}
support::support(const support &rhs) :
m_duration(rhs.m_duration)
{
  int length = strlen(rhs.m_ticket);
  m_ticket = new char[length];
  //copy(m_ticket, m_ticket, (sizeof(rhs.m_ticket)));
}
//copy assigment operator
support& support::operator=(const support &rhs)
{
  if (this == &rhs)
  {
    return *this;
  }
  support temp(rhs);
  delete m_ticket;
  swap(temp);
  return *this;
}
void support::swap(support &rhs)
{
  using std::swap;
  swap(this->m_duration, rhs.m_duration);
  swap(this->m_ticket, rhs.m_ticket);
}
//destructor
support::~support()
{
  if (m_ticket)
    m_duration = 0;
    delete m_ticket;
  //  m_ticket = nullptr;
    
}
std::ostream &operator<<(std::ostream &output, const support &rhs)
{
  output << rhs.m_ticket
    << rhs.m_duration;
  return output;
}
//Get
const char* support::getTicket() const{ return m_ticket; }
//Lame functionality.  
time_t support::getTime() const{ return m_duration; }
//Set
void support::setTicket(char *n)
{
  int length = strlen(n);
  memcpy(m_ticket, n, length);
  //m_ticket[length - 1] = 0;
  //time(&m_duration);
}