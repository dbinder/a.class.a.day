//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef QUINCE_H
#define QUINCE_H
#include <iostream>
//TODO quince finish this class
class quince
{
  enum ripe
  {
    E_NOPE,
    E_RIPE
  };
  public:
    //constructor
    quince();
    //copy constructor
    quince(const quince &rhs);
    //move constructor
    quince(quince &&rhs);
    //destructor
    ~quince();
    //assignment operator
    quince& operator=(const quince &rhs);
    friend std::ostream& operator<<(std::ostream &output, const quince &rhs);
    friend std::istream& operator>>(std::istream &input, quince &lhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    unsigned int m_amount;
    quince::ripe m_ripe;
};

#endif /* QUINCE_H */