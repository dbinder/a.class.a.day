//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BELL_H
#define BELL_H

#include <iostream>
#include <string>
class Bell
{
  //constructor
  public:
    Bell();
  //destructor
    ~Bell();
  //copy constuctor
    Bell(const Bell &rhs);
  //assignment operator
    Bell& operator=(const Bell &rhs);
    friend std::ostream &operator<<(std::ostream &output, const Bell &rhs);
    friend std::istream &operator>>(std::istream &input, Bell &lhs);

    //Methods

    //Get
    unsigned int getWeight() const;
    unsigned int getHeight() const;
    unsigned int getLength() const;
    std::string  getTone() const;

    //Set
    void setWeight(unsigned int i);
    void setHeight(unsigned int i);
    void setLength(unsigned int i);
    void setTone(std::string n);

  protected:
  private:
    unsigned int m_weight;
    unsigned int m_height;
    unsigned int m_length;
    std::string m_tone;
};

#endif /* BELL_H */