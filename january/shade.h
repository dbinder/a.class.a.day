//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SHADE_H
#define SHADE_H
#include <iostream>
//Like shade from a tree, do the creature part else where
class shade
{
  public:
    //constructor
    shade();
    shade::shade(double length, double width, double height);
    //destructor
    ~shade();
    //copy constuctor
    shade(const shade &rhs);
    //assignment operator
    shade& operator=(const shade &rhs);
    //ostream 
    friend std::ostream& operator<<(std::ostream &output, const shade &rhs);
    friend std::istream& operator>>(std::istream &input, shade &lhs);

    //arithmetic operators
    shade operator+(const shade &rhs);
    shade operator-(const shade &rhs);
    //comparison operators
    bool operator==(const shade &rhs);
    bool operator!=(const shade &rhs);
    bool operator>(const shade &rhs);
    bool operator<(const shade &rhs);
    bool operator>=(const shade &rhs);
    bool operator<=(const shade &rhs);
  
    //Methods
    void area();
    void volume();
    //Get
    double getLength() const;
    double getWidth() const;
    double getHeight() const;
    double getArea() const;
    double getVolume() const;
    //Set
    void setLength(double n);
    void setWidth(double n);
    void setHeigth(double n);
  
  protected:
  private:
    double m_length;
    double m_width;
    double m_height;
    double m_area;
    double m_volume;
};

#endif /* SHADE_H */