//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <sister.h>

sister::sister():
  m_fName("Mary"),
  m_mName("Kinsey"),
  m_lName("Howellington"),
  m_hairColor("Red"),
  m_eyeColor("Red"),
  m_age(32),
  m_height(72),
  m_weight(160)
  {}

sister::~sister(){}

sister::sister(const sister &rhs)
{
  //std::cout << "copy constructor!\n";
  m_fName = rhs.m_fName;
  m_mName = rhs.m_mName;
  m_lName = rhs.m_lName;
  m_hairColor = rhs.m_hairColor;
  m_eyeColor = rhs.m_eyeColor;
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;
}

sister& sister::operator=(const sister &rhs)
{
  if (this == &rhs)
    return *this;
  //std::cout << "assignment operator\n";
  m_fName = rhs.m_fName;
  m_mName = rhs.m_mName;
  m_lName = rhs.m_lName;
  m_hairColor = rhs.m_hairColor;
  m_eyeColor = rhs.m_eyeColor;
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;

  return *this;
}

std::ostream& operator<<(std::ostream &out, const sister &rhs)
{
  // Since operator<< is a friend of the sister class, we can access
  // sister's members directly.
  out << "(" 
      << rhs.m_fName 
      << ", " 
      << rhs.m_mName 
      << ", " 
      << rhs.m_lName 
      << ")"
      << "\t("
      << rhs.m_hairColor
      << ", "
      << rhs.m_eyeColor
      << ")"
      << "\t("
      << rhs.m_age
      << ", "
      << rhs.m_height
      << ", "  
      << rhs.m_weight
      << ")"
      << std::endl;
  return out;
}

std::istream &operator>>(std::istream &input, sister &lhs)
{
  // Since operator<< is a friend of the sister class, we can access
  // sister's members directly.
  input >> lhs.m_fName;
  input >> lhs.m_mName;
  input >> lhs.m_lName;
  input >> lhs.m_hairColor;
  input >> lhs.m_eyeColor;
  input >> lhs.m_age;
  input >> lhs.m_height;
  input >> lhs.m_weight;
  return input;
}

//comparison operators
bool operator==(const sister &rhs1, const sister &rhs2)
{
  return rhs1.m_age == rhs2.m_age &&
    rhs1.m_fName == rhs2.m_fName &&
    rhs1.m_mName == rhs2.m_mName &&
    rhs1.m_lName == rhs2.m_lName &&
    rhs1.m_weight == rhs2.m_weight &&
    rhs1.m_height == rhs2.m_height &&
    rhs1.m_hairColor == rhs2.m_hairColor &&
    rhs1.m_eyeColor == rhs2.m_eyeColor;
}
bool operator!=(const sister &rhs1, const sister &rhs2)
{
  return !(rhs1 == rhs2);
}

std::string sister::getFName(){return m_fName;}
std::string sister::getMName(){return m_mName;}
std::string sister::getLName(){return m_lName;}
std::string sister::getHairColor(){return m_hairColor;}
std::string sister::getEyeColor(){return m_eyeColor;}
unsigned int sister::getAge()const {return m_age;}
double sister::getHeight(){return m_height;}
double sister::getWeight(){return m_weight;}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void sister::setFName(std::string n)
{
  m_fName = n;
}

void sister::setMName(std::string n)
{
  m_mName = n;
}

void sister::setLName(std::string n)
{
  m_lName = n;
}

void sister::setHairColor(std::string n)
{
  m_hairColor = n;
}

void sister::setEyeColor(std::string n)
{
  m_eyeColor = n;
}

void sister::setAge(unsigned int i)
{
  m_age = i;
}
void sister::setHeight(double n)
{
  m_height = n;
}
void sister::setWeight(double n)
{
  m_weight = n;
}