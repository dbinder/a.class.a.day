//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <leather.h>
//constructor
leather::leather():
  m_amount(0),
  m_length(0),
  m_width(0)
{}
leather::leather(const leather &rhs):
  m_amount(rhs.m_amount),
  m_length(rhs.m_length),
  m_width(rhs.m_width)
{}
//move constructor
leather::leather(leather &&rhs)
{
  m_amount = rhs.m_amount;
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  rhs.m_amount = 0;
  rhs.m_length = 0;
  rhs.m_width = 0;
}
//destructor
leather::~leather(){}
//assignment operator
leather& leather::operator=(const leather &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_length = rhs.m_length;
    m_width = rhs.m_width;
  }
  return *this;
}
//move assignment operator
leather& leather::operator=(leather &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    rhs.m_amount = 0;
    rhs.m_length = 0;
    rhs.m_width = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const leather &rhs)
{
  output << rhs.m_amount
    << rhs.m_length
    << rhs.m_width;
  return output;
}
std::istream &operator>>(std::istream &input, leather &lhs)
{
  input >> lhs.m_amount
    >> lhs.m_length
    >> lhs.m_width;
  return input;
}

//Methods
double leather::getArea() const
{
  return m_length * m_width;
}
//Get
unsigned int leather::getAmount()const
{
  return m_amount;
}
double leather::getLength()const
{
  return m_length;
}
double leather::getWidth()const
{
  return m_width;
}
//Set
void leather::setAmount(unsigned int i)
{
  m_amount = i;
}
void leather::setLength(double n)
{
  m_length = n;
}
void leather::setWidth(double n)
{
  m_width = n;
}