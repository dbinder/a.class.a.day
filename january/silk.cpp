//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <silk.h>
//constructor
silk::silk():
  m_amount(0),
  m_threadCount(0),
  m_length(0),
  m_width(0)
{}
//TODO destructor
silk::~silk(){}

silk::silk(const silk &rhs)
{
  m_amount = rhs.m_amount;
  m_threadCount = rhs.m_threadCount;
  m_length = rhs.m_length;
  m_width = rhs.m_width;
}

//assignment operator
silk& silk::operator=(const silk &rhs)
{
  if (this == &rhs)
    return *this;
  m_amount = rhs.m_amount;
  m_threadCount = rhs.m_threadCount;
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  return *this;
}
//TODO ostream operators
std::ostream& operator<<(std::ostream &output, const silk &rhs)
{
  return output;
}
std::istream& operator>>(std::istream &input, silk &lhs)
{
  return input;
}

//Methods
double silk::getArea() const
{
  return m_length * m_width;
}
//Get
unsigned int silk::getThreadCount() const
{
  return m_threadCount;
}
unsigned int silk::getAmount()const
{
  return m_amount;
}
double silk::getLength()const
{
  return m_length;
}
double silk::getWidth()const
{
  return m_width;
}
//Set
void silk::setThreadCount(unsigned int i)
{
  m_threadCount = i;
}
void silk::setAmount(unsigned int i)
{
  m_amount = i;
}
void silk::setLength(double n)
{
  m_length = n;
}
void silk::setWidth(double n)
{
  m_width = n;
}