//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef JAM_H
#define JAM_H

#include <iostream>
#include <string>
//What is jam, I am thinking of jam/jelly stuff you put on toast
class jam
{
  public:
    //constructor
    jam();
    //destructor
    ~jam();
    //copy constuctor
    jam(const jam &rhs);
    //assignment operator
    jam& operator=(const jam &rhs);
    friend std::ostream& operator<<(std::ostream &output, const jam &rhs);
    friend std::istream& operator>>(std::istream &input, jam &lhs);
    //Methods
    //Get
    double getViscosity() const;
    std::string getFlavor() const;
    //Set
    void setViscosity(double n);
    void setFlavor(std::string n);
  protected:
  private:
    double m_viscosity;
    std::string m_flavor;

};

#endif /* JAM_H */