//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <truck.h>
//constructor
truck::truck():
  m_numWheels(0),
  m_mileage(0),
  m_driveTrain("Stick"),
  m_brandName("Ford")
{}

truck::truck(const truck &rhs)
{
  m_numWheels = rhs.m_numWheels;
  m_mileage = rhs.m_mileage;
  m_driveTrain = rhs.m_driveTrain;
  m_brandName = rhs.m_brandName;
}

truck& truck::operator=(const truck &rhs)
{
  if (this == &rhs)
    return *this;
  m_numWheels = rhs.m_numWheels;
  m_mileage = rhs.m_mileage;
  m_driveTrain = rhs.m_driveTrain;
  m_brandName = rhs.m_brandName;
  return *this;
}

//destructor
truck::~truck(){}
std::ostream& operator<<(std::ostream &output, const truck &rhs)
{
  return output;
}
std::istream& operator>>(std::istream &input, truck &lhs)
{
  return input;
}
//Methods

//Get
unsigned int truck::getNumWheels() const{ return m_numWheels; }
unsigned int truck::getMileage()   const{ return m_mileage; }
std::string truck::getDriveTrain() const{ return m_driveTrain; }
std::string truck::getBrandName()  const{ return m_brandName; }
//Set
void truck::setNumWheels(unsigned int i)
{
  m_numWheels = i;
}
void truck::setMileage(unsigned int i)
{
  m_mileage = i;
}
void truck::setDriveTrain(std::string n)
{
  m_driveTrain = n;
}
void truck::setBrandName(std::string n)
{
  m_brandName = n;
}