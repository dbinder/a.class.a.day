//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DISEASE_H
#define DISEASE_H
#include <iostream>

//castable spell
class disease
{
  public:
    //constructor
    disease();
    //destructor
    ~disease();
    //copy constuctor
    disease(const disease &rhs);
    //assignment operator
    disease& operator=(const disease &rhs);
    friend std::ostream& operator<<(std::ostream &output, const disease &rhs);
    friend std::istream& operator>>(std::istream &input, disease &lhs);
    //Methods
    double calcDot() const;
    //Get
    unsigned int getDuration() const;
    double getDamage() const;
    double getModifier() const;
    //Set
    void setDuration(unsigned int i);
    void setDamage(double n);
    void setModifier(double n);
  protected:
  private:
    unsigned int m_duration;
    double m_damage;
    double m_modifier;
};

#endif /* DISEASE_H */