//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef ROOM_H
#define ROOM_H
#include <iostream>
//TODO maybe make classes of objects that go in the room->Dresser->lamp->picture->bed
class room
{
  public:
    //constructor
    room();
    //copy constuctor
    room(const room &rhs);
    //move constructor
    room(room &&rhs);
    //destructor
    ~room();
    //assignment operator
    room& operator=(const room &rhs);
    //move assignement
    room& operator=(room &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const room &rhs);
    friend std::istream& operator>>(std::istream &input, room &lhs);
    //Methods
    double area();
    //Get
    bool getDoor() const;
    bool getWindow() const;
    bool getLight() const;
    //Set
  protected:
  private:
    double m_length;
    double m_width;
    double m_height;
    bool m_door;
    bool m_window;
    bool m_light;
};

#endif /* ROOM_H */