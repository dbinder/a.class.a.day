//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SACK_H
#define SACK_H

#include <iostream>

class sack
{
  //constructor
  public:
    sack();
    //destructor
    ~sack();
    //copy constuctor
    sack(const sack &rhs);
    //assignment operator
    sack& operator=(const sack &rhs);
    friend std::ostream &operator<<(std::ostream &output, const sack &rhs);
    friend std::istream &operator>>(std::istream &input, sack &lhs);

    //get
    unsigned int getSack() const;
    //set
    void setSack(unsigned int i);
  protected:
  private:
    unsigned int m_slots;

};

#endif /* SACK_H */