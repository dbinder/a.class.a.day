//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <record.h>
//constructor

record::record():
  m_tracklist(0),
  m_rpm(THIRTYTHREE),
  m_diameter(SEVEN),
  m_year(1980),
  m_artist("McStabberton"),
  m_publisher("Stubbs"),
  m_duration(0)
{}
//copy constructor
record::record(const record &rhs):
  m_tracklist(rhs.m_tracklist),
  m_rpm(rhs.m_rpm),
  m_diameter(rhs.m_diameter),
  m_year(rhs.m_year),
  m_artist(rhs.m_artist),
  m_publisher(rhs.m_publisher),
  m_duration(rhs.m_duration)
{}
record::record(record &&rhs):
  m_tracklist(0),
  m_rpm(THIRTYTHREE),
  m_diameter(SEVEN),
  m_year(1980),
  m_artist("McStabberton"),
  m_publisher("Stubbs"),
  m_duration(0)
{
  m_tracklist = rhs.m_tracklist;
  m_rpm = rhs.m_rpm;
  m_diameter = rhs.m_diameter;
  m_year = rhs.m_year;
  m_artist = rhs.m_artist;
  m_publisher = rhs.m_publisher;
  m_duration = rhs.m_duration;
  

  delete &rhs.m_tracklist; 
  rhs.m_rpm = THIRTYTHREE;
  rhs.m_diameter = SEVEN;
  rhs.m_year = 0;
  rhs.m_artist = "";
  rhs.m_publisher = "";
  rhs.m_duration = 0;
}
//destructor
record::~record(){}
//assignment operator
record& record::operator=(const record &rhs)
{
  if (this != &rhs)
  {
    m_tracklist = rhs.m_tracklist;
    m_rpm = rhs.m_rpm;
    m_diameter = rhs.m_diameter;
    m_year = rhs.m_year;
    m_artist = rhs.m_artist;
    m_publisher = rhs.m_publisher;
    m_duration = rhs.m_duration;
  }
  return *this;
}
//move assignment
record& record::operator=(record &&rhs)
{
  if (this != &rhs)
  {
    m_tracklist = rhs.m_tracklist;
    m_rpm = rhs.m_rpm;
    m_diameter = rhs.m_diameter;
    m_year = rhs.m_year;
    m_artist = rhs.m_artist;
    m_publisher = rhs.m_publisher;
    m_duration = rhs.m_duration;

    delete &rhs.m_tracklist;
    rhs.m_rpm = THIRTYTHREE;
    rhs.m_diameter = SEVEN;
    rhs.m_year = 0;
    rhs.m_artist = "";
    rhs.m_publisher = "";
    rhs.m_duration = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const record &rhs)
{
  output << rhs.m_rpm
    << rhs.m_diameter
    << rhs.m_year
    << rhs.m_artist.c_str()
    << rhs.m_publisher.c_str()
    << rhs.m_duration;
  return output;
}
std::istream &operator>>(std::istream &input, record &lhs)
{
  input >> lhs.m_rpm
    >> lhs.m_diameter
    >> lhs.m_year
    >> lhs.m_artist
    >> lhs.m_publisher
    >> lhs.m_duration;
  return input;
}
std::istream& operator>>(std::istream &input, record::rpm &lhs)
{
  int inVal;
  input >> inVal;
  switch (inVal)
  {
    case 0:
      lhs = record::rpm::SIXTEEN;
      break;
    case 1:
      lhs = record::rpm::THIRTYTHREE;
      break;
    case 2:
      lhs = record::rpm::FOURTYFIVE;
      break;
    case 3:
      lhs = record::rpm::SEVENTYEIGHT;
      break;
    default:
      lhs = record::rpm::SIXTEEN;
      break;
  }
  return input;
}
std::istream& operator>>(std::istream &input, record::diameter &lhs)
{
  int inVal;
  input >> inVal;
  switch (inVal)
  {
  case 0:
    lhs = record::diameter::SEVEN;
    break;
  case 1:
    lhs = record::diameter::TEN;
    break;
  case 2:
    lhs = record::diameter::TWELVE;
    break;
  default:
    lhs = record::diameter::SEVEN;
    break;
  }
  return input;
}
std::vector<const char *> record::getTracks() const
{
  return m_tracklist;
}