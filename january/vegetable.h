//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef VEGETABLE_H
#define VEGETABLE_H
#include <iostream>
#include <string>
//TODO vegetable finish this class
class vegetable
{
  public:
    //constructor
    vegetable();
    //copy constructor
    vegetable(const vegetable &rhs);
    //move constructor
    vegetable(vegetable &&rhs);
    //destructor
    ~vegetable();
    //assignment operator
    vegetable& operator=(const vegetable &rhs);
    //move assignement operator
    vegetable& operator=(vegetable &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const vegetable &rhs);
    friend std::istream& operator>>(std::istream &input, vegetable &lhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    unsigned int m_amount;
    std::string m_flavor;
    std::string m_type;
};

#endif /* VEGETABLE_H */