//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <Child.h>
//constructor
Child::Child():
  m_age(9),
  m_height(),
  m_weight(123),
  m_eyeColor("Red"),
  m_hairColor("Red")
  {}

//destructor
Child::~Child(){}
//copy constructor
Child::Child(const Child &rhs)
{
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;
  m_eyeColor = rhs.m_eyeColor;
  m_hairColor = rhs.m_hairColor;
}

//assignment operator
Child& Child::operator=(const Child &rhs)
{
  if (this == &rhs)
    return *this;
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;
  m_eyeColor = rhs.m_eyeColor;
  m_hairColor = rhs.m_hairColor;
  return *this;
}

//iostream operators
std::ostream& operator<<(std::ostream &output, const Child &rhs)
{
  output << rhs.m_age
    << rhs.m_height
    << rhs.m_weight
    << rhs.m_eyeColor
    << rhs.m_hairColor
    << std::endl;
  return output;
}
std::istream& operator>>(std::istream &input, Child &lhs)
{
  input >> lhs.m_age
    >> lhs.m_height
    >> lhs.m_weight
    >> lhs.m_eyeColor
    >> lhs.m_hairColor;
  return input;
}

//comparison operators
bool operator==(const Child &rhs1, const Child &rhs2)
{
  return rhs1.m_age == rhs2.m_age &&
    rhs1.m_weight == rhs2.m_weight &&
    rhs1.m_height == rhs2.m_height &&
    rhs1.m_hairColor == rhs2.m_hairColor &&
    rhs1.m_eyeColor == rhs2.m_eyeColor;
}
bool operator!=(const Child &rhs1, const Child &rhs2)
{
  return !(rhs1 == rhs2);
}

//Methods
//Get
unsigned int Child::getAge(){return m_age;}
double Child::getHeight(){return m_height;}
double Child::getWeight(){return m_weight;}
std::string Child::getEyeColor(){return m_eyeColor;}
std::string Child::getHairColor(){return m_hairColor;}

//Set
void Child::setAge(unsigned int i)
{
  m_age = i;
}
void Child::setHeight(double n)
{
  m_height = n;
}
void Child::setWeight(double n)
{
  m_weight = n;
}
void Child::setEyeColor(std::string n)
{
  m_eyeColor = n; 
}
void Child::setHairColor(std::string n)
{
  m_hairColor = n;
}