//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TRUCK_H
#define TRUCK_H

#include <iostream>
#include <string>

class truck
{

  public:
    //constructor
    truck();
    //destructor
    virtual ~truck();
    //copy constuctor
    truck(const truck &rhs);
    //assignment operator
    truck& operator=(const truck &rhs);
    friend std::ostream& operator<<(std::ostream &output, const truck &rhs);
    friend std::istream& operator>>(std::istream &input, truck &lhs);
  //Methods

  //Get
    unsigned int getNumWheels() const;
    unsigned int getMileage()   const;
    std::string getDriveTrain() const;
    std::string getBrandName()  const;
  //Set
    void setNumWheels(unsigned int i);
    void setMileage(unsigned int i);
    void setDriveTrain(std::string n);
    void setBrandName(std::string n);

protected:
  private:
    unsigned int m_numWheels;
    unsigned int m_mileage;
    std::string m_driveTrain;
    std::string m_brandName;


};

#endif /* TRUCK_H */