//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <key.h>
//constructor
//a key to open things
//types like door, chest, window or whatever
//enum the types
//will have an owner of said key.
key::key():
  m_type(DOOR),
  m_owner(GOBLIN)
{}
//copy constructor
key::key(const key &rhs) :
  m_type(rhs.m_type),
  m_owner(rhs.m_owner)
{}
//move constructor
key::key(key &&rhs):
  m_type(DOOR),
  m_owner(GOBLIN)
{
  m_type = rhs.m_type;
  m_owner = rhs.m_owner;
  rhs.m_type = DOOR;
  rhs.m_owner = GOBLIN;
}
//destructor
key::~key(){}
//asignment operator
key& key::operator=(const key &rhs)
{
  if (this != &rhs)
  {
    m_type = rhs.m_type;
    m_owner = rhs.m_owner;
  }
  return *this;
}
//move assignment
key& key::operator=(key &&rhs)
{
  if (this != &rhs)
  {
    m_type = rhs.m_type;
    m_owner = rhs.m_owner;
    rhs.m_type = DOOR;
    rhs.m_owner = GOBLIN;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const key &rhs)
{
  output << rhs.m_type
    << rhs.m_owner;
  return output;
}

