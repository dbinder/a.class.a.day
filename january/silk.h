//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SILK_H
#define SILK_H

#include <iostream>

class silk
{
  public:
    //constructor
    silk();
    //destructor
    ~silk();
    //copy constuctor
    silk(const silk &rhs);
    //assignment operator
    silk& operator=(const silk &rhs);
    friend std::ostream& operator<<(std::ostream &output, const silk &rhs);
    friend std::istream& operator>>(std::istream &input, silk &lhs);

    //Methods
    double getArea() const;

    //Get
    unsigned int getThreadCount() const;
    unsigned int getAmount() const;
    double getLength() const;
    double getWidth() const;
    //Set
    void setThreadCount(unsigned int i);
    void setAmount(unsigned int i);
    void setLength(double n);
    void setWidth(double n);


  protected:
  private:
    unsigned int m_threadCount;
    unsigned int m_amount;
    double m_length;
    double m_width;
};

#endif /* SILK_H */