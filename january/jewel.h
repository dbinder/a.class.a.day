//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef JEWEL_H
#define JEWEL_H
#include <iostream>
#include <string>
class jewel
{
  public:
    //constructor
    jewel();
    //copy constuctor
    jewel(const jewel &rhs);
    //move constructor
    jewel(jewel &&rhs);
    //destructor
    ~jewel();
    //assignment operator
    jewel& operator=(const jewel &rhs);
    //move assignment operator
    jewel& operator=(jewel &&rhs);
    //ostream operators
    friend std::ostream& operator<<(std::ostream &output, const jewel &rhs);
    friend std::istream& operator>>(std::istream &input, jewel &lhs);
    
    //comparison operators
    bool operator==(const jewel &rhs);
    bool operator!=(const jewel &rhs);
    bool operator>(const jewel &rhs);
    bool operator<(const jewel &rhs);
    bool operator>=(const jewel &rhs);
    bool operator<=(const jewel &rhs);
    //arithmetic operators
    jewel operator+(const jewel &rhs);
    jewel operator-(const jewel &rhs);

    //Methods
    //Get
    //Set
  protected:
  private:
    std::string m_color;//make this restricted make this a meta_class
    unsigned int m_size;//this is restricted
    bool m_upgradable;//everything but the max size is upgradable.
    unsigned int m_goldValue; //how much one jewel is worth
    double m_weight; //how much it weighs make this a meta
};

#endif /* JEWEL_H */