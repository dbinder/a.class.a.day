//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <vegetable.h>
//constructor
vegetable::vegetable():
  m_amount(0),
  m_flavor(nullptr),
  m_type(nullptr)
{}
vegetable::vegetable(const vegetable &rhs):
  m_amount(rhs.m_amount),
  m_flavor(rhs.m_flavor),
  m_type(rhs.m_type)
{}
vegetable::vegetable(vegetable &&rhs):
  m_amount(0),
  m_flavor(nullptr),
  m_type(nullptr)
{
  m_amount = rhs.m_amount;
  m_flavor = rhs.m_flavor;
  m_type = rhs.m_type;

  rhs.m_amount = 0;
  rhs.m_flavor = nullptr;
  rhs.m_type = nullptr;
}
//destructor
vegetable::~vegetable(){}
//assignment operator
vegetable& vegetable::operator=(const vegetable &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_flavor = rhs.m_flavor;
    m_type = rhs.m_type;
  }
  return *this;
}

vegetable& vegetable::operator=(vegetable &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_flavor = rhs.m_flavor;
    m_type = rhs.m_type;

    rhs.m_amount = 0;
    rhs.m_flavor = nullptr;
    rhs.m_type = nullptr;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const vegetable &rhs)
{
  output << rhs.m_amount
    << rhs.m_flavor
    << rhs.m_type;
  return output;
}
std::istream &operator>>(std::istream &input, vegetable &lhs)
{
  input >> lhs.m_amount
    >> lhs.m_flavor
    >> lhs.m_type;
  return input;
}


