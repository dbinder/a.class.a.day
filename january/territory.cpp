//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <territory.h>
//constructor
territory::territory():
  m_width(0),
  m_length(0)
  {}

territory::territory(unsigned int width, unsigned int length)
{
  m_width = width;
  m_length = length;
}

//destructor
territory::~territory(){}
//copy constructor
territory::territory(const territory &rhs)
{
  m_width = rhs.m_width;
  m_length = rhs.m_length;
}
//assignment operator
territory& territory::operator=(const territory &rhs)
{
  return *this;
}

//isotream operators
std::ostream &operator<<(std::ostream &output, const territory &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, territory &lhs)
{
  return input;
}

//arithmetic operators
territory territory::operator+(const territory &rhs)
{
  territory ter;
  ter.m_width = this->m_width + rhs.m_width;
  ter.m_length = this->m_length + rhs.m_length;
  return ter;
}
territory territory::operator-(const territory &rhs)
{
  territory ter;
  ter.m_width = this->m_width - rhs.m_width;
  ter.m_length = this->m_length - rhs.m_length;
  return ter;

}
//comparison operators
bool territory::operator==(const territory &rhs)
{
  return this->m_length == rhs.m_length &&
    this->m_width == rhs.m_width;
}
bool territory::operator!=(const territory &rhs)
{
  return !(*this == rhs);
}
bool territory::operator>(const territory &rhs)
{
  return this->getSquareFeet() > rhs.getSquareFeet();
}
bool territory::operator<(const territory &rhs)
{
  return this->getSquareFeet() < rhs.getSquareFeet();
}
bool territory::operator>=(const territory &rhs)
{
  return this->getSquareFeet() >= rhs.getSquareFeet();
}
bool territory::operator<=(const territory &rhs)
{
  return this->getSquareFeet() <= rhs.getSquareFeet();
}


//Methods
double territory::getAcres()
{
  //1 acre equals 4,840 sqaure yards
  //1 yard = 3 feet
  return m_length * m_width * 9;
}
unsigned int territory::getSquareFeet() const
{
  return m_length * m_width;
}
double territory::getSquareMeters()
{
  // 1 foot = .3048 meters
  return m_length * m_width * 0.3048 * 0.3048;
}
double territory::getSquareInches()
{
  return  m_length/12.0 * m_width/12.0;
}

//Get
unsigned int territory::getWidth()
{
  return m_width;
}
unsigned int territory::getLength()
{
  return m_length;
}

//Set
void territory::setWidth(unsigned int i)
{
  m_width = i;
}
void territory::setLength(unsigned int i)
{
  m_length = i;
}