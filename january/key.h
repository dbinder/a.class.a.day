//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef KEY_H
#define KEY_H
#include <iostream>
class key
{
  enum type
  {
    DOOR,
    CHEST,
    WINDOW
  };
  enum owner
  {
    GOBLIN,
    PIRATE,
    SKELETON
  };
  public:
    //constructor
    key();
    //copy constuctor
    key(const key &rhs);
    //move constructor
    key(key &&rhs);
    //destructor
    ~key();
    //assignment operator
    key& operator=(const key &rhs);
    //move assignement
    key& operator=(key &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const key &rhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    key::type m_type;
    key::owner m_owner;

};

#endif /* KEY_H */