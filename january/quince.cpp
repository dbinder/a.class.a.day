//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <quince.h>
//constructor
quince::quince():
  m_amount(0),
  m_ripe(E_NOPE)
{}
//destructor
quince::~quince(){}
std::ostream &operator<<(std::ostream &output, const quince &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, quince &lhs)
{
  return input;
}
