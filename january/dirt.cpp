//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <dirt.h>
//constructor
dirt::dirt():
  m_weight(0)
{}
//copy constructor
dirt::dirt(const dirt &rhs):
  m_weight(rhs.m_weight)
{}
//move constructor
dirt::dirt(dirt &&rhs):
  m_weight(0)
{
  m_weight = rhs.m_weight;
  rhs.m_weight = 0;
}
//destructor
dirt::~dirt(){}
//assignment operator
dirt& dirt::operator=(const dirt &rhs)
{
  if (this != &rhs)
  {
    m_weight = rhs.m_weight;
  }
  return *this;
}
//move assignment operator
dirt& dirt::operator=(dirt &&rhs)
{
  if (this != &rhs)
  {
    m_weight = rhs.m_weight;
    rhs.m_weight = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const dirt &rhs)
{
  output << rhs.m_weight;
  return output;
}
std::istream &operator>>(std::istream &input, dirt &lhs)
{
  input >> lhs.m_weight;
  return input;
}
