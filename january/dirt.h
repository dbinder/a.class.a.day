//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DIRT_H
#define DIRT_H
#include <iostream>
//TODO dirt finish this class
class dirt
{
  public:
    //constructor
    dirt();
    //copy constructor
    dirt(const dirt &rhs);
    //move constructor
    dirt(dirt &&rhs);
    //destructor
    ~dirt();
    //assignment operator
    dirt& operator=(const dirt &rhs);
    //move assignment
    dirt& operator=(dirt &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const dirt &rhs);
    friend std::istream& operator>>(std::istream &input, dirt &lhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    float m_weight;
};

#endif /* DIRT_H */