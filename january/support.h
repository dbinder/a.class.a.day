//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SUPPORT_H
#define SUPPORT_H
#include <iostream>
#include <time.h>
//TODO support finish this class
class support
{
  public:
    //constructor
    support();
    //destructor
    ~support();
    //copy constuctor
    support(const support &rhs);
    //assignment operator
    support& operator=(const support &rhs);
    void swap(support &rhs);
    friend std::ostream& operator<<(std::ostream &output, const support &rhs);
    friend std::istream& operator>>(std::istream &input, support &lhs);//not implemented
    //Methods
    //Get
    const char* getTicket() const;
    time_t getTime() const;
    //Set
    void setTicket(char *n);
  protected:
  private:
    char *m_ticket;
    time_t m_duration;
};

#endif /* SUPPORT_H */