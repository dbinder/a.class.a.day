//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <Gold.h>
//constructor
Gold::Gold():
 	m_amount(0), 
  m_worth(0),
  m_value(0)
  {}

Gold::Gold(unsigned int i)
{
  m_amount = i;
}
//TODO: Limit CHECKS and errors
//destructor
Gold::~Gold(){}
//copy constructor
Gold::Gold(const Gold &rhs)
{
  m_amount = rhs.m_amount;
  m_worth = rhs.m_worth;
  m_value = rhs.m_value;
}

Gold& Gold::operator=(const Gold &rhs)
{
  if (this == &rhs)
    return *this;
    this->m_amount = rhs.m_amount;
    this->m_worth = rhs.m_worth;
    this->m_value = rhs.m_value;
  return *this;
}

std::ostream& operator<<(std::ostream &output, const Gold &rhs)
{
  output << rhs.m_amount
    << rhs.m_worth
    << rhs.m_value
    << std::endl;
  return output;
}

std::istream& operator>>(std::istream &input, Gold &lhs)
{
  input >> lhs.m_amount
    >> lhs.m_worth
    >> lhs.m_value;
  return input;
}

//arithmetic operators
Gold Gold::operator+(const Gold &rhs)
{
  return Gold(this->m_amount + rhs.m_amount);
}

Gold Gold::operator-(const Gold &rhs)
{
  //TODO check negative. unsigned will do bad things careful, can't go negative

  return Gold(this->m_amount - rhs.m_amount);
}

bool Gold::operator==(const Gold &rhs)
{
  return this->m_amount == rhs.m_amount &&
    this->m_value == rhs.m_value &&
    this->m_worth == rhs.m_worth;
}

bool Gold::operator!=(const Gold &rhs)
{
  return !(*this == rhs);
}

bool Gold::operator>(const Gold &rhs)
{
  return this->m_amount > rhs.m_amount;
}
bool Gold::operator<(const Gold &rhs)
{
  return this->m_amount < rhs.m_amount;
}
bool Gold::operator>=(const Gold &rhs)
{
  return this->m_amount >= rhs.m_amount;
}
bool Gold::operator<=(const Gold &rhs)
{
  return this->m_amount <= rhs.m_amount;
}



//Methods
//Increment
void Gold::add(unsigned int i)
{
  //check for max
  if ((m_amount + i) < m_amount)
  {
    m_amount = UINT_MAX;
  }
  else
  {
    m_amount += i;
  }
}
//decrement
void Gold::subtract(unsigned int i)
{
  //check for min
  if ((m_amount - i) > m_amount)
  {
    m_amount = 0;
  }
  else
  {
    m_amount -= i;
  }
}
//caluclate worth. 
double Gold::value() 
{
  m_value = m_amount * m_worth;
  return m_value;
}

//Get
unsigned int Gold::getAmount() const{ return m_amount; }
double Gold::getWorth() const { return m_worth; }
double Gold::getValue(){ return value(); }

//Set
void Gold::setAmount(unsigned int i)
{
  m_amount = i;
}
void Gold::setWorth(double n)
{
  m_worth = n;
}
