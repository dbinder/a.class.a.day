//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef RECORD_H
#define RECORD_H
#include <iostream>
#include <vector>
#include <string>
//A record as in one you play on a turn-table
//TODO record class, Maybe make a track to go with thise one. 
class record
{
  enum rpm
  {
    SIXTEEN,
    THIRTYTHREE,
    FOURTYFIVE,
    SEVENTYEIGHT
  };
  enum diameter
  {
    SEVEN,
    TEN,
    TWELVE
  };
  public:
    //constructor
    record();
    //copy constuctor
    record(const record &rhs);
    //move constructor
    record(record &&rhs);
    //destructor
    ~record();
    //assignment operator
    record& operator=(const record &rhs);
    //move assignment operator
    record& operator=(record &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const record &rhs);
    friend std::istream& operator>>(std::istream &input, record &lhs);
    friend std::istream& operator>>(std::istream &input, rpm &lhs);
    friend std::istream& operator>>(std::istream &input, diameter &lhs);
    //Methods
    //Get
    std::vector<const char *> getTracks() const;

    //Set
  protected:
  private:
    std::vector<const char*> m_tracklist;
    record::rpm m_rpm;
    record::diameter m_diameter;
    unsigned int m_year;
    std::string m_artist;
    std::string m_publisher;
    time_t m_duration;
};

#endif /* RECORD_H */