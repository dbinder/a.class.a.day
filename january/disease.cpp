//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <disease.h>

//constructor
disease::disease():
  m_duration(0),
  m_damage(0),
  m_modifier(0)
{}
//copy constructor
disease::disease(const disease &rhs)
{
  m_duration = rhs.m_duration;
  m_damage = rhs.m_damage;
  m_modifier = rhs.m_modifier;
}
disease& disease::operator=(const disease &rhs)
{
  if (this == &rhs)
    return *this;
  m_duration = rhs.m_duration;
  m_damage = rhs.m_damage;
  m_modifier = rhs.m_modifier;
  return *this;
}
//destructor
disease::~disease(){}

std::ostream &operator<<(std::ostream &output, const disease &rhs)
{
  output << rhs.m_duration
    << rhs.m_damage
    << rhs.m_modifier
    << std::endl;
  return output;
}
std::istream &operator>>(std::istream &input, disease &lhs)
{
  input >> lhs.m_duration
    >> lhs.m_damage
    >> lhs.m_modifier;
  return input;
}

//Methods
double disease::calcDot() const{ return m_duration * m_modifier * m_damage; }
//Get
unsigned int disease::getDuration() const{ return m_duration; }
double disease::getDamage() const{ return m_damage; }
double disease::getModifier() const{ return m_modifier; }
//Set
void disease::setDuration(unsigned int i)
{
  m_duration = i;
}
void disease::setDamage(double n)
{
  m_damage = n;
}
void disease::setModifier(double n)
{
  m_modifier = n;
}