//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <trade.h>
//constructor
trade::trade():
  m_happen(false)
{}
trade::trade(const trade &rhs):
  m_happen(rhs.m_happen)
{}
//move constructor
trade::trade(trade &&rhs):
m_happen(false)
{
  m_happen = rhs.m_happen;
  rhs.m_happen = false;
}
//destructor
trade::~trade(){}
//assignment operator
trade& trade::operator=(const trade &rhs)
{
  if (this != &rhs)
  {
    m_happen = rhs.m_happen;
  }
  return *this;
}
//move assignment
trade& trade::operator=(trade &&rhs)
{
  if (this != &rhs)
  {
    m_happen = rhs.m_happen;
    rhs.m_happen = false;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const trade &rhs)
{
  output << rhs.m_happen;
  return output;
}
std::istream &operator>>(std::istream &input, trade &lhs)
{
  input >> lhs.m_happen;
  return input;
}
