//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <sack.h>
//constructor
sack::sack() :
m_slots(0)
{}
//destructor
sack::~sack(){}
sack::sack(const sack &rhs)
{
  m_slots = rhs.m_slots;
}

//assignment operator
sack& sack::operator=(const sack &rhs)
{
  if (this == &rhs)
    return *this;
  m_slots = rhs.m_slots;
  return *this;
}
std::ostream &operator<<(std::ostream &output, const sack &rhs)
{
  output << rhs.m_slots
    << std::endl;
  return output;
}

std::istream &operator>>(std::istream &input, sack &lhs)
{
  input >> lhs.m_slots;
  return input;
}

//methods
//get
unsigned int sack::getSack() const
{
  return m_slots;
}
//set
void sack::setSack(unsigned int i)
{
  m_slots = i;
}