//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <self.h>
//constructor
self::self():
  m_self("I")
{}
self::self(const self &rhs)
{
  m_self = rhs.m_self;
}
//destructor
self::~self()
{
  delete[] m_self;
}
//copy assignment operator
self& self::operator=(const self &rhs)
{
  if (this == &rhs)
    return *this;
  self temp(rhs);
  swap(temp);
  return *this;
}

void self::swap(self &rhs)
{
  using std::swap;
  swap(this->m_self, rhs.m_self);
}
std::ostream &operator<<(std::ostream &output, const self &rhs)
{
  output << rhs.m_self;
  return output;
}

//Get
const char* self::getSelf() const{ return m_self; }
//Set
void self::setSelf(char *n)
{
  m_self = n;
}