//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SELF_H
#define SELF_H
#include <iostream>
class self
{
  public:
    //constructor
    self();
    //destructor
    ~self();
    //copy constuctor
    self(const self &rhs);
    //copy assignment operator
    self& operator=(const self &rhs);
    void swap(self &rhs);
    friend std::ostream& operator<<(std::ostream &output, const self &rhs);
    friend std::istream& operator>>(std::istream &input, self &lhs);//not implemented
    //Methods
    //Get
    const char* getSelf() const;
    //Set
    void setSelf(char *);
  protected:
  private:
    const char *m_self;
};

#endif /* SELF_H */