//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SUBSTANCE_H
#define SUBSTANCE_H
#include <iostream>
//substance a type of thing, hmm maybe have types, like a food type, drink type? 
class substance
{
  //TODO substance
  enum type
  {
    FOOD,
    DRINK
  };
  public:
    //constructor
    substance();
    //copy constuctor
    substance(const substance &rhs);
    //move constructor
    substance(substance &&rhs);
    //destructor
    ~substance();
    //assignment operator
    substance& operator=(const substance &rhs);
    //move assignment
    substance& operator=(substance &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const substance &rhs);
    friend std::istream& operator>>(std::istream &input, substance &lhs);
    friend std::istream& operator>>(std::istream &input, type &lhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    substance::type m_type;

};

#endif /* SUBSTANCE_H */