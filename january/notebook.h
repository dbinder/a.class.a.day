//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef NOTEBOOK_H
#define NOTEBOOK_H
#include <iostream>
class notebook
{
  public:
    //constructor
    notebook();
    //destructor
    ~notebook();
    //copy constuctor
    notebook(const notebook &rhs);
    //move constructor
    notebook(notebook&& rhs);
    //assignment operator
    notebook& operator=(const notebook &rhs);
    //swap
    void swap(notebook &rhs);
    friend std::ostream& operator<<(std::ostream &output, const notebook &rhs);
    friend std::istream& operator>>(std::istream &input, notebook &lhs);
    //Methods
    //Get
    const char* getData() const;
    unsigned int getPages() const;
    //Set
    void setPages(unsigned int i);
    void setData(char *c);
  protected:
  private:
    unsigned int m_pages;
    const char *m_data;
};

#endif /* NOTEBOOK_H */