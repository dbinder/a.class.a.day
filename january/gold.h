//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef GOLD_H
#define GOLD_H
#include <iostream>

class Gold
{
  public:
    //constructor
    Gold();
    Gold(unsigned int i);
    //destructor
    ~Gold();
    //copy constructor
    Gold(const Gold &rhs);
    //assignment constructor
    Gold& operator=(const Gold &rhs);

    //iostream operator overloading
    friend std::ostream& operator<<(std::ostream &output, const Gold &rhs);
    friend std::istream& operator>>(std::istream &input, Gold &lhs);

    //arithmetic operators
    Gold operator+(const Gold &rhs);
    Gold operator-(const Gold &rhs);
    //comparison operators
    bool operator==(const Gold &rhs);
    bool operator!=(const Gold &rhs);
    bool operator>(const Gold &rhs);
    bool operator<(const Gold &rhs);
    bool operator>=(const Gold &rhs);
    bool operator<=(const Gold &rhs);

    //Methods
    //increment
    //plus some value
    void add(unsigned int i);
    //decrement
    void subtract(unsigned int i);
    //caluclate worth. 
    double value();

    //Get
    unsigned int getAmount() const;
    double getWorth() const;
    double getValue();
    //Set
    void setAmount(unsigned int i);
    void setWorth(double n);
  protected:
  private:
    unsigned int m_amount;
    double m_worth;
    double m_value;
};

#endif /* GOLD_H */