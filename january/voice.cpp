//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <voice.h>
//constructor
voice::voice():
  m_tone(nullptr)
{}
voice::voice(const char *rhs):
  m_tone(new char[std::strlen(rhs) + 1])
{
  memcpy(m_tone, rhs, std::strlen(rhs) + 1);
}
//destructor
voice::~voice()
{
  if (m_tone)
  {
    delete m_tone;
  }
}
//copy constructor
voice::voice(const voice &rhs):
  m_tone(new char[std::strlen(rhs.m_tone) + 1])
{
  memcpy(m_tone, rhs.m_tone, std::strlen(rhs.m_tone) + 1);
}
//copy assignment operator
voice& voice::operator=(const voice &rhs)
{
  if (this != &rhs)
  {
    delete m_tone;
    m_tone = new char[std::strlen(rhs.m_tone) + 1];
    memcpy(m_tone, rhs.m_tone, std::strlen(rhs.m_tone) + 1);
  }
  return *this;
}
//move constructor
voice::voice(voice&& rhs):
  m_tone(nullptr)
{
  m_tone = rhs.m_tone;
  rhs.m_tone = nullptr;
}
//move assignment;
voice& voice::operator=(voice&& rhs)
{
  if (this != &rhs)
  {
    // Free the existing resource.
    delete m_tone;
    // Copy the data pointer from the source object.
    m_tone = rhs.m_tone;
    // Release the data pointer from the source object so that
    // the destructor does not free the memory multiple times.
    rhs.m_tone = nullptr;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const voice &rhs)
{
  output << rhs.m_tone
    << std::endl;
  return output;
}
std::istream &operator>>(std::istream &input, voice &lhs)
{
  input >> lhs.m_tone;
  return input;
}
