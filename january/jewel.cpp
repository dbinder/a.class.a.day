//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <jewel.h>
//constructor
jewel::jewel():
  m_color("Red"),
  m_size(1),
  m_upgradable(true),
  m_goldValue(10)
{}
jewel::jewel(const jewel &rhs):
  m_color(rhs.m_color),
  m_size(rhs.m_size),
  m_upgradable(rhs.m_upgradable),
  m_goldValue(rhs.m_goldValue), 
  m_weight(rhs.m_weight)
{}
jewel::jewel(jewel &&rhs) :
  m_color(""),
  m_size(0),
  m_upgradable(false),
  m_goldValue(0),
  m_weight(0)
{
  m_color = rhs.m_color;
  m_size = rhs.m_size;
  m_upgradable = rhs.m_upgradable;
  m_goldValue = rhs.m_goldValue;
  m_weight = rhs.m_weight;

  rhs.m_color = "";
  rhs.m_size = 0;
  rhs.m_upgradable = 0;
  rhs.m_goldValue = 0;
  rhs.m_weight = 0;
}
//destructor
jewel::~jewel(){}
jewel& jewel::operator=(const jewel &rhs)
{
  if (this != &rhs)
  {
    m_color = rhs.m_color;
    m_size = rhs.m_size;
    m_upgradable = rhs.m_upgradable;
    m_goldValue = rhs.m_goldValue;
    m_weight = rhs.m_weight;
  }
  return *this;
}
jewel& jewel::operator=(jewel &&rhs)
{
  if (this != &rhs)
  {
    m_color = rhs.m_color;
    m_size = rhs.m_size;
    m_upgradable = rhs.m_upgradable;
    m_goldValue = rhs.m_goldValue;
    m_weight = rhs.m_weight;

    rhs.m_color = "";
    rhs.m_size = 0;
    rhs.m_upgradable = 0;
    rhs.m_goldValue = 0;
    rhs.m_weight = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const jewel &rhs)
{
  output << rhs.m_color
    << rhs.m_size
    << rhs.m_upgradable
    << rhs.m_goldValue
    << rhs.m_weight;
  return output;
}
std::istream &operator>>(std::istream &input, jewel &lhs)
{
  input >> lhs.m_color
    >> lhs.m_size
    >> lhs.m_upgradable
    >> lhs.m_goldValue
    >> lhs.m_weight;
  return input;
}
