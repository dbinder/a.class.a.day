//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <room.h>
//constructor
room::room():
  m_length(0),
  m_width(0),
  m_height(0),
  m_door(true),
  m_window(true),
  m_light(true)
{}
room::room(const room &rhs):
  m_length(rhs.m_length),
  m_width(rhs.m_width),
  m_height(rhs.m_height),
  m_door(rhs.m_door),
  m_window(rhs.m_window),
  m_light(rhs.m_light)
{}
//move constructor
room::room(room &&rhs):
  m_length(0),
  m_width(0),
  m_height(0),
  m_door(true),
  m_window(true),
  m_light(true)
{
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  m_height = rhs.m_height;
  m_door = rhs.m_door;
  m_window = rhs.m_window;
  m_light = rhs.m_light;

  rhs.m_length = 0;
  rhs.m_width = 0;
  rhs.m_height = 0;
  rhs.m_door =  true;
  rhs.m_window = true;
  rhs.m_light = true;

}
//destructor
room::~room(){}
//assignment operator
room& room::operator=(const room &rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    m_height = rhs.m_height;
    m_door = rhs.m_door;
    m_window = rhs.m_window;
    m_light = rhs.m_light;
  }
  return *this;
}
//move assignment
room& room::operator=(room &&rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    m_height = rhs.m_height;
    m_door = rhs.m_door;
    m_window = rhs.m_window;
    m_light = rhs.m_light;

    rhs.m_length = 0;
    rhs.m_width = 0;
    rhs.m_height = 0;
    rhs.m_door = true;
    rhs.m_window = true;
    rhs.m_light = true;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const room &rhs)
{
  output << rhs.m_length
    << rhs.m_width
    << rhs.m_height
    << rhs.m_door
    << rhs.m_window
    << rhs.m_light;
  return output;
}
std::istream &operator>>(std::istream &input, room &lhs)
{
  input >> lhs.m_length
    >> lhs.m_width
    >> lhs.m_height
    >> lhs.m_door
    >> lhs.m_window
    >> lhs.m_light;
  return input;
}
//Methods
double room::area()
{
  return m_length * m_width * m_height;
}
//Get
bool room::getDoor() const
{
  return m_door;
}
bool room::getWindow() const
{
  return m_window;
}
bool room::getLight() const
{
  return m_light;
}
//Set