//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef VOICE_H
#define VOICE_H
#include <iostream>
//TODO voice finish this class
class voice
{
  public:
    //constructor
    voice();
    voice(const char *rhs);
    //destructor
    ~voice();
    //copy constuctor
    voice(const voice &rhs);
    //copy assignment operator
    voice& operator=(const voice &rhs);
    //move constructor
    voice(voice&& rhs);
    //move assignment operator
    voice& operator=(voice&& rhs);

    friend std::ostream& operator<<(std::ostream &output, const voice &rhs);
    friend std::istream& operator>>(std::istream &input, voice &lhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    char *m_tone;
};

#endif /* VOICE_H */