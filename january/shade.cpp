//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <shade.h>
//constructor
shade::shade() :
  m_length(0),
  m_width(0),
  m_height(0),
  m_area(0),
  m_volume(0)
{}

shade::shade(double length, double width, double height)
{
  m_length = length;
  m_width = width;
  m_height = height;
}

shade::shade(const shade &rhs)
{
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  m_height = rhs.m_height;
  m_area = rhs.m_area;
  m_volume = rhs.m_volume;
}

shade& shade::operator=(const shade &rhs)
{
  if (this == &rhs)
    return *this;
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  m_height = rhs.m_height;
  m_area = rhs.m_area;
  m_volume = rhs.m_volume;

  return *this;
}
//destructor
shade::~shade(){}
std::ostream &operator<<(std::ostream &output, const shade &rhs)
{
  output << rhs.m_length
    << rhs.m_width
    << rhs.m_height
    << rhs.m_area
    << rhs.m_volume;
  return output;
}
std::istream &operator>>(std::istream &input, shade &lhs)
{ //take precacluted numbers
  input >> lhs.m_length
    >> lhs.m_width
    >> lhs.m_height
    >> lhs.m_area
    >> lhs.m_volume;
  return input;
}

//arithmetic operators
shade shade::operator+(const shade &rhs)
{
  return shade(this->m_length + rhs.m_length,
    this->m_width + rhs.m_width,
    this->m_height + rhs.m_height);
}
shade shade::operator-(const shade &rhs)
{
  return shade(this->m_length - rhs.m_length,
    this->m_width - rhs.m_width,
    this->m_height - rhs.m_height);
}
//comparison operators
bool shade::operator==(const shade &rhs)
{
  return this->m_length == rhs.m_length 
    && this->m_width == rhs.m_width
    && this->m_height == rhs.m_height;
}
bool shade::operator!=(const shade &rhs)
{
  return !(this == &rhs);
}
bool shade::operator>(const shade &rhs)
{
  return this->m_length > rhs.m_length
    && this->m_width > rhs.m_width
    && this->m_height > rhs.m_height;
}
bool shade::operator<(const shade &rhs)
{
  return this->m_length < rhs.m_length
    && this->m_width < rhs.m_width
    && this->m_height < rhs.m_height;
}
bool shade::operator>=(const shade &rhs)
{
  return this->m_length >= rhs.m_length
    && this->m_width >= rhs.m_width
    && this->m_height >= rhs.m_height;
}
bool shade::operator<=(const shade &rhs)
{
  return this->m_length <= rhs.m_length
    && this->m_width <= rhs.m_width
    && this->m_height <= rhs.m_height;
}

//Methods
void shade::area()
{
  m_area = m_length * m_width;
}
void shade::volume()
{
  m_volume = m_length * m_width * m_height;
}
//Get
double shade::getLength() const{ return m_length; }
double shade::getWidth() const{ return m_width; }
double shade::getHeight() const{ return m_height; }
double shade::getArea() const{ return m_area; }
double shade::getVolume() const{ return m_volume;}
//Set
void shade::setLength(double n)
{
  m_length = n;
}
void shade::setWidth(double n)
{
  m_width = n;
}
void shade::setHeigth(double n)
{
  m_height = n;
}