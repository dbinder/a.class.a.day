//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <pie.h>
//constructor
pie::pie():
  m_flavor(APPLE),
  m_slicesLeft(8)
{}
pie::pie(const pie &rhs) :
  m_flavor(rhs.m_flavor),
  m_slicesLeft(rhs.m_slicesLeft)
{}
//destructor
pie::~pie(){}
pie& pie::operator=(const pie &rhs)
{
  if (this == &rhs)
    return *this;
  pie temp(rhs);
  swap(temp);
  return *this;
}
void pie::swap(pie &rhs)
{
  using std::swap;
  swap(this->m_flavor, rhs.m_flavor);
  swap(this->m_slicesLeft, rhs.m_slicesLeft);
}
std::ostream &operator<<(std::ostream &output, const pie &rhs)
{
  output << rhs.m_flavor
    << rhs.m_slicesLeft;
  return output;
}
std::istream &operator>>(std::istream &input, pie &lhs)
{
  input >> lhs.m_slicesLeft;
  return input;
}

pie::flavor pie::getFlavor()
{
  return m_flavor;
}

void pie::flavorize(pie::flavor i)
{
  m_flavor = i;
}