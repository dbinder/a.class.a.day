//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef STOMACH_H
#define STOMACH_H
#include <iostream>
//Where food goes
class stomach
{
  //add more maybe?
  enum state
  {
    EMPTY,
    HALF,
    FULL
  };
  public:
    //constructor
    stomach();


    //copy constuctor
    stomach(const stomach &rhs);
    //move constructor
    stomach(stomach &&rhs);
    //destructor
    ~stomach();
    //copy assignment operator
    stomach& operator=(const stomach &rhs);
    //move assignment operator
    stomach& operator=(stomach &&rhs);
    //Methods
    //Get
    stomach::state getState() const;
    //Set
  protected:
  private:
    stomach::state m_state;

};

#endif /* STOMACH_H */