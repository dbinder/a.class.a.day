//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TERRITORY_H
#define TERRITORY_H
#include <iostream>
//TODO make operators member functions. 
class territory
{
  public:
    //constructor
    territory();
    territory(unsigned int i, unsigned int j);
    //destructor
    ~territory();
    //copy constructor
    territory(const territory &rhs);
    //assignment construstor
    territory& operator=(const territory &rhs);

    //isotream operators
    friend std::ostream &operator<<(std::ostream &output, const territory &rhs);
    friend std::istream &operator>>(std::istream &input, territory &lhs);

    //arithmetic operators
    territory operator+(const territory &rhs);
    territory operator-(const territory &rhs);
    //comparison operators
    bool operator==(const territory &rhs);
    bool operator!=(const territory &rhs);
    bool operator>(const territory &rhs);
    bool operator<(const territory &rhs);
    bool operator>=(const territory &rhs);
    bool operator<=(const territory &rhs);



    //Methods
    double getAcres();
    unsigned int getSquareFeet() const;
    double getSquareMeters();
    double getSquareInches();
    //Get
    unsigned int getWidth();
    unsigned int getLength();
    //Set
    void setWidth(unsigned int i);
    void setLength(unsigned int i);

  protected:
  private:
    unsigned int m_width;//in feet
    unsigned int m_length;//in feet
};

#endif /* TERRITORY_H */