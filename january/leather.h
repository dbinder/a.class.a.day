//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LEATHER_H
#define LEATHER_H
#include <iostream>
//TODO leather finish this class
class leather
{
  public:
    //constructor
    leather();
    //copy constuctor
    leather(const leather &rhs);
    //move constructor
    leather(leather &&rhs);
    //destructor
    ~leather();
    //assignment operator
    leather& operator=(const leather &rhs);
    //move assignment operator
    leather& operator=(leather &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const leather &rhs);
    friend std::istream& operator>>(std::istream &input, leather &lhs);
    //Methods
    double getArea() const;

    //Get
    unsigned int getThreadCount() const;
    unsigned int getAmount() const;
    double getLength() const;
    double getWidth() const;
    //Set
    void setThreadCount(unsigned int i);
    void setAmount(unsigned int i);
    void setLength(double n);
    void setWidth(double n);

  protected:
  private:
    unsigned int m_amount;
    double m_length;
    double m_width;
};

#endif /* LEATHER_H */