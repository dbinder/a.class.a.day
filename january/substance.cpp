//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <substance.h>
//constructor
substance::substance():
  m_type(FOOD)
{}
substance::substance(const substance &rhs):
  m_type(rhs.m_type)
{}
substance::substance(substance &&rhs):
  m_type(FOOD)
{
  m_type = rhs.m_type;
  rhs.m_type = FOOD;
}
//destructor
substance::~substance(){}
//assignment operator
substance& substance::operator=(const substance &rhs)
{
  if (this != &rhs)
  {
    m_type = rhs.m_type;
  }
  return *this;
}
substance& substance::operator=(substance &&rhs)
{
  if (this != &rhs)
  {
    m_type = rhs.m_type;
    rhs.m_type = FOOD;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const substance &rhs)
{
  output << rhs.m_type;
  return output;
}
std::istream &operator>>(std::istream &input, substance &lhs)
{
  input >> lhs.m_type;
  return input;
}
std::istream& operator>>(std::istream &input, substance::type &lhs)
{
  int inVal;
  input >> inVal;
  switch (inVal)
  {
  case 0:
    lhs = substance::type::FOOD;
    break;
  case 1:
    lhs = substance::type::DRINK;
    break;
  default:
    lhs = substance::type::FOOD;
    break;
  }
  return input;
}