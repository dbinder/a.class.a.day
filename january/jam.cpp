//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <jam.h>
//constructor
jam::jam():
  m_viscosity(0),
  m_flavor("Cherry")
{}

//copy constructor
jam::jam(const jam &rhs)
{
  m_flavor = rhs.m_flavor;
  m_viscosity = rhs.m_viscosity;
}
//assignment constructor
jam& jam::operator=(const jam &rhs)
{
  if (this == &rhs)
    return *this;
  m_flavor = rhs.m_flavor;
  m_viscosity = rhs.m_viscosity;
  return *this;
}
//destructor
jam::~jam(){}
std::ostream &operator<<(std::ostream &output, const jam &rhs)
{
  output << rhs.m_viscosity
    << rhs.m_viscosity
    << std::endl;
  return output;
}
std::istream &operator>>(std::istream &input, jam &lhs)
{
  input >> lhs.m_viscosity
    >> lhs.m_flavor;
  return input;
}

//Get
double jam::getViscosity() const{ return m_viscosity; }
std::string jam::getFlavor() const{ return m_flavor; }
//Set
void jam::setViscosity(double n)
{
  m_viscosity = n;
}
void jam::setFlavor(std::string n)
{
  m_flavor = n;
}