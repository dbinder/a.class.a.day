//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef VAN_H
#define VAN_H
#include <iostream>
#include <truck.h>
//TODO van finish this class
class van : public truck
{
  public:
    //constructor
    van();
    //copy constuctor
    van(const van &rhs);
    //move constructor
    van(van &&rhs);
    //destructor
    ~van();
    //assignment operator
    van& operator=(const van &rhs);
    //move assignment
    van& operator=(van &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const van &rhs);

    //Methods
    //Get
    //Set
  protected:
  private:
    unsigned int m_numdoors;
};

#endif /* VAN_H */