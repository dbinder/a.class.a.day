//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <nation.h>
//constructor
nation::nation():
  m_population(0),
  m_area(0),
  m_leader("Leader"),
  m_gop(0)
{}
//copy constructor
nation::nation(const nation &rhs)
{
  m_population = rhs.m_population;
  m_area = rhs.m_area;
  m_leader = rhs.m_leader;
  m_gop = rhs.m_gop;
}

//destructor
nation::~nation(){}
//copy assignment constructor
nation& nation::operator=(const nation &rhs)
{
  if (this == &rhs)
    return *this;
  nation temp(rhs);
  swap(temp);
  return *this;
}
//swap
void nation::swap(nation &rhs)
{
  using std::swap;
  swap(this->m_population, rhs.m_population);
  swap(this->m_area, rhs.m_area);
  swap(this->m_leader, rhs.m_leader);
  swap(this->m_gop, rhs.m_gop);
}

std::ostream &operator<<(std::ostream &output, const nation &rhs)
{
  output << rhs.m_population
    << rhs.m_area
    << rhs.m_leader
    << rhs.m_gop;
  return output;
}
std::istream &operator>>(std::istream &input, nation &lhs)
{
  //input date minus great leader;
  input >>lhs.m_population
    >> lhs.m_area
    >> lhs.m_gop;
  return input;
}
unsigned int nation::getPopulation() const{ return m_population; }
double nation::getArea() const{ return m_area; }
const char* nation::getLeader() const{ return m_leader; }
double nation::getGop() const{ return m_gop; }

//Set
void nation::setPopulation(unsigned int i)
{
  m_population = i;
}
void nation::setArea(double n)
{
  m_area = n;
}
void nation::setLeader(char *n)
{
  m_leader = n;
}
void nation::setGop(double n)
{
  m_gop = n;
}