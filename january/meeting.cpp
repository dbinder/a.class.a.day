//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <meeting.h>
//constructor
meeting::meeting() :
  m_location(nullptr),
  m_duration(0),
  m_topic(nullptr),
  m_attendess(nullptr)
{}
meeting::meeting(const char *n) :
  m_location(nullptr),
  m_duration(0),
  m_topic(nullptr),
  m_attendess(new char[std::strlen(n) + 1])
{
  memcpy(m_attendess, n, std::strlen(n) + 1);
}
//copy constructor
meeting::meeting(const meeting &rhs) :
  m_location(new char[std::strlen(rhs.m_location) + 1]),
  m_duration(rhs.m_duration),
  m_topic(new char[std::strlen(rhs.m_topic) + 1]),
  m_attendess(new char[std::strlen(rhs.m_attendess) + 1])
{
  memcpy(m_location, rhs.m_location, std::strlen(rhs.m_location) + 1);
  memcpy(m_topic, rhs.m_topic, std::strlen(rhs.m_topic) + 1);
  memcpy(m_attendess, rhs.m_attendess, std::strlen(rhs.m_attendess) + 1);
}
//move constructor
meeting::meeting(meeting &&rhs):
  m_location(nullptr),
  m_duration(0),
  m_topic(nullptr),
  m_attendess(nullptr)
{
  m_location = rhs.m_location; 
  m_duration = rhs.m_duration;
  m_topic = rhs.m_topic;
  m_attendess = rhs.m_attendess;

  rhs.m_location = nullptr;
  rhs.m_duration = 0;
  rhs.m_topic = nullptr;
  rhs.m_attendess = nullptr;
}
//destructor
meeting::~meeting()
{
  if (m_location)
  {
    delete m_location;
  }
  if (m_topic)
  {
    delete m_topic;
  }
  if (m_attendess)
  {//crashing dual delete, not newing somewhere?
    //or deleting a pointer that points to two things

    delete m_attendess;
  }
}

meeting& meeting::operator=(const meeting &rhs)
{
  if (this != &rhs)
  {
    meeting::~meeting();

    memcpy(m_location, rhs.m_location, std::strlen(rhs.m_location) + 1);
    m_duration = rhs.m_duration;
    memcpy(m_topic, rhs.m_topic, std::strlen(rhs.m_topic) + 1);
    memcpy(m_attendess, rhs.m_attendess, std::strlen(rhs.m_attendess) + 1);
  }
  return *this;
}

meeting& meeting::operator=(meeting &&rhs)
{
  if (this != &rhs)
  {
    meeting::~meeting();

    memcpy(m_location, rhs.m_location, std::strlen(rhs.m_location) + 1);
    m_duration = rhs.m_duration;
    memcpy(m_topic, rhs.m_topic, std::strlen(rhs.m_topic) + 1);
    memcpy(m_attendess, rhs.m_attendess, std::strlen(rhs.m_attendess) + 1);

    rhs.m_location = nullptr;
    rhs.m_duration = 0;
    rhs.m_topic = nullptr;
    rhs.m_attendess = nullptr;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const meeting &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, meeting &lhs)
{
  return input;
}
void meeting::setLocation(const char *n)
{
  m_location = (new char[std::strlen(n) + 1]);
  memcpy(m_location, n, std::strlen(n) + 1);
}
void meeting::setTopic(const char *n)
{
  m_topic = (new char[std::strlen(n) + 1]);
  memcpy(m_topic, n, std::strlen(n) + 1);
}
void meeting::setAttendess(const char *n)
{
  m_attendess = (new char[std::strlen(n) + 1]);
  memcpy(m_attendess, n, std::strlen(n) + 1);
}