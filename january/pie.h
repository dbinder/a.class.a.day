//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PIE_H
#define PIE_H
#include <iostream>
//Pie as in the edible kind
class pie
{
  public:
    enum flavor
    {
      APPLE,
      PECAN,
      CHERRY,
      KEYLIME
    };
    //constructor
    pie();
    //destructor
    ~pie();
    //copy constuctor
    pie(const pie &rhs);
    //assignment operator
    pie& operator=(const pie &rhs);
    //swap
    void swap(pie &rhs);
    friend std::ostream& operator<<(std::ostream &output, const pie &rhs);
    friend std::istream& operator>>(std::istream &input, pie &lhs);
    //Methods
    //Get
    flavor getFlavor();
    //Set
    void flavorize(pie::flavor i);
  protected:
  private:
    pie::flavor m_flavor;
    unsigned int m_slicesLeft;

};

#endif /* PIE_H */