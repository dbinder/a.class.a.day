//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once

#ifndef SISTER_H
#define SISTER_H

#include <iostream>
#include <string>

//What does a sister have?
//Age, name, height weight
//eye color, hair color. 

class sister
{
  public:
    //constructor
    sister();
    //destructor
    ~sister();
    //copy constructor
    sister(const sister &rhs);

    //assignment operator
    sister& operator=(const sister &rhs);
    friend std::ostream& operator<<(std::ostream &out, const sister &rhs);
    friend std::istream& operator>>(std::istream &in, sister &lhs);
    //comparison operators
    friend bool operator==(const sister &rhs1, const sister &rhs2);
    friend bool operator!=(const sister &rhs1, const sister &rhs2);

    std::string getFName();
    std::string getMName();
    std::string getLName();
    std::string getHairColor();
    std::string getEyeColor();
    unsigned int getAge() const;
    double getHeight();
    double getWeight();

    void setFName(std::string n);
    void setMName(std::string n);
    void setLName(std::string n);
    void setHairColor(std::string n);
    void setEyeColor(std::string n);
    void setAge(unsigned int i);
    void setHeight(double n);
    void setWeight(double n);

  protected:
  private:
    std::string m_fName;
    std::string m_mName;
    std::string m_lName;
    std::string m_hairColor;
    std::string m_eyeColor;
    unsigned int m_age;
    double m_height;
    double m_weight;
};

#endif /* SISTER_H */