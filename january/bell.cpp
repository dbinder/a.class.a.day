//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <Bell.h>
//constructor
Bell::Bell():
  m_weight(0),
  m_height(0),
  m_length(0),
  m_tone("c")
  {}
//destructor
Bell::~Bell()
{
  std::cout << "Bell destroyed";
}
//copy constructor
Bell::Bell(const Bell &rhs)
{
  m_weight = rhs.m_weight;
  m_height = rhs.m_height;
  m_length = rhs.m_length;
  m_tone = rhs.m_tone;
}

//assignment constructor
Bell& Bell::operator=(const Bell &rhs)
{
  if (this == &rhs)
    return *this;
  m_weight = rhs.m_weight;
  m_height = rhs.m_height;
  m_length = rhs.m_length;
  m_tone = rhs.m_tone;
  return *this;
}

std::ostream& operator<<(std::ostream &output, const Bell &rhs)
{
  output << rhs.m_weight
    << rhs.m_height
    << rhs.m_length
    << rhs.m_tone
    << std::endl;
  
  return output;
}
std::istream& operator>>(std::istream &input, Bell &lhs)
{
  input >> lhs.m_weight
    >> lhs.m_height
    >> lhs.m_length
    >> lhs.m_tone;
    return input;
}


//Methods

//Get
unsigned int Bell::getWeight() const { return m_weight; }
unsigned int Bell::getHeight() const { return m_height; }
unsigned int Bell::getLength() const { return m_length; }
std::string  Bell::getTone() const { return m_tone; }

//Set
void Bell::setWeight(unsigned int i)
{
  m_weight = i;
}
void Bell::setHeight(unsigned int i)
{
  m_height = i;
}
void Bell::setLength(unsigned int i)
{
  m_length = i;
}
void Bell::setTone(std::string n)
{
  m_tone = n;
}