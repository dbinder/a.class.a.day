//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SINK_H
#define SINK_H
#include <iostream>

class sink
{
  public:
    //constructor
    sink();
    //destructor
    ~sink();
    //copy constuctor
    sink(const sink &rhs);
    //assignment operator
    sink& operator=(const sink &rhs);
    friend std::ostream& operator<<(std::ostream &output, const sink &rhs);
    friend std::istream& operator>>(std::istream &input, sink &lhs);
   
    //Method
    //Get
    unsigned int getWaterFlow() const;
    double getLength() const;
    double getWidth() const;
    //Set
    void setThreadCount(unsigned int i);
    void setWaterFlow(unsigned int i);
    void setLength(double n);
    void setWidth(double n);
  protected:
  private:
    unsigned int m_waterFlow;
    double m_length;
    double m_width;
};

#endif /* SINK_H */