//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef MEETING_H
#define MEETING_H
#include <iostream>

class meeting
{
  public:
    //constructor
    meeting();
    meeting(const char *n);
    //copy constructor
    meeting(const meeting &rhs);
    //move constructor
    meeting(meeting &&rhs);
    //destructor
    ~meeting();
    //copy assignment operator
    meeting& operator=(const meeting &rhs);
    //move assignment operator
    meeting& operator=(meeting &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const meeting &rhs);
    friend std::istream& operator>>(std::istream &input, meeting &lhs);
    //Methods
    //Get
    //Set
    void setLocation(const char *n);
    void setTopic(const char *n);
    void setAttendess(const char *n);
  protected:
  private:
    char *m_location;
    time_t m_duration; 
    char *m_topic; 
    char *m_attendess;
};

#endif /* MEETING_H */