//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef NATION_H
#define NATION_H
#include <iostream>
class nation
{
  //TODO nation
  public:
    //constructor
    nation();
    //destructor
    ~nation();
    //copy constuctor
    nation(const nation &rhs);
    //copy assignment operator
    nation& operator=(const nation &rhs);
    //swap
    void swap(nation &rhs);
    friend std::ostream& operator<<(std::ostream &output, const nation &rhs);
    friend std::istream& operator>>(std::istream &input, nation &lhs);
    //Methods
    //Get
    unsigned int getPopulation() const;
    double getArea() const;
    const char* getLeader() const;
    double getGop() const;

    //Set
    void setPopulation(unsigned int i);
    void setArea(double n);
    void setLeader(char *n);
    void setGop(double n);
  protected:
  private:
    unsigned int m_population;
    double m_area;
    char *m_leader;
    double m_gop;
};

#endif /* NATION_H */