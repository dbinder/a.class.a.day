//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TRADE_H
#define TRADE_H
#include <iostream>
//TODO trade finish this class
class trade
{
  public:
    //constructor
    trade();
    //copy constuctor
    trade(const trade &rhs);
    //move constructor
    trade(trade &&rhs);
    //destructor
    ~trade();
    //assignment operator
    trade& operator=(const trade &rhs);
    //move assignment
    trade& operator=(trade &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const trade &rhs);
    friend std::istream& operator>>(std::istream &input, trade &lhs);
    //Methods
    //Get
    //Set
  protected:
  private:
    bool m_happen;
};

#endif /* TRADE_H */