//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <van.h>
//constructor
van::van():
 m_numdoors(0)
{}
van::van(const van &rhs):
m_numdoors(rhs.m_numdoors)
{}
van::van(van &&rhs):
m_numdoors(0)
{
  m_numdoors = rhs.m_numdoors;
  rhs.m_numdoors = 0;
}
//destructor
van::~van(){}
//copy assignment
van& van::operator=(const van &rhs)
{
  if (this != &rhs)
  {
    m_numdoors = rhs.m_numdoors;
  }
  return *this;
}
//move assignment
van& van::operator=(van &&rhs)
{
  if (this != &rhs)
  {
    m_numdoors = rhs.m_numdoors;
    rhs.m_numdoors = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const van &rhs)
{
  output << rhs.m_numdoors
    << rhs.getBrandName()
    << rhs.getDriveTrain()
    << rhs.getMileage()
    << rhs.getNumWheels();
  return output;
}

