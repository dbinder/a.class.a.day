//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef CHILD_H
#define CHILD_H

#include <iostream>
#include <string>
//what are things a Child has?
//age, height, weight, eye color, hair color
//status?

class Child
{
  public:
    //constructor
    Child();
    //destructor
    ~Child();
    //copy constructor
    Child(const Child &rhs);
    //assignment operator
    Child &operator=(const Child &rhs);

    //iostream operators
    friend std::ostream &operator<<(std::ostream &output, const Child &rhs);
    friend std::istream &operator>>(std::istream &input, Child &lhs);

    //comparison operators
    friend bool operator==(const Child &rhs1, const Child &rhs2);
    friend bool operator!=(const Child &rhs1, const Child &rhs2);

    //Methods
    //Get
    unsigned int getAge();
    double getHeight();
    double getWeight();
    std::string getEyeColor();
    std::string getHairColor();

    //Set
    void setAge(unsigned int i);
    void setHeight(double n);
    void setWeight(double n);
    void setEyeColor(std::string n);
    void setHairColor(std::string n);
  protected:
  private:
    unsigned int m_age;
    double m_height;
    double m_weight;
    std::string m_eyeColor;
    std::string m_hairColor;
};

#endif /* CHILD_H */