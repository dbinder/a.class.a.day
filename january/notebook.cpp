//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <notebook.h>
//constructor
notebook::notebook():
  m_pages(0),
  m_data("Stuffonsaidpages")
{}
//destructor
notebook::~notebook(){}
//copy constructor
notebook::notebook(const notebook &rhs)
{
  m_pages = rhs.m_pages;
  m_data = rhs.m_data;
}
notebook::notebook(notebook&& rhs)
{
  using std::move;
  move(rhs.m_data);
  move(rhs.m_pages);
}
//assignment operator
notebook& notebook::operator=(const notebook &rhs)
{
  if (this == &rhs)
    return *this;
  notebook temp(rhs);
  swap(temp);
  return *this;
}

void notebook::swap(notebook &rhs)
{
  using std::swap;
  swap(this->m_data, rhs.m_data);
  swap(this->m_pages, rhs.m_pages);
}
std::ostream &operator<<(std::ostream &output, const notebook &rhs)
{
  output << rhs.m_pages
    << rhs.m_data;
  return output;
}
std::istream &operator>>(std::istream &input, notebook &lhs)
{
  input >> lhs.m_pages;
  return input;
}
//Get
const char *notebook::getData() const
{
  return m_data;
}
unsigned int  notebook::getPages() const
{
  return m_pages;
}
//Set
void  notebook::setPages(unsigned int i)
{
  m_pages = i;
}
void notebook::setData(char *c)
{
  m_data = c;
}