//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SONGS_H
#define SONGS_H
#include <iostream>
//TODO Songs finish this class
class Songs
{
  public:
    //constructor
    Songs();
    //copy constructor
    Songs(const Songs &rhs);
    //move constructor
    Songs(Songs &&rhs);
    //destructor
    ~Songs();
    //copy assignment operator
    Songs& operator=(const Songs &rhs);
    //move assignment operator
    Songs& operator=(Songs &&rhs);
    //swap
    void swap(Songs &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Songs &rhs);
    friend std::istream& operator>>(std::istream &input, Songs &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SONGS_H */