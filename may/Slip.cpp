//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Slip.h>
//constructor
Slip::Slip():
  m_placeholder(0)
{}

Slip::Slip(const Slip &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Slip::Slip(Slip &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Slip::~Slip(){}
Slip& Slip::operator=(const Slip &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Slip& Slip::operator=(Slip &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Slip &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Slip &lhs)
{
  return input;
}

