//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <mist.h>
//constructor
mist::mist():
  m_density(0)
{}
mist::mist(const mist &rhs):
  m_density(rhs.m_density)
{}
mist::mist(mist &&rhs):
  m_density(0)
{
  m_density = rhs.m_density;
  rhs.m_density = 0;
}
//destructor
mist::~mist(){}
mist& mist::operator=(const mist &rhs)
{
  if (this != &rhs)
  {
    m_density = rhs.m_density;
  }
  return *this;
}
mist& mist::operator=(mist &&rhs)
{
  if (this != &rhs)
  {
    m_density = rhs.m_density;
    rhs.m_density = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const mist &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, mist &lhs)
{
  return input;
}
