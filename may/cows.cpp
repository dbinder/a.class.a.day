//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <cows.h>
//constructor
cows::cows():
  m_exist(0)
{}
cows::cows(const cows &rhs):
  m_exist(rhs.m_exist)
{}
cows::cows(cows &&rhs):
  m_exist(0)
{
  m_exist = rhs.m_exist;
  rhs.m_exist = 0;
}
//destructor
cows::~cows(){}
cows& cows::operator=(const cows &rhs)
{
  if (this != &rhs)
  {
    m_exist = rhs.m_exist;
  }
  return *this;
}
cows& cows::operator=(cows &&rhs)
{
  if (this != &rhs)
  {
    m_exist = rhs.m_exist;
    rhs.m_exist = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const cows &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, cows &lhs)
{
  return input;
}
