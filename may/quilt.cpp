//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <quilt.h>
//constructor
quilt::quilt():
  m_numSquares(0)
{}
quilt::quilt(const quilt &rhs):
  m_numSquares(rhs.m_numSquares)
{}
quilt::quilt(quilt &&rhs):
  m_numSquares(0)
{
  m_numSquares = rhs.m_numSquares;
  rhs.m_numSquares = 0;
}
//destructor
quilt::~quilt(){}
quilt& quilt::operator=(const quilt &rhs)
{
  if (this != &rhs)
  {
    m_numSquares = rhs.m_numSquares;
  }
  return *this;
}
quilt& quilt::operator=(quilt &&rhs)
{
  if (this != &rhs)
  {
    m_numSquares = rhs.m_numSquares;
    rhs.m_numSquares = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const quilt &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, quilt &lhs)
{
  return input;
}
