//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef YARD_H
#define YARD_H
#include <iostream>
//TODO Yard finish this class
class Yard
{
  public:
    //constructor
    Yard();
    //copy constructor
    Yard(const Yard &rhs);
    //move constructor
    Yard(Yard &&rhs);
    //destructor
    ~Yard();
    //copy assignment operator
    Yard& operator=(const Yard &rhs);
    //move assignment operator
    Yard& operator=(Yard &&rhs);
    //swap
    void swap(Yard &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Yard &rhs);
    friend std::istream& operator>>(std::istream &input, Yard &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* YARD_H */