//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Hill.h>
//constructor
Hill::Hill():
  m_placeholder(0)
{}

Hill::Hill(const Hill &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Hill::Hill(Hill &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Hill::~Hill(){}
Hill& Hill::operator=(const Hill &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Hill& Hill::operator=(Hill &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Hill &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Hill &lhs)
{
  return input;
}

