//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CHERRIES_H
#define CHERRIES_H
#include <iostream>
//TODO Cherries finish this class
class Cherries
{
  public:
    //constructor
    Cherries();
    //copy constructor
    Cherries(const Cherries &rhs);
    //move constructor
    Cherries(Cherries &&rhs);
    //destructor
    ~Cherries();
    //copy assignment operator
    Cherries& operator=(const Cherries &rhs);
    //move assignment operator
    Cherries& operator=(Cherries &&rhs);
    //swap
    void swap(Cherries &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cherries &rhs);
    friend std::istream& operator>>(std::istream &input, Cherries &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CHERRIES_H */