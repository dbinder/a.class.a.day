//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef THRILL_H
#define THRILL_H
#include <iostream>
//TODO thrill finish this class
class thrill
{
  public:
    //constructor
    thrill();
    //copy constructor
    thrill(const thrill &rhs);
    //move constructor
    thrill(thrill &&rhs);
    //destructor
    ~thrill();
    //copy assignment operator
    thrill& operator=(const thrill &rhs);
    //move assignment operator
    thrill& operator=(thrill &&rhs);
    //swap
    void swap(thrill &rhs);
    friend std::ostream& operator<<(std::ostream &output, const thrill &rhs);
    friend std::istream& operator>>(std::istream &input, thrill &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_desciprtion;
};

#endif /* THRILL_H */