//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <cheese.h>
//constructor
cheese::cheese():
  m_flavor(0)
{}
cheese::cheese(const cheese &rhs):
  m_flavor(rhs.m_flavor)
{}
cheese::cheese(cheese &&rhs):
  m_flavor(0)
{
  m_flavor = rhs.m_flavor;
  rhs.m_flavor = "";
}
//destructor
cheese::~cheese(){}
cheese& cheese::operator=(const cheese &rhs)
{
  if (this != &rhs)
  {
    m_flavor = rhs.m_flavor;
  }
  return *this;
}
cheese& cheese::operator=(cheese &&rhs)
{
  if (this != &rhs)
  {
    m_flavor = rhs.m_flavor;
    rhs.m_flavor = "";
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const cheese &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, cheese &lhs)
{
  return input;
}
