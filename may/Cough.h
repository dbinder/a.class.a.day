//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef COUGH_H
#define COUGH_H
#include <iostream>
//TODO Cough finish this class
class Cough
{
  public:
    //constructor
    Cough();
    //copy constructor
    Cough(const Cough &rhs);
    //move constructor
    Cough(Cough &&rhs);
    //destructor
    ~Cough();
    //copy assignment operator
    Cough& operator=(const Cough &rhs);
    //move assignment operator
    Cough& operator=(Cough &&rhs);
    //swap
    void swap(Cough &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cough &rhs);
    friend std::istream& operator>>(std::istream &input, Cough &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* COUGH_H */