//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Throat.h>
//constructor
Throat::Throat():
  m_placeholder(0)
{}

Throat::Throat(const Throat &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Throat::Throat(Throat &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Throat::~Throat(){}
Throat& Throat::operator=(const Throat &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Throat& Throat::operator=(Throat &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Throat &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Throat &lhs)
{
  return input;
}

