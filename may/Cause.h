//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CAUSE_H
#define CAUSE_H
#include <iostream>
//TODO Cause finish this class
class Cause
{
  public:
    //constructor
    Cause();
    //copy constructor
    Cause(const Cause &rhs);
    //move constructor
    Cause(Cause &&rhs);
    //destructor
    ~Cause();
    //copy assignment operator
    Cause& operator=(const Cause &rhs);
    //move assignment operator
    Cause& operator=(Cause &&rhs);
    //swap
    void swap(Cause &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cause &rhs);
    friend std::istream& operator>>(std::istream &input, Cause &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CAUSE_H */