//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Note.h>
//constructor
Note::Note():
  m_placeholder(0)
{}

Note::Note(const Note &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Note::Note(Note &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Note::~Note(){}
Note& Note::operator=(const Note &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Note& Note::operator=(Note &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Note &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Note &lhs)
{
  return input;
}

