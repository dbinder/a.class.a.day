//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SMASH_H
#define SMASH_H
#include <iostream>
//TODO smash finish this class
class smash
{
  public:
    //constructor
    smash();
    //copy constructor
    smash(const smash &rhs);
    //move constructor
    smash(smash &&rhs);
    //destructor
    ~smash();
    //copy assignment operator
    smash& operator=(const smash &rhs);
    //move assignment operator
    smash& operator=(smash &&rhs);
    //swap
    void swap(smash &rhs);
    friend std::ostream& operator<<(std::ostream &output, const smash &rhs);
    friend std::istream& operator>>(std::istream &input, smash &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_succes;
};

#endif /* SMASH_H */