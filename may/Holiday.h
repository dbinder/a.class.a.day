//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HOLIDAY_H
#define HOLIDAY_H
#include <iostream>
//TODO Holiday finish this class
class Holiday
{
  public:
    //constructor
    Holiday();
    //copy constructor
    Holiday(const Holiday &rhs);
    //move constructor
    Holiday(Holiday &&rhs);
    //destructor
    ~Holiday();
    //copy assignment operator
    Holiday& operator=(const Holiday &rhs);
    //move assignment operator
    Holiday& operator=(Holiday &&rhs);
    //swap
    void swap(Holiday &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Holiday &rhs);
    friend std::istream& operator>>(std::istream &input, Holiday &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HOLIDAY_H */