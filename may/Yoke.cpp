//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Yoke.h>
//constructor
Yoke::Yoke():
  m_placeholder(0)
{}

Yoke::Yoke(const Yoke &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Yoke::Yoke(Yoke &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Yoke::~Yoke(){}
Yoke& Yoke::operator=(const Yoke &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Yoke& Yoke::operator=(Yoke &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Yoke &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Yoke &lhs)
{
  return input;
}

