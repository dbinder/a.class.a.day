//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CREAM_H
#define CREAM_H
#include <iostream>
//TODO Cream finish this class
class Cream
{
  public:
    //constructor
    Cream();
    //copy constructor
    Cream(const Cream &rhs);
    //move constructor
    Cream(Cream &&rhs);
    //destructor
    ~Cream();
    //copy assignment operator
    Cream& operator=(const Cream &rhs);
    //move assignment operator
    Cream& operator=(Cream &&rhs);
    //swap
    void swap(Cream &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Cream &rhs);
    friend std::istream& operator>>(std::istream &input, Cream &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CREAM_H */