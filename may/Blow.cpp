//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Blow.h>
//constructor
Blow::Blow():
  m_placeholder(0)
{}

Blow::Blow(const Blow &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Blow::Blow(Blow &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Blow::~Blow(){}
Blow& Blow::operator=(const Blow &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Blow& Blow::operator=(Blow &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Blow &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Blow &lhs)
{
  return input;
}

