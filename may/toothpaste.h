//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TOOTHPASTE_H
#define TOOTHPASTE_H
#include <iostream>
//TODO toothpaste finish this class
class toothpaste
{
  public:
    //constructor
    toothpaste();
    //copy constructor
    toothpaste(const toothpaste &rhs);
    //move constructor
    toothpaste(toothpaste &&rhs);
    //destructor
    ~toothpaste();
    //copy assignment operator
    toothpaste& operator=(const toothpaste &rhs);
    //move assignment operator
    toothpaste& operator=(toothpaste &&rhs);
    //swap
    void swap(toothpaste &rhs);
    friend std::ostream& operator<<(std::ostream &output, const toothpaste &rhs);
    friend std::istream& operator>>(std::istream &input, toothpaste &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_flavor;
};

#endif /* TOOTHPASTE_H */