//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BEE_H
#define BEE_H
#include <iostream>
//TODO bee finish this class
class bee
{
  public:
    //constructor
    bee();
    //copy constructor
    bee(const bee &rhs);
    //move constructor
    bee(bee &&rhs);
    //destructor
    ~bee();
    //copy assignment operator
    bee& operator=(const bee &rhs);
    //move assignment operator
    bee& operator=(bee &&rhs);
    //swap
    void swap(bee &rhs);
    friend std::ostream& operator<<(std::ostream &output, const bee &rhs);
    friend std::istream& operator>>(std::istream &input, bee &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_stripes;
};

#endif /* BEE_H */