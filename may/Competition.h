//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef COMPETITION_H
#define COMPETITION_H
#include <iostream>
//TODO Competition finish this class
class Competition
{
  public:
    //constructor
    Competition();
    //copy constructor
    Competition(const Competition &rhs);
    //move constructor
    Competition(Competition &&rhs);
    //destructor
    ~Competition();
    //copy assignment operator
    Competition& operator=(const Competition &rhs);
    //move assignment operator
    Competition& operator=(Competition &&rhs);
    //swap
    void swap(Competition &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Competition &rhs);
    friend std::istream& operator>>(std::istream &input, Competition &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* COMPETITION_H */