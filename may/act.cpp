//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <act.h>
//constructor
act::act():
  m_acting(0)
{}
act::act(const act &rhs):
  m_acting(rhs.m_acting)
{}
act::act(act &&rhs):
  m_acting(0)
{
  m_acting = rhs.m_acting;
  rhs.m_acting = 0;
}
//destructor
act::~act(){}
act& act::operator=(const act &rhs)
{
  if (this != &rhs)
  {
    m_acting = rhs.m_acting;
  }
  return *this;
}
act& act::operator=(act &&rhs)
{
  if (this != &rhs)
  {
    m_acting = rhs.m_acting;
    rhs.m_acting = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const act &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, act &lhs)
{
  return input;
}
