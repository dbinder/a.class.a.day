//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HILL_H
#define HILL_H
#include <iostream>
//TODO Hill finish this class
class Hill
{
  public:
    //constructor
    Hill();
    //copy constructor
    Hill(const Hill &rhs);
    //move constructor
    Hill(Hill &&rhs);
    //destructor
    ~Hill();
    //copy assignment operator
    Hill& operator=(const Hill &rhs);
    //move assignment operator
    Hill& operator=(Hill &&rhs);
    //swap
    void swap(Hill &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Hill &rhs);
    friend std::istream& operator>>(std::istream &input, Hill &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HILL_H */