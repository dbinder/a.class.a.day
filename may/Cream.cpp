//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cream.h>
//constructor
Cream::Cream():
  m_placeholder(0)
{}

Cream::Cream(const Cream &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cream::Cream(Cream &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cream::~Cream(){}
Cream& Cream::operator=(const Cream &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cream& Cream::operator=(Cream &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cream &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cream &lhs)
{
  return input;
}

