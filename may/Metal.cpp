//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Metal.h>
//constructor
Metal::Metal():
  m_placeholder(0)
{}

Metal::Metal(const Metal &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Metal::Metal(Metal &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Metal::~Metal(){}
Metal& Metal::operator=(const Metal &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Metal& Metal::operator=(Metal &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Metal &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Metal &lhs)
{
  return input;
}

