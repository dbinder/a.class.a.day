//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef QUILT_H
#define QUILT_H
#include <iostream>
//TODO quilt finish this class
class quilt
{
  public:
    //constructor
    quilt();
    //copy constructor
    quilt(const quilt &rhs);
    //move constructor
    quilt(quilt &&rhs);
    //destructor
    ~quilt();
    //copy assignment operator
    quilt& operator=(const quilt &rhs);
    //move assignment operator
    quilt& operator=(quilt &&rhs);
    //swap
    void swap(quilt &rhs);
    friend std::ostream& operator<<(std::ostream &output, const quilt &rhs);
    friend std::istream& operator>>(std::istream &input, quilt &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_numSquares;
};

#endif /* QUILT_H */