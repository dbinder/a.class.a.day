//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CARD_H
#define CARD_H
#include <iostream>
//TODO Card finish this class
class Card
{
  public:
    //constructor
    Card();
    //copy constructor
    Card(const Card &rhs);
    //move constructor
    Card(Card &&rhs);
    //destructor
    ~Card();
    //copy assignment operator
    Card& operator=(const Card &rhs);
    //move assignment operator
    Card& operator=(Card &&rhs);
    //swap
    void swap(Card &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Card &rhs);
    friend std::istream& operator>>(std::istream &input, Card &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CARD_H */