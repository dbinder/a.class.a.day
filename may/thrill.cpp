//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <thrill.h>
//constructor
thrill::thrill():
  m_desciprtion(0)
{}
thrill::thrill(const thrill &rhs):
  m_desciprtion(rhs.m_desciprtion)
{}
thrill::thrill(thrill &&rhs):
  m_desciprtion(0)
{
  m_desciprtion = rhs.m_desciprtion;
  rhs.m_desciprtion = "";
}
//destructor
thrill::~thrill(){}
thrill& thrill::operator=(const thrill &rhs)
{
  if (this != &rhs)
  {
    m_desciprtion = rhs.m_desciprtion;
  }
  return *this;
}
thrill& thrill::operator=(thrill &&rhs)
{
  if (this != &rhs)
  {
    m_desciprtion = rhs.m_desciprtion;
    rhs.m_desciprtion = "";
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const thrill &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, thrill &lhs)
{
  return input;
}
