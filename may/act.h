//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef ACT_H
#define ACT_H
#include <iostream>
//TODO act finish this class
class act
{
  public:
    //constructor
    act();
    //copy constructor
    act(const act &rhs);
    //move constructor
    act(act &&rhs);
    //destructor
    ~act();
    //copy assignment operator
    act& operator=(const act &rhs);
    //move assignment operator
    act& operator=(act &&rhs);
    //swap
    void swap(act &rhs);
    friend std::ostream& operator<<(std::ostream &output, const act &rhs);
    friend std::istream& operator>>(std::istream &input, act &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_acting;
};

#endif /* ACT_H */