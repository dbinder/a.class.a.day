//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Root.h>
//constructor
Root::Root():
  m_placeholder(0)
{}

Root::Root(const Root &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Root::Root(Root &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Root::~Root(){}
Root& Root::operator=(const Root &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Root& Root::operator=(Root &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Root &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Root &lhs)
{
  return input;
}

