//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Holiday.h>
//constructor
Holiday::Holiday():
  m_placeholder(0)
{}

Holiday::Holiday(const Holiday &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Holiday::Holiday(Holiday &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Holiday::~Holiday(){}
Holiday& Holiday::operator=(const Holiday &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Holiday& Holiday::operator=(Holiday &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Holiday &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Holiday &lhs)
{
  return input;
}

