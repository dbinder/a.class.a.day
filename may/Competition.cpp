//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Competition.h>
//constructor
Competition::Competition():
  m_placeholder(0)
{}

Competition::Competition(const Competition &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Competition::Competition(Competition &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Competition::~Competition(){}
Competition& Competition::operator=(const Competition &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Competition& Competition::operator=(Competition &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Competition &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Competition &lhs)
{
  return input;
}

