//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef READING_H
#define READING_H
#include <iostream>
//TODO Reading finish this class
class Reading
{
  public:
    //constructor
    Reading();
    //copy constructor
    Reading(const Reading &rhs);
    //move constructor
    Reading(Reading &&rhs);
    //destructor
    ~Reading();
    //copy assignment operator
    Reading& operator=(const Reading &rhs);
    //move assignment operator
    Reading& operator=(Reading &&rhs);
    //swap
    void swap(Reading &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Reading &rhs);
    friend std::istream& operator>>(std::istream &input, Reading &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* READING_H */