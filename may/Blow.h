//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BLOW_H
#define BLOW_H
#include <iostream>
//TODO Blow finish this class
class Blow
{
  public:
    //constructor
    Blow();
    //copy constructor
    Blow(const Blow &rhs);
    //move constructor
    Blow(Blow &&rhs);
    //destructor
    ~Blow();
    //copy assignment operator
    Blow& operator=(const Blow &rhs);
    //move assignment operator
    Blow& operator=(Blow &&rhs);
    //swap
    void swap(Blow &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Blow &rhs);
    friend std::istream& operator>>(std::istream &input, Blow &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BLOW_H */