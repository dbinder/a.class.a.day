//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Fairies.h>
//constructor
Fairies::Fairies():
  m_placeholder(0)
{}

Fairies::Fairies(const Fairies &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Fairies::Fairies(Fairies &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Fairies::~Fairies(){}
Fairies& Fairies::operator=(const Fairies &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Fairies& Fairies::operator=(Fairies &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Fairies &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Fairies &lhs)
{
  return input;
}

