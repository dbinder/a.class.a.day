//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <store.h>
//constructor
store::store():
  m_hours(0)
{}
store::store(const store &rhs):
  m_hours(rhs.m_hours)
{}
store::store(store &&rhs):
  m_hours(0)
{
  m_hours = rhs.m_hours;
  rhs.m_hours = 0;
}
//destructor
store::~store(){}
store& store::operator=(const store &rhs)
{
  if (this != &rhs)
  {
    m_hours = rhs.m_hours;
  }
  return *this;
}
store& store::operator=(store &&rhs)
{
  if (this != &rhs)
  {
    m_hours = rhs.m_hours;
    rhs.m_hours = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const store &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, store &lhs)
{
  return input;
}
