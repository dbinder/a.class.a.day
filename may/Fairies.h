//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef FAIRIES_H
#define FAIRIES_H
#include <iostream>
//TODO Fairies finish this class
class Fairies
{
  public:
    //constructor
    Fairies();
    //copy constructor
    Fairies(const Fairies &rhs);
    //move constructor
    Fairies(Fairies &&rhs);
    //destructor
    ~Fairies();
    //copy assignment operator
    Fairies& operator=(const Fairies &rhs);
    //move assignment operator
    Fairies& operator=(Fairies &&rhs);
    //swap
    void swap(Fairies &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Fairies &rhs);
    friend std::istream& operator>>(std::istream &input, Fairies &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* FAIRIES_H */