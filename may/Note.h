//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef NOTE_H
#define NOTE_H
#include <iostream>
//TODO Note finish this class
class Note
{
  public:
    //constructor
    Note();
    //copy constructor
    Note(const Note &rhs);
    //move constructor
    Note(Note &&rhs);
    //destructor
    ~Note();
    //copy assignment operator
    Note& operator=(const Note &rhs);
    //move assignment operator
    Note& operator=(Note &&rhs);
    //swap
    void swap(Note &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Note &rhs);
    friend std::istream& operator>>(std::istream &input, Note &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* NOTE_H */