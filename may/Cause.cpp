//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cause.h>
//constructor
Cause::Cause():
  m_placeholder(0)
{}

Cause::Cause(const Cause &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cause::Cause(Cause &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cause::~Cause(){}
Cause& Cause::operator=(const Cause &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cause& Cause::operator=(Cause &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cause &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cause &lhs)
{
  return input;
}

