//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Yard.h>
//constructor
Yard::Yard():
  m_placeholder(0)
{}

Yard::Yard(const Yard &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Yard::Yard(Yard &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Yard::~Yard(){}
Yard& Yard::operator=(const Yard &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Yard& Yard::operator=(Yard &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Yard &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Yard &lhs)
{
  return input;
}

