//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Tent.h>
//constructor
Tent::Tent():
  m_placeholder(0)
{}

Tent::Tent(const Tent &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Tent::Tent(Tent &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Tent::~Tent(){}
Tent& Tent::operator=(const Tent &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Tent& Tent::operator=(Tent &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Tent &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Tent &lhs)
{
  return input;
}

