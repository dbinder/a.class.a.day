//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Reading.h>
//constructor
Reading::Reading():
  m_placeholder(0)
{}

Reading::Reading(const Reading &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Reading::Reading(Reading &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Reading::~Reading(){}
Reading& Reading::operator=(const Reading &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Reading& Reading::operator=(Reading &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Reading &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Reading &lhs)
{
  return input;
}

