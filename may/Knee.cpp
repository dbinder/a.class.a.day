//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Knee.h>
//constructor
Knee::Knee():
  m_placeholder(0)
{}

Knee::Knee(const Knee &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Knee::Knee(Knee &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Knee::~Knee(){}
Knee& Knee::operator=(const Knee &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Knee& Knee::operator=(Knee &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Knee &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Knee &lhs)
{
  return input;
}

