//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef COWS_H
#define COWS_H
#include <iostream>
//TODO cows finish this class
class cows
{
  public:
    //constructor
    cows();
    //copy constructor
    cows(const cows &rhs);
    //move constructor
    cows(cows &&rhs);
    //destructor
    ~cows();
    //copy assignment operator
    cows& operator=(const cows &rhs);
    //move assignment operator
    cows& operator=(cows &&rhs);
    //swap
    void swap(cows &rhs);
    friend std::ostream& operator<<(std::ostream &output, const cows &rhs);
    friend std::istream& operator>>(std::istream &input, cows &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_exist;
};

#endif /* COWS_H */