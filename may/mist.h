//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef MIST_H
#define MIST_H
#include <iostream>
//TODO mist finish this class
class mist
{
  public:
    //constructor
    mist();
    //copy constructor
    mist(const mist &rhs);
    //move constructor
    mist(mist &&rhs);
    //destructor
    ~mist();
    //copy assignment operator
    mist& operator=(const mist &rhs);
    //move assignment operator
    mist& operator=(mist &&rhs);
    //swap
    void swap(mist &rhs);
    friend std::ostream& operator<<(std::ostream &output, const mist &rhs);
    friend std::istream& operator>>(std::istream &input, mist &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    float m_density;
};

#endif /* MIST_H */