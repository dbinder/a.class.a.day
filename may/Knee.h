//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef KNEE_H
#define KNEE_H
#include <iostream>
//TODO Knee finish this class
class Knee
{
  public:
    //constructor
    Knee();
    //copy constructor
    Knee(const Knee &rhs);
    //move constructor
    Knee(Knee &&rhs);
    //destructor
    ~Knee();
    //copy assignment operator
    Knee& operator=(const Knee &rhs);
    //move assignment operator
    Knee& operator=(Knee &&rhs);
    //swap
    void swap(Knee &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Knee &rhs);
    friend std::istream& operator>>(std::istream &input, Knee &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* KNEE_H */