//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cherries.h>
//constructor
Cherries::Cherries():
  m_placeholder(0)
{}

Cherries::Cherries(const Cherries &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cherries::Cherries(Cherries &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cherries::~Cherries(){}
Cherries& Cherries::operator=(const Cherries &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cherries& Cherries::operator=(Cherries &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cherries &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cherries &lhs)
{
  return input;
}

