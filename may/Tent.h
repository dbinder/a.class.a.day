//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TENT_H
#define TENT_H
#include <iostream>
//TODO Tent finish this class
class Tent
{
  public:
    //constructor
    Tent();
    //copy constructor
    Tent(const Tent &rhs);
    //move constructor
    Tent(Tent &&rhs);
    //destructor
    ~Tent();
    //copy assignment operator
    Tent& operator=(const Tent &rhs);
    //move assignment operator
    Tent& operator=(Tent &&rhs);
    //swap
    void swap(Tent &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Tent &rhs);
    friend std::istream& operator>>(std::istream &input, Tent &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TENT_H */