//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef CHEESE_H
#define CHEESE_H
#include <iostream>
//TODO cheese finish this class
class cheese
{
  public:
    //constructor
    cheese();
    //copy constructor
    cheese(const cheese &rhs);
    //move constructor
    cheese(cheese &&rhs);
    //destructor
    ~cheese();
    //copy assignment operator
    cheese& operator=(const cheese &rhs);
    //move assignment operator
    cheese& operator=(cheese &&rhs);
    //swap
    void swap(cheese &rhs);
    friend std::ostream& operator<<(std::ostream &output, const cheese &rhs);
    friend std::istream& operator>>(std::istream &input, cheese &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_flavor;
};

#endif /* CHEESE_H */