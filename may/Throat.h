//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef THROAT_H
#define THROAT_H
#include <iostream>
//TODO Throat finish this class
class Throat
{
  public:
    //constructor
    Throat();
    //copy constructor
    Throat(const Throat &rhs);
    //move constructor
    Throat(Throat &&rhs);
    //destructor
    ~Throat();
    //copy assignment operator
    Throat& operator=(const Throat &rhs);
    //move assignment operator
    Throat& operator=(Throat &&rhs);
    //swap
    void swap(Throat &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Throat &rhs);
    friend std::istream& operator>>(std::istream &input, Throat &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* THROAT_H */