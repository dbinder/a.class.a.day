//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <toothpaste.h>
//constructor
toothpaste::toothpaste():
  m_flavor(0)
{}
toothpaste::toothpaste(const toothpaste &rhs):
  m_flavor(rhs.m_flavor)
{}
toothpaste::toothpaste(toothpaste &&rhs):
  m_flavor(0)
{
  m_flavor = rhs.m_flavor;
  rhs.m_flavor = "";
}
//destructor
toothpaste::~toothpaste(){}
toothpaste& toothpaste::operator=(const toothpaste &rhs)
{
  if (this != &rhs)
  {
    m_flavor = rhs.m_flavor;
  }
  return *this;
}
toothpaste& toothpaste::operator=(toothpaste &&rhs)
{
  if (this != &rhs)
  {
    m_flavor = rhs.m_flavor;
    rhs.m_flavor = "";
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const toothpaste &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, toothpaste &lhs)
{
  return input;
}
