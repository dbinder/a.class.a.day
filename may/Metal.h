//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef METAL_H
#define METAL_H
#include <iostream>
//TODO Metal finish this class
class Metal
{
  public:
    //constructor
    Metal();
    //copy constructor
    Metal(const Metal &rhs);
    //move constructor
    Metal(Metal &&rhs);
    //destructor
    ~Metal();
    //copy assignment operator
    Metal& operator=(const Metal &rhs);
    //move assignment operator
    Metal& operator=(Metal &&rhs);
    //swap
    void swap(Metal &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Metal &rhs);
    friend std::istream& operator>>(std::istream &input, Metal &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* METAL_H */