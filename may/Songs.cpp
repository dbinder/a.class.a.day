//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Songs.h>
//constructor
Songs::Songs():
  m_placeholder(0)
{}

Songs::Songs(const Songs &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Songs::Songs(Songs &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Songs::~Songs(){}
Songs& Songs::operator=(const Songs &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Songs& Songs::operator=(Songs &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Songs &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Songs &lhs)
{
  return input;
}

