//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <bee.h>
//constructor
bee::bee():
  m_stripes(0)
{}
bee::bee(const bee &rhs):
  m_stripes(rhs.m_stripes)
{}
bee::bee(bee &&rhs):
  m_stripes(0)
{
  m_stripes = rhs.m_stripes;
  rhs.m_stripes = 0;
}
//destructor
bee::~bee(){}
bee& bee::operator=(const bee &rhs)
{
  if (this != &rhs)
  {
    m_stripes = rhs.m_stripes;
  }
  return *this;
}
bee& bee::operator=(bee &&rhs)
{
  if (this != &rhs)
  {
    m_stripes = rhs.m_stripes;
    rhs.m_stripes = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const bee &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, bee &lhs)
{
  return input;
}
