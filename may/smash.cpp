//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <smash.h>
//constructor
smash::smash():
  m_succes(0)
{}
smash::smash(const smash &rhs):
  m_succes(rhs.m_succes)
{}
smash::smash(smash &&rhs):
  m_succes(0)
{
  m_succes = rhs.m_succes;
  rhs.m_succes = 0;
}
//destructor
smash::~smash(){}
smash& smash::operator=(const smash &rhs)
{
  if (this != &rhs)
  {
    m_succes = rhs.m_succes;
  }
  return *this;
}
smash& smash::operator=(smash &&rhs)
{
  if (this != &rhs)
  {
    m_succes = rhs.m_succes;
    rhs.m_succes = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const smash &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, smash &lhs)
{
  return input;
}
