//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef YOKE_H
#define YOKE_H
#include <iostream>
//TODO Yoke finish this class
class Yoke
{
  public:
    //constructor
    Yoke();
    //copy constructor
    Yoke(const Yoke &rhs);
    //move constructor
    Yoke(Yoke &&rhs);
    //destructor
    ~Yoke();
    //copy assignment operator
    Yoke& operator=(const Yoke &rhs);
    //move assignment operator
    Yoke& operator=(Yoke &&rhs);
    //swap
    void swap(Yoke &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Yoke &rhs);
    friend std::istream& operator>>(std::istream &input, Yoke &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* YOKE_H */