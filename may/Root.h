//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ROOT_H
#define ROOT_H
#include <iostream>
//TODO Root finish this class
class Root
{
  public:
    //constructor
    Root();
    //copy constructor
    Root(const Root &rhs);
    //move constructor
    Root(Root &&rhs);
    //destructor
    ~Root();
    //copy assignment operator
    Root& operator=(const Root &rhs);
    //move assignment operator
    Root& operator=(Root &&rhs);
    //swap
    void swap(Root &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Root &rhs);
    friend std::istream& operator>>(std::istream &input, Root &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ROOT_H */