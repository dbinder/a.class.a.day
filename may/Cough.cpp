//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Cough.h>
//constructor
Cough::Cough():
  m_placeholder(0)
{}

Cough::Cough(const Cough &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Cough::Cough(Cough &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Cough::~Cough(){}
Cough& Cough::operator=(const Cough &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Cough& Cough::operator=(Cough &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Cough &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Cough &lhs)
{
  return input;
}

