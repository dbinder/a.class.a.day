//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Card.h>
//constructor
Card::Card():
  m_placeholder(0)
{}

Card::Card(const Card &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Card::Card(Card &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Card::~Card(){}
Card& Card::operator=(const Card &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Card& Card::operator=(Card &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Card &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Card &lhs)
{
  return input;
}

