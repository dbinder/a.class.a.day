//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef STORE_H
#define STORE_H
#include <iostream>
//TODO store finish this class
class store
{
  public:
    //constructor
    store();
    //copy constructor
    store(const store &rhs);
    //move constructor
    store(store &&rhs);
    //destructor
    ~store();
    //copy assignment operator
    store& operator=(const store &rhs);
    //move assignment operator
    store& operator=(store &&rhs);
    //swap
    void swap(store &rhs);
    friend std::ostream& operator<<(std::ostream &output, const store &rhs);
    friend std::istream& operator>>(std::istream &input, store &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_hours;
};

#endif /* STORE_H */