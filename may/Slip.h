//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SLIP_H
#define SLIP_H
#include <iostream>
//TODO Slip finish this class
class Slip
{
  public:
    //constructor
    Slip();
    //copy constructor
    Slip(const Slip &rhs);
    //move constructor
    Slip(Slip &&rhs);
    //destructor
    ~Slip();
    //copy assignment operator
    Slip& operator=(const Slip &rhs);
    //move assignment operator
    Slip& operator=(Slip &&rhs);
    //swap
    void swap(Slip &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Slip &rhs);
    friend std::istream& operator>>(std::istream &input, Slip &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SLIP_H */