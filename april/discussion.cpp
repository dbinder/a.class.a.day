//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <discussion.h>
//constructor
discussion::discussion():
  m_tookPlace(0)
{}
discussion::discussion(const discussion &rhs):
  m_tookPlace(rhs.m_tookPlace)
{}
discussion::discussion(discussion &&rhs):
  m_tookPlace(0)
{
  m_tookPlace = rhs.m_tookPlace;
  rhs.m_tookPlace = false;
}
//destructor
discussion::~discussion(){}
discussion& discussion::operator=(const discussion &rhs)
{
  if (this != &rhs)
  {
    m_tookPlace = rhs.m_tookPlace;
  }
  return *this;
}
discussion& discussion::operator=(discussion &&rhs)
{
  if (this != &rhs)
  {
    rhs.m_tookPlace = false;
  }
  return *this;
}