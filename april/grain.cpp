//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <grain.h>
//constructor
grain::grain():
  m_count(0)
{}
grain::grain(const grain &rhs):
  m_count(rhs.m_count)
{}
grain::grain(grain &&rhs):
  m_count(0)
{
  m_count = rhs.m_count;
  rhs.m_count = 0;
}
//destructor
grain::~grain(){}
grain& grain::operator=(const grain &rhs)
{
  if (this != &rhs)
  {
    m_count = rhs.m_count;
  }
  return *this;
}
grain& grain::operator=(grain &&rhs)
{
  if (this != &rhs)
  {
    m_count = rhs.m_count;
    rhs.m_count = 0;
  }
  return *this;
}