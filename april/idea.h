//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef IDEA_H
#define IDEA_H
#include <iostream>
//TODO idea finish this class
class idea
{
  public:
    //constructor
    idea();
    //copy constructor
    idea(const idea &rhs);
    //move constructor
    idea(idea &&rhs);
    //destructor
    ~idea();
    //copy assignment operator
    idea& operator=(const idea &rhs);
    //move assignment operator
    idea& operator=(idea &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_info;
};

#endif /* IDEA_H */