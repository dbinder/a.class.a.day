//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PORTER_H
#define PORTER_H
#include <iostream>
//TODO porter finish this class
class porter
{
  public:
    //constructor
    porter();
    //copy constructor
    porter(const porter &rhs);
    //move constructor
    porter(porter &&rhs);
    //destructor
    ~porter();
    //copy assignment operator
    porter& operator=(const porter &rhs);
    //move assignment operator
    porter& operator=(porter &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
};

#endif /* PORTER_H */