//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef MOM_H
#define MOM_H
#include <iostream>
//TODO mom finish this class
class mom
{
  public:
    //constructor
    mom();
    //copy constructor
    mom(const mom &rhs);
    //move constructor
    mom(mom &&rhs);
    //destructor
    ~mom();
    //copy assignment operator
    mom& operator=(const mom &rhs);
    //move assignment operator
    mom& operator=(mom &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_age;
};

#endif /* MOM_H */