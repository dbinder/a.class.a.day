//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <rainstorm.h>
//constructor
rainstorm::rainstorm():
  m_duration(0)
{}
rainstorm::rainstorm(const rainstorm &rhs):
  m_duration(rhs.m_duration)
{}
rainstorm::rainstorm(rainstorm &&rhs) :
  m_duration(0)
{
  m_duration = rhs.m_duration;
  rhs.m_duration = 0;
}
//destructor
rainstorm::~rainstorm(){}
rainstorm& rainstorm::operator=(const rainstorm &rhs)
{
  if (this != &rhs)
  {
    m_duration = rhs.m_duration;
  }
  return *this;
}
rainstorm& rainstorm::operator=(rainstorm &&rhs)
{
  if (this != &rhs)
  {
    m_duration = rhs.m_duration;
    rhs.m_duration = 0;
  }
  return *this;
}