//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef STEEL_H
#define STEEL_H
#include <iostream>
//TODO steel finish this class
class steel
{
  public:
    //constructor
    steel();
    //copy constructor
    steel(const steel &rhs);
    //move constructor
    steel(steel &&rhs);
    //destructor
    ~steel();
    //copy assignment operator
    steel& operator=(const steel &rhs);
    //move assignment operator
    steel& operator=(steel &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_wieght;
};

#endif /* STEEL_H */