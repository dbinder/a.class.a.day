//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef AIRPLANE_H
#define AIRPLANE_H
#include <iostream>
//TODO airplane finish this class
class airplane
{
  public:
    //constructor
    airplane();
    //copy constructor
    airplane(const airplane &rhs);
    //move constructor
    airplane(airplane &&rhs);
    //destructor
    ~airplane();
    //copy assignment operator
    airplane& operator=(const airplane &rhs);
    //move assignment operator
    airplane& operator=(airplane &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    float m_time;
};

#endif /* AIRPLANE_H */