//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <visitor.h>
//constructor
visitor::visitor():
  m_time(0)
{}
visitor::visitor(const visitor &rhs):
  m_time(rhs.m_time)
{}
visitor::visitor(visitor &&rhs):
  m_time(0)
{
  m_time = rhs.m_time;
  rhs.m_time = 0;
}
//destructor
visitor::~visitor(){}
visitor& visitor::operator=(const visitor &rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
  }
  return *this;
}
visitor& visitor::operator=(visitor &&rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
    rhs.m_time = 0;
  }
  return *this;
}