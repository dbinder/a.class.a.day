//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <weather.h>
//constructor
weather::weather():
  m_bad(0)
{}
weather::weather(const weather &rhs):
  m_bad(rhs.m_bad)
{}
weather::weather(weather &&rhs):
  m_bad(0)
{
  m_bad = rhs.m_bad;
  rhs.m_bad = 0;
}
//destructor
weather::~weather(){}
weather& weather::operator=(const weather &rhs)
{
  if (this != &rhs)
  {
    m_bad = rhs.m_bad;
  }
  return *this;
}
weather& weather::operator=(weather &&rhs)
{
  if (this != &rhs)
  {
    m_bad = rhs.m_bad;
    rhs.m_bad = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const weather &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, weather &lhs)
{
  return input;
}
