//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef OFFER_H
#define OFFER_H
#include <iostream>
//TODO offer finish this class
class offer
{
  public:
    //constructor
    offer();
    //copy constructor
    offer(const offer &rhs);
    //move constructor
    offer(offer &&rhs);
    //destructor
    ~offer();
    //copy assignment operator
    offer& operator=(const offer &rhs);
    //move assignment operator
    offer& operator=(offer &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
};

#endif /* OFFER_H */