//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef FLIGHT_H
#define FLIGHT_H
#include <iostream>
//TODO flight finish this class
class flight
{
  public:
    //constructor
    flight();
    //copy constructor
    flight(const flight &rhs);
    //move constructor
    flight(flight &&rhs);
    //destructor
    ~flight();
    //copy assignment operator
    flight& operator=(const flight &rhs);
    //move assignment operator
    flight& operator=(flight &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_duration;
};

#endif /* FLIGHT_H */