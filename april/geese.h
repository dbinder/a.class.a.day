//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef GEESE_H
#define GEESE_H
#include <iostream>
//TODO geese finish this class
class geese
{
  public:
    //constructor
    geese();
    //copy constructor
    geese(const geese &rhs);
    //move constructor
    geese(geese &&rhs);
    //destructor
    ~geese();
    //copy assignment operator
    geese& operator=(const geese &rhs);
    //move assignment operator
    geese& operator=(geese &&rhs);
    //swap
    void swap(geese &rhs);
    friend std::ostream& operator<<(std::ostream &output, const geese &rhs);
    friend std::istream& operator>>(std::istream &input, geese &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    char* m_name;
};

#endif /* GEESE_H */