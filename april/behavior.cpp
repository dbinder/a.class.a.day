//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <behavior.h>
//constructor
behavior::behavior():
  m_exists(0)
{}
behavior::behavior(const behavior &rhs):
  m_exists(rhs.m_exists)
{}
behavior::behavior(behavior &&rhs):
  m_exists(0)
{
  m_exists = rhs.m_exists;
  rhs.m_exists = 0;
}
//destructor
behavior::~behavior(){}
behavior& behavior::operator=(const behavior &rhs)
{
  if (this != &rhs)
  {
    m_exists = rhs.m_exists;
  }
  return *this;
}
behavior& behavior::operator=(behavior &&rhs)
{
  if (this != &rhs)
  {
    m_exists = rhs.m_exists;
    rhs.m_exists = 0;
  }
  return *this;
}