  //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <mom.h>
//constructor
mom::mom():
  m_age(0)
{}
mom::mom(const mom &rhs):
  m_age(rhs.m_age)
{}
mom::mom(mom &&rhs):
  m_age(0)
{
  m_age = rhs.m_age;
  rhs.m_age = 0;
}
//destructor
mom::~mom(){}
mom& mom::operator=(const mom &rhs)
{
  if (this != &rhs)
  {  
    m_age = rhs.m_age;
  }
  return *this;
}
mom& mom::operator=(mom &&rhs)
{
  if (this != &rhs)
  {
    m_age = rhs.m_age;
    rhs.m_age = 0;
  }
  return *this;
}