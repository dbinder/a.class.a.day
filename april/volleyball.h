//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef VOLLEYBALL_H
#define VOLLEYBALL_H
#include <iostream>
//TODO volleyball finish this class
class volleyball
{
  public:
    //constructor
    volleyball();
    //copy constructor
    volleyball(const volleyball &rhs);
    //move constructor
    volleyball(volleyball &&rhs);
    //destructor
    ~volleyball();
    //copy assignment operator
    volleyball& operator=(const volleyball &rhs);
    //move assignment operator
    volleyball& operator=(volleyball &&rhs);
    //Access
    //Modify
  protected:
  private:
    float m_radius;
};

#endif /* VOLLEYBALL_H */