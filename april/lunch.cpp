//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <lunch.h>
//constructor
lunch::lunch():
  m_price(0)
{}
lunch::lunch(const lunch &rhs):
  m_price(rhs.m_price)
{}
lunch::lunch(lunch &&rhs):
  m_price(0)
{
  m_price = rhs.m_price;
  rhs.m_price = 0;
}
//destructor
lunch::~lunch(){}
lunch& lunch::operator=(const lunch &rhs)
{
  if (this != &rhs)
  {
    m_price = rhs.m_price;
  }
  return *this;
}
lunch& lunch::operator=(lunch &&rhs)
{
  if (this != &rhs)
  {
    m_price = rhs.m_price;
    rhs.m_price = 0;
  }
  return *this;
}