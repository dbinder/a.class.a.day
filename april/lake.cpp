//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <lake.h>
//constructor
lake::lake():
  m_depth(0)
{}
lake::lake(const lake &rhs):
  m_depth(rhs.m_depth)
{}
lake::lake(lake &&rhs):
  m_depth(0)
{
  m_depth = rhs.m_depth;
  rhs.m_depth = 0;
}
//destructor
lake::~lake(){}
lake& lake::operator=(const lake &rhs)
{
  if (this != &rhs)
  {
    m_depth = rhs.m_depth;
  }
  return *this;
}
lake& lake::operator=(lake &&rhs)
{
  if (this != &rhs)
  {
    m_depth = rhs.m_depth;
    rhs.m_depth = 0;
  }
  return *this;
}