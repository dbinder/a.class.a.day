//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef VEST_H
#define VEST_H
#include <iostream>
//TODO vest finish this class
class vest
{
  public:
    //constructor
    vest();
    //copy constructor
    vest(const vest &rhs);
    //move constructor
    vest(vest &&rhs);
    //destructor
    ~vest();
    //copy assignment operator
    vest& operator=(const vest &rhs);
    //move assignment operator
    vest& operator=(vest &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_buttons;
};

#endif /* VEST_H */