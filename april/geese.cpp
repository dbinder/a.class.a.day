//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <geese.h>
//constructor
geese::geese():
  m_name(0)
{}
geese::geese(const geese &rhs):
  m_name(rhs.m_name)
{}
geese::geese(geese &&rhs):
  m_name(0)
{
  m_name = rhs.m_name;
  rhs.m_name = 0;
}
//destructor
geese::~geese(){}
geese& geese::operator=(const geese &rhs)
{
  if (this != &rhs)
  {
    m_name = rhs.m_name;
  }
  return *this;
}
geese& geese::operator=(geese &&rhs)
{
  if (this != &rhs)
  {
    m_name = rhs.m_name;
    rhs.m_name = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const geese &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, geese &lhs)
{
  return input;
}
