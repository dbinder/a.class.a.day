//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef WEATHER_H
#define WEATHER_H
#include <iostream>
//TODO weather finish this class
class weather
{
  public:
    //constructor
    weather();
    //copy constructor
    weather(const weather &rhs);
    //move constructor
    weather(weather &&rhs);
    //destructor
    ~weather();
    //copy assignment operator
    weather& operator=(const weather &rhs);
    //move assignment operator
    weather& operator=(weather &&rhs);
    //swap
    void swap(weather &rhs);
    friend std::ostream& operator<<(std::ostream &output, const weather &rhs);
    friend std::istream& operator>>(std::istream &input, weather &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_bad;
};

#endif /* WEATHER_H */