//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <vest.h>
//constructor
vest::vest():
  m_buttons(0)
{}
vest::vest(const vest &rhs):
  m_buttons(rhs.m_buttons)
{}
vest::vest(vest &&rhs):
  m_buttons(0)
{
  m_buttons = rhs.m_buttons;
  rhs.m_buttons = 0;
}
//destructor
vest::~vest(){}
vest& vest::operator=(const vest &rhs)
{
  if (this != &rhs)
  {
    m_buttons = rhs.m_buttons;
  }
  return *this;
}
vest& vest::operator=(vest &&rhs)
{
  if (this != &rhs)
  {
    m_buttons = rhs.m_buttons;
    rhs.m_buttons = 0;
  }
  return *this;
}