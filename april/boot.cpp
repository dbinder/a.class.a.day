//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <boot.h>
//constructor
boot::boot():
  m_size(0)
{}
boot::boot(const boot &rhs):
  m_size(rhs.m_size)
{}
boot::boot(boot &&rhs):
  m_size(0)
{
  m_size = rhs.m_size;
  rhs.m_size = 0;
}
//destructor
boot::~boot(){}
boot& boot::operator=(const boot &rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
  }
  return *this;
}
boot& boot::operator=(boot &&rhs)
{
  if (this != &rhs)
  {  
    m_size = rhs.m_size;
    rhs.m_size = 0;
  }
  return *this;
}