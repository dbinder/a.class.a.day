//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <flight.h>
//constructor
flight::flight():
  m_duration(0)
{}
flight::flight(const flight &rhs):
  m_duration(rhs.m_duration)
{}
flight::flight(flight &&rhs):
  m_duration(0)
{
  m_duration = rhs.m_duration;
  rhs.m_duration = 0;
}
//destructor
flight::~flight(){}
flight& flight::operator=(const flight &rhs)
{
  if (this != &rhs)
  {
    m_duration = rhs.m_duration;
  }
  return *this;
}
flight& flight::operator=(flight &&rhs)
{
  if (this != &rhs)
  {
    m_duration = rhs.m_duration;
    rhs.m_duration = 0;
  }
  return *this;
}