//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef INVENTION_H
#define INVENTION_H
#include <iostream>
//TODO invention finish this class
class invention
{
  public:
    //constructor
    invention();
    //copy constructor
    invention(const invention &rhs);
    //move constructor
    invention(invention &&rhs);
    //destructor
    ~invention();
    //copy assignment operator
    invention& operator=(const invention &rhs);
    //move assignment operator
    invention& operator=(invention &&rhs);
    //Access
    //Modify
  protected:
  private:
    int m_inventionNumber;
};

#endif /* INVENTION_H */