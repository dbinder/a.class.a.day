//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <ink.h>
//constructor
ink::ink():
  m_color("")
{}
ink::ink(const ink &rhs):
  m_color(rhs.m_color)
{}
ink::ink(ink &&rhs):
  m_color("")
{
  m_color = rhs.m_color;
  rhs.m_color = "";
}
//destructor
ink::~ink(){}
ink& ink::operator=(const ink &rhs)
{
  if (this != &rhs)
  {
    m_color = rhs.m_color;
  }
  return *this;
}
ink& ink::operator=(ink &&rhs)
{
  if (this != &rhs)
  { 
    m_color = rhs.m_color;
    rhs.m_color = "";
  }
  return *this;
}