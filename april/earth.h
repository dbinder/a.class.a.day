//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef EARTH_H
#define EARTH_H
#include <iostream>
//TODO earth finish this class
class earth
{
  public:
    //constructor
    earth();
    //copy constructor
    earth(const earth &rhs);
    //move constructor
    earth(earth &&rhs);
    //destructor
    ~earth();
    //copy assignment operator
    earth& operator=(const earth &rhs);
    //move assignment operator
    earth& operator=(earth &&rhs);
    //swap
    void swap(earth &rhs);
    friend std::ostream& operator<<(std::ostream &output, const earth &rhs);
    friend std::istream& operator>>(std::istream &input, earth &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_population;
};

#endif /* EARTH_H */