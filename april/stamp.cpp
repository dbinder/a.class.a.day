//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <stamp.h>
//constructor
stamp::stamp():
  m_price(0)
{}
stamp::stamp(const stamp &rhs):
  m_price(rhs.m_price)
{}
stamp::stamp(stamp &&rhs):
  m_price(0)
{
  m_price = rhs.m_price;
  rhs.m_price = 0;
}
//destructor
stamp::~stamp(){}
stamp& stamp::operator=(const stamp &rhs)
{
  if (this != &rhs)
  {
    m_price = rhs.m_price;
  }
  return *this;
}
stamp& stamp::operator=(stamp &&rhs)
{
  if (this != &rhs)
  {
    m_price = rhs.m_price;
    rhs.m_price = 0;
  }
  return *this;
}