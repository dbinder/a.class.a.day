//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <idea.h>
//constructor
idea::idea():
  m_info(0)
{}
idea::idea(const idea &rhs):
  m_info(rhs.m_info)
{}
idea::idea(idea &&rhs):
  m_info(0)
{
  m_info = rhs.m_info;
  rhs.m_info = "";
}
//destructor
idea::~idea(){}
idea& idea::operator=(const idea &rhs)
{
  if (this != &rhs)
  {
    m_info = rhs.m_info;
  }
  return *this;
}
idea& idea::operator=(idea &&rhs)
{
  if (this != &rhs)
  {
    m_info = rhs.m_info;
    rhs.m_info = "";
  }
  return *this;
}