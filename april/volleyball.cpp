//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <volleyball.h>
//constructor
volleyball::volleyball():
  m_radius(0)
{}
volleyball::volleyball(const volleyball &rhs):
  m_radius(rhs.m_radius)
{}
volleyball::volleyball(volleyball &&rhs):
  m_radius(0)
{
  m_radius = rhs.m_radius;
  rhs.m_radius = 0;
}
//destructor
volleyball::~volleyball(){}
volleyball& volleyball::operator=(const volleyball &rhs)
{
  if (this != &rhs)
  {
    m_radius = rhs.m_radius;
  }
  return *this;
}
volleyball& volleyball::operator=(volleyball &&rhs)
{
  if (this != &rhs)
  {
    m_radius = rhs.m_radius;
    rhs.m_radius = 0;
  }
  return *this;
}