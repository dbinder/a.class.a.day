//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <invention.h>
//constructor
invention::invention():
  m_inventionNumber(0)
{}
invention::invention(const invention &rhs):
  m_inventionNumber(rhs.m_inventionNumber)
{}
invention::invention(invention &&rhs):
  m_inventionNumber(0)
{
  m_inventionNumber = rhs.m_inventionNumber;
  rhs.m_inventionNumber = 0;
}
//destructor
invention::~invention(){}
invention& invention::operator=(const invention &rhs)
{
  if (this != &rhs)
  {
    m_inventionNumber = rhs.m_inventionNumber;
  }
  return *this;
}
invention& invention::operator=(invention &&rhs)
{
  if (this != &rhs)
  {
    m_inventionNumber = rhs.m_inventionNumber;
    rhs.m_inventionNumber = 0;
  }
  return *this;
}