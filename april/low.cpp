//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <low.h>
//constructor
low::low():
  m_low(0)
{}
low::low(const low &rhs):
  m_low(rhs.m_low)
{}
low::low(low &&rhs):
  m_low(0)
{
  m_low = rhs.m_low;
  rhs.m_low = false;
}
//destructor
low::~low(){}
low& low::operator=(const low &rhs)
{
  if (this != &rhs)
  {
    m_low = rhs.m_low;
  }
  return *this;
}
low& low::operator=(low &&rhs)
{
  if (this != &rhs)
  {
    m_low = rhs.m_low;
    rhs.m_low = false;
  }
  return *this;
}
