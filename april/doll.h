//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DOLL_H
#define DOLL_H
#include <iostream>
//TODO doll finish this class
class doll
{
  public:
    //constructor
    doll();
    //copy constructor
    doll(const doll &rhs);
    //move constructor
    doll(doll &&rhs);
    //destructor
    ~doll();
    //copy assignment operator
    doll& operator=(const doll &rhs);
    //move assignment operator
    doll& operator=(doll &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_age;
};

#endif /* DOLL_H */