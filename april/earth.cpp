//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <earth.h>
//constructor
earth::earth():
  m_population(0)
{}
earth::earth(const earth &rhs):
  m_population(rhs.m_population)
{}
earth::earth(earth &&rhs):
  m_population(0)
{
  m_population = rhs.m_population;
  rhs.m_population = 0;
}
//destructor
earth::~earth(){}
earth& earth::operator=(const earth &rhs)
{
  if (this != &rhs)
  {
    m_population = rhs.m_population;
  }
  return *this;
}
earth& earth::operator=(earth &&rhs)
{
  if (this != &rhs)
  {
    m_population = rhs.m_population;
    rhs.m_population = 0;
  }
  return *this;
}