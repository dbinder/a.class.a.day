//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SHAPE_H
#define SHAPE_H
#include <iostream>
//TODO shape finish this class
class shape
{
  public:
    //constructor
    shape();
    //copy constructor
    shape(const shape &rhs);
    //move constructor
    shape(shape &&rhs);
    //destructor
    ~shape();
    //copy assignment operator
    shape& operator=(const shape &rhs);
    //move assignment operator
    shape& operator=(shape &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    float m_radius;
};

#endif /* SHAPE_H */