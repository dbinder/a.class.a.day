//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <doll.h>
//constructor
doll::doll():
  m_age(0)
{}
doll::doll(const doll &rhs):
  m_age(rhs.m_age)
{}
doll::doll(doll &&rhs):
  m_age(0)
{
  m_age = rhs.m_age;
  rhs.m_age = 0;
}
//destructor
doll::~doll(){}
doll& doll::operator=(const doll &rhs)
{
  if (this != &rhs)
  {
    m_age = rhs.m_age;
  }
  return *this;
}
doll& doll::operator=(doll &&rhs)
{
  if (this != &rhs)
  {
    m_age = rhs.m_age;
    rhs.m_age = 0;
  }
  return *this;
}