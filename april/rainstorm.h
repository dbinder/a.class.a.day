//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef RAINSTORM_H
#define RAINSTORM_H
#include <iostream>
//TODO rainstorm finish this class
class rainstorm
{
  public:
    //constructor
    rainstorm();
    //copy constructor
    rainstorm(const rainstorm &rhs);
    //move constructor
    rainstorm(rainstorm &&rhs);
    //destructor
    ~rainstorm();
    //copy assignment operator
    rainstorm& operator=(const rainstorm &rhs);
    //move assignment operator
    rainstorm& operator=(rainstorm &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_duration;
};

#endif /* RAINSTORM_H */