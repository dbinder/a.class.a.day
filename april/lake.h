//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LAKE_H
#define LAKE_H
#include <iostream>
//TODO lake finish this class
class lake
{
  public:
    //constructor
    lake();
    //copy constructor
    lake(const lake &rhs);
    //move constructor
    lake(lake &&rhs);
    //destructor
    ~lake();
    //copy assignment operator
    lake& operator=(const lake &rhs);
    //move assignment operator
    lake& operator=(lake &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_depth;
};

#endif /* LAKE_H */