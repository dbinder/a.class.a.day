//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LOW_H
#define LOW_H
#include <iostream>
//TODO low finish this class
class low
{
  public:
    //constructor
    low();
    //copy constructor
    low(const low &rhs);
    //move constructor
    low(low &&rhs);
    //destructor
    ~low();
    //copy assignment operator
    low& operator=(const low &rhs);
    //move assignment operator
    low& operator=(low &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_low;
};

#endif /* LOW_H */