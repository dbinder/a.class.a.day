//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef HOBBIES_H
#define HOBBIES_H
#include <iostream>
//TODO hobbies finish this class
class hobbies
{
  public:
    //constructor
    hobbies();
    //copy constructor
    hobbies(const hobbies &rhs);
    //move constructor
    hobbies(hobbies &&rhs);
    //destructor
    ~hobbies();
    //copy assignment operator
    hobbies& operator=(const hobbies &rhs);
    //move assignment operator
    hobbies& operator=(hobbies &&rhs);
    //Access
    //Modify
  protected:
  private:
    double m_time;
};

#endif /* HOBBIES_H */