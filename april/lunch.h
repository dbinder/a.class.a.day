//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LUNCH_H
#define LUNCH_H
#include <iostream>
//TODO lunch finish this class
class lunch
{
  public:
    //constructor
    lunch();
    //copy constructor
    lunch(const lunch &rhs);
    //move constructor
    lunch(lunch &&rhs);
    //destructor
    ~lunch();
    //copy assignment operator
    lunch& operator=(const lunch &rhs);
    //move assignment operator
    lunch& operator=(lunch &&rhs);
    //Access
    //Modify
  protected:
  private:
    double m_price;
};

#endif /* LUNCH_H */