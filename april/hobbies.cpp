//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <hobbies.h>
//constructor
hobbies::hobbies():
  m_time(0)
{}
hobbies::hobbies(const hobbies &rhs):
  m_time(rhs.m_time)
{}
hobbies::hobbies(hobbies &&rhs):
  m_time(0)
{
  m_time = rhs.m_time;
  rhs.m_time = 0;
}
//destructor
hobbies::~hobbies(){}
hobbies& hobbies::operator=(const hobbies &rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
  }
  return *this;
}
hobbies& hobbies::operator=(hobbies &&rhs)
{
  if (this != &rhs)
  {  
    m_time = rhs.m_time;
    rhs.m_time = 0;
  }
  return *this;
}