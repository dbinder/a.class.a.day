//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BOOT_H
#define BOOT_H
#include <iostream>
//TODO boot finish this class
class boot
{
  public:
    //constructor
    boot();
    //copy constructor
    boot(const boot &rhs);
    //move constructor
    boot(boot &&rhs);
    //destructor
    ~boot();
    //copy assignment operator
    boot& operator=(const boot &rhs);
    //move assignment operator
    boot& operator=(boot &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    float m_size;
};

#endif /* BOOT_H */