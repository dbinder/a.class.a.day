//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef OIL_H
#define OIL_H
#include <iostream>
//TODO oil finish this class
class oil
{
  public:
    //constructor
    oil();
    //copy constructor
    oil(const oil &rhs);
    //move constructor
    oil(oil &&rhs);
    //destructor
    ~oil();
    //copy assignment operator
    oil& operator=(const oil &rhs);
    //move assignment operator
    oil& operator=(oil &&rhs);
    //swap
    void swap(oil &rhs);
    friend std::ostream& operator<<(std::ostream &output, const oil &rhs);
    friend std::istream& operator>>(std::istream &input, oil &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
};

#endif /* OIL_H */