//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <offer.h>
//constructor
offer::offer():
  m_amount(0)
{}
offer::offer(const offer &rhs):
  m_amount(rhs.m_amount)
{}
offer::offer(offer &&rhs):
  m_amount(0)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
offer::~offer(){}
offer& offer::operator=(const offer &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
offer& offer::operator=(offer &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  }
  return *this;
}