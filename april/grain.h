//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef GRAIN_H
#define GRAIN_H
#include <iostream>
//TODO grain finish this class
class grain
{
  public:
    //constructor
    grain();
    //copy constructor
    grain(const grain &rhs);
    //move constructor
    grain(grain &&rhs);
    //destructor
    ~grain();
    //copy assignment operator
    grain& operator=(const grain &rhs);
    //move assignment operator
    grain& operator=(grain &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_count;
};

#endif /* GRAIN_H */