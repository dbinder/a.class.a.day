//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <relation.h>
//constructor
relation::relation():
  m_flag(0)
{}
relation::relation(const relation &rhs):
  m_flag(rhs.m_flag)
{}
relation::relation(relation &&rhs):
  m_flag(0)
{
  m_flag = rhs.m_flag;
  rhs.m_flag = 0;
}
//destructor
relation::~relation(){}
relation& relation::operator=(const relation &rhs)
{
  if (this != &rhs)
  {
    m_flag = rhs.m_flag;
  }
  return *this;
}
relation& relation::operator=(relation &&rhs)
{
  if (this != &rhs)
  {  
    m_flag = rhs.m_flag;
    rhs.m_flag = 0;
  }
  return *this;
}