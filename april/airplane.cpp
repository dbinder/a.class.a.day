//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <airplane.h>
//constructor
airplane::airplane():
  m_time(0)
{}
airplane::airplane(const airplane &rhs):
  m_time(rhs.m_time)
{}
airplane::airplane(airplane &&rhs):
  m_time(0)
{
  m_time = rhs.m_time;
  rhs.m_time = 0;
}
//destructor
airplane::~airplane(){}
airplane& airplane::operator=(const airplane &rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
  }
  return *this;
}
airplane& airplane::operator=(airplane &&rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
    rhs.m_time = 0;
  }
  return *this;
}