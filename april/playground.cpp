//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <playground.h>
//constructor
playground::playground():
  m_location("")
{}
playground::playground(const playground &rhs):
  m_location(rhs.m_location)
{}
playground::playground(playground &&rhs):
  m_location("")
{
  m_location = rhs.m_location;
  rhs.m_location = "";
}
//destructor
playground::~playground(){}
playground& playground::operator=(const playground &rhs)
{
  if (this != &rhs)
  {
    m_location = rhs.m_location;
  }
  return *this;
}
playground& playground::operator=(playground &&rhs)
{
  if (this != &rhs)
  {
    m_location = rhs.m_location;
    rhs.m_location = "";
  }
  return *this;
}