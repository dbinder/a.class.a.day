//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef INK_H
#define INK_H
#include <iostream>
//TODO ink finish this class
class ink
{
  public:
    //constructor
    ink();
    //copy constructor
    ink(const ink &rhs);
    //move constructor
    ink(ink &&rhs);
    //destructor
    ~ink();
    //copy assignment operator
    ink& operator=(const ink &rhs);
    //move assignment operator
    ink& operator=(ink &&rhs);
    //swap
    void swap(ink &rhs);
    friend std::ostream& operator<<(std::ostream &output, const ink &rhs);
    friend std::istream& operator>>(std::istream &input, ink &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_color;
};

#endif /* INK_H */