//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef VISITOR_H
#define VISITOR_H
#include <iostream>
//TODO visitor finish this class
class visitor
{
  public:
    //constructor
    visitor();
    //copy constructor
    visitor(const visitor &rhs);
    //move constructor
    visitor(visitor &&rhs);
    //destructor
    ~visitor();
    //copy assignment operator
    visitor& operator=(const visitor &rhs);
    //move assignment operator
    visitor& operator=(visitor &&rhs);
    //swap
    void swap(visitor &rhs);
    friend std::ostream& operator<<(std::ostream &output, const visitor &rhs);
    friend std::istream& operator>>(std::istream &input, visitor &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    float m_time;
};

#endif /* VISITOR_H */