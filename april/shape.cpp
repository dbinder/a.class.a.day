//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <shape.h>
//constructor
shape::shape():
  m_radius(0)
{}
shape::shape(const shape &rhs):
  m_radius(rhs.m_radius)
{}
shape::shape(shape &&rhs):
  m_radius(0)
{
  m_radius = rhs.m_radius;
  rhs.m_radius = 0;
}

//destructor
shape::~shape(){}
shape& shape::operator=(const shape &rhs)
{
  if (this != &rhs)
  {
    m_radius = rhs.m_radius;
  }
  return *this;
}
shape& shape::operator=(shape &&rhs)
{
  if (this != &rhs)
  {
    m_radius = rhs.m_radius;
    rhs.m_radius = 0;
  }
  return *this;
}