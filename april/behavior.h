//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BEHAVIOR_H
#define BEHAVIOR_H
#include <iostream>
//TODO behavior finish this class
class behavior
{
  public:
    //constructor
    behavior();
    //copy constructor
    behavior(const behavior &rhs);
    //move constructor
    behavior(behavior &&rhs);
    //destructor
    ~behavior();
    //copy assignment operator
    behavior& operator=(const behavior &rhs);
    //move assignment operator
    behavior& operator=(behavior &&rhs);
    //swap
    void swap(behavior &rhs);
    friend std::ostream& operator<<(std::ostream &output, const behavior &rhs);
    friend std::istream& operator>>(std::istream &input, behavior &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_exists;
};

#endif /* BEHAVIOR_H */