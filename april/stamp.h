//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef STAMP_H
#define STAMP_H
#include <iostream>
//TODO stamp finish this class
class stamp
{
  public:
    //constructor
    stamp();
    //copy constructor
    stamp(const stamp &rhs);
    //move constructor
    stamp(stamp &&rhs);
    //destructor
    ~stamp();
    //copy assignment operator
    stamp& operator=(const stamp &rhs);
    //move assignment operator
    stamp& operator=(stamp &&rhs);
    //swap
    void swap(stamp &rhs);
    friend std::ostream& operator<<(std::ostream &output, const stamp &rhs);
    friend std::istream& operator>>(std::istream &input, stamp &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_price;
};

#endif /* STAMP_H */