//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DISCUSSION_H
#define DISCUSSION_H
#include <iostream>
//TODO discussion finish this class
class discussion
{
  public:
    //constructor
    discussion();
    //copy constructor
    discussion(const discussion &rhs);
    //move constructor
    discussion(discussion &&rhs);
    //destructor
    ~discussion();
    //copy assignment operator
    discussion& operator=(const discussion &rhs);
    //move assignment operator
    discussion& operator=(discussion &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_tookPlace;
};

#endif /* DISCUSSION_H */