//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <oil.h>
//constructor
oil::oil():
  m_amount(0)
{}
oil::oil(const oil &rhs):
  m_amount(rhs.m_amount)
{}
oil::oil(oil &&rhs):
  m_amount(0)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
oil::~oil(){}
oil& oil::operator=(const oil &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
oil& oil::operator=(oil &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const oil &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, oil &lhs)
{
  return input;
}
