//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <steel.h>
//constructor
steel::steel():
  m_wieght(0)
{}
steel::steel(const steel &rhs):
  m_wieght(rhs.m_wieght)
{}
steel::steel(steel &&rhs):
  m_wieght(0)
{
  m_wieght = rhs.m_wieght;
  rhs.m_wieght = 0;
}
//destructor
steel::~steel(){}
steel& steel::operator=(const steel &rhs)
{
  if (this != &rhs)
  {
    m_wieght = rhs.m_wieght;
  }
  return *this;
}
steel& steel::operator=(steel &&rhs)
{
  if (this != &rhs)
  {
    m_wieght = rhs.m_wieght;
    rhs.m_wieght = 0;
  }
  return *this;
}