//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef RELATION_H
#define RELATION_H
#include <iostream>
//TODO relation finish this class
class relation
{
  public:
    //constructor
    relation();
    //copy constructor
    relation(const relation &rhs);
    //move constructor
    relation(relation &&rhs);
    //destructor
    ~relation();
    //copy assignment operator
    relation& operator=(const relation &rhs);
    //move assignment operator
    relation& operator=(relation &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_flag;
};

#endif /* RELATION_H */