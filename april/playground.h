//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PLAYGROUND_H
#define PLAYGROUND_H
#include <iostream>
//TODO playground finish this class
class playground
{
  public:
    //constructor
    playground();
    //copy constructor
    playground(const playground &rhs);
    //move constructor
    playground(playground &&rhs);
    //destructor
    ~playground();
    //copy assignment operator
    playground& operator=(const playground &rhs);
    //move assignment operator
    playground& operator=(playground &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_location;
};

#endif /* PLAYGROUND_H */