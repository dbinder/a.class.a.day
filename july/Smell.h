//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SMELL_H
#define SMELL_H
#include <iostream>
//TODO Smell finish this class
class Smell
{
  public:
    //constructor
    Smell();
    //copy constructor
    Smell(const Smell &rhs);
    //move constructor
    Smell(Smell &&rhs);
    //destructor
    ~Smell();
    //copy assignment operator
    Smell& operator=(const Smell &rhs);
    //move assignment operator
    Smell& operator=(Smell &&rhs);
    //swap
    void swap(Smell &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Smell &rhs);
    friend std::istream& operator>>(std::istream &input, Smell &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SMELL_H */