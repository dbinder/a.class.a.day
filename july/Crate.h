//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CRATE_H
#define CRATE_H
#include <iostream>
//TODO Crate finish this class
class Crate
{
  public:
    //constructor
    Crate();
    //copy constructor
    Crate(const Crate &rhs);
    //move constructor
    Crate(Crate &&rhs);
    //destructor
    ~Crate();
    //copy assignment operator
    Crate& operator=(const Crate &rhs);
    //move assignment operator
    Crate& operator=(Crate &&rhs);
    //swap
    void swap(Crate &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Crate &rhs);
    friend std::istream& operator>>(std::istream &input, Crate &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CRATE_H */