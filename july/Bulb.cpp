//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Bulb.h>
//constructor
Bulb::Bulb():
  m_placeholder(0)
{}

Bulb::Bulb(const Bulb &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Bulb::Bulb(Bulb &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Bulb::~Bulb(){}
Bulb& Bulb::operator=(const Bulb &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Bulb& Bulb::operator=(Bulb &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Bulb &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Bulb &lhs)
{
  return input;
}

