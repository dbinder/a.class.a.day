//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BEDS_H
#define BEDS_H
#include <iostream>
//TODO Beds finish this class
class Beds
{
  public:
    //constructor
    Beds();
    //copy constructor
    Beds(const Beds &rhs);
    //move constructor
    Beds(Beds &&rhs);
    //destructor
    ~Beds();
    //copy assignment operator
    Beds& operator=(const Beds &rhs);
    //move assignment operator
    Beds& operator=(Beds &&rhs);
    //swap
    void swap(Beds &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Beds &rhs);
    friend std::istream& operator>>(std::istream &input, Beds &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BEDS_H */