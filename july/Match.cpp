//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Match.h>
//constructor
Match::Match():
  m_placeholder(0)
{}

Match::Match(const Match &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Match::Match(Match &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Match::~Match(){}
Match& Match::operator=(const Match &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Match& Match::operator=(Match &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Match &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Match &lhs)
{
  return input;
}

