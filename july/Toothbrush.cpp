//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Toothbrush.h>
//constructor
Toothbrush::Toothbrush():
  m_placeholder(0)
{}

Toothbrush::Toothbrush(const Toothbrush &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Toothbrush::Toothbrush(Toothbrush &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Toothbrush::~Toothbrush(){}
Toothbrush& Toothbrush::operator=(const Toothbrush &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Toothbrush& Toothbrush::operator=(Toothbrush &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Toothbrush &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Toothbrush &lhs)
{
  return input;
}

