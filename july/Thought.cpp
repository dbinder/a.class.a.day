//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Thought.h>
//constructor
Thought::Thought():
  m_placeholder(0)
{}

Thought::Thought(const Thought &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Thought::Thought(Thought &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Thought::~Thought(){}
Thought& Thought::operator=(const Thought &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Thought& Thought::operator=(Thought &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Thought &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Thought &lhs)
{
  return input;
}

