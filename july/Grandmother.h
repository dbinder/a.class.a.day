//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef GRANDMOTHER_H
#define GRANDMOTHER_H
#include <iostream>
//TODO Grandmother finish this class
class Grandmother
{
  public:
    //constructor
    Grandmother();
    //copy constructor
    Grandmother(const Grandmother &rhs);
    //move constructor
    Grandmother(Grandmother &&rhs);
    //destructor
    ~Grandmother();
    //copy assignment operator
    Grandmother& operator=(const Grandmother &rhs);
    //move assignment operator
    Grandmother& operator=(Grandmother &&rhs);
    //swap
    void swap(Grandmother &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Grandmother &rhs);
    friend std::istream& operator>>(std::istream &input, Grandmother &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* GRANDMOTHER_H */