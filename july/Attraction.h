//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef ATTRACTION_H
#define ATTRACTION_H
#include <iostream>
//TODO Attraction finish this class
class Attraction
{
  public:
    //constructor
    Attraction();
    //copy constructor
    Attraction(const Attraction &rhs);
    //move constructor
    Attraction(Attraction &&rhs);
    //destructor
    ~Attraction();
    //copy assignment operator
    Attraction& operator=(const Attraction &rhs);
    //move assignment operator
    Attraction& operator=(Attraction &&rhs);
    //swap
    void swap(Attraction &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Attraction &rhs);
    friend std::istream& operator>>(std::istream &input, Attraction &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* ATTRACTION_H */