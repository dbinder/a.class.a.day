//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Line.h>
//constructor
Line::Line():
  m_placeholder(0)
{}

Line::Line(const Line &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Line::Line(Line &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Line::~Line(){}
Line& Line::operator=(const Line &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Line& Line::operator=(Line &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Line &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Line &lhs)
{
  return input;
}

