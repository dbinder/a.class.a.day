//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Attraction.h>
//constructor
Attraction::Attraction():
  m_placeholder(0)
{}

Attraction::Attraction(const Attraction &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Attraction::Attraction(Attraction &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Attraction::~Attraction(){}
Attraction& Attraction::operator=(const Attraction &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Attraction& Attraction::operator=(Attraction &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Attraction &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Attraction &lhs)
{
  return input;
}

