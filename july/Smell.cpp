//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Smell.h>
//constructor
Smell::Smell():
  m_placeholder(0)
{}

Smell::Smell(const Smell &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Smell::Smell(Smell &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Smell::~Smell(){}
Smell& Smell::operator=(const Smell &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Smell& Smell::operator=(Smell &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Smell &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Smell &lhs)
{
  return input;
}

