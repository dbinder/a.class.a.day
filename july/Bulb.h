//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BULB_H
#define BULB_H
#include <iostream>
//TODO Bulb finish this class
class Bulb
{
  public:
    //constructor
    Bulb();
    //copy constructor
    Bulb(const Bulb &rhs);
    //move constructor
    Bulb(Bulb &&rhs);
    //destructor
    ~Bulb();
    //copy assignment operator
    Bulb& operator=(const Bulb &rhs);
    //move assignment operator
    Bulb& operator=(Bulb &&rhs);
    //swap
    void swap(Bulb &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Bulb &rhs);
    friend std::istream& operator>>(std::istream &input, Bulb &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BULB_H */