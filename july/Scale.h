//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SCALE_H
#define SCALE_H
#include <iostream>
//TODO Scale finish this class
class Scale
{
  public:
    //constructor
    Scale();
    //copy constructor
    Scale(const Scale &rhs);
    //move constructor
    Scale(Scale &&rhs);
    //destructor
    ~Scale();
    //copy assignment operator
    Scale& operator=(const Scale &rhs);
    //move assignment operator
    Scale& operator=(Scale &&rhs);
    //swap
    void swap(Scale &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Scale &rhs);
    friend std::istream& operator>>(std::istream &input, Scale &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SCALE_H */