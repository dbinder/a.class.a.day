//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Fog.h>
//constructor
Fog::Fog():
  m_placeholder(0)
{}

Fog::Fog(const Fog &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Fog::Fog(Fog &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Fog::~Fog(){}
Fog& Fog::operator=(const Fog &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Fog& Fog::operator=(Fog &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Fog &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Fog &lhs)
{
  return input;
}

