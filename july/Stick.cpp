//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Stick.h>
//constructor
Stick::Stick():
  m_placeholder(0)
{}

Stick::Stick(const Stick &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Stick::Stick(Stick &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Stick::~Stick(){}
Stick& Stick::operator=(const Stick &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Stick& Stick::operator=(Stick &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Stick &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Stick &lhs)
{
  return input;
}

