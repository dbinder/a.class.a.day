//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Jellyfish.h>
//constructor
Jellyfish::Jellyfish():
  m_placeholder(0)
{}

Jellyfish::Jellyfish(const Jellyfish &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Jellyfish::Jellyfish(Jellyfish &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Jellyfish::~Jellyfish(){}
Jellyfish& Jellyfish::operator=(const Jellyfish &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Jellyfish& Jellyfish::operator=(Jellyfish &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Jellyfish &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Jellyfish &lhs)
{
  return input;
}

