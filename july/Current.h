//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CURRENT_H
#define CURRENT_H
#include <iostream>
//TODO Current finish this class
class Current
{
  public:
    //constructor
    Current();
    //copy constructor
    Current(const Current &rhs);
    //move constructor
    Current(Current &&rhs);
    //destructor
    ~Current();
    //copy assignment operator
    Current& operator=(const Current &rhs);
    //move assignment operator
    Current& operator=(Current &&rhs);
    //swap
    void swap(Current &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Current &rhs);
    friend std::istream& operator>>(std::istream &input, Current &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CURRENT_H */