//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Carriage.h>
//constructor
Carriage::Carriage():
  m_placeholder(0)
{}

Carriage::Carriage(const Carriage &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Carriage::Carriage(Carriage &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Carriage::~Carriage(){}
Carriage& Carriage::operator=(const Carriage &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Carriage& Carriage::operator=(Carriage &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Carriage &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Carriage &lhs)
{
  return input;
}

