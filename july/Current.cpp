//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Current.h>
//constructor
Current::Current():
  m_placeholder(0)
{}

Current::Current(const Current &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Current::Current(Current &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Current::~Current(){}
Current& Current::operator=(const Current &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Current& Current::operator=(Current &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Current &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Current &lhs)
{
  return input;
}

