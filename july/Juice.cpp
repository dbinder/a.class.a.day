//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Juice.h>
//constructor
Juice::Juice():
  m_placeholder(0)
{}

Juice::Juice(const Juice &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Juice::Juice(Juice &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Juice::~Juice(){}
Juice& Juice::operator=(const Juice &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Juice& Juice::operator=(Juice &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Juice &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Juice &lhs)
{
  return input;
}

