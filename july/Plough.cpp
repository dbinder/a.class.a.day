//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Plough.h>
//constructor
Plough::Plough():
  m_placeholder(0)
{}

Plough::Plough(const Plough &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Plough::Plough(Plough &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Plough::~Plough(){}
Plough& Plough::operator=(const Plough &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Plough& Plough::operator=(Plough &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Plough &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Plough &lhs)
{
  return input;
}

