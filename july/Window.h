//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WINDOW_H
#define WINDOW_H
#include <iostream>
//TODO Window finish this class
class Window
{
  public:
    //constructor
    Window();
    //copy constructor
    Window(const Window &rhs);
    //move constructor
    Window(Window &&rhs);
    //destructor
    ~Window();
    //copy assignment operator
    Window& operator=(const Window &rhs);
    //move assignment operator
    Window& operator=(Window &&rhs);
    //swap
    void swap(Window &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Window &rhs);
    friend std::istream& operator>>(std::istream &input, Window &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WINDOW_H */