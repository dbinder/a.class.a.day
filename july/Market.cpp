//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Market.h>
//constructor
Market::Market():
  m_placeholder(0)
{}

Market::Market(const Market &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Market::Market(Market &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Market::~Market(){}
Market& Market::operator=(const Market &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Market& Market::operator=(Market &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Market &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Market &lhs)
{
  return input;
}

