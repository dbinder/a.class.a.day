//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef JUICE_H
#define JUICE_H
#include <iostream>
//TODO Juice finish this class
class Juice
{
  public:
    //constructor
    Juice();
    //copy constructor
    Juice(const Juice &rhs);
    //move constructor
    Juice(Juice &&rhs);
    //destructor
    ~Juice();
    //copy assignment operator
    Juice& operator=(const Juice &rhs);
    //move assignment operator
    Juice& operator=(Juice &&rhs);
    //swap
    void swap(Juice &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Juice &rhs);
    friend std::istream& operator>>(std::istream &input, Juice &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* JUICE_H */