//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Grandmother.h>
//constructor
Grandmother::Grandmother():
  m_placeholder(0)
{}

Grandmother::Grandmother(const Grandmother &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Grandmother::Grandmother(Grandmother &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Grandmother::~Grandmother(){}
Grandmother& Grandmother::operator=(const Grandmother &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Grandmother& Grandmother::operator=(Grandmother &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Grandmother &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Grandmother &lhs)
{
  return input;
}

