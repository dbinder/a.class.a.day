//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Property.h>
//constructor
Property::Property():
  m_placeholder(0)
{}

Property::Property(const Property &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Property::Property(Property &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Property::~Property(){}
Property& Property::operator=(const Property &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Property& Property::operator=(Property &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Property &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Property &lhs)
{
  return input;
}

