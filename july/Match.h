//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MATCH_H
#define MATCH_H
#include <iostream>
//TODO Match finish this class
class Match
{
  public:
    //constructor
    Match();
    //copy constructor
    Match(const Match &rhs);
    //move constructor
    Match(Match &&rhs);
    //destructor
    ~Match();
    //copy assignment operator
    Match& operator=(const Match &rhs);
    //move assignment operator
    Match& operator=(Match &&rhs);
    //swap
    void swap(Match &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Match &rhs);
    friend std::istream& operator>>(std::istream &input, Match &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MATCH_H */