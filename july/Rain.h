//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef RAIN_H
#define RAIN_H
#include <iostream>
//TODO Rain finish this class
class Rain
{
  public:
    //constructor
    Rain();
    //copy constructor
    Rain(const Rain &rhs);
    //move constructor
    Rain(Rain &&rhs);
    //destructor
    ~Rain();
    //copy assignment operator
    Rain& operator=(const Rain &rhs);
    //move assignment operator
    Rain& operator=(Rain &&rhs);
    //swap
    void swap(Rain &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Rain &rhs);
    friend std::istream& operator>>(std::istream &input, Rain &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* RAIN_H */