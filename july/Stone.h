//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STONE_H
#define STONE_H
#include <iostream>
//TODO Stone finish this class
class Stone
{
  public:
    //constructor
    Stone();
    //copy constructor
    Stone(const Stone &rhs);
    //move constructor
    Stone(Stone &&rhs);
    //destructor
    ~Stone();
    //copy assignment operator
    Stone& operator=(const Stone &rhs);
    //move assignment operator
    Stone& operator=(Stone &&rhs);
    //swap
    void swap(Stone &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Stone &rhs);
    friend std::istream& operator>>(std::istream &input, Stone &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STONE_H */