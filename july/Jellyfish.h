//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef JELLYFISH_H
#define JELLYFISH_H
#include <iostream>
//TODO Jellyfish finish this class
class Jellyfish
{
  public:
    //constructor
    Jellyfish();
    //copy constructor
    Jellyfish(const Jellyfish &rhs);
    //move constructor
    Jellyfish(Jellyfish &&rhs);
    //destructor
    ~Jellyfish();
    //copy assignment operator
    Jellyfish& operator=(const Jellyfish &rhs);
    //move assignment operator
    Jellyfish& operator=(Jellyfish &&rhs);
    //swap
    void swap(Jellyfish &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Jellyfish &rhs);
    friend std::istream& operator>>(std::istream &input, Jellyfish &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* JELLYFISH_H */