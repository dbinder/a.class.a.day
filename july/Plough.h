//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PLOUGH_H
#define PLOUGH_H
#include <iostream>
//TODO Plough finish this class
class Plough
{
  public:
    //constructor
    Plough();
    //copy constructor
    Plough(const Plough &rhs);
    //move constructor
    Plough(Plough &&rhs);
    //destructor
    ~Plough();
    //copy assignment operator
    Plough& operator=(const Plough &rhs);
    //move assignment operator
    Plough& operator=(Plough &&rhs);
    //swap
    void swap(Plough &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Plough &rhs);
    friend std::istream& operator>>(std::istream &input, Plough &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PLOUGH_H */