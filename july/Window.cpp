//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Window.h>
//constructor
Window::Window():
  m_placeholder(0)
{}

Window::Window(const Window &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Window::Window(Window &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Window::~Window(){}
Window& Window::operator=(const Window &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Window& Window::operator=(Window &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Window &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Window &lhs)
{
  return input;
}

