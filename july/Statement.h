//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STATEMENT_H
#define STATEMENT_H
#include <iostream>
//TODO Statement finish this class
class Statement
{
  public:
    //constructor
    Statement();
    //copy constructor
    Statement(const Statement &rhs);
    //move constructor
    Statement(Statement &&rhs);
    //destructor
    ~Statement();
    //copy assignment operator
    Statement& operator=(const Statement &rhs);
    //move assignment operator
    Statement& operator=(Statement &&rhs);
    //swap
    void swap(Statement &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Statement &rhs);
    friend std::istream& operator>>(std::istream &input, Statement &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STATEMENT_H */