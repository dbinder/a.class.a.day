//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MARKET_H
#define MARKET_H
#include <iostream>
//TODO Market finish this class
class Market
{
  public:
    //constructor
    Market();
    //copy constructor
    Market(const Market &rhs);
    //move constructor
    Market(Market &&rhs);
    //destructor
    ~Market();
    //copy assignment operator
    Market& operator=(const Market &rhs);
    //move assignment operator
    Market& operator=(Market &&rhs);
    //swap
    void swap(Market &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Market &rhs);
    friend std::istream& operator>>(std::istream &input, Market &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MARKET_H */