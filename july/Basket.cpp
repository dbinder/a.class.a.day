//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Basket.h>
//constructor
Basket::Basket():
  m_placeholder(0)
{}

Basket::Basket(const Basket &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Basket::Basket(Basket &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Basket::~Basket(){}
Basket& Basket::operator=(const Basket &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Basket& Basket::operator=(Basket &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Basket &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Basket &lhs)
{
  return input;
}

