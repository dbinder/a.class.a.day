//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef CARRIAGE_H
#define CARRIAGE_H
#include <iostream>
//TODO Carriage finish this class
class Carriage
{
  public:
    //constructor
    Carriage();
    //copy constructor
    Carriage(const Carriage &rhs);
    //move constructor
    Carriage(Carriage &&rhs);
    //destructor
    ~Carriage();
    //copy assignment operator
    Carriage& operator=(const Carriage &rhs);
    //move assignment operator
    Carriage& operator=(Carriage &&rhs);
    //swap
    void swap(Carriage &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Carriage &rhs);
    friend std::istream& operator>>(std::istream &input, Carriage &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* CARRIAGE_H */