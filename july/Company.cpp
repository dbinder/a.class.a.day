//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Company.h>
//constructor
Company::Company():
  m_placeholder(0)
{}

Company::Company(const Company &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Company::Company(Company &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Company::~Company(){}
Company& Company::operator=(const Company &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Company& Company::operator=(Company &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Company &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Company &lhs)
{
  return input;
}

