//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DESIRE_H
#define DESIRE_H
#include <iostream>
//TODO Desire finish this class
class Desire
{
  public:
    //constructor
    Desire();
    //copy constructor
    Desire(const Desire &rhs);
    //move constructor
    Desire(Desire &&rhs);
    //destructor
    ~Desire();
    //copy assignment operator
    Desire& operator=(const Desire &rhs);
    //move assignment operator
    Desire& operator=(Desire &&rhs);
    //swap
    void swap(Desire &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Desire &rhs);
    friend std::istream& operator>>(std::istream &input, Desire &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DESIRE_H */