//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TOOTHBRUSH_H
#define TOOTHBRUSH_H
#include <iostream>
//TODO Toothbrush finish this class
class Toothbrush
{
  public:
    //constructor
    Toothbrush();
    //copy constructor
    Toothbrush(const Toothbrush &rhs);
    //move constructor
    Toothbrush(Toothbrush &&rhs);
    //destructor
    ~Toothbrush();
    //copy assignment operator
    Toothbrush& operator=(const Toothbrush &rhs);
    //move assignment operator
    Toothbrush& operator=(Toothbrush &&rhs);
    //swap
    void swap(Toothbrush &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Toothbrush &rhs);
    friend std::istream& operator>>(std::istream &input, Toothbrush &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TOOTHBRUSH_H */