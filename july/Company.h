//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef COMPANY_H
#define COMPANY_H
#include <iostream>
//TODO Company finish this class
class Company
{
  public:
    //constructor
    Company();
    //copy constructor
    Company(const Company &rhs);
    //move constructor
    Company(Company &&rhs);
    //destructor
    ~Company();
    //copy assignment operator
    Company& operator=(const Company &rhs);
    //move assignment operator
    Company& operator=(Company &&rhs);
    //swap
    void swap(Company &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Company &rhs);
    friend std::istream& operator>>(std::istream &input, Company &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* COMPANY_H */