//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Stone.h>
//constructor
Stone::Stone():
  m_placeholder(0)
{}

Stone::Stone(const Stone &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Stone::Stone(Stone &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Stone::~Stone(){}
Stone& Stone::operator=(const Stone &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Stone& Stone::operator=(Stone &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Stone &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Stone &lhs)
{
  return input;
}

