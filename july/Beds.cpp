//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Beds.h>
//constructor
Beds::Beds():
  m_placeholder(0)
{}

Beds::Beds(const Beds &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Beds::Beds(Beds &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Beds::~Beds(){}
Beds& Beds::operator=(const Beds &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Beds& Beds::operator=(Beds &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Beds &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Beds &lhs)
{
  return input;
}

