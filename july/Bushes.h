//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BUSHES_H
#define BUSHES_H
#include <iostream>
//TODO Bushes finish this class
class Bushes
{
  public:
    //constructor
    Bushes();
    //copy constructor
    Bushes(const Bushes &rhs);
    //move constructor
    Bushes(Bushes &&rhs);
    //destructor
    ~Bushes();
    //copy assignment operator
    Bushes& operator=(const Bushes &rhs);
    //move assignment operator
    Bushes& operator=(Bushes &&rhs);
    //swap
    void swap(Bushes &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Bushes &rhs);
    friend std::istream& operator>>(std::istream &input, Bushes &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BUSHES_H */