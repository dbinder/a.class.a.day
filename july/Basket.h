//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BASKET_H
#define BASKET_H
#include <iostream>
//TODO Basket finish this class
class Basket
{
  public:
    //constructor
    Basket();
    //copy constructor
    Basket(const Basket &rhs);
    //move constructor
    Basket(Basket &&rhs);
    //destructor
    ~Basket();
    //copy assignment operator
    Basket& operator=(const Basket &rhs);
    //move assignment operator
    Basket& operator=(Basket &&rhs);
    //swap
    void swap(Basket &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Basket &rhs);
    friend std::istream& operator>>(std::istream &input, Basket &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BASKET_H */