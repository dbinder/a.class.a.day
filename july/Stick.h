//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STICK_H
#define STICK_H
#include <iostream>
//TODO Stick finish this class
class Stick
{
  public:
    //constructor
    Stick();
    //copy constructor
    Stick(const Stick &rhs);
    //move constructor
    Stick(Stick &&rhs);
    //destructor
    ~Stick();
    //copy assignment operator
    Stick& operator=(const Stick &rhs);
    //move assignment operator
    Stick& operator=(Stick &&rhs);
    //swap
    void swap(Stick &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Stick &rhs);
    friend std::istream& operator>>(std::istream &input, Stick &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STICK_H */