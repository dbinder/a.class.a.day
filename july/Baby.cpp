//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Baby.h>
//constructor
Baby::Baby():
  m_placeholder(0)
{}

Baby::Baby(const Baby &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Baby::Baby(Baby &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Baby::~Baby(){}
Baby& Baby::operator=(const Baby &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Baby& Baby::operator=(Baby &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Baby &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Baby &lhs)
{
  return input;
}

