//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Increase.h>
//constructor
Increase::Increase():
  m_placeholder(0)
{}

Increase::Increase(const Increase &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Increase::Increase(Increase &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Increase::~Increase(){}
Increase& Increase::operator=(const Increase &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Increase& Increase::operator=(Increase &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Increase &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Increase &lhs)
{
  return input;
}

