//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef DIRECTION_H
#define DIRECTION_H
#include <iostream>
//TODO Direction finish this class
class Direction
{
  public:
    //constructor
    Direction();
    //copy constructor
    Direction(const Direction &rhs);
    //move constructor
    Direction(Direction &&rhs);
    //destructor
    ~Direction();
    //copy assignment operator
    Direction& operator=(const Direction &rhs);
    //move assignment operator
    Direction& operator=(Direction &&rhs);
    //swap
    void swap(Direction &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Direction &rhs);
    friend std::istream& operator>>(std::istream &input, Direction &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* DIRECTION_H */