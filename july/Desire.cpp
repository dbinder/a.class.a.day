//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Desire.h>
//constructor
Desire::Desire():
  m_placeholder(0)
{}

Desire::Desire(const Desire &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Desire::Desire(Desire &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Desire::~Desire(){}
Desire& Desire::operator=(const Desire &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Desire& Desire::operator=(Desire &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Desire &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Desire &lhs)
{
  return input;
}

