//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Crate.h>
//constructor
Crate::Crate():
  m_placeholder(0)
{}

Crate::Crate(const Crate &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Crate::Crate(Crate &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Crate::~Crate(){}
Crate& Crate::operator=(const Crate &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Crate& Crate::operator=(Crate &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Crate &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Crate &lhs)
{
  return input;
}

