//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef INCREASE_H
#define INCREASE_H
#include <iostream>
//TODO Increase finish this class
class Increase
{
  public:
    //constructor
    Increase();
    //copy constructor
    Increase(const Increase &rhs);
    //move constructor
    Increase(Increase &&rhs);
    //destructor
    ~Increase();
    //copy assignment operator
    Increase& operator=(const Increase &rhs);
    //move assignment operator
    Increase& operator=(Increase &&rhs);
    //swap
    void swap(Increase &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Increase &rhs);
    friend std::istream& operator>>(std::istream &input, Increase &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* INCREASE_H */