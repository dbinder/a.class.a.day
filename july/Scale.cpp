//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Scale.h>
//constructor
Scale::Scale():
  m_placeholder(0)
{}

Scale::Scale(const Scale &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Scale::Scale(Scale &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Scale::~Scale(){}
Scale& Scale::operator=(const Scale &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Scale& Scale::operator=(Scale &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Scale &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Scale &lhs)
{
  return input;
}

