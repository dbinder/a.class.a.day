//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Bushes.h>
//constructor
Bushes::Bushes():
  m_placeholder(0)
{}

Bushes::Bushes(const Bushes &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Bushes::Bushes(Bushes &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Bushes::~Bushes(){}
Bushes& Bushes::operator=(const Bushes &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Bushes& Bushes::operator=(Bushes &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Bushes &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Bushes &lhs)
{
  return input;
}

