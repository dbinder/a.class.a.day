//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef LINE_H
#define LINE_H
#include <iostream>
//TODO Line finish this class
class Line
{
  public:
    //constructor
    Line();
    //copy constructor
    Line(const Line &rhs);
    //move constructor
    Line(Line &&rhs);
    //destructor
    ~Line();
    //copy assignment operator
    Line& operator=(const Line &rhs);
    //move assignment operator
    Line& operator=(Line &&rhs);
    //swap
    void swap(Line &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Line &rhs);
    friend std::istream& operator>>(std::istream &input, Line &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* LINE_H */