//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BABY_H
#define BABY_H
#include <iostream>
//TODO Baby finish this class
class Baby
{
  public:
    //constructor
    Baby();
    //copy constructor
    Baby(const Baby &rhs);
    //move constructor
    Baby(Baby &&rhs);
    //destructor
    ~Baby();
    //copy assignment operator
    Baby& operator=(const Baby &rhs);
    //move assignment operator
    Baby& operator=(Baby &&rhs);
    //swap
    void swap(Baby &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Baby &rhs);
    friend std::istream& operator>>(std::istream &input, Baby &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BABY_H */