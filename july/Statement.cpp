//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Statement.h>
//constructor
Statement::Statement():
  m_placeholder(0)
{}

Statement::Statement(const Statement &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Statement::Statement(Statement &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Statement::~Statement(){}
Statement& Statement::operator=(const Statement &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Statement& Statement::operator=(Statement &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Statement &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Statement &lhs)
{
  return input;
}

