//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PROPERTY_H
#define PROPERTY_H
#include <iostream>
//TODO Property finish this class
class Property
{
  public:
    //constructor
    Property();
    //copy constructor
    Property(const Property &rhs);
    //move constructor
    Property(Property &&rhs);
    //destructor
    ~Property();
    //copy assignment operator
    Property& operator=(const Property &rhs);
    //move assignment operator
    Property& operator=(Property &&rhs);
    //swap
    void swap(Property &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Property &rhs);
    friend std::istream& operator>>(std::istream &input, Property &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PROPERTY_H */