//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef FOG_H
#define FOG_H
#include <iostream>
//TODO Fog finish this class
class Fog
{
  public:
    //constructor
    Fog();
    //copy constructor
    Fog(const Fog &rhs);
    //move constructor
    Fog(Fog &&rhs);
    //destructor
    ~Fog();
    //copy assignment operator
    Fog& operator=(const Fog &rhs);
    //move assignment operator
    Fog& operator=(Fog &&rhs);
    //swap
    void swap(Fog &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Fog &rhs);
    friend std::istream& operator>>(std::istream &input, Fog &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* FOG_H */