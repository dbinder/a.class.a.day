//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Rain.h>
//constructor
Rain::Rain():
  m_placeholder(0)
{}

Rain::Rain(const Rain &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Rain::Rain(Rain &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Rain::~Rain(){}
Rain& Rain::operator=(const Rain &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Rain& Rain::operator=(Rain &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Rain &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Rain &lhs)
{
  return input;
}

