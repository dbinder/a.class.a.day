//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Direction.h>
//constructor
Direction::Direction():
  m_placeholder(0)
{}

Direction::Direction(const Direction &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Direction::Direction(Direction &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Direction::~Direction(){}
Direction& Direction::operator=(const Direction &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Direction& Direction::operator=(Direction &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Direction &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Direction &lhs)
{
  return input;
}

