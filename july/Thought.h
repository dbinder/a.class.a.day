//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef THOUGHT_H
#define THOUGHT_H
#include <iostream>
//TODO Thought finish this class
class Thought
{
  public:
    //constructor
    Thought();
    //copy constructor
    Thought(const Thought &rhs);
    //move constructor
    Thought(Thought &&rhs);
    //destructor
    ~Thought();
    //copy assignment operator
    Thought& operator=(const Thought &rhs);
    //move assignment operator
    Thought& operator=(Thought &&rhs);
    //swap
    void swap(Thought &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Thought &rhs);
    friend std::istream& operator>>(std::istream &input, Thought &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* THOUGHT_H */