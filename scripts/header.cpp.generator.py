import argparse
import os

def fileHeader():
  return """//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n"""

#quickly done for now

def write(name, t, variable):
  uppercaseName = name.upper()
  defines = "#pragma once\n#ifndef %s_H\n#define %s_H\n#include <iostream>\n" % (uppercaseName, uppercaseName)
  filefooter = "#endif /* %s_H */" % uppercaseName
  todoNote = "//TODO %s finish this class\n" % name
  public = '  public:\n'
  protected = '  protected:\n'
  private = '  private:\n    %s %s;' % (t, variable)
  constructor = '    //constructor\n    %s();\n' % name
  moveconstructor = '    //move constructor\n    %s(%s &&rhs);\n' % (name, name)
  copyConstructor = '    //copy constructor\n    %s(const %s &rhs);\n' % (name, name)
  assignOperator = '    //copy assignment operator\n    %s& operator=(const %s &rhs);\n' % (name, name)
  moveassignOperator = '    //move assignment operator\n    %s& operator=(%s &&rhs);\n' % (name, name)
  destructor  = '    //destructor\n    ~%s();\n' % name
  swap =  '    //swap\n    void swap(%s &rhs);\n' % name
  ostreamOperator = '    friend std::ostream& operator<<(std::ostream &output, const %s &rhs);\n' % name
  istreamOperator = '    friend std::istream& operator>>(std::istream &input, %s &lhs);\n' % name
  methods = '    //Methods\n' \
            '    //Access\n' \
            '    //Modify\n'
  body = "class %s\n{\n%s%s%s%s%s%s%s%s%s%s%s%s%s\n};\n\n"  \
  % (name, public, constructor, copyConstructor, moveconstructor, destructor, assignOperator, \
     moveassignOperator, swap, ostreamOperator, istreamOperator, methods, protected, private)



  includes = "#include <%s.h>\n" % name
  cppConstructor = '//constructor\n%s::%s():\n  %s(0)\n{}\n\n' % (name, name, variable)
  cppcopyConstructor = '%s::%s(const %s &rhs):\n  %s(rhs.%s)\n{}\n\n' % (name, name, name, variable, variable)
  cppMoveConstructor = '%s::%s(%s &&rhs):\n  %s(0)\n{\n  %s = rhs.%s;\n  rhs.%s = 0;\n}\n\n' % (name, name, name, variable, variable, variable, variable)
  cppDestructor = '//destructor\n%s::~%s(){}\n' % (name, name)
  cppAssignmentOperator = '%s& %s::operator=(const %s &rhs)\n{\n  if (this != &rhs)\n  {\n    %s = rhs.%s;\n  }\n  return *this;\n}\n\n'\
   % (name, name, name, variable, variable)
  cppMoveAssignmentOperator = '%s& %s::operator=(%s &&rhs)\n{\n  if (this != &rhs)\n  {\n    %s = rhs.%s;\n    rhs.%s = 0;\n  }\n  return *this;\n}\n\n'\
   % (name, name, name, variable, variable, variable)
  #cppSwap = 'void %s::swap(%s &rhs)\n{\n  using std::swap;\n}\n' %(name, name)
  cppOstreamOperator = 'std::ostream &operator<<(std::ostream &output, const %s &rhs)\n{\n  return output;\n}\n\n' % name
  cppIstreamOperator = 'std::istream &operator>>(std::istream &input, %s &lhs)\n{\n  return input;\n}\n\n' % name
#print (fileHeader())
#print (filefooter)
  
  header = open("../december/"+ name + ".h", 'w')
  cpp = open("../december/" + name +".cpp", 'w')
  header.write(fileHeader())
  header.write(defines)
  header.write(todoNote)
  header.write(body)
  header.write(filefooter)
  cpp.write(fileHeader())
  cpp.write(includes)
  cpp.write(cppConstructor)
  cpp.write(cppcopyConstructor)
  cpp.write(cppMoveConstructor)
  cpp.write(cppDestructor)
  cpp.write(cppAssignmentOperator)
  cpp.write(cppMoveAssignmentOperator)
  cpp.write(cppOstreamOperator)
  cpp.write(cppIstreamOperator)
  header.close()
  cpp.close()
#fix for cpp file. use place holder.
def parse_argument():
  #Create the parser
  parser = argparse.ArgumentParser(
    prog="cpp and h generator",
    description='Make skeleton .h and .cpp files',
    add_help=True)
  parser.add_argument('-n', '--name', action='store', dest='name', help="name of the files to generate")
  parser.add_argument('-v', '--variable', action='store', dest='variable', help="variable name")
  parser.add_argument('-t', '--type', action='store', dest='t', help="type of variable")
  args = parser.parse_args()
  name = args.name
  t = args.t
  variable = args.variable
  write(name, t, variable)
  return 1



def main():
  if parse_argument():
      print ("Done.")
  else:
    print ("Argument error!")

if __name__ == "__main__":
    main()

#  C(const C&) = default;               // Copy constructor
#  C(C&&) = default;                    // Move constructor
#  C& operator=(const C&) & = default;  // Copy assignment operator
#  C& operator=(C&&) & = default;       // Move assignment operator
#  virtual ~C() { }                     // Destructor