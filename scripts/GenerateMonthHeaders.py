import glob

months = [
  'february',
  'march',
  'april',
  'may',
  'june',
  'july',
  'august',
  'september',
  'october',
  'november',
  'december'
]

for m in months:
  dir = '../' + m
  fileType = '/*.h'
  files = glob.glob(dir + fileType)

  with open(m.title() + '.h', 'w')as f:
    for x in files:
      x = x.lstrip(dir + '\\')
      line = '#include <%s>\n' % x
      f.write(line)
