//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <parcel.h>
//constructor
parcel::parcel() :
m_weight(0)
{}
//copy constructor
parcel::parcel(const parcel &rhs) :
m_weight(rhs.m_weight)
{}
//move constructor
parcel::parcel(parcel &&rhs) :
m_weight(0)
{
  m_weight = rhs.m_weight;
  rhs.m_weight = 0;
}
//destructor
parcel::~parcel()
{}
//copy assignment operator
parcel& parcel::operator=(const parcel &rhs)
{
  if (this != &rhs)
  {
    m_weight = rhs.m_weight;
  }
  return *this;
}
//move assignment operator
parcel& parcel::operator=(parcel &&rhs)
{
  if (this != &rhs)
  {
    m_weight = rhs.m_weight;
    rhs.m_weight = 0;
  }
  return *this;
}
