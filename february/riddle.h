//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef RIDDLE_H
#define RIDDLE_H
#include <iostream>
#include <string>
//TODO riddle finish this class
class riddle
{
  public:
    //constructor
    riddle();
    //copy constructor
    riddle(const riddle &rhs);
    //move constructor
    riddle(riddle &&rhs);
    //destructor
    ~riddle();
    //copy assignment operator
    riddle& operator=(const riddle &rhs);
    //move assignment operator
    riddle& operator=(riddle &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_riddle;
};

#endif /* RIDDLE_H */