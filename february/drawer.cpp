//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <drawer.h>
//constructor
drawer::drawer():
  m_placeholder(nullptr)
{}
drawer::drawer(const drawer &rhs) :
  m_placeholder(rhs.m_placeholder)
{}
drawer::drawer(drawer &&rhs):
  m_placeholder(nullptr)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = nullptr;
}
//destructor
drawer::~drawer()
{
  if (m_placeholder)
  {
    delete m_placeholder;
  }
}
drawer& drawer::operator=(const drawer &rhs)
{
  if (this != &rhs)
  {
    delete m_placeholder;
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}
drawer& drawer::operator=(drawer &&rhs)
{
  if (this != &rhs)
  {
    delete m_placeholder;
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = nullptr;
  }
  return *this;
}

