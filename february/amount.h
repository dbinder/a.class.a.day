//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef AMOUNT_H
#define AMOUNT_H
#include <iostream>
//TODO Amount finish this class
//this is a straight up counter. 
//for use in other classes as an Amount tracker
class Amount
{
  public:
    //constructor
    Amount();
    //copy constructor
    Amount(const Amount &rhs);
    //constructor
    Amount(unsigned long long i);
    //move constructor
    Amount(Amount &&rhs);
    //destructor
    ~Amount();
    //assignment operator
    Amount& operator=(const Amount &rhs);
    //move assignment operator
    Amount& operator=(Amount &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const Amount &rhs);
    //arithmetic operators
    Amount operator+(const Amount &rhs);
    Amount operator-(const Amount &rhs);
    Amount operator*(const Amount &rhs);
    Amount operator/(const Amount &rhs);
    //increment operators
    Amount& operator++();
    Amount& operator--();
    //comparison operators
    bool operator==(const Amount &rhs);
    bool operator!=(const Amount &rhs);
    bool operator<(const Amount &rhs);
    bool operator>(const Amount &rhs);
    bool operator>=(const Amount &rhs);
    bool operator<=(const Amount &rhs);
    //Methods
    //Get
    unsigned long long getAmount() const;
    //Set
  protected:
  private:
    unsigned long long m_amount;
};

#endif /* AMOUNT_H */