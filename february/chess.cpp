//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <chess.h>
//constructor
chess::chess():
  m_gameOver(false),
  m_timePlayed(0)
{}
chess::chess(const chess &rhs):
  m_gameOver(rhs.m_gameOver),
  m_timePlayed(rhs.m_timePlayed)
{}
chess::chess(chess &&rhs):
  m_gameOver(false),
  m_timePlayed(0)
{
  m_gameOver = rhs.m_gameOver;
  m_timePlayed = rhs.m_timePlayed;
  rhs.m_gameOver = false;
  rhs.m_timePlayed = 0;
}
//destructor
chess::~chess(){}
chess& chess::operator=(const chess &rhs)
{
  if (this != &rhs)
  {
    m_gameOver = rhs.m_gameOver;
    m_timePlayed = rhs.m_timePlayed;
  }
  return *this;
}
chess& chess::operator=(chess &&rhs)
{
  if (this != &rhs)
  {
    m_gameOver = rhs.m_gameOver;
    m_timePlayed = rhs.m_timePlayed;
    rhs.m_gameOver = false;
    rhs.m_timePlayed = 0;
  }
  return *this;
}
