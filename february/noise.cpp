//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <noise.h>
//constructor
noise::noise():
  m_decibel(0)
{}
noise::noise(const noise &rhs):
  m_decibel(rhs.m_decibel)
{}
noise::noise(noise &&rhs):
  m_decibel(0)
{
  m_decibel = rhs.m_decibel;
  rhs.m_decibel = 0;
}
//destructor
noise::~noise(){}
noise& noise::operator=(const noise &rhs)
{
  if (this != &rhs)
  {
    m_decibel = rhs.m_decibel;
  }
  return *this;
}
noise& noise::operator=(noise &&rhs)
{
  if (this != &rhs)
  {
    m_decibel = rhs.m_decibel;
    rhs.m_decibel = 0;
  }
  return *this;
}