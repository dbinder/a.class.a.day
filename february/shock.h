//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SHOCK_H
#define SHOCK_H
#include <iostream>
//TODO shock finish this class
class shock
{
  public:
    //constructor
    shock();
    //copy constructor
    shock(const shock &rhs);
    //move constructor
    shock(shock &&rhs);
    //destructor
    ~shock();
    //copy assignment operator
    shock& operator=(const shock &rhs);
    //move assignment operator
    shock& operator=(shock &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_damage;
};

#endif /* SHOCK_H */