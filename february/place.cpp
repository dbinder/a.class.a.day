//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <place.h>
//constructor
place::place():
  m_latitude(1000),
  m_longitude(1000)
{
  m_location[m_lat] = m_latitude;
  m_location[m_long] = m_longitude;
}
place::place(const place &rhs):
  m_latitude(rhs.m_latitude),
  m_longitude(rhs.m_longitude)
{
  m_location[m_lat] = rhs.m_latitude;
  m_location[m_long] = rhs.m_longitude;
}
place::place(place &&rhs):
  m_latitude(0),
  m_longitude(0)
{
  m_latitude = rhs.m_latitude;
  m_longitude = rhs.m_longitude;
  m_location[m_lat] = rhs.m_latitude;
  m_location[m_long] = rhs.m_longitude;
  rhs.m_latitude = 0;
  rhs.m_longitude = 0;
  rhs.m_location[m_lat] = 0;
  rhs.m_location[m_long] = 0;

}
//destructor
place::~place(){}
place& place::operator=(const place &rhs)
{
  if (this != &rhs)
  {
    m_latitude = rhs.m_latitude;
    m_longitude = rhs.m_longitude;
    m_location[m_lat] = rhs.m_latitude;
    m_location[m_long] = rhs.m_longitude;
  }
  return *this;
}
place& place::operator=(place &&rhs)
{
  if (this != &rhs)
  {
    m_latitude = rhs.m_latitude;
    m_longitude = rhs.m_longitude;
    m_location[m_lat] = rhs.m_latitude;
    m_location[m_long] = rhs.m_longitude;
    rhs.m_latitude = 0;
    rhs.m_longitude = 0;
    rhs.m_location[m_lat] = 0;
    rhs.m_location[m_long] = 0;
  }
  return *this;
}
std::map<std::string, double> place::getLocation() const
{
  return m_location;
}