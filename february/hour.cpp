//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <hour.h>
//constructor
hour::hour():
  m_minutes(0)
{}
//copy constructor
hour::hour(const hour &rhs):
  m_minutes(rhs.m_minutes)
{}
//move constructor
hour::hour(hour &&rhs):
  m_minutes(0)
{
  m_minutes = rhs.m_minutes;
  rhs.m_minutes = 0;
}
//destructor
hour::~hour()
{}
//copy assignment operator
hour& hour::operator=(const hour &rhs)
{
  if (this != &rhs)
  {
    m_minutes = rhs.m_minutes;
  }
  return *this;
}
//move assignment operator
hour& hour::operator=(hour &&rhs)
{
  if (this != &rhs)
  {
    m_minutes = rhs.m_minutes;
    rhs.m_minutes = 0;
  }
  return *this;
}
unsigned int hour::run()
{
  std::this_thread::sleep_for(std::chrono::hours(1));
  m_minutes = 60;
  return m_minutes;
}