//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef COPPER_H
#define COPPER_H
#include <iostream>
#include <Amount.h>
//TODO copper finish this class
class copper
{
  public:
    //constructor
    copper();
    //copy constuctor
    copper(const copper &rhs);
    //move constructor
    copper(copper &&rhs);
    //destructor
    ~copper();
    //assignment operator
    copper& operator=(const copper &rhs);
    //move assignment operator
    copper& operator=(copper &&rhs);
    //Methods
    //Get
    size_t getValue() const;
    //Set
  protected:
  private:
    size_t m_value;
    Amount m_amount;
};

#endif /* COPPER_H */