//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <advertisement.h>
//constructor
advertisement::advertisement():
  m_amount(0)
{}
advertisement::advertisement(const advertisement &rhs):
  m_amount(rhs.m_amount)
{}
advertisement::advertisement(advertisement &&rhs):
  m_amount(0)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
advertisement::~advertisement(){}
advertisement& advertisement::operator=(const advertisement &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
advertisement& advertisement::operator=(advertisement &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  }
  return *this;
}
