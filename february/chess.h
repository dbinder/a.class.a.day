//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef CHESS_H
#define CHESS_H
#include <iostream>
//TODO chess finish this class
class chess
{
  public:
    //constructor
    chess();
    //copy constructor
    chess(const chess &rhs);
    //move constructor
    chess(chess &&rhs);
    //destructor
    ~chess();
    //copy assignment operator
    chess& operator=(const chess &rhs);
    //move assignment operator
    chess& operator=(chess &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_gameOver;
    time_t m_timePlayed;
};

#endif /* CHESS_H */