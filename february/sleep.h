//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SLEEP_H
#define SLEEP_H
#include <iostream>
#include <chrono>
#include <thread>
//TODO sleep finish this class
//sleep in milliseconds

class sleep
{
  public:
    //constructor
    sleep();
    sleep(const int i);
    //copy constructor
    sleep(const sleep &rhs);
    //move constructor
    sleep(sleep &&rhs);
    //destructor
    ~sleep();
    //copy assignment operator
    sleep& operator=(const sleep &rhs);
    //move assignment operator
    sleep& operator=(sleep &&rhs);
    //Methods
    void run();
    //Access
    //Modify
  protected:
  private:
    unsigned int m_time;
};

#endif /* SLEEP_H */