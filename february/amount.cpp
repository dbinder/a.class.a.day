//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <Amount.h>
//constructor
Amount::Amount():
  m_amount(0)
{}
//copy constructor
Amount::Amount(const Amount &rhs):
  m_amount(rhs.m_amount)
{}
//constructor
Amount::Amount(unsigned long long i):
  m_amount(i)
{}
//move constructor
Amount::Amount(Amount &&rhs):
  m_amount(rhs.m_amount)
{
  rhs.m_amount = 0;
}
//destructor
Amount::~Amount()
{}
//assignment operator
Amount& Amount::operator=(const Amount &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
//move assignment operator
Amount& Amount::operator=(Amount &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  }
  return *this;
}
std::ostream& operator<<(std::ostream &output, const Amount &rhs)
{
  output << rhs.m_amount;
  return output;
}
//arithmetic operators
Amount Amount::operator+(const Amount &rhs)
{
  return this->m_amount + rhs.m_amount;
}
Amount Amount::operator-(const Amount &rhs)
{
  return this->m_amount - rhs.m_amount;
}
Amount Amount::operator*(const Amount &rhs)
{
  return this->m_amount * rhs.m_amount;
}
Amount Amount::operator/(const Amount &rhs)
{
  return this->m_amount / rhs.m_amount;
}
//increment operators
Amount& Amount::operator++()
{
  ++m_amount;
  return *this;
}
Amount& Amount::operator--()
{
  --m_amount;
  return *this;
}
//comparison operators
bool Amount::operator==(const Amount &rhs)
{
  return this->m_amount == rhs.m_amount;
}
bool Amount::operator!=(const Amount &rhs)
{
  return !(this == &rhs);
}
bool Amount::operator<(const Amount &rhs)
{
  return this->m_amount < rhs.m_amount;
}
bool Amount::operator>(const Amount &rhs)
{
  return this->m_amount > rhs.m_amount;
}
bool Amount::operator>=(const Amount &rhs)
{
  return this->m_amount >= rhs.m_amount;
}
bool Amount::operator<=(const Amount &rhs)
{
  return this->m_amount <= rhs.m_amount;
}
unsigned long long Amount::getAmount() const
{
  return m_amount;
}