//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TRAIL_H
#define TRAIL_H
#include <iostream>
#include <place.h>
//TODO trail finish this class
class trail : public place
{
public:
  //constructor
  trail();
  //copy constuctor
  trail(const trail &rhs);
  //move constructor
  trail(trail &&rhs);
  //destructor
  virtual ~trail();
  //assignment operator
  trail& operator=(const trail &rhs);
  //move assignment operator
  trail& operator=(trail &&rhs);
  //Methods
  double getLength() const;
  std::map<std::string, double> getLocation() const;
  //Get
  //Set
protected:
private:
  double m_length;
};
#endif /* TRAIL_H */