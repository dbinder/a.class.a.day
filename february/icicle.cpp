//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <icicle.h>
//constructor
icicle::icicle():
  m_height(0),
  m_radius(0),
  m_slantHeight(0)
{}
icicle::icicle(const icicle &rhs):
  m_height(rhs.m_height),
  m_radius(rhs.m_radius),
  m_slantHeight(rhs.m_slantHeight)
{}
icicle::icicle(double height, double radius)
{
  m_height = height;
  m_radius = radius;
  m_slantHeight = sqrt(m_height * m_height + m_radius * m_radius);
}
icicle::icicle(icicle &&rhs):
  m_height(0),
  m_radius(0),
  m_slantHeight(0)
{
  m_height = rhs.m_height;
  m_radius = rhs.m_radius;
  m_slantHeight = rhs.m_slantHeight;
  rhs.m_height = 0;
  rhs.m_radius = 0;
  rhs.m_slantHeight = 0;
}
//destructor
icicle::~icicle(){}
icicle& icicle::operator=(const icicle &rhs)
{
  if (this != &rhs)
  { 
    m_height = rhs.m_height;
    m_radius = rhs.m_radius;
    m_slantHeight = rhs.m_slantHeight;
  }
  return *this;
}
icicle& icicle::operator=(icicle &&rhs)
{
  if (this != &rhs)
  {
    m_height = rhs.m_height;
    m_radius = rhs.m_radius;
    m_slantHeight = rhs.m_slantHeight;
    rhs.m_height = 0;
    rhs.m_radius = 0;
    rhs.m_slantHeight = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const icicle &rhs)
{
  output << rhs.m_height
    << rhs.m_radius
    << rhs.m_slantHeight;
  return output;
}
std::istream &operator>>(std::istream &input, icicle &lhs)
{
  input >> lhs.m_height
    >> lhs.m_radius
    >> lhs.m_slantHeight;
  return input;
}
