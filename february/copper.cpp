//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <copper.h>
//constructor
copper::copper():
  m_value(0),
  m_amount(0)
{}
copper::copper(const copper &rhs):
  m_value(rhs.m_value),
  m_amount(rhs.m_amount)
{}
//move constructor
copper::copper(copper &&rhs):
  m_value(0),
  m_amount(0)
{
  m_value = rhs.m_value;
  m_amount = rhs.m_amount;
  rhs.m_value = 0;
  rhs.m_amount = 0;
}
//destructor
copper::~copper(){}
//assignment operator
copper& copper::operator=(const copper &rhs)
{
  if (this != &rhs)
  {
    m_value = rhs.m_value;
    m_amount = rhs.m_amount;
  }
  return *this;
}
//move assignment operator
copper& copper::operator=(copper &&rhs)
{
  if (this != &rhs)
  {
    m_value = rhs.m_value;
    m_amount = rhs.m_amount;
    rhs.m_value = 0;
    rhs.m_amount = 0;
  }
  return *this;
}
size_t copper::getValue() const
{
  return m_value * m_amount.getAmount();
}