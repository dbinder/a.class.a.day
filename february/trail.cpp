//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <trail.h>
//constructor
trail::trail():
  m_length(0)
{
}
//copy constructor
trail::trail(const trail &rhs):
  m_length(rhs.m_length)
{
}
//move constructor
trail::trail(trail &&rhs)
{
  m_length = rhs.m_length;
  rhs.m_length = 0;
}
//destructor
trail::~trail(){}
//assignment operator
trail& trail::operator=(const trail &rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
  }
  return *this;
}
//move assignment operator
trail& trail::operator=(trail &&rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
    rhs.m_length = 0;
  }
  return *this;
}
//Methods
double trail::getLength() const
{
  return m_length;
}
std::map<std::string, double> trail::getLocation() const
{
  return place::getLocation();
}
//Get
//Set