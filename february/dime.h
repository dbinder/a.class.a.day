//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DIME_H
#define DIME_H
#include <iostream>
#include <Amount.h>
//TODO dime finish this class
class dime
{
  public:
    //constructor
    dime();
    dime(Amount n);
    //copy constuctor
    dime(const dime &rhs);
    //move constuctor
    dime(dime &&rhs);
    //destructor
    ~dime();
    //assignment operator
    dime& operator=(const dime &rhs) = default;
    //move assignment operator
    dime& operator=(dime &&rhs);
    //arithmetic operators
    dime operator+(const dime &rhs);
    dime operator-(const dime &rhs);
    dime operator*(const dime &rhs);
    dime operator/(const dime &rhs);
    //increment operators
    dime& operator++();
    dime& operator--();
    //comparison operators
    bool operator==(const dime &rhs);
    bool operator!=(const dime &rhs);
    bool operator<(const dime &rhs);
    bool operator>(const dime &rhs);
    bool operator>=(const dime &rhs);
    bool operator<=(const dime &rhs);
    //Get
    double getTotal() const;
    //Set
  protected:
  private:
    const double m_value = .1;
    Amount m_amount;
};

#endif /* DIME_H */