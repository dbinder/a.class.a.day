//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <table.h>
//constructor
table::table():
  m_height(0),
  m_length(0),
  m_width(0),
  m_numseats(0)
{}
table::table(const table &rhs):
  m_height(rhs.m_height),
  m_length(rhs.m_length),
  m_width(rhs.m_width),
  m_numseats(rhs.m_numseats)
{
}
table::table(table &&rhs):
  m_height(0),
  m_length(0),
  m_width(0),
  m_numseats(0)
{
  m_height = rhs.m_height;
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  m_numseats = rhs.m_numseats;
  rhs.m_height = 0;
  rhs.m_length = 0;
  rhs.m_width = 0;
  rhs.m_numseats = 0;
}
//destructor
table::~table(){}
table& table::operator=(const table &rhs)
{
  if (this != &rhs)
  {
    m_height = rhs.m_height;
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    m_numseats = rhs.m_numseats;
  }
  return *this;
}
table& table::operator=(table &&rhs)
{
  if (this != &rhs)
  {
    m_height = rhs.m_height;
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    m_numseats = rhs.m_numseats;
    rhs.m_height = 0;
    rhs.m_length = 0;
    rhs.m_width = 0;
    rhs.m_numseats = 0;
  }
  return *this;
}
