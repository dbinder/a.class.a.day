//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <station.h>
//constructor
station::station():
  m_placeholder(0)
{}
station::station(const station &rhs){}
//destructor
station::~station(){}
station& station::operator=(const station &rhs)
{
  if (this == &rhs)
    return *this;
  station temp(rhs);
  swap(temp);
  return *this;
}
void station::swap(station &rhs)
{
  using std::swap;
}
std::ostream &operator<<(std::ostream &output, const station &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, station &lhs)
{
  return input;
}
