//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TABLE_H
#define TABLE_H
#include <iostream>
#include <length.h>
//TODO table finish this class
class table
{
  public:
    //constructor
    table();
    //copy constructor
    table(const table &rhs);
    //move constructor
    table(table &&rhs);
    //destructor
    ~table();
    //copy assignment operator
    table& operator=(const table &rhs);
    //move assignment operator
    table& operator=(table &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_height;
    length m_length;
    double m_width;
    int m_numseats;
};

#endif /* TABLE_H */