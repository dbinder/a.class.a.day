//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <underwear.h>
//constructor
underwear::underwear():
  m_size(0)
{}
underwear::underwear(const underwear &rhs):
  m_size(rhs.m_size)
{}
underwear::underwear(underwear &&rhs):
  m_size(0)
{
  m_size = rhs.m_size;
  rhs.m_size = 0;
}
//destructor
underwear::~underwear(){}
underwear& underwear::operator=(const underwear &rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
  }
  return *this;
}
underwear& underwear::operator=(underwear &&rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
    rhs.m_size = 0;
  }
  return *this;
}
