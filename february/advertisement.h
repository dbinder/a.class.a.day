//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef ADVERTISEMENT_H
#define ADVERTISEMENT_H
#include <iostream>
#include <Amount.h>
//TODO advertisement finish this class
class advertisement
{
  public:
    //constructor
    advertisement();
    //copy constructor
    advertisement(const advertisement &rhs);
    //move constructor
    advertisement(advertisement &&rhs);
    //destructor
    ~advertisement();
    //copy assignment operator
    advertisement& operator=(const advertisement &rhs);
    //move assignment operator
    advertisement& operator=(advertisement &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    Amount m_amount;
};

#endif /* ADVERTISEMENT_H */