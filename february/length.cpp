//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <length.h>
//constructor
length::length():
  m_length(0)
{}
//copy constructor
length::length(const length &rhs):
  m_length(rhs.m_length)
{}
//constructor
length::length(unsigned long long i):
  m_length(i)
{}
//move constructor
length::length(length &&rhs):
  m_length(rhs.m_length)
{
  rhs.m_length = 0;
}
//destructor
length::~length()
{}
//assignment operator
length& length::operator=(const length &rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
  }
  return *this;
}
//move assignment operator
length& length::operator=(length &&rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
    rhs.m_length = 0;
  }
  return *this;
}
std::ostream& operator<<(std::ostream &output, const length &rhs)
{
  output << rhs.m_length;
  return output;
}
//arithmetic operators
length length::operator+(const length &rhs)
{
  return this->m_length + rhs.m_length;
}
length length::operator-(const length &rhs)
{
    return this->m_length - rhs.m_length;
}
length length::operator*(const length &rhs)
{
  return this->m_length * rhs.m_length;
}
length length::operator/(const length &rhs)
{
  return this->m_length / rhs.m_length;
}
//increment operators
length& length::operator++()
{
  ++m_length;
  return *this;
}
length& length::operator--()
{
  --m_length;
  return *this;
}
//comparison operators
bool length::operator==(const length &rhs)
{
  return this->m_length == rhs.m_length;
}
bool length::operator!=(const length &rhs)
{
  return !(this == &rhs);
}
bool length::operator<(const length &rhs)
{
  return this->m_length < rhs.m_length;
}
bool length::operator>(const length &rhs)
{
  return this->m_length > rhs.m_length;
}
bool length::operator>=(const length &rhs)
{
  return this->m_length >= rhs.m_length;
}
bool length::operator<=(const length &rhs)
{
  return this->m_length <= rhs.m_length;
}
void length::setZero()
{
  m_length = 0;
}
unsigned long long length::getLength() const
{
  return m_length;
}