//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LUMBER_H
#define LUMBER_H
#include <iostream>
//TODO lumber finish this class
class lumber
{
  public:
    //constructor
    lumber();
    lumber(unsigned int Amount, double length, double width, double height);
    //copy constructor
    lumber(const lumber &rhs);
    //move constructor
    lumber(lumber &&rhs);
    //destructor
    ~lumber();
    //copy assignment operator
    lumber& operator=(const lumber &rhs);
    //move assignment operator
    lumber& operator=(lumber &&rhs);
    //ostream operators
    friend std::ostream& operator<<(std::ostream &output, const lumber &rhs);
    friend std::istream& operator>>(std::istream &input, lumber &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
    double m_length;
    double m_width;
    double m_height;
};

#endif /* LUMBER_H */