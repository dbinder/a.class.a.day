//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <shock.h>
//constructor
shock::shock():
  m_damage(0)
{}
shock::shock(const shock &rhs):
  m_damage(rhs.m_damage)
{}
shock::shock(shock &&rhs):
  m_damage(0)
{
  m_damage = rhs.m_damage;
  rhs.m_damage = 0;
}
//destructor
shock::~shock(){}
shock& shock::operator=(const shock &rhs)
{
  if (this != &rhs)
  {
    m_damage = rhs.m_damage;
  }
  return *this;
}
shock& shock::operator=(shock &&rhs)
{
  if (this != &rhs)
  {
    m_damage = rhs.m_damage;
    rhs.m_damage = 0;
  }
  return *this;
}
