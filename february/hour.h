//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef HOUR_H
#define HOUR_H
#include <iostream>
#include <thread>
#include <chrono>
//TODO hour finish this class
class hour
{
  public:
    //constructor
    hour();
    //copy constructor
    hour(const hour &rhs);
    //move constructor
    hour(hour &&rhs);
    //destructor
    ~hour();
    //copy assignment operator
    hour& operator=(const hour &rhs);
    //move assignment operator
    hour& operator=(hour &&rhs);
    //Methods
    unsigned int run();
    //Access
    //Modify
  protected:
  private:
    unsigned int m_minutes;
};

#endif /* HOUR_H */