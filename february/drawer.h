//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DRAWER_H
#define DRAWER_H
#include <iostream>
//TODO drawer finish this class
//this is a container. it stores stuff
//otherwise in the physical world known as a box. 
class drawer
{
  public:
    //constructor
    drawer();
    //copy constructor
    drawer(const drawer &rhs);
    //move constructor
    drawer(drawer &&rhs);
    //destructor
    ~drawer();
    //copy assignment operator
    drawer& operator=(const drawer &rhs);
    //move assignment operator
    drawer& operator=(drawer &&rhs);
    //ostream operator

    //Methods
    //Access
    //Modify
  protected:
  private:
    void *m_placeholder;
};

#endif /* DRAWER_H */