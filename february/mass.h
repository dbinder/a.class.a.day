//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef MASS_H
#define MASS_H
#include <iostream>
//TODO mass finish this class
class mass
{
  public:
    //constructor
    mass();
    mass(float x);
    //copy constructor
    mass(const mass &rhs);
    mass(mass &&rhs);
    //destructor
    ~mass();
    //assignment operator
    mass& operator=(const mass &rhs);
    //move assignment
    mass& operator=(mass &&rhs);
    friend std::ostream& operator<<(std::ostream &output, const mass &rhs);
    friend std::istream& operator>>(std::istream &input, mass &lhs);
    //arithmetic operators
    mass operator+(const mass &rhs);
    mass operator-(const mass &rhs);
    //comparison operators
    bool operator==(const mass &rhs);
    bool operator!=(const mass &rhs);
    bool operator<(const mass &rhs);
    bool operator>(const mass &rhs);
    bool operator<=(const mass &rhs);
    bool operator>=(const mass &rhs);
    //Methods
    //Maybe return conversions, from kgs.
    //Get
    //Set
    bool setZero();
  protected:
  private:
    float m_mass;//in kilograms should never go to below zero
};

#endif /* MASS_H */