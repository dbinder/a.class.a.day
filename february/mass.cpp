//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <mass.h>
//constructor
mass::mass():
  m_mass(0)
{}
mass::mass(float x):
m_mass(x)
{}
mass::mass(const mass &rhs) :
  m_mass(rhs.m_mass)
{}
mass::mass(mass &&rhs) :
  m_mass(0)
{
  m_mass = rhs.m_mass;
  rhs.m_mass = 0;
}
//destructor
mass::~mass(){}
//assignment operator
mass& mass::operator=(const mass &rhs)
{
  if (this != &rhs)
  {
    m_mass = rhs.m_mass;
  }
  return *this;
}
//move assignment operator
mass& mass::operator=(mass &&rhs)
{
  if (this != &rhs)
  {
    m_mass = rhs.m_mass;
    rhs.m_mass = 0;
  }
  return *this;
}
std::ostream& operator<<(std::ostream &output, const mass &rhs)
{
  output << rhs.m_mass;
  return output;
}
std::istream& operator>>(std::istream &input, mass &lhs)
{
  input >> lhs.m_mass;
  return input;
}

//arithmetic operators
mass mass::operator+(const mass &rhs)
{
  return (this->m_mass + rhs.m_mass);
}
mass mass::operator-(const mass &rhs)
{
  //TODO checkout for less than zero. 
  return (this->m_mass - rhs.m_mass);
}
//comparison operators
bool mass::operator==(const mass &rhs)
{
  return (this->m_mass == rhs.m_mass);
}
bool mass::operator!=(const mass &rhs)
{
  return !(this == &rhs);
}
bool mass::operator<(const mass &rhs)
{
  return (this->m_mass < rhs.m_mass);
}
bool mass::operator>(const mass &rhs)
{
  return (this->m_mass > rhs.m_mass);
}
bool mass::operator<=(const mass &rhs)
{
  return (this->m_mass <= rhs.m_mass);
}
bool mass::operator>=(const mass &rhs)
{
  return (this->m_mass >= rhs.m_mass);
}

bool mass::setZero()
{
  return this->m_mass = 0;
}