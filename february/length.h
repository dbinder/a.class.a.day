//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LENGTH_H
#define LENGTH_H
#include <iostream>
//TODO length finish this class
//this is a class to hold a length. 
//fix this it is unsigned so less than zero checks will cause problems

class length
{
public:
  //constructor
  length();
  //copy constructor
  length(const length &rhs);
  //constructor
  length(unsigned long long i);
  //move constructor
  length(length &&rhs);
  //destructor
  ~length();
  //assignment operator
  length& operator=(const length &rhs);
  //move assignment operator
  length& operator=(length &&rhs);
  friend std::ostream& operator<<(std::ostream &output, const length &rhs);
  //arithmetic operators
  length operator+(const length &rhs);
  length operator-(const length &rhs);
  length operator*(const length &rhs);
  length operator/(const length &rhs);
  //increment operators
  length& operator++();
  length& operator--();
  //comparison operators
  bool operator==(const length &rhs);
  bool operator!=(const length &rhs);
  bool operator<(const length &rhs);
  bool operator>(const length &rhs);
  bool operator>=(const length &rhs);
  bool operator<=(const length &rhs);
  //Methods
  void setZero();
  //Get
  unsigned long long getLength() const;
  //Set
protected:
private:
  unsigned long long m_length;
};

#endif /* LENGTH_H */