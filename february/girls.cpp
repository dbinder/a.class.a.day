//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <girls.h>
//constructor
girls::girls():
  m_amount(0)
{}
girls::girls(const girls &rhs):
  m_amount(rhs.m_amount)
{}
girls::girls(girls &&rhs):
  m_amount(0)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
girls::~girls(){}
girls& girls::operator=(const girls &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
girls& girls::operator=(girls &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  } 
  return *this;
}
