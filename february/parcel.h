//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PARCEL_H
#define PARCEL_H
#include <iostream>
//TODO parcel finish this class
class parcel
{
  public:
    //constructor
    parcel();
    //copy constructor
    parcel(const parcel &rhs);
    //move constructor
    parcel(parcel &&rhs);
    //destructor
    ~parcel();
    //copy assignment operator
    parcel& operator=(const parcel &rhs);
    //move assignment operator
    parcel& operator=(parcel &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_weight;
};

#endif /* PARCEL_H */