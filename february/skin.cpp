//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <skin.h>
//constructor
skin::skin():
  m_placeholder(0)
{}
skin::skin(const skin &rhs){}
skin::skin(skin &&rhs){}
//destructor
skin::~skin(){}
skin& skin::operator=(const skin &rhs)
{
  if (this != &rhs)
  {
  }
  return *this;
}
skin& skin::operator=(skin &&rhs)
{
  if (this != &rhs)
  {
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const skin &rhs)
{
  return output;
}
std::istream &operator>>(std::istream &input, skin &lhs)
{
  return input;
}
