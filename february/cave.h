//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef CAVE_H
#define CAVE_H
#include <iostream>
#include <place.h>
//TODO cave finish this class
class cave : public place
{
  public:
    //constructor
    cave();
    //copy constructor
    cave(const cave &rhs);
    //move constructor
    cave(cave &&rhs);
    //destructor
    virtual ~cave();
    //copy assignment operator
    cave& operator=(const cave &rhs);
    //move assignment operator
    cave& operator=(cave &&rhs);
    //swap
    //Methods
    std::map<std::string, double> cave::getLocation() const;
    //Access
    //Modify
  protected:
  private:
    double m_depth;
};

#endif /* CAVE_H */