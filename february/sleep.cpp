//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::{}
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <sleep.h>
//constructor
sleep::sleep():
  m_time(0)
{}
sleep::sleep(const int i):
  m_time(i)
{}
//copy constructor
sleep::sleep(const sleep &rhs):
  m_time(rhs.m_time)
{}
//move constructor
sleep::sleep(sleep &&rhs):
  m_time(0)
{
  m_time = rhs.m_time;
  rhs.m_time = 0;
}
//destructor
sleep::~sleep(){}
//copy assignment operator
sleep& sleep::operator=(const sleep &rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
  }
  return *this;
}
//move assignment operator
sleep& sleep::operator=(sleep &&rhs)
{
  if (this != &rhs)
  {
    m_time = rhs.m_time;
  }
  return *this;
}
//Methods
void sleep::run()
{
  std::this_thread::sleep_for(std::chrono::milliseconds(m_time));
}