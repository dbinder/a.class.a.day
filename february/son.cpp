//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <son.h>
son::son():
  m_age(32),
  m_height(72),
  m_weight(160)
  {}
son::son(const son &rhs):
  m_age(rhs.m_age),
  m_height(rhs.m_height),
  m_weight(rhs.m_weight)
{
  //std::cout << "copy constructor!\n";
}
//move constructor
son::son(son &&rhs):
  m_age(0),
  m_height(0),
  m_weight(0)
{
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;
  rhs.m_age = 0;
  rhs.m_height = 0;
  rhs.m_weight = 0;
}
//destructor
son::~son(){}
//copy assigment
son& son::operator=(const son &rhs)
{
  if (this != &rhs)
  {
    //std::cout << "assignment operator\n";
    m_age = rhs.m_age;
    m_height = rhs.m_height;
    m_weight = rhs.m_weight;
  }
  return *this;
}
//move assignment operator
son& son::operator=(son &&rhs)
{
  if (this != &rhs)
  {
    //std::cout << "move assignment operator\n";
    m_age = rhs.m_age;
    m_height = rhs.m_height;
    m_weight = rhs.m_weight;
    rhs.m_age = 0;
    rhs.m_height = 0;
    rhs.m_weight = 0;
  }
  return *this;
}

std::ostream& operator<<(std::ostream &out, const son &rhs)
{
  // Since operator<< is a friend of the son class, we can access
  // son's members directly.
  out << rhs.m_age
    << ", "
    << rhs.m_height
    << ", "  
    << rhs.m_weight
    << ")"
    << std::endl;
  return out;
}

std::istream &operator>>(std::istream &input, son &lhs)
{
  // Since operator<< is a friend of the son class, we can access
  // son's members directly.
  input >> lhs.m_age;
  input >> lhs.m_height;
  input >> lhs.m_weight;
  return input;
}

//comparison operators
bool operator==(const son &rhs1, const son &rhs2)
{
  return rhs1.m_age == rhs2.m_age &&
    rhs1.m_weight == rhs2.m_weight &&
    rhs1.m_height == rhs2.m_height;
}
bool operator!=(const son &rhs1, const son &rhs2)
{
  return !(rhs1 == rhs2);
}

unsigned int son::getAge()const {return m_age;}
double son::getHeight(){return m_height;}
double son::getWeight(){return m_weight;}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

void son::setAge(unsigned int i)
{
  m_age = i;
}
void son::setHeight(double n)
{
  m_height = n;
}
void son::setWeight(double n)
{
  m_weight = n;
}