//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef COW_H
#define COW_H
#include <string>
//TODO cow finish this class
class cow
{
  public:
    //constructor
    cow();
    //copy constructor
    cow(const cow &rhs);
    //move constructor
    cow(cow &&rhs);
    //destructor
    ~cow();
    //copy assignment operator
    cow& operator=(const cow &rhs);
    //move assignment operator
    cow& operator=(cow &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_name;
};

#endif /* COW_H */