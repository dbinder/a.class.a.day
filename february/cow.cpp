//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <cow.h>
//constructor
cow::cow():
  m_name(nullptr)
{}
cow::cow(const cow &rhs):
  m_name(rhs.m_name)
{}
cow::cow(cow &&rhs):
  m_name(nullptr)
{
  m_name = rhs.m_name;
  rhs.m_name = nullptr;
}
//destructor
cow::~cow(){}
cow& cow::operator=(const cow &rhs)
{
  if (this != &rhs)
  {
    m_name = rhs.m_name;
  }
  return *this;
}
cow& cow::operator=(cow &&rhs)
{
  if (this != &rhs)
  {  
    m_name = rhs.m_name;
    rhs.m_name = nullptr;
  }
  return *this;
}
