//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SON_H
#define SON_H
#include <iostream>
//TODO son finish this class
class son
{
  public:
    //constructor
    son();
    //copy constructor
    son(const son &rhs);
    //move constructor
    son(son &&rhs);
    //destructor
    ~son();
    //assignment operator
    son& operator=(const son &rhs);
    son& operator=(son &&rhs);
    friend std::ostream& operator<<(std::ostream &out, const son &rhs);
    friend std::istream& operator>>(std::istream &in, son &lhs);
    //comparison operators
    friend bool operator==(const son &rhs1, const son &rhs2);
    friend bool operator!=(const son &rhs1, const son &rhs2);

    unsigned int getAge() const;
    double getHeight();
    double getWeight();

    void setAge(unsigned int i);
    void setHeight(double n);
    void setWeight(double n);

  protected:
  private:
    unsigned int m_age;
    double m_height;
    double m_weight;
};

#endif /* SON_H */