//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef GIRLS_H
#define GIRLS_H
#include <iostream>
#include <Amount.h>
//TODO girls finish this class
class girls
{
  public:
    //constructor
    girls();
    //copy constructor
    girls(const girls &rhs);
    //move constructor
    girls(girls &&rhs);
    //destructor
    ~girls();
    //copy assignment operator
    girls& operator=(const girls &rhs);
    //move assignment operator
    girls& operator=(girls &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    Amount m_amount;
};

#endif /* GIRLS_H */