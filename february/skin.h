//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SKIN_H
#define SKIN_H
#include <iostream>
//TODO skin finish this class
class skin
{
  public:
    //constructor
    skin();
    //copy constructor
    skin(const skin &rhs);
    //move constructor
    skin(skin &&rhs);
    //destructor
    ~skin();
    //copy assignment operator
    skin& operator=(const skin &rhs);
    //move assignment operator
    skin& operator=(skin &&rhs);
    //swap
    void swap(skin &rhs);
    friend std::ostream& operator<<(std::ostream &output, const skin &rhs);
    friend std::istream& operator>>(std::istream &input, skin &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_placeholder;
};

#endif /* SKIN_H */