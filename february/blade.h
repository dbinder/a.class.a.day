//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BLADE_H
#define BLADE_H
#include <iostream>
//TODO blade finish this class
//base class for bladed weapons
//swords knives daggers

class blade
{
  enum type//hands required
  {
    ONE,
    TWO
  };
  public:
    //constructor
    blade();
    blade(unsigned int i, blade::type m_type);
    //copy constructor
    blade(const blade &rhs);
    //move constructor
    blade(blade &&rhs);
    //destructor
    ~blade();
    //copy assignment operator
    blade& operator=(const blade &rhs);
    //move assignment operator
    blade& operator=(blade &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_damage;
    blade::type m_type;
};

#endif /* BLADE_H */