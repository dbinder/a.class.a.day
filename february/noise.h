//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef NOISE_H
#define NOISE_H
#include <iostream>
//TODO noise finish this class
class noise
{
  public:
    //constructor
    noise();
    //copy constructor
    noise(const noise &rhs);
    //move constructor
    noise(noise &&rhs);
    //destructor
    ~noise();
    //copy assignment operator
    noise& operator=(const noise &rhs);
    //move assignment operator
    noise& operator=(noise &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_decibel;
};

#endif /* NOISE_H */