//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <blade.h>
//constructor
blade::blade():
  m_damage(0),
  m_type(ONE)
{}
blade::blade(unsigned int i, blade::type type):
  m_damage(i),
  m_type(type)
{}
blade::blade(const blade &rhs):
  m_damage(rhs.m_damage),
  m_type(rhs.m_type)
{}
//move constructor
blade::blade(blade &&rhs):
  m_damage(0),
  m_type(ONE)
{
  m_damage = rhs.m_damage;
  m_type = rhs.m_type;
  rhs.m_damage = 0;
  rhs.m_type = ONE;
}
//destructor
blade::~blade(){}
blade& blade::operator=(const blade &rhs)
{
  if (this != &rhs)
  {
    m_damage = rhs.m_damage;
    m_type = rhs.m_type;
  }
  return *this;
}
blade& blade::operator=(blade &&rhs)
{
  if (this != &rhs)
  {
    m_damage = rhs.m_damage;
    m_type = rhs.m_type;
    rhs.m_damage = 0;
    rhs.m_type = ONE;
  }
  return *this;
}

