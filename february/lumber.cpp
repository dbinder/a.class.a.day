//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <lumber.h>
//constructor
lumber::lumber():
  m_amount(0),
  m_length(0), 
  m_width(0),
  m_height(0)
{}
lumber::lumber(unsigned int Amount, double length, double width, double height):
  m_amount(Amount),
  m_length(length), 
  m_width(width),
  m_height(height)
{}
//copy constructor
lumber::lumber(const lumber &rhs):
  m_amount(rhs.m_amount),
  m_length(rhs.m_length), 
  m_width(rhs.m_width),
  m_height(rhs.m_height)
{}
//move constructor
lumber::lumber(lumber &&rhs):
  m_amount(0),
  m_length(0), 
  m_width(0),
  m_height(0)
{
  m_amount = rhs.m_amount;
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  m_height = rhs.m_height;
  
  rhs.m_amount = 0;
  rhs.m_length = 0;
  rhs.m_width = 0;
  rhs.m_height = 0;
}
//destructor
lumber::~lumber(){}
//assignment operator
lumber& lumber::operator=(const lumber &rhs)
{
  if (this != &rhs)
  {  
    m_amount = rhs.m_amount;
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    m_height = rhs.m_height;
  }
  return *this;
}
//move assignment operator
lumber& lumber::operator=(lumber &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    m_height = rhs.m_height;
  
    rhs.m_amount = 0;
    rhs.m_length = 0;
    rhs.m_width = 0;
    rhs.m_height = 0;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const lumber &rhs)
{
  output << rhs.m_amount
    << rhs.m_length
    << rhs.m_width
    << rhs.m_height;
  return output;
}
std::istream &operator>>(std::istream &input, lumber &lhs)
{
  input >> lhs.m_amount
    >> lhs.m_length
    >> lhs.m_width
    >> lhs.m_height;
  return input;
}
