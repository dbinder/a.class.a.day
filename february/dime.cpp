//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <dime.h>
//constructor
dime::dime():
  m_amount(1)
{}
dime::dime(Amount Amount)
{
  m_amount = Amount;
}
//copy constuctor
dime::dime(const dime &rhs):
  m_amount(rhs.m_amount)
{}
//move constuctor
dime::dime(dime &&rhs)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
dime::~dime(){}
//assignment operator
dime& dime::operator=(dime &&rhs)
{
  m_amount = rhs.m_amount;
  return *this;
}
dime dime::operator+(const dime &rhs)
{
  return this->m_amount + rhs.m_amount;
}
double dime::getTotal() const
{
  return m_amount.getAmount() * m_value;
}