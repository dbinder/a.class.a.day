//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef STATION_H
#define STATION_H
#include <iostream>
//TODO station finish this class
class station
{
  public:
    //constructor
    station();
    //copy constructor
    station(const station &rhs);
    //move constructor
    station(station &&rhs);
    //destructor
    ~station();
    //copy assignment operator
    station& operator=(const station &rhs);
    //move assignment operator
    station& operator=(station &&rhs);
    //swap
    void swap(station &rhs);
    friend std::ostream& operator<<(std::ostream &output, const station &rhs);
    friend std::istream& operator>>(std::istream &input, station &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_placeholder;
};

#endif /* STATION_H */