//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PLACE_H
#define PLACE_H
#include <iostream>
#include <map>
//TODO place finish this class
class place
{
  public:
    //constructor
    place();
    //copy constructor
    place(const place &rhs);
    //move constructor
    place(place &&rhs);
    //destructor
    virtual ~place();
    //copy assignment operator
    place& operator=(const place &rhs);
    //move assignment operator
    place& operator=(place &&rhs);
    //Methods
    virtual std::map<std::string, double> getLocation() const;
    //Access
    //Modify
  protected:
  private:
    double m_latitude;
    double m_longitude;
    std::map<std::string, double> m_location;
    std::string const m_lat = "Latitude";
    std::string const m_long = "Longitude";
};

#endif /* PLACE_H */