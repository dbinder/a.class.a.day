//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef ICICLE_H
#define ICICLE_H
#include <iostream>
//TODO icicle finish this class
//this is a cone, 
class icicle
{
  public:
    //constructor
    icicle();
    //copy constructor
    icicle(const icicle &rhs);
    icicle::icicle(double height, double radius);
    //move constructor
    icicle(icicle &&rhs);
    //destructor
    ~icicle();
    //copy assignment operator
    icicle& operator=(const icicle &rhs);
    //move assignment operator
    icicle& operator=(icicle &&rhs);
    //ostream operators
    friend std::ostream& operator<<(std::ostream &output, const icicle &rhs);
    friend std::istream& operator>>(std::istream &input, icicle &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_height;
    double m_radius;
    double m_slantHeight;
};

#endif /* ICICLE_H */