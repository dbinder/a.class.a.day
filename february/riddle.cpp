//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <riddle.h>
//constructor
riddle::riddle():
  m_riddle("")
{}
riddle::riddle(const riddle &rhs):
  m_riddle(rhs.m_riddle)
{}
riddle::riddle(riddle &&rhs):
  m_riddle("")
{
  m_riddle = rhs.m_riddle;
  rhs.m_riddle = "";
}
//destructor
riddle::~riddle(){}
riddle& riddle::operator=(const riddle &rhs)
{
  if (this != &rhs)
  {
    m_riddle = rhs.m_riddle;
  }
  return *this;
}
riddle& riddle::operator=(riddle &&rhs)
{
  if (this != &rhs)
  {
    m_riddle = rhs.m_riddle;
    rhs.m_riddle = "";
  }
  return *this;
}
