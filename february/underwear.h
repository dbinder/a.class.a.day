//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef UNDERWEAR_H
#define UNDERWEAR_H
#include <iostream>
//TODO underwear finish this class
class underwear
{
  public:
    //constructor
    underwear();
    //copy constructor
    underwear(const underwear &rhs);
    //move constructor
    underwear(underwear &&rhs);
    //destructor
    ~underwear();
    //copy assignment operator
    underwear& operator=(const underwear &rhs);
    //move assignment operator
    underwear& operator=(underwear &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_size;
};

#endif /* UNDERWEAR_H */