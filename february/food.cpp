//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <food.h>
//constructor
food::food():
  m_amount(0),
  m_type(E_SOLID)
{}
//copy constructor
food::food(const food &rhs):
  m_amount(rhs.m_amount),
  m_type(rhs.m_type)
{}
//destructor
food::~food(){}
//assigment operator
food& food::operator=(const food &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_type = rhs.m_type;
  }
  return *this;
}
//move assignemtn operator
food& food::operator=(food &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    m_type = rhs.m_type;
    rhs.m_amount = 0;
    rhs.m_type = E_SOLID;
  }
  return *this;
}
std::ostream &operator<<(std::ostream &output, const food &rhs)
{
  output << rhs.m_amount
    << rhs.m_type;
  return output;
}
