//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef FOOD_H
#define FOOD_H
#include <iostream>
//TODO food finish this class
class food
{
  enum type
  {
    E_SOLID,
    E_LIQUID
  };
  public:
    //constructor
    food();
    //copy constructor
    food(const food &rhs);
    //move constructor
    food(food &&rhs);
    //destructor
    ~food();
    //copy assignment operator
    food& operator=(const food &rhs);
    //move assignment operator
    food& operator=(food &&rhs);
    //assigment operator
    friend std::ostream& operator<<(std::ostream &output, const food &rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
    food::type m_type;
};

#endif /* FOOD_H */