//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <cave.h>
//constructor
cave::cave():
  m_depth(0)
{}
cave::cave(const cave &rhs):
  m_depth(rhs.m_depth)
{}
cave::cave(cave &&rhs):
  m_depth(0)
{
  m_depth = rhs.m_depth;
  rhs.m_depth = 0;
}
//destructor
cave::~cave(){}
cave& cave::operator=(const cave &rhs)
{
  if (this != &rhs)
  {
    m_depth = rhs.m_depth;
  }
  return *this;
}
cave& cave::operator=(cave &&rhs)
{
  if (this != &rhs)
  {
    m_depth = rhs.m_depth;
    rhs.m_depth = 0;
  }
  return *this;
}
std::map<std::string, double> cave::getLocation() const
{
  return getLocation();
}