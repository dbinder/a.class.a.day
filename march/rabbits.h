//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef RABBITS_H
#define RABBITS_H
#include <iostream>
//TODO rabbits finish this class
class rabbits
{
  public:
    //constructor
    rabbits();
    //copy constructor
    rabbits(const rabbits &rhs);
    //move constructor
    rabbits(rabbits &&rhs);
    //destructor
    ~rabbits();
    //copy assignment operator
    rabbits& operator=(const rabbits &rhs);
    //move assignment operator
    rabbits& operator=(rabbits &&rhs);
    //swap
    void swap(rabbits &rhs);
    friend std::ostream& operator<<(std::ostream &output, const rabbits &rhs);
    friend std::istream& operator>>(std::istream &input, rabbits &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
};

#endif /* RABBITS_H */