//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <foot.h>
//constructor
foot::foot():
  m_numToes(0),
  m_size(0)
{}
foot::foot(const foot &rhs):
  m_numToes(rhs.m_numToes),
  m_size(rhs.m_size)
{}
foot::foot(foot &&rhs):
  m_numToes(0),
  m_size(0)
{
  m_numToes = rhs.m_numToes;
  m_size = rhs.m_size;
  rhs.m_numToes = 0;
  rhs.m_size = 0;
}
//destructor
foot::~foot(){}
foot& foot::operator=(const foot &rhs)
{
  if (this != &rhs)
  {
    m_numToes = rhs.m_numToes;
    m_size = rhs.m_size;
  }
  return *this;
}
foot& foot::operator=(foot &&rhs)
{
  if (this != &rhs)
  {
    m_numToes = rhs.m_numToes;
    m_size = rhs.m_size;
    rhs.m_numToes = 0;
    rhs.m_size = 0;
  }
  return *this;
}
