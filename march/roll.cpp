//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <roll.h>
//constructor
roll::roll():
  m_action(0)
{}
roll::roll(const roll &rhs):
  m_action(rhs.m_action)
{}
roll::roll(roll &&rhs):
  m_action(0)
{
  m_action = rhs.m_action;
  rhs.m_action = 0;
}
//destructor
roll::~roll(){}
roll& roll::operator=(const roll &rhs)
{
  if (this != &rhs)
  {
    m_action = rhs.m_action;
  }
  return *this;
}
roll& roll::operator=(roll &&rhs)
{
  if (this != &rhs)
  {
    m_action = rhs.m_action;
    rhs.m_action = 0;
  }
  return *this;
}
