//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <government.h>
//constructor
government::government():
  m_session(0)
{}
government::government(const government &rhs):
  m_session(rhs.m_session)
{}
government::government(government &&rhs):
  m_session(0)
{
  m_session = rhs.m_session;
  rhs.m_session = 0;
}
//destructor
government::~government(){}
government& government::operator=(const government &rhs)
{
  if (this != &rhs)
  {
    m_session = rhs.m_session;
  }
  return *this;
}
government& government::operator=(government &&rhs)
{
  if (this != &rhs)
  {
    m_session = rhs.m_session;
    rhs.m_session = 0;
  }
  return *this;
}
