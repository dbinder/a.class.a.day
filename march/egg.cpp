//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <egg.h>
//constructor
egg::egg():
  m_cracked(0)
{}
egg::egg(const egg &rhs):
  m_cracked(rhs.m_cracked)
{}
egg::egg(egg &&rhs):
  m_cracked(0)
{
  m_cracked = rhs.m_cracked;
  rhs.m_cracked = 0;
}
//destructor
egg::~egg(){}
egg& egg::operator=(const egg &rhs)
{
  if (this != &rhs)
  {
    m_cracked = rhs.m_cracked;
  }
  return *this;
}
egg& egg::operator=(egg &&rhs)
{
  if (this != &rhs)
  {
    m_cracked = rhs.m_cracked;
    rhs.m_cracked = 0;
  }
  return *this;
}
