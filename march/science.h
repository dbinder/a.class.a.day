//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SCIENCE_H
#define SCIENCE_H
#include <iostream>
//TODO science finish this class
class science
{
  public:
    //constructor
    science();
    //copy constructor
    science(const science &rhs);
    //move constructor
    science(science &&rhs);
    //destructor
    ~science();
    //copy assignment operator
    science& operator=(const science &rhs);
    //move assignment operator
    science& operator=(science &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
};

#endif /* SCIENCE_H */