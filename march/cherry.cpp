//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <cherry.h>
//constructor
cherry::cherry():
  m_amount(0)
{}
cherry::cherry(const cherry &rhs):
  m_amount(rhs.m_amount)
{}
cherry::cherry(cherry &&rhs):
  m_amount(0)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
cherry::~cherry(){}
cherry& cherry::operator=(const cherry &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
cherry& cherry::operator=(cherry &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  }
  return *this;
}