//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SPRING_H
#define SPRING_H
#include <iostream>
//TODO spring finish this class
class spring
{
  public:
    //constructor
    spring();
    //copy constructor
    spring(const spring &rhs);
    //move constructor
    spring(spring &&rhs);
    //destructor
    ~spring();
    //copy assignment operator
    spring& operator=(const spring &rhs);
    //move assignment operator
    spring& operator=(spring &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    float m_coefficient;
};

#endif /* SPRING_H */