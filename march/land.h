//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LAND_H
#define LAND_H
#include <iostream>
//TODO land finish this class
class land
{
  public:
    //constructor
    land();
    //copy constructor
    land(const land &rhs);
    //move constructor
    land(land &&rhs);
    //destructor
    ~land();
    //copy assignment operator
    land& operator=(const land &rhs);
    //move assignment operator
    land& operator=(land &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_area;
};

#endif /* LAND_H */