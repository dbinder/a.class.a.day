//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <selection.h>
//constructor
selection::selection():
  m_selected(true)
{}
selection::selection(const selection &rhs):
  m_selected(rhs.m_selected)
{}
selection::selection(selection &&rhs):
  m_selected(true)
{
  m_selected = rhs.m_selected;
  rhs.m_selected = false;
}
//destructor
selection::~selection(){}
selection& selection::operator=(const selection &rhs)
{
  if (this != &rhs)
  {
    m_selected = rhs.m_selected;
  }
  return *this;
}
selection& selection::operator=(selection &&rhs)
{
  if (this != &rhs)
  {
    m_selected = rhs.m_selected;
    rhs.m_selected = false;
  }
  return *this;
}
