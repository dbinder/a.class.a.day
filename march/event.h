//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef EVENT_H
#define EVENT_H
#include <iostream>
//TODO event finish this class
class event
{
  public:
    //constructor
    event();
    //copy constructor
    event(const event &rhs);
    //move constructor
    event(event &&rhs);
    //destructor
    ~event();
    //copy assignment operator
    event& operator=(const event &rhs);
    //move assignment operator
    event& operator=(event &&rhs);

    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_duration;
    bool m_complete;
    unsigned int m_time;
    std::string m_location;
};

#endif /* EVENT_H */