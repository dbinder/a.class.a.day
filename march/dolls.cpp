//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <dolls.h>
//constructor
dolls::dolls():
  m_size(0)
{}
dolls::dolls(const dolls &rhs):
  m_size(rhs.m_size)
{}
dolls::dolls(dolls &&rhs):
  m_size(0)
{
  m_size = rhs.m_size;
  rhs.m_size = 0;
}
//destructor
dolls::~dolls(){}
dolls& dolls::operator=(const dolls &rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
  }
  return *this;
}
dolls& dolls::operator=(dolls &&rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
    rhs.m_size = 0;
  }
  return *this;
}
