//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <stitch.h>
//constructor
stitch::stitch():
  m_count(0)
{}
stitch::stitch(const stitch &rhs):
  m_count(rhs.m_count)
{}
stitch::stitch(stitch &&rhs):
  m_count(0)
{
  m_count = rhs.m_count;
  rhs.m_count = 0;
}
//destructor
stitch::~stitch(){}
stitch& stitch::operator=(const stitch &rhs)
{
  if (this != &rhs)
  {
    m_count = rhs.m_count;
  }
  return *this;
}
stitch& stitch::operator=(stitch &&rhs)
{
  if (this != &rhs)
  {
    m_count = rhs.m_count;
    rhs.m_count = 0;
  }
  return *this;
}
