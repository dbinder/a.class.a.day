//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <boy.h>
//constructor
boy::boy():
  m_age(9),
  m_height(),
  m_weight(123),
  m_eyeColor("Red"),
  m_hairColor("Red")
  {}

//copy constructor
boy::boy(const boy &rhs):
  m_age(rhs.m_age),
  m_height(rhs.m_height),
  m_weight(rhs.m_weight),
  m_eyeColor(rhs.m_eyeColor),
  m_hairColor(rhs.m_hairColor)
{}
//move constructor
boy::boy(boy &&rhs):
  m_age(9),
  m_height(),
  m_weight(123),
  m_eyeColor("Red"),
  m_hairColor("Red")
{
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;
  m_eyeColor = rhs.m_eyeColor;
  m_hairColor = rhs.m_hairColor;
  rhs.m_age = 0;
  rhs.m_height = 0;
  rhs.m_weight = 0;
  rhs.m_eyeColor = "";
  rhs.m_hairColor = "";
}
//destructor
boy::~boy(){}

//assignment operator
boy& boy::operator=(const boy &rhs)
{
  if (this == &rhs)
    return *this;
  m_age = rhs.m_age;
  m_height = rhs.m_height;
  m_weight = rhs.m_weight;
  m_eyeColor = rhs.m_eyeColor;
  m_hairColor = rhs.m_hairColor;
  return *this;
}
//move assignment operator
boy& boy::operator=(boy &&rhs)
{
  if (this != &rhs)
  {
    m_age = rhs.m_age;
    m_height = rhs.m_height;
    m_weight = rhs.m_weight;
    m_eyeColor = rhs.m_eyeColor;
    m_hairColor = rhs.m_hairColor;
    rhs.m_age = 0;
    rhs.m_height = 0;
    rhs.m_weight = 0;
    rhs.m_eyeColor = " ";
    rhs.m_hairColor = " ";
  }
  return *this;
}
//iostream operators
std::ostream& operator<<(std::ostream &output, const boy &rhs)
{
  output << rhs.m_age
    << rhs.m_height
    << rhs.m_weight
    << rhs.m_eyeColor
    << rhs.m_hairColor
    << std::endl;
  return output;
}
std::istream& operator>>(std::istream &input, boy &lhs)
{
  input >> lhs.m_age
    >> lhs.m_height
    >> lhs.m_weight
    >> lhs.m_eyeColor
    >> lhs.m_hairColor;
  return input;
}

//comparison operators
bool operator==(const boy &rhs1, const boy &rhs2)
{
  return rhs1.m_age == rhs2.m_age &&
    rhs1.m_weight == rhs2.m_weight &&
    rhs1.m_height == rhs2.m_height &&
    rhs1.m_hairColor == rhs2.m_hairColor &&
    rhs1.m_eyeColor == rhs2.m_eyeColor;
}
bool operator!=(const boy &rhs1, const boy &rhs2)
{
  return !(rhs1 == rhs2);
}

//Methods
//Get
unsigned int boy::getAge(){return m_age;}
double boy::getHeight(){return m_height;}
double boy::getWeight(){return m_weight;}
std::string boy::getEyeColor(){return m_eyeColor;}
std::string boy::getHairColor(){return m_hairColor;}

//Set
void boy::setAge(unsigned int i)
{
  m_age = i;
}
void boy::setHeight(double n)
{
  m_height = n;
}
void boy::setWeight(double n)
{
  m_weight = n;
}
void boy::setEyeColor(std::string n)
{
  m_eyeColor = n; 
}
void boy::setHairColor(std::string n)
{
  m_hairColor = n;
}