//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PLAY_H
#define PLAY_H
#include <iostream>
//TODO play finish this class
class play
{
  public:
    //constructor
    play();
    //copy constructor
    play(const play &rhs);
    //move constructor
    play(play &&rhs);
    //destructor
    ~play();
    //copy assignment operator
    play& operator=(const play &rhs);
    //move assignment operator
    play& operator=(play &&rhs);
    //Access
    //Modify
  protected:
  private:
    bool m_paused;
};

#endif /* PLAY_H */