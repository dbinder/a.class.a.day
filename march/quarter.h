//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef QUARTER_H
#define QUARTER_H
#include <iostream>

//TODO quarter finish this class
class quarter
{
  public:
    //constructor
    quarter();
    quarter(int n);
    //copy constuctor
    quarter(const quarter &rhs);
    //move constuctor
    quarter(quarter &&rhs);
    //destructor
    ~quarter();
    //assignment operator
    quarter& operator=(const quarter &rhs);
    //move assignment operator
    quarter& operator=(quarter &&rhs);
    //arithmetic operators
    quarter operator+(const quarter &rhs);
    quarter operator-(const quarter &rhs);
    quarter operator*(const quarter &rhs);
    quarter operator/(const quarter &rhs);
    //increment operators
    quarter& operator++();
    quarter& operator--();
    //comparison operators
    bool operator==(const quarter &rhs);
    bool operator!=(const quarter &rhs);
    bool operator<(const quarter &rhs);
    bool operator>(const quarter &rhs);
    bool operator>=(const quarter &rhs);
    bool operator<=(const quarter &rhs);
    //Get
    double getTotal() const;
    //Set
protected:
private:
  const double m_value = .25;
  int m_amount;
};

#endif /* QUARTER_H */