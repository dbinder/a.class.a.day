//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <land.h>
//constructor
land::land():
  m_area(0)
{}
land::land(const land &rhs):
  m_area(rhs.m_area)
{}
land::land(land &&rhs):
  m_area(0)
{
  m_area = rhs.m_area;
  rhs.m_area = 0;
}
//destructor
land::~land(){}
land& land::operator=(const land &rhs)
{
  if (this != &rhs)
  {
    m_area = rhs.m_area;
  }
  return *this;
}
land& land::operator=(land &&rhs)
{
  if (this != &rhs)
  {
    m_area = rhs.m_area;
    rhs.m_area = 0;
  }
  return *this;
}
