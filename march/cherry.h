//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef CHERRY_H
#define CHERRY_H
#include <iostream>
//TODO cherry finish this class
class cherry
{
  public:
    //constructor
    cherry();
    //copy constructor
    cherry(const cherry &rhs);
    //move constructor
    cherry(cherry &&rhs);
    //destructor
    ~cherry();
    //copy assignment operator
    cherry& operator=(const cherry &rhs);
    //move assignment operator
    cherry& operator=(cherry &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_amount;
};

#endif /* CHERRY_H */