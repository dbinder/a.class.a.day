//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <spring.h>
//constructor
spring::spring():
  m_coefficient(0)
{}
spring::spring(const spring &rhs):
  m_coefficient(rhs.m_coefficient)
{}
spring::spring(spring &&rhs):
  m_coefficient(0)
{
  m_coefficient = rhs.m_coefficient;
  rhs.m_coefficient = 0;
}
//destructor
spring::~spring(){}
spring& spring::operator=(const spring &rhs)
{
  if (this != &rhs)
  {
    m_coefficient = rhs.m_coefficient;
  }
  return *this;
}
spring& spring::operator=(spring &&rhs)
{
  if (this != &rhs)
  {
    m_coefficient = rhs.m_coefficient;
    rhs.m_coefficient = 0;
  }
  return *this;
}