//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <existence.h>
//constructor
existence::existence():
  m_isthisreallife(0)
{}
existence::existence(const existence &rhs):
  m_isthisreallife(rhs.m_isthisreallife)
{}
existence::existence(existence &&rhs):
  m_isthisreallife(0)
{
  m_isthisreallife = rhs.m_isthisreallife;
  rhs.m_isthisreallife = 0;
}
//destructor
existence::~existence(){}
existence& existence::operator=(const existence &rhs)
{
  if (this != &rhs)
  {
    m_isthisreallife = rhs.m_isthisreallife;
  }
  return *this;
}
existence& existence::operator=(existence &&rhs)
{
  if (this != &rhs)
  {
    m_isthisreallife = rhs.m_isthisreallife;
    rhs.m_isthisreallife = 0;
  }
  return *this;
}
