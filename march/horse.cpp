//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <horse.h>
//constructor
horse::horse():
  m_name("Mr Ed")
{}
horse::horse(const horse &rhs):
  m_name(rhs.m_name)
{}
horse::horse(horse &&rhs):
  m_name("")
{
  m_name = rhs.m_name;
  rhs.m_name = "";
}
//destructor
horse::~horse(){}
horse& horse::operator=(const horse &rhs)
{
  if (this != &rhs)
  {
    m_name = rhs.m_name;
  }
  return *this;
}
horse& horse::operator=(horse &&rhs)
{
  if (this != &rhs)
  {
    m_name = rhs.m_name;
    rhs.m_name = "";
  }
  return *this;
}