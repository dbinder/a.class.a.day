//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef GOVERNMENT_H
#define GOVERNMENT_H
#include <iostream>
//TODO government finish this class
class government
{
  public:
    //constructor
    government();
    //copy constructor
    government(const government &rhs);
    //move constructor
    government(government &&rhs);
    //destructor
    ~government();
    //copy assignment operator
    government& operator=(const government &rhs);
    //move assignment operator
    government& operator=(government &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_session;
};

#endif /* GOVERNMENT_H */