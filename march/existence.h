//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef EXISTENCE_H
#define EXISTENCE_H
#include <iostream>
//TODO existence finish this class
class existence
{
  public:
    //constructor
    existence();
    //copy constructor
    existence(const existence &rhs);
    //move constructor
    existence(existence &&rhs);
    //destructor
    ~existence();
    //copy assignment operator
    existence& operator=(const existence &rhs);
    //move assignment operator
    existence& operator=(existence &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_isthisreallife;
};

#endif /* EXISTENCE_H */