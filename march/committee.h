//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef COMMITTEE_H
#define COMMITTEE_H
#include <iostream>
//TODO committee finish this class
class committee
{
  public:
    //constructor
    committee();
    //copy constructor
    committee(const committee &rhs);
    //move constructor
    committee(committee &&rhs);
    //destructor
    ~committee();
    //copy assignment operator
    committee& operator=(const committee &rhs);
    //move assignment operator
    committee& operator=(committee &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_purpose;
    std::string m_leader;
};

#endif /* COMMITTEE_H */