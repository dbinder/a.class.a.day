//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <detail.h>
//constructor
detail::detail():
  m_info("")
{}
detail::detail(const detail &rhs):
  m_info(rhs.m_info)
{}
detail::detail(detail &&rhs):
  m_info(0)
{
  m_info = rhs.m_info;
  rhs.m_info = "";
}
//destructor
detail::~detail(){}
detail& detail::operator=(const detail &rhs)
{
  if (this != &rhs)
  {
    m_info = rhs.m_info;
  }
  return *this;
}
detail& detail::operator=(detail &&rhs)
{
  if (this != &rhs)
  {
    m_info = rhs.m_info;
    rhs.m_info = "";
  }
  return *this;
}
