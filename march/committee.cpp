//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <committee.h>
//constructor
committee::committee():
  m_purpose(""),
  m_leader("")
{}
committee::committee(const committee &rhs):
  m_purpose(rhs.m_purpose),
  m_leader(rhs.m_leader)
{}
committee::committee(committee &&rhs):
  m_purpose(""),
  m_leader("")
{
  m_purpose = rhs.m_purpose;
  m_leader = rhs.m_leader;
  rhs.m_purpose = nullptr;
  rhs.m_leader = nullptr;
}
//destructor
committee::~committee(){}
committee& committee::operator=(const committee &rhs)
{
  if (this != &rhs)
  {
    m_purpose = rhs.m_purpose;
    m_leader = rhs.m_leader;
  }
  return *this;
}
committee& committee::operator=(committee &&rhs)
{
  if (this != &rhs)
  {
    m_purpose = rhs.m_purpose;
    m_leader = rhs.m_leader;
    rhs.m_purpose = nullptr;
    rhs.m_leader = nullptr;
  }
  return *this;
}
