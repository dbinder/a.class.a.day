//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <minute.h>
//constructor
minute::minute():
  m_seconds(0)
{}
//copy constructor
minute::minute(const minute &rhs) :
  m_seconds(rhs.m_seconds)
{}
//move constructor
minute::minute(minute &&rhs) :
  m_seconds(0)
{
  m_seconds = rhs.m_seconds;
  rhs.m_seconds = 0;
}
//destructor
minute::~minute()
{}
//copy assignment operator
minute& minute::operator=(const minute &rhs)
{
  if (this != &rhs)
  {
    m_seconds = rhs.m_seconds;
  }
  return *this;
}
//move assignment operator
minute& minute::operator=(minute &&rhs)
{
  if (this != &rhs)
  {
    m_seconds = rhs.m_seconds;
    rhs.m_seconds = 0;
  }
  return *this;
}
unsigned int minute::run()
{
  std::this_thread::sleep_for(std::chrono::seconds(60));
  m_seconds = 60;
  return m_seconds;
}