//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DRESS_H
#define DRESS_H
#include <iostream>
//TODO dress finish this class
class dress
{
  public:
    //constructor
    dress();
    //copy constructor
    dress(const dress &rhs);
    //move constructor
    dress(dress &&rhs);
    //destructor
    ~dress();
    //copy assignment operator
    dress& operator=(const dress &rhs);
    //move assignment operator
    dress& operator=(dress &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_size;
    std::string m_color;
    std::string m_description;

};

#endif /* DRESS_H */