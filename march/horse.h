//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef HORSE_H
#define HORSE_H
#include <iostream>
//TODO horse finish this class
class horse
{
  public:
    //constructor
    horse();
    //copy constructor
    horse(const horse &rhs);
    //move constructor
    horse(horse &&rhs);
    //destructor
    ~horse();
    //copy assignment operator
    horse& operator=(const horse &rhs);
    //move assignment operator
    horse& operator=(horse &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_name;
};

#endif /* HORSE_H */