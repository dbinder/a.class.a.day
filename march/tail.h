//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef TAIL_H
#define TAIL_H
#include <iostream>
//TODO tail finish this class
class tail
{
  public:
    //constructor
    tail();
    //copy constructor
    tail(const tail &rhs);
    //move constructor
    tail(tail &&rhs);
    //destructor
    ~tail();
    //copy assignment operator
    tail& operator=(const tail &rhs);
    //move assignment operator
    tail& operator=(tail &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_segments;
};

#endif /* TAIL_H */