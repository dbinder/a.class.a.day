//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <condition.h>
//constructor
condition::condition():
  m_conditionalFlag(true)
{}
condition::condition(const condition &rhs):
  m_conditionalFlag(rhs.m_conditionalFlag)
{}
condition::condition(condition &&rhs):
  m_conditionalFlag(true)
{
  m_conditionalFlag = rhs.m_conditionalFlag;
  rhs.m_conditionalFlag = false;
}
//destructor
condition::~condition(){}
condition& condition::operator=(const condition &rhs)
{
  if (this != &rhs)
  {
    m_conditionalFlag = rhs.m_conditionalFlag;
  }
  return *this;
}
condition& condition::operator=(condition &&rhs)
{
  if (this != &rhs)
  {
    m_conditionalFlag = rhs.m_conditionalFlag;
    rhs.m_conditionalFlag = false;
  }
  return *this;
}
