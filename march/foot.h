//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef FOOT_H
#define FOOT_H
#include <iostream>
//TODO foot finish this class
class foot
{
  public:
    //constructor
    foot();
    //copy constructor
    foot(const foot &rhs);
    //move constructor
    foot(foot &&rhs);
    //destructor
    ~foot();
    //copy assignment operator
    foot& operator=(const foot &rhs);
    //move assignment operator
    foot& operator=(foot &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_numToes;
    double m_size;
};

#endif /* FOOT_H */