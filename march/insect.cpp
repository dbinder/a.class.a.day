//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <insect.h>
//constructor
insect::insect():
  m_invertabrate(0)
{}
insect::insect(const insect &rhs):
  m_invertabrate(rhs.m_invertabrate)
{}
insect::insect(insect &&rhs):
  m_invertabrate(0)
{
  m_invertabrate = rhs.m_invertabrate;
  rhs.m_invertabrate = 0;
}
//destructor
insect::~insect(){}
insect& insect::operator=(const insect &rhs)
{
  if (this != &rhs)
  {
    m_invertabrate = rhs.m_invertabrate;
  }
  return *this;
}
insect& insect::operator=(insect &&rhs)
{
  if (this != &rhs)
  {
    m_invertabrate = rhs.m_invertabrate;
    rhs.m_invertabrate = 0;
  }
  return *this;
}
