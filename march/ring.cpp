//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <ring.h>
//constructor
ring::ring():
  m_raduis(0)
{}
ring::ring(const ring &rhs):
  m_raduis(rhs.m_raduis)
{}
ring::ring(ring &&rhs):
  m_raduis(0)
{
  m_raduis = rhs.m_raduis;
  rhs.m_raduis = 0;
}
//destructor
ring::~ring(){}
ring& ring::operator=(const ring &rhs)
{
  if (this != &rhs)
  {
    m_raduis = rhs.m_raduis;
  }
  return *this;
}
ring& ring::operator=(ring &&rhs)
{
  if (this != &rhs)
  {
    m_raduis = rhs.m_raduis;
    rhs.m_raduis = 0;
  }
  return *this;
}
