//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef INSECT_H
#define INSECT_H
#include <iostream>
//TODO insect finish this class
class insect
{
  public:
    //constructor
    insect();
    //copy constructor
    insect(const insect &rhs);
    //move constructor
    insect(insect &&rhs);
    //destructor
    ~insect();
    //copy assignment operator
    insect& operator=(const insect &rhs);
    //move assignment operator
    insect& operator=(insect &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_invertabrate;
};

#endif /* INSECT_H */