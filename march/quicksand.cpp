//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <quicksand.h>
//constructor
quicksand::quicksand():
  m_viscosity(0)
{}
quicksand::quicksand(const quicksand &rhs):
  m_viscosity(rhs.m_viscosity)
{}
quicksand::quicksand(quicksand &&rhs):
  m_viscosity(0)
{
  m_viscosity = rhs.m_viscosity;
  rhs.m_viscosity = 0;
}
//destructor
quicksand::~quicksand(){}
quicksand& quicksand::operator=(const quicksand &rhs)
{
  if (this != &rhs)
  {
    m_viscosity = rhs.m_viscosity;
  }
  return *this;
}
quicksand& quicksand::operator=(quicksand &&rhs)
{
  if (this != &rhs)
  {
    m_viscosity = rhs.m_viscosity;
    rhs.m_viscosity = 0;
  }
  return *this;
}
