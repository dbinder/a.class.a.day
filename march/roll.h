//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef ROLL_H
#define ROLL_H
#include <iostream>
//TODO roll finish this class
class roll
{
  public:
    //constructor
    roll();
    //copy constructor
    roll(const roll &rhs);
    //move constructor
    roll(roll &&rhs);
    //destructor
    ~roll();
    //copy assignment operator
    roll& operator=(const roll &rhs);
    //move assignment operator
    roll& operator=(roll &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_action;
};

#endif /* ROLL_H */