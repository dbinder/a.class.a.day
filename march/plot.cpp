//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <plot.h>
//constructor
plot::plot():
  m_length(0),
  m_width(0)
{}
plot::plot(const plot &rhs):
  m_length(rhs.m_length),
  m_width(rhs.m_width)
{}
plot::plot(plot &&rhs):
  m_length(0),
  m_width(0)
{
  m_length = rhs.m_length;
  m_width = rhs.m_width;
  rhs.m_length = 0;
  rhs.m_width = 0;
}
//destructor
plot::~plot(){}
plot& plot::operator=(const plot &rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
    m_width = rhs.m_width;
  }
  return *this;
}
plot& plot::operator=(plot &&rhs)
{
  if (this != &rhs)
  {
    m_length = rhs.m_length;
    m_width = rhs.m_width;
    rhs.m_length = 0;
    rhs.m_width = 0;
  }
  return *this;
}