//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <rabbits.h>
//constructor
rabbits::rabbits():
  m_amount(0)
{}
rabbits::rabbits(const rabbits &rhs):
  m_amount(rhs.m_amount)
{}
rabbits::rabbits(rabbits &&rhs):
  m_amount(0)
{
  m_amount = rhs.m_amount;
  rhs.m_amount = 0;
}
//destructor
rabbits::~rabbits(){}
rabbits& rabbits::operator=(const rabbits &rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
  }
  return *this;
}
rabbits& rabbits::operator=(rabbits &&rhs)
{
  if (this != &rhs)
  {
    m_amount = rhs.m_amount;
    rhs.m_amount = 0;
  }
  return *this;
}
