//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef STITCH_H
#define STITCH_H
#include <iostream>
//TODO stitch finish this class
class stitch
{
  public:
    //constructor
    stitch();
    //copy constructor
    stitch(const stitch &rhs);
    //move constructor
    stitch(stitch &&rhs);
    //destructor
    ~stitch();
    //copy assignment operator
    stitch& operator=(const stitch &rhs);
    //move assignment operator
    stitch& operator=(stitch &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_count;
};

#endif /* STITCH_H */