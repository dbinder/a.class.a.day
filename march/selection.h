//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
//TODO selection finish this class
class selection
{
  public:
    //constructor
    selection();
    //copy constructor
    selection(const selection &rhs);
    //move constructor
    selection(selection &&rhs);
    //destructor
    ~selection();
    //copy assignment operator
    selection& operator=(const selection &rhs);
    //move assignment operator
    selection& operator=(selection &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_selected;
};

#endif /* SELECTION_H */