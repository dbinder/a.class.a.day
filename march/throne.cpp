//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <throne.h>
//constructor
throne::throne():
  m_faction("")
{}
throne::throne(const throne &rhs):
  m_faction(rhs.m_faction)
{}
throne::throne(throne &&rhs):
  m_faction("")
{
  m_faction = rhs.m_faction;
  rhs.m_faction = nullptr;
}
//destructor
throne::~throne(){}
throne& throne::operator=(const throne &rhs)
{
  if (this != &rhs)
  {
    m_faction = rhs.m_faction;
  }
  return *this;
}
throne& throne::operator=(throne &&rhs)
{
  if (this != &rhs)
  {
    m_faction = rhs.m_faction;
    rhs.m_faction = nullptr;
  }
  return *this;
}
