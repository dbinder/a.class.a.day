//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef QUICKSAND_H
#define QUICKSAND_H
#include <iostream>
//TODO quicksand finish this class
class quicksand
{
  public:
    //constructor
    quicksand();
    //copy constructor
    quicksand(const quicksand &rhs);
    //move constructor
    quicksand(quicksand &&rhs);
    //destructor
    ~quicksand();
    //copy assignment operator
    quicksand& operator=(const quicksand &rhs);
    //move assignment operator
    quicksand& operator=(quicksand &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_viscosity;
};

#endif /* QUICKSAND_H */