//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef THRONE_H
#define THRONE_H
#include <iostream>
//TODO throne finish this class
class throne
{
  public:
    //constructor
    throne();
    //copy constructor
    throne(const throne &rhs);
    //move constructor
    throne(throne &&rhs);
    //destructor
    ~throne();
    //copy assignment operator
    throne& operator=(const throne &rhs);
    //move assignment operator
    throne& operator=(throne &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_faction;
};

#endif /* THRONE_H */