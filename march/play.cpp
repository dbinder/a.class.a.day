//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <play.h>
//constructor
play::play():
  m_paused(0)
{}
play::play(const play &rhs):
  m_paused(rhs.m_paused)
{}
play::play(play &&rhs):
  m_paused(0)
{
  m_paused = rhs.m_paused;
  rhs.m_paused = 0;
}
//destructor
play::~play(){}
play& play::operator=(const play &rhs)
{
  if (this != &rhs)
  {
    m_paused = rhs.m_paused;
  }
  return *this;
}
play& play::operator=(play &&rhs)
{
  if (this != &rhs)
  {
    m_paused = rhs.m_paused;
    rhs.m_paused = 0;
  }
  return *this;
}
