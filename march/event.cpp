//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <event.h>
//constructor
event::event():
  m_duration(0),
  m_complete(0),
  m_time(0),
  m_location("")
{}
event::event(const event &rhs):
  m_duration(rhs.m_duration),
  m_complete(rhs.m_complete),
  m_time(rhs.m_time),
  m_location(rhs.m_location)
{}
event::event(event &&rhs):
  m_duration(0),
  m_complete(0),
  m_time(0),
  m_location("")
{
  m_duration = rhs.m_duration;
  m_complete = rhs.m_complete;
  m_time = rhs.m_time;
  m_location = rhs.m_location;
  rhs.m_duration = 0;
  rhs.m_complete = 0;
  rhs.m_time = 0;
  rhs.m_location = nullptr;
}
//destructor
event::~event(){}
event& event::operator=(const event &rhs)
{
  if (this != &rhs)
  {
    m_duration = rhs.m_duration;
    m_complete = rhs.m_complete;
    m_time = rhs.m_time;
    m_location = rhs.m_location;
  }
  return *this;
}
event& event::operator=(event &&rhs)
{
  if (this != &rhs)
  {
    m_duration = rhs.m_duration;
    m_complete = rhs.m_complete;
    m_time = rhs.m_time;
    m_location = rhs.m_location;
    rhs.m_duration = 0;
    rhs.m_complete = 0;
    rhs.m_time = 0;
    rhs.m_location = nullptr;
  }
  return *this;
}