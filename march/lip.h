//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef LIP_H
#define LIP_H
#include <iostream>
//TODO lip finish this class
class lip
{
  public:
    //constructor
    lip();
    //copy constructor
    lip(const lip &rhs);
    //move constructor
    lip(lip &&rhs);
    //destructor
    ~lip();
    //copy assignment operator
    lip& operator=(const lip &rhs);
    //move assignment operator
    lip& operator=(lip &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_saying;
};

#endif /* LIP_H */