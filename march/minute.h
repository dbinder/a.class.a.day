//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef MINUTE_H
#define MINUTE_H
#include <iostream>
#include <thread>
#include <chrono>
//TODO minute finish this class
class minute
{
public:
  //constructor
  minute();
  //copy constructor
  minute(const minute &rhs);
  //move constructor
  minute(minute &&rhs);
  //destructor
  ~minute();
  //copy assignment operator
  minute& operator=(const minute &rhs);
  //move assignment operator
  minute& operator=(minute &&rhs);
  //Methods
  unsigned int run();
  //Access
  //Modify
protected:
private:
  unsigned int m_seconds;
};

#endif /* MINUTE_H */