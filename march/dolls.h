//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DOLLS_H
#define DOLLS_H
#include <iostream>
//TODO dolls finish this class
class dolls
{
  public:
    //constructor
    dolls();
    //copy constructor
    dolls(const dolls &rhs);
    //move constructor
    dolls(dolls &&rhs);
    //destructor
    ~dolls();
    //copy assignment operator
    dolls& operator=(const dolls &rhs);
    //move assignment operator
    dolls& operator=(dolls &&rhs);

    //Methods
    //Access
    //Modify
  protected:
  private:
    unsigned int m_size;
};

#endif /* DOLLS_H */