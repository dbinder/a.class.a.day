//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <believe.h>
//constructor
believe::believe():
  m_xfile(0)
{}
believe::believe(const believe &rhs):
  m_xfile(rhs.m_xfile)
{}
believe::believe(believe &&rhs):
  m_xfile(0)
{
  m_xfile = rhs.m_xfile;
  rhs.m_xfile = 0;
}
//destructor
believe::~believe(){}
believe& believe::operator=(const believe &rhs)
{
  if (this != &rhs)
  {
    m_xfile = rhs.m_xfile;
  }
  return *this;
}
believe& believe::operator=(believe &&rhs)
{
  if (this != &rhs)
  {
    m_xfile = rhs.m_xfile;
    rhs.m_xfile = 0;
  }
  return *this;
}
