//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BELIEVE_H
#define BELIEVE_H
#include <iostream>
//TODO believe finish this class
class believe
{
  public:
    //constructor
    believe();
    //copy constructor
    believe(const believe &rhs);
    //move constructor
    believe(believe &&rhs);
    //destructor
    ~believe();
    //copy assignment operator
    believe& operator=(const believe &rhs);
    //move assignment operator
    believe& operator=(believe &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_xfile;
};

#endif /* BELIEVE_H */