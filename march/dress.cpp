//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <dress.h>
//constructor
dress::dress():
  m_size(0),
  m_color(nullptr),
  m_description(nullptr)
{}
dress::dress(const dress &rhs):
  m_size(rhs.m_size),
  m_color(rhs.m_color),
  m_description(rhs.m_description)
{}
dress::dress(dress &&rhs):
  m_size(0),
  m_color(nullptr),
  m_description(nullptr)
{
  m_size = rhs.m_size;
  m_color = rhs.m_color;
  m_description = rhs.m_description;
  rhs.m_size = 0;
  rhs.m_color = nullptr;
  rhs.m_description = nullptr;
}
//destructor
dress::~dress(){}
dress& dress::operator=(const dress &rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
    m_color = rhs.m_color;
    m_description = rhs.m_description;
  }
  return *this;
}
dress& dress::operator=(dress &&rhs)
{
  if (this != &rhs)
  {
    m_size = rhs.m_size;
    m_color = rhs.m_color;
    m_description = rhs.m_description;
    rhs.m_size = 0;
    rhs.m_color = nullptr;
    rhs.m_description = nullptr;
  }
  return *this;
}
