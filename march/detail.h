//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef DETAIL_H
#define DETAIL_H
#include <iostream>
//TODO detail finish this class
class detail
{
  public:
    //constructor
    detail();
    //copy constructor
    detail(const detail &rhs);
    //move constructor
    detail(detail &&rhs);
    //destructor
    ~detail();
    //copy assignment operator
    detail& operator=(const detail &rhs);
    //move assignment operator
    detail& operator=(detail &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    std::string m_info;
};

#endif /* DETAIL_H */