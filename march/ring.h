//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef RING_H
#define RING_H
#include <iostream>
//TODO ring finish this class
class ring
{
  public:
    //constructor
    ring();
    //copy constructor
    ring(const ring &rhs);
    //move constructor
    ring(ring &&rhs);
    //destructor
    ~ring();
    //copy assignment operator
    ring& operator=(const ring &rhs);
    //move assignment operator
    ring& operator=(ring &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_raduis;
};

#endif /* RING_H */