//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <tail.h>
//constructor
tail::tail():
  m_segments(0)
{}
tail::tail(const tail &rhs):
  m_segments(rhs.m_segments)
{}
tail::tail(tail &&rhs):
  m_segments(0)
{
  m_segments = rhs.m_segments;
  rhs.m_segments = 0;
}
//destructor
tail::~tail(){}
tail& tail::operator=(const tail &rhs)
{
  if (this != &rhs)
  {
    m_segments = rhs.m_segments;
  }
  return *this;
}
tail& tail::operator=(tail &&rhs)
{
  if (this != &rhs)
  {
    m_segments = rhs.m_segments;
    rhs.m_segments = 0;
  }
  return *this;
}
