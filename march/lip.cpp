//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#include <lip.h>
//constructor
lip::lip():
  m_saying(nullptr)
{}
lip::lip(const lip &rhs):
  m_saying(rhs.m_saying)
{}
lip::lip(lip &&rhs):
  m_saying(nullptr)
{
  m_saying = rhs.m_saying;
  rhs.m_saying = nullptr;
}
//destructor
lip::~lip(){}
lip& lip::operator=(const lip &rhs)
{
  if (this != &rhs)
  {
    m_saying = rhs.m_saying;
  }
  return *this;
}
lip& lip::operator=(lip &&rhs)
{
  if (this != &rhs)
  {
    m_saying = rhs.m_saying;
    rhs.m_saying = nullptr;
  }
  return *this;
}
