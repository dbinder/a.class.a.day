//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef PLOT_H
#define PLOT_H
#include <iostream>
//TODO plot finish this class
class plot
{
  public:
    //constructor
    plot();
    //copy constructor
    plot(const plot &rhs);
    //move constructor
    plot(plot &&rhs);
    //destructor
    ~plot();
    //copy assignment operator
    plot& operator=(const plot &rhs);
    //move assignment operator
    plot& operator=(plot &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    double m_length;
    double m_width;
};

#endif /* PLOT_H */