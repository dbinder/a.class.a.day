//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef EGG_H
#define EGG_H
#include <iostream>
//TODO egg finish this class
class egg
{
  public:
    //constructor
    egg();
    //copy constructor
    egg(const egg &rhs);
    //move constructor
    egg(egg &&rhs);
    //destructor
    ~egg();
    //copy assignment operator
    egg& operator=(const egg &rhs);
    //move assignment operator
    egg& operator=(egg &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_cracked;
};

#endif /* EGG_H */