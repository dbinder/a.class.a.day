//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef BOY_H
#define BOY_H
#include <iostream>

#include <string>
//TODO boy finish this class
class boy
{
  public:
    //constructor
    boy();

    //copy constructor
    boy(const boy &rhs);
    //move constructor
    boy(boy &&rhs);
    //destructor
    ~boy();
    //assignment operator
    boy &operator=(const boy &rhs);
    //move assignment
    boy& operator=(boy &&rhs);
    //iostream operators
    friend std::ostream &operator<<(std::ostream &output, const boy &rhs);
    friend std::istream &operator>>(std::istream &input, boy &lhs);

    //comparison operators
    friend bool operator==(const boy &rhs1, const boy &rhs2);
    friend bool operator!=(const boy &rhs1, const boy &rhs2);

    //Methods
    //Get
    unsigned int getAge();
    double getHeight();
    double getWeight();
    std::string getEyeColor();
    std::string getHairColor();

    //Set
    void setAge(unsigned int i);
    void setHeight(double n);
    void setWeight(double n);
    void setEyeColor(std::string n);
    void setHairColor(std::string n);
  protected:
  private:
    unsigned int m_age;
    double m_height;
    double m_weight;
    std::string m_eyeColor;
    std::string m_hairColor;
};

#endif /* BOY_H */