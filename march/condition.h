//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#pragma once
#ifndef CONDITION_H
#define CONDITION_H
#include <iostream>
//TODO condition finish this class
class condition
{
  public:
    //constructor
    condition();
    //copy constructor
    condition(const condition &rhs);
    //move constructor
    condition(condition &&rhs);
    //destructor
    ~condition();
    //copy assignment operator
    condition& operator=(const condition &rhs);
    //move assignment operator
    condition& operator=(condition &&rhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    bool m_conditionalFlag;
};

#endif /* CONDITION_H */