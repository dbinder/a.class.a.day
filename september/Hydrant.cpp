//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Hydrant.h>
//constructor
Hydrant::Hydrant():
  m_placeholder(0)
{}

Hydrant::Hydrant(const Hydrant &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Hydrant::Hydrant(Hydrant &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Hydrant::~Hydrant(){}
Hydrant& Hydrant::operator=(const Hydrant &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Hydrant& Hydrant::operator=(Hydrant &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Hydrant &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Hydrant &lhs)
{
  return input;
}

