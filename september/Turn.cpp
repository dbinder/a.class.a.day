//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Turn.h>
//constructor
Turn::Turn():
  m_placeholder(0)
{}

Turn::Turn(const Turn &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Turn::Turn(Turn &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Turn::~Turn(){}
Turn& Turn::operator=(const Turn &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Turn& Turn::operator=(Turn &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Turn &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Turn &lhs)
{
  return input;
}

