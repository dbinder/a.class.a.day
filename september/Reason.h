//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef REASON_H
#define REASON_H
#include <iostream>
//TODO Reason finish this class
class Reason
{
  public:
    //constructor
    Reason();
    //copy constructor
    Reason(const Reason &rhs);
    //move constructor
    Reason(Reason &&rhs);
    //destructor
    ~Reason();
    //copy assignment operator
    Reason& operator=(const Reason &rhs);
    //move assignment operator
    Reason& operator=(Reason &&rhs);
    //swap
    void swap(Reason &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Reason &rhs);
    friend std::istream& operator>>(std::istream &input, Reason &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* REASON_H */