//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Minister.h>
//constructor
Minister::Minister():
  m_placeholder(0)
{}

Minister::Minister(const Minister &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Minister::Minister(Minister &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Minister::~Minister(){}
Minister& Minister::operator=(const Minister &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Minister& Minister::operator=(Minister &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Minister &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Minister &lhs)
{
  return input;
}

