//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HEAT_H
#define HEAT_H
#include <iostream>
//TODO Heat finish this class
class Heat
{
  public:
    //constructor
    Heat();
    //copy constructor
    Heat(const Heat &rhs);
    //move constructor
    Heat(Heat &&rhs);
    //destructor
    ~Heat();
    //copy assignment operator
    Heat& operator=(const Heat &rhs);
    //move assignment operator
    Heat& operator=(Heat &&rhs);
    //swap
    void swap(Heat &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Heat &rhs);
    friend std::istream& operator>>(std::istream &input, Heat &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HEAT_H */