//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef FORCE_H
#define FORCE_H
#include <iostream>
//TODO Force finish this class
class Force
{
  public:
    //constructor
    Force();
    //copy constructor
    Force(const Force &rhs);
    //move constructor
    Force(Force &&rhs);
    //destructor
    ~Force();
    //copy assignment operator
    Force& operator=(const Force &rhs);
    //move assignment operator
    Force& operator=(Force &&rhs);
    //swap
    void swap(Force &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Force &rhs);
    friend std::istream& operator>>(std::istream &input, Force &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* FORCE_H */