//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Teaching.h>
//constructor
Teaching::Teaching():
  m_placeholder(0)
{}

Teaching::Teaching(const Teaching &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Teaching::Teaching(Teaching &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Teaching::~Teaching(){}
Teaching& Teaching::operator=(const Teaching &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Teaching& Teaching::operator=(Teaching &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Teaching &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Teaching &lhs)
{
  return input;
}

