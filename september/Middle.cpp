//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Middle.h>
//constructor
Middle::Middle():
  m_placeholder(0)
{}

Middle::Middle(const Middle &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Middle::Middle(Middle &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Middle::~Middle(){}
Middle& Middle::operator=(const Middle &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Middle& Middle::operator=(Middle &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Middle &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Middle &lhs)
{
  return input;
}

