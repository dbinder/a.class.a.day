//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HORSES_H
#define HORSES_H
#include <iostream>
//TODO Horses finish this class
class Horses
{
  public:
    //constructor
    Horses();
    //copy constructor
    Horses(const Horses &rhs);
    //move constructor
    Horses(Horses &&rhs);
    //destructor
    ~Horses();
    //copy assignment operator
    Horses& operator=(const Horses &rhs);
    //move assignment operator
    Horses& operator=(Horses &&rhs);
    //swap
    void swap(Horses &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Horses &rhs);
    friend std::istream& operator>>(std::istream &input, Horses &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HORSES_H */