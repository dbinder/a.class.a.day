//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Yak.h>
//constructor
Yak::Yak():
  m_placeholder(0)
{}

Yak::Yak(const Yak &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Yak::Yak(Yak &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Yak::~Yak(){}
Yak& Yak::operator=(const Yak &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Yak& Yak::operator=(Yak &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Yak &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Yak &lhs)
{
  return input;
}

