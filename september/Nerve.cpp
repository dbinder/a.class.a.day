//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Nerve.h>
//constructor
Nerve::Nerve():
  m_placeholder(0)
{}

Nerve::Nerve(const Nerve &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Nerve::Nerve(Nerve &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Nerve::~Nerve(){}
Nerve& Nerve::operator=(const Nerve &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Nerve& Nerve::operator=(Nerve &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Nerve &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Nerve &lhs)
{
  return input;
}

