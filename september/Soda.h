//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SODA_H
#define SODA_H
#include <iostream>
//TODO Soda finish this class
class Soda
{
  public:
    //constructor
    Soda();
    //copy constructor
    Soda(const Soda &rhs);
    //move constructor
    Soda(Soda &&rhs);
    //destructor
    ~Soda();
    //copy assignment operator
    Soda& operator=(const Soda &rhs);
    //move assignment operator
    Soda& operator=(Soda &&rhs);
    //swap
    void swap(Soda &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Soda &rhs);
    friend std::istream& operator>>(std::istream &input, Soda &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SODA_H */