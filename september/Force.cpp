//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Force.h>
//constructor
Force::Force():
  m_placeholder(0)
{}

Force::Force(const Force &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Force::Force(Force &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Force::~Force(){}
Force& Force::operator=(const Force &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Force& Force::operator=(Force &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Force &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Force &lhs)
{
  return input;
}

