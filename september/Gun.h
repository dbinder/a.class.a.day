//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef GUN_H
#define GUN_H
#include <iostream>
//TODO Gun finish this class
class Gun
{
  public:
    //constructor
    Gun();
    //copy constructor
    Gun(const Gun &rhs);
    //move constructor
    Gun(Gun &&rhs);
    //destructor
    ~Gun();
    //copy assignment operator
    Gun& operator=(const Gun &rhs);
    //move assignment operator
    Gun& operator=(Gun &&rhs);
    //swap
    void swap(Gun &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Gun &rhs);
    friend std::istream& operator>>(std::istream &input, Gun &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* GUN_H */