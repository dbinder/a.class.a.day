//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Soda.h>
//constructor
Soda::Soda():
  m_placeholder(0)
{}

Soda::Soda(const Soda &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Soda::Soda(Soda &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Soda::~Soda(){}
Soda& Soda::operator=(const Soda &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Soda& Soda::operator=(Soda &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Soda &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Soda &lhs)
{
  return input;
}

