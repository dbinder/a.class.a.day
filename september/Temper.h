//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TEMPER_H
#define TEMPER_H
#include <iostream>
//TODO Temper finish this class
class Temper
{
  public:
    //constructor
    Temper();
    //copy constructor
    Temper(const Temper &rhs);
    //move constructor
    Temper(Temper &&rhs);
    //destructor
    ~Temper();
    //copy assignment operator
    Temper& operator=(const Temper &rhs);
    //move assignment operator
    Temper& operator=(Temper &&rhs);
    //swap
    void swap(Temper &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Temper &rhs);
    friend std::istream& operator>>(std::istream &input, Temper &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TEMPER_H */