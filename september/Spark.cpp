//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Spark.h>
//constructor
Spark::Spark():
  m_placeholder(0)
{}

Spark::Spark(const Spark &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Spark::Spark(Spark &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Spark::~Spark(){}
Spark& Spark::operator=(const Spark &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Spark& Spark::operator=(Spark &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Spark &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Spark &lhs)
{
  return input;
}

