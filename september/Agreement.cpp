//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Agreement.h>
//constructor
Agreement::Agreement():
  m_placeholder(0)
{}

Agreement::Agreement(const Agreement &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Agreement::Agreement(Agreement &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Agreement::~Agreement(){}
Agreement& Agreement::operator=(const Agreement &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Agreement& Agreement::operator=(Agreement &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Agreement &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Agreement &lhs)
{
  return input;
}

