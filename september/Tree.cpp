//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Tree.h>
//constructor
Tree::Tree():
  m_placeholder(0)
{}

Tree::Tree(const Tree &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Tree::Tree(Tree &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Tree::~Tree(){}
Tree& Tree::operator=(const Tree &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Tree& Tree::operator=(Tree &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Tree &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Tree &lhs)
{
  return input;
}

