//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SILVER_H
#define SILVER_H
#include <iostream>
//TODO Silver finish this class
class Silver
{
  public:
    //constructor
    Silver();
    //copy constructor
    Silver(const Silver &rhs);
    //move constructor
    Silver(Silver &&rhs);
    //destructor
    ~Silver();
    //copy assignment operator
    Silver& operator=(const Silver &rhs);
    //move assignment operator
    Silver& operator=(Silver &&rhs);
    //swap
    void swap(Silver &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Silver &rhs);
    friend std::istream& operator>>(std::istream &input, Silver &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SILVER_H */