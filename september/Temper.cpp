//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Temper.h>
//constructor
Temper::Temper():
  m_placeholder(0)
{}

Temper::Temper(const Temper &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Temper::Temper(Temper &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Temper::~Temper(){}
Temper& Temper::operator=(const Temper &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Temper& Temper::operator=(Temper &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Temper &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Temper &lhs)
{
  return input;
}

