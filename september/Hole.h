//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HOLE_H
#define HOLE_H
#include <iostream>
//TODO Hole finish this class
class Hole
{
  public:
    //constructor
    Hole();
    //copy constructor
    Hole(const Hole &rhs);
    //move constructor
    Hole(Hole &&rhs);
    //destructor
    ~Hole();
    //copy assignment operator
    Hole& operator=(const Hole &rhs);
    //move assignment operator
    Hole& operator=(Hole &&rhs);
    //swap
    void swap(Hole &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Hole &rhs);
    friend std::istream& operator>>(std::istream &input, Hole &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HOLE_H */