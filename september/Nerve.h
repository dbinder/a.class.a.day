//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef NERVE_H
#define NERVE_H
#include <iostream>
//TODO Nerve finish this class
class Nerve
{
  public:
    //constructor
    Nerve();
    //copy constructor
    Nerve(const Nerve &rhs);
    //move constructor
    Nerve(Nerve &&rhs);
    //destructor
    ~Nerve();
    //copy assignment operator
    Nerve& operator=(const Nerve &rhs);
    //move assignment operator
    Nerve& operator=(Nerve &&rhs);
    //swap
    void swap(Nerve &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Nerve &rhs);
    friend std::istream& operator>>(std::istream &input, Nerve &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* NERVE_H */