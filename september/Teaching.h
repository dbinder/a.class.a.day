//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TEACHING_H
#define TEACHING_H
#include <iostream>
//TODO Teaching finish this class
class Teaching
{
  public:
    //constructor
    Teaching();
    //copy constructor
    Teaching(const Teaching &rhs);
    //move constructor
    Teaching(Teaching &&rhs);
    //destructor
    ~Teaching();
    //copy assignment operator
    Teaching& operator=(const Teaching &rhs);
    //move assignment operator
    Teaching& operator=(Teaching &&rhs);
    //swap
    void swap(Teaching &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Teaching &rhs);
    friend std::istream& operator>>(std::istream &input, Teaching &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TEACHING_H */