//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MIDDLE_H
#define MIDDLE_H
#include <iostream>
//TODO Middle finish this class
class Middle
{
  public:
    //constructor
    Middle();
    //copy constructor
    Middle(const Middle &rhs);
    //move constructor
    Middle(Middle &&rhs);
    //destructor
    ~Middle();
    //copy assignment operator
    Middle& operator=(const Middle &rhs);
    //move assignment operator
    Middle& operator=(Middle &&rhs);
    //swap
    void swap(Middle &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Middle &rhs);
    friend std::istream& operator>>(std::istream &input, Middle &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MIDDLE_H */