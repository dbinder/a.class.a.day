//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef VASE_H
#define VASE_H
#include <iostream>
//TODO Vase finish this class
class Vase
{
  public:
    //constructor
    Vase();
    //copy constructor
    Vase(const Vase &rhs);
    //move constructor
    Vase(Vase &&rhs);
    //destructor
    ~Vase();
    //copy assignment operator
    Vase& operator=(const Vase &rhs);
    //move assignment operator
    Vase& operator=(Vase &&rhs);
    //swap
    void swap(Vase &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Vase &rhs);
    friend std::istream& operator>>(std::istream &input, Vase &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* VASE_H */