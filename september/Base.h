//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BASE_H
#define BASE_H
#include <iostream>
//TODO Base finish this class
class Base
{
  public:
    //constructor
    Base();
    //copy constructor
    Base(const Base &rhs);
    //move constructor
    Base(Base &&rhs);
    //destructor
    ~Base();
    //copy assignment operator
    Base& operator=(const Base &rhs);
    //move assignment operator
    Base& operator=(Base &&rhs);
    //swap
    void swap(Base &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Base &rhs);
    friend std::istream& operator>>(std::istream &input, Base &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BASE_H */