//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Horses.h>
//constructor
Horses::Horses():
  m_placeholder(0)
{}

Horses::Horses(const Horses &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Horses::Horses(Horses &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Horses::~Horses(){}
Horses& Horses::operator=(const Horses &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Horses& Horses::operator=(Horses &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Horses &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Horses &lhs)
{
  return input;
}

