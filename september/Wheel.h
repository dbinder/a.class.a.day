//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WHEEL_H
#define WHEEL_H
#include <iostream>
//TODO Wheel finish this class
class Wheel
{
  public:
    //constructor
    Wheel();
    //copy constructor
    Wheel(const Wheel &rhs);
    //move constructor
    Wheel(Wheel &&rhs);
    //destructor
    ~Wheel();
    //copy assignment operator
    Wheel& operator=(const Wheel &rhs);
    //move assignment operator
    Wheel& operator=(Wheel &&rhs);
    //swap
    void swap(Wheel &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wheel &rhs);
    friend std::istream& operator>>(std::istream &input, Wheel &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WHEEL_H */