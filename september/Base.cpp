//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Base.h>
//constructor
Base::Base():
  m_placeholder(0)
{}

Base::Base(const Base &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Base::Base(Base &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Base::~Base(){}
Base& Base::operator=(const Base &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Base& Base::operator=(Base &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Base &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Base &lhs)
{
  return input;
}

