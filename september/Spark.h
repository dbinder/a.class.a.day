//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SPARK_H
#define SPARK_H
#include <iostream>
//TODO Spark finish this class
class Spark
{
  public:
    //constructor
    Spark();
    //copy constructor
    Spark(const Spark &rhs);
    //move constructor
    Spark(Spark &&rhs);
    //destructor
    ~Spark();
    //copy assignment operator
    Spark& operator=(const Spark &rhs);
    //move assignment operator
    Spark& operator=(Spark &&rhs);
    //swap
    void swap(Spark &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Spark &rhs);
    friend std::istream& operator>>(std::istream &input, Spark &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SPARK_H */