//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MINISTER_H
#define MINISTER_H
#include <iostream>
//TODO Minister finish this class
class Minister
{
  public:
    //constructor
    Minister();
    //copy constructor
    Minister(const Minister &rhs);
    //move constructor
    Minister(Minister &&rhs);
    //destructor
    ~Minister();
    //copy assignment operator
    Minister& operator=(const Minister &rhs);
    //move assignment operator
    Minister& operator=(Minister &&rhs);
    //swap
    void swap(Minister &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Minister &rhs);
    friend std::istream& operator>>(std::istream &input, Minister &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MINISTER_H */