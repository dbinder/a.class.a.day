//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Heat.h>
//constructor
Heat::Heat():
  m_placeholder(0)
{}

Heat::Heat(const Heat &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Heat::Heat(Heat &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Heat::~Heat(){}
Heat& Heat::operator=(const Heat &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Heat& Heat::operator=(Heat &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Heat &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Heat &lhs)
{
  return input;
}

