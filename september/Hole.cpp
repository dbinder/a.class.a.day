//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Hole.h>
//constructor
Hole::Hole():
  m_placeholder(0)
{}

Hole::Hole(const Hole &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Hole::Hole(Hole &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Hole::~Hole(){}
Hole& Hole::operator=(const Hole &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Hole& Hole::operator=(Hole &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Hole &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Hole &lhs)
{
  return input;
}

