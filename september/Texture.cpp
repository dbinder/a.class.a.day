//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Texture.h>
//constructor
Texture::Texture():
  m_placeholder(0)
{}

Texture::Texture(const Texture &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Texture::Texture(Texture &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Texture::~Texture(){}
Texture& Texture::operator=(const Texture &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Texture& Texture::operator=(Texture &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Texture &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Texture &lhs)
{
  return input;
}

