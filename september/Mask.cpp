//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Mask.h>
//constructor
Mask::Mask():
  m_placeholder(0)
{}

Mask::Mask(const Mask &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Mask::Mask(Mask &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Mask::~Mask(){}
Mask& Mask::operator=(const Mask &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Mask& Mask::operator=(Mask &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Mask &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Mask &lhs)
{
  return input;
}

