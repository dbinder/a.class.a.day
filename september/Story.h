//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef STORY_H
#define STORY_H
#include <iostream>
//TODO Story finish this class
class Story
{
  public:
    //constructor
    Story();
    //copy constructor
    Story(const Story &rhs);
    //move constructor
    Story(Story &&rhs);
    //destructor
    ~Story();
    //copy assignment operator
    Story& operator=(const Story &rhs);
    //move assignment operator
    Story& operator=(Story &&rhs);
    //swap
    void swap(Story &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Story &rhs);
    friend std::istream& operator>>(std::istream &input, Story &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* STORY_H */