//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H
#include <iostream>
//TODO Texture finish this class
class Texture
{
  public:
    //constructor
    Texture();
    //copy constructor
    Texture(const Texture &rhs);
    //move constructor
    Texture(Texture &&rhs);
    //destructor
    ~Texture();
    //copy assignment operator
    Texture& operator=(const Texture &rhs);
    //move assignment operator
    Texture& operator=(Texture &&rhs);
    //swap
    void swap(Texture &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Texture &rhs);
    friend std::istream& operator>>(std::istream &input, Texture &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TEXTURE_H */