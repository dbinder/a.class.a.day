//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Regret.h>
//constructor
Regret::Regret():
  m_placeholder(0)
{}

Regret::Regret(const Regret &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Regret::Regret(Regret &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Regret::~Regret(){}
Regret& Regret::operator=(const Regret &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Regret& Regret::operator=(Regret &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Regret &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Regret &lhs)
{
  return input;
}

