//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef PIPE_H
#define PIPE_H
#include <iostream>
//TODO Pipe finish this class
class Pipe
{
  public:
    //constructor
    Pipe();
    //copy constructor
    Pipe(const Pipe &rhs);
    //move constructor
    Pipe(Pipe &&rhs);
    //destructor
    ~Pipe();
    //copy assignment operator
    Pipe& operator=(const Pipe &rhs);
    //move assignment operator
    Pipe& operator=(Pipe &&rhs);
    //swap
    void swap(Pipe &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Pipe &rhs);
    friend std::istream& operator>>(std::istream &input, Pipe &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* PIPE_H */