//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef WAX_H
#define WAX_H
#include <iostream>
//TODO Wax finish this class
class Wax
{
  public:
    //constructor
    Wax();
    //copy constructor
    Wax(const Wax &rhs);
    //move constructor
    Wax(Wax &&rhs);
    //destructor
    ~Wax();
    //copy assignment operator
    Wax& operator=(const Wax &rhs);
    //move assignment operator
    Wax& operator=(Wax &&rhs);
    //swap
    void swap(Wax &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Wax &rhs);
    friend std::istream& operator>>(std::istream &input, Wax &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* WAX_H */