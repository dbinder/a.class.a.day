//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Beginner.h>
//constructor
Beginner::Beginner():
  m_placeholder(0)
{}

Beginner::Beginner(const Beginner &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Beginner::Beginner(Beginner &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Beginner::~Beginner(){}
Beginner& Beginner::operator=(const Beginner &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Beginner& Beginner::operator=(Beginner &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Beginner &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Beginner &lhs)
{
  return input;
}

