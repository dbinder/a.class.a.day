//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wax.h>
//constructor
Wax::Wax():
  m_placeholder(0)
{}

Wax::Wax(const Wax &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wax::Wax(Wax &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wax::~Wax(){}
Wax& Wax::operator=(const Wax &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wax& Wax::operator=(Wax &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wax &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wax &lhs)
{
  return input;
}

