//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TURN_H
#define TURN_H
#include <iostream>
//TODO Turn finish this class
class Turn
{
  public:
    //constructor
    Turn();
    //copy constructor
    Turn(const Turn &rhs);
    //move constructor
    Turn(Turn &&rhs);
    //destructor
    ~Turn();
    //copy assignment operator
    Turn& operator=(const Turn &rhs);
    //move assignment operator
    Turn& operator=(Turn &&rhs);
    //swap
    void swap(Turn &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Turn &rhs);
    friend std::istream& operator>>(std::istream &input, Turn &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TURN_H */