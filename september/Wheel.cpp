//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Wheel.h>
//constructor
Wheel::Wheel():
  m_placeholder(0)
{}

Wheel::Wheel(const Wheel &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Wheel::Wheel(Wheel &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Wheel::~Wheel(){}
Wheel& Wheel::operator=(const Wheel &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Wheel& Wheel::operator=(Wheel &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Wheel &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Wheel &lhs)
{
  return input;
}

