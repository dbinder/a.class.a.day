//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Silver.h>
//constructor
Silver::Silver():
  m_placeholder(0)
{}

Silver::Silver(const Silver &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Silver::Silver(Silver &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Silver::~Silver(){}
Silver& Silver::operator=(const Silver &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Silver& Silver::operator=(Silver &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Silver &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Silver &lhs)
{
  return input;
}

