//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef SWEATER_H
#define SWEATER_H
#include <iostream>
//TODO Sweater finish this class
class Sweater
{
  public:
    //constructor
    Sweater();
    //copy constructor
    Sweater(const Sweater &rhs);
    //move constructor
    Sweater(Sweater &&rhs);
    //destructor
    ~Sweater();
    //copy assignment operator
    Sweater& operator=(const Sweater &rhs);
    //move assignment operator
    Sweater& operator=(Sweater &&rhs);
    //swap
    void swap(Sweater &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Sweater &rhs);
    friend std::istream& operator>>(std::istream &input, Sweater &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* SWEATER_H */