//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Story.h>
//constructor
Story::Story():
  m_placeholder(0)
{}

Story::Story(const Story &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Story::Story(Story &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Story::~Story(){}
Story& Story::operator=(const Story &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Story& Story::operator=(Story &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Story &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Story &lhs)
{
  return input;
}

