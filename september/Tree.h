//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef TREE_H
#define TREE_H
#include <iostream>
//TODO Tree finish this class
class Tree
{
  public:
    //constructor
    Tree();
    //copy constructor
    Tree(const Tree &rhs);
    //move constructor
    Tree(Tree &&rhs);
    //destructor
    ~Tree();
    //copy assignment operator
    Tree& operator=(const Tree &rhs);
    //move assignment operator
    Tree& operator=(Tree &&rhs);
    //swap
    void swap(Tree &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Tree &rhs);
    friend std::istream& operator>>(std::istream &input, Tree &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* TREE_H */