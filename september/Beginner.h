//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef BEGINNER_H
#define BEGINNER_H
#include <iostream>
//TODO Beginner finish this class
class Beginner
{
  public:
    //constructor
    Beginner();
    //copy constructor
    Beginner(const Beginner &rhs);
    //move constructor
    Beginner(Beginner &&rhs);
    //destructor
    ~Beginner();
    //copy assignment operator
    Beginner& operator=(const Beginner &rhs);
    //move assignment operator
    Beginner& operator=(Beginner &&rhs);
    //swap
    void swap(Beginner &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Beginner &rhs);
    friend std::istream& operator>>(std::istream &input, Beginner &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* BEGINNER_H */