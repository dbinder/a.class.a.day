//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Vase.h>
//constructor
Vase::Vase():
  m_placeholder(0)
{}

Vase::Vase(const Vase &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Vase::Vase(Vase &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Vase::~Vase(){}
Vase& Vase::operator=(const Vase &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Vase& Vase::operator=(Vase &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Vase &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Vase &lhs)
{
  return input;
}

