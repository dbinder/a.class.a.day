//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef AGREEMENT_H
#define AGREEMENT_H
#include <iostream>
//TODO Agreement finish this class
class Agreement
{
  public:
    //constructor
    Agreement();
    //copy constructor
    Agreement(const Agreement &rhs);
    //move constructor
    Agreement(Agreement &&rhs);
    //destructor
    ~Agreement();
    //copy assignment operator
    Agreement& operator=(const Agreement &rhs);
    //move assignment operator
    Agreement& operator=(Agreement &&rhs);
    //swap
    void swap(Agreement &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Agreement &rhs);
    friend std::istream& operator>>(std::istream &input, Agreement &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* AGREEMENT_H */