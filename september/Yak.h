//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef YAK_H
#define YAK_H
#include <iostream>
//TODO Yak finish this class
class Yak
{
  public:
    //constructor
    Yak();
    //copy constructor
    Yak(const Yak &rhs);
    //move constructor
    Yak(Yak &&rhs);
    //destructor
    ~Yak();
    //copy assignment operator
    Yak& operator=(const Yak &rhs);
    //move assignment operator
    Yak& operator=(Yak &&rhs);
    //swap
    void swap(Yak &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Yak &rhs);
    friend std::istream& operator>>(std::istream &input, Yak &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* YAK_H */