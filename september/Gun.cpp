//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Gun.h>
//constructor
Gun::Gun():
  m_placeholder(0)
{}

Gun::Gun(const Gun &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Gun::Gun(Gun &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Gun::~Gun(){}
Gun& Gun::operator=(const Gun &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Gun& Gun::operator=(Gun &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Gun &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Gun &lhs)
{
  return input;
}

