//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef HYDRANT_H
#define HYDRANT_H
#include <iostream>
//TODO Hydrant finish this class
class Hydrant
{
  public:
    //constructor
    Hydrant();
    //copy constructor
    Hydrant(const Hydrant &rhs);
    //move constructor
    Hydrant(Hydrant &&rhs);
    //destructor
    ~Hydrant();
    //copy assignment operator
    Hydrant& operator=(const Hydrant &rhs);
    //move assignment operator
    Hydrant& operator=(Hydrant &&rhs);
    //swap
    void swap(Hydrant &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Hydrant &rhs);
    friend std::istream& operator>>(std::istream &input, Hydrant &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* HYDRANT_H */