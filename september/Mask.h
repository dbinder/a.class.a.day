//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef MASK_H
#define MASK_H
#include <iostream>
//TODO Mask finish this class
class Mask
{
  public:
    //constructor
    Mask();
    //copy constructor
    Mask(const Mask &rhs);
    //move constructor
    Mask(Mask &&rhs);
    //destructor
    ~Mask();
    //copy assignment operator
    Mask& operator=(const Mask &rhs);
    //move assignment operator
    Mask& operator=(Mask &&rhs);
    //swap
    void swap(Mask &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Mask &rhs);
    friend std::istream& operator>>(std::istream &input, Mask &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* MASK_H */