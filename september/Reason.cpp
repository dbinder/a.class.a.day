//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Reason.h>
//constructor
Reason::Reason():
  m_placeholder(0)
{}

Reason::Reason(const Reason &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Reason::Reason(Reason &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Reason::~Reason(){}
Reason& Reason::operator=(const Reason &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Reason& Reason::operator=(Reason &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Reason &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Reason &lhs)
{
  return input;
}

