//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Sweater.h>
//constructor
Sweater::Sweater():
  m_placeholder(0)
{}

Sweater::Sweater(const Sweater &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Sweater::Sweater(Sweater &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Sweater::~Sweater(){}
Sweater& Sweater::operator=(const Sweater &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Sweater& Sweater::operator=(Sweater &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Sweater &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Sweater &lhs)
{
  return input;
}

