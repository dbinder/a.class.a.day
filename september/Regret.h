//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#pragma once
#ifndef REGRET_H
#define REGRET_H
#include <iostream>
//TODO Regret finish this class
class Regret
{
  public:
    //constructor
    Regret();
    //copy constructor
    Regret(const Regret &rhs);
    //move constructor
    Regret(Regret &&rhs);
    //destructor
    ~Regret();
    //copy assignment operator
    Regret& operator=(const Regret &rhs);
    //move assignment operator
    Regret& operator=(Regret &&rhs);
    //swap
    void swap(Regret &rhs);
    friend std::ostream& operator<<(std::ostream &output, const Regret &rhs);
    friend std::istream& operator>>(std::istream &input, Regret &lhs);
    //Methods
    //Access
    //Modify
  protected:
  private:
    int m_placeholder;
};

#endif /* REGRET_H */