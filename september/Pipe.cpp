//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Dylan Binder
// Copyright 2015
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include <Pipe.h>
//constructor
Pipe::Pipe():
  m_placeholder(0)
{}

Pipe::Pipe(const Pipe &rhs):
  m_placeholder(rhs.m_placeholder)
{}

Pipe::Pipe(Pipe &&rhs):
  m_placeholder(0)
{
  m_placeholder = rhs.m_placeholder;
  rhs.m_placeholder = 0;
}

//destructor
Pipe::~Pipe(){}
Pipe& Pipe::operator=(const Pipe &rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
  }
  return *this;
}

Pipe& Pipe::operator=(Pipe &&rhs)
{
  if (this != &rhs)
  {
    m_placeholder = rhs.m_placeholder;
    rhs.m_placeholder = 0;
  }
  return *this;
}

std::ostream &operator<<(std::ostream &output, const Pipe &rhs)
{
  return output;
}

std::istream &operator>>(std::istream &input, Pipe &lhs)
{
  return input;
}

