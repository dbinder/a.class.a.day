#include <UnitTest++.h>
#include <Amount.h>
SUITE(ClassTesting)
{
  TEST(amountClass)
  {
    Amount am;
    CHECK(am == 0);
    Amount am2(7);

    CHECK(am2 == 7);
  }

  TEST(amountIncrement)
  {
    Amount am;
    ++am;
    CHECK(am == 1);
    --am;
    CHECK(am == 0);
  }

  TEST(amountAdd)
  {
    Amount am;
    Amount am2(8);

    am = am + am2;
    CHECK(am == 8);
  }

  TEST(amountCompare)
  {
    Amount am(8);
    Amount am2(8);
    CHECK(am == am2);
  }
}