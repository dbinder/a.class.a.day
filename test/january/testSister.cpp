#include <UnitTest++.h>
#include <sister.h>


TEST(SetSister)
{
  sister one;
 
  one.setFName("TestFname");
  one.setMName("TestMname");
  one.setLName("TestLName");
  one.setHairColor("RED");
  one.setEyeColor("RED");
  one.setAge(3);
  one.setHeight(72);
  one.setWeight(64);

  CHECK(one.getFName() == "TestFname");
  CHECK(one.getMName() == "TestMname");
  CHECK(one.getLName() == "TestLName");
  CHECK(one.getHairColor() == "RED");
  CHECK(one.getEyeColor() == "RED");
  CHECK(one.getAge() == 3);
  CHECK(one.getHeight() == 72);
  CHECK(one.getWeight() == 64);
}

TEST(compareSister)
{
  sister one;
  sister two;

  CHECK_EQUAL(one, two);
  CHECK((one == two), 1);
  one.setAge(1);
  CHECK((one != two), 1);
}