#include <UnitTest++.h>
#include <January.h>

SUITE(ClassTesting){
  TEST(SetChild)
  {
    Child one;

    one.setAge(8);
    //  one.setHeight(100);
    one.setWeight(200);
    one.setEyeColor("BLUE");
    one.setHairColor("BLUE");

    CHECK(one.getAge() == 8);
    // CHECK(one.getHeight() == 100);
    CHECK(one.getWeight() == 200);
    CHECK(one.getEyeColor() == "BLUE");
    CHECK(one.getHairColor() == "BLUE");
  }

  TEST(compareChild)
  {
    Child one;
    Child two;

    CHECK_EQUAL(one, two);
    CHECK((one == two), 1);
    one.setAge(1);
    CHECK((one != two), 1);
  }
}