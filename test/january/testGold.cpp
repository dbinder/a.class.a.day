#include <UnitTest++.h>
#include <Gold.h>

//TODO: Write tests for the arithmetic operators
//TODO Write tests for the comparison operatots

TEST(SetGold)
{
  Gold test;
  //Get
  CHECK(test.getAmount() == 0);
  CHECK(test.getValue() == 0);
  CHECK(test.getWorth() == 0);

  test.setAmount(100);
  test.setWorth(1);
  CHECK(test.getAmount() == 100);
  CHECK(test.getValue() == 100);
  CHECK(test.getWorth() == 1);

}

TEST(WorthGold)
{
  Gold test2;
  test2.setAmount(100);
  test2.setWorth(2);
  CHECK(test2.getValue() == 200);

}

TEST(IncrementGold)
{
  Gold test3;
  test3.add(1);
  CHECK(test3.getAmount() == 1);
  test3.setAmount(UINT_MAX - 1);
  test3.add(1);
  CHECK_EQUAL(UINT_MAX, test3.getAmount());
  test3.add(100);
  CHECK_EQUAL(UINT_MAX, test3.getAmount());
  //decrement
  test3.subtract(1);
  CHECK_EQUAL(UINT_MAX - 1, test3.getAmount());
}

TEST(compareGold)
{
  Gold one;
  Gold two;

  //CHECK_EQUAL(one, two);
  CHECK((one == two), 1);
  one.setAmount(100);
  CHECK((one != two), 1);
  CHECK((one > two), 1);
  CHECK((one >= two), 1);
  two.setAmount(100);
  CHECK((one <= two), 1);
  two.setAmount(101);
  CHECK((one < two), 1);
}

TEST(subtractGold)
{
  Gold player1;
  player1.setAmount(100);

  player1.subtract(101);

  CHECK_EQUAL(0, player1.getAmount());
}
