#include <January.h>
#include <UnitTest++.h>

TEST(Dot)
{
  disease plague;
  plague.setDamage(10);
  plague.setDuration(5);
  plague.setModifier(1.5);
  
  CHECK(plague.getModifier(), 1.5);

  double damage = plague.calcDot();
  CHECK(damage, 75);
}