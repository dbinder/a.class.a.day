#include <UnitTest++.h>
#include <XmlTestReporter.h>
#include <fstream>
int main(int argc, char* argv[])
{
  std::ofstream f("aClassADay.xml");
  UnitTest::XmlTestReporter reporter(f);
  UnitTest::TestRunner runner(reporter);

  runner.RunTestsIf(UnitTest::Test::GetTestList(),
    NULL, UnitTest::True(),
    0);
  return 0;
}